layui.define(["layer", "setter", "admin", "element", "upload", "util", "form"], function (exports) {
    "use strict";
    var element = layui.element;
    var layer = layui.layer;
    var setter = layui.setter;
    var admin = layui.admin;
    var upload = layui.upload;
    var util = layui.util;
    var form = layui.form;

    var adjfut = {};

    adjfut.url = function (data) {
        var url = setter.request.server;
        return url += data;
    }

    adjfut.date_string = function (string) {
        string = string.toString()
        if (!isNaN(string)) {
            if (string.length == 10) {
                string *= 1000
            }
        }
        return util.toDateString(string)
    }

    adjfut.popupRight = function (options) {
        return admin.popupRight(options)
    }



    // adjfut.img_exists = function (path) {
    //     $.ajax({
    //         url: path,
    //         success: 
    //     })
    //     // var img = new Image();
    //     // img.src = path;
    //     // console.log(img,img.width,img.height)
    //     // return img.fileSize > 0 || (img.width > 0 && img.height > 0)
    //     // if (img.fileSize > 0 || (img.width > 0 && img.height > 0)) {
    //     //     return true;
    //     // } else {
    //     //     return false;
    //     // }
    // }

    adjfut.token = function (type) {
        if (!type) {
            type = "data"
        }
        var table = layui.data(setter.tableName);
        var token_name = setter.request.tokenName
        var token = table[token_name];
        if (token) {
            var re = {};
            if (type === "header") {
                token_name = token_name.replace(/\_/, "-");
            }
            re[token_name] = token
            return re;
        } else {
            admin.exit();
            return false;
        }
    }

    adjfut.upload_path = function (data) {
        var url = setter.upload_path;
        return url += data;
    }

    // adjfut.upload = function (option) {
    //     var re = []
    //     var elem = option.elem
    //     var $elem = $(elem)
    //     var success = option.success
    //     if (!success) {
    //         success = function () {}
    //     }
    //     var error = option.error
    //     if (!error) {
    //         error = function () {}
    //     }
    //     delete option.data
    //     delete option.elem
    //     delete option.choose
    //     delete option.done
    //     delete option.error

    //     var style = {
    //         file_list: [
    //             "padding: 15px",
    //             "border: 2px dashed #e7e6e6"
    //         ].join(";"),
    //         section: [
    //             "width: 120px",
    //             "position: relative",
    //             "padding: 0 30px",
    //             "display: inline-block",
    //             "margin: 15px 0 0 0"
    //         ].join(";"),
    //         left: [
    //             "position: absolute",
    //             "top: 50%",
    //             "left: 0",
    //             "text-align: center",
    //             "font-size: 30px",
    //             "cursor:pointer",
    //         ].join(";"),
    //         right: [
    //             "position: absolute",
    //             "top: 50%",
    //             "right: 0",
    //             "text-align: center",
    //             "font-size: 30px",
    //             "cursor:pointer",
    //         ].join(";"),
    //         close: [
    //             "position: absolute",
    //             "top: 0",
    //             "right: 0",
    //             "font-size: 30px",
    //             "cursor:pointer",
    //         ].join(";"),
    //         img: [
    //             "width: 100%"
    //         ].join(";"),
    //         p: [
    //             "word-wrap:break-word",
    //             "word-break:break-all",
    //             "height: 50px",
    //             "overflow: hidden"
    //         ].join(";"),
    //         button: [
    //             "position: absolute",
    //             "top: 50%",
    //             "left: 0",
    //             "margin: 0 0 0 60px",
    //             "display: none",
    //         ].join(";")
    //     }
    //     $elem.html([
    //         '<blockquote class="layui-elem-quote layui-quote-nm">',
    //         '<div class="layui-btn-group">',
    //         '<button type="button" class="layui-btn choose-button">选择文件上传</button>',
    //         // '<button type="button" class="layui-btn upload-button">文件上传</button>',
    //         '</div>',
    //         '</blockquote>',
    //         '<div class="file_list" style="' + style.file_list + '">',
    //         '</div>'
    //     ].join(""))
    //     var optiopns = $.extend(option, {
    //         elem: $elem.find(".choose-button"),
    //         headers: adjfut.token(),
    //         multiple: true,
    //         // auto: false,
    //         // bindAction: $elem.find(".upload-button"),
    //         value: [],
    //         file_html: function (index, file, result) {
    //             var that = this
    //             var img = "";
    //             if (file === null) {
    //                 file = {
    //                     name: ""
    //                 }
    //                 img = '<img style="' + style.img + '" src="' + result + '">'
    //             } else {
    //                 switch (file.type) {
    //                     case "image/jpeg":
    //                         img = '<img style="' + style.img + '" src="' + result + '">'
    //                         break;
    //                     case "image/png":
    //                         img = '<img style="' + style.img + '" src="' + result + '">'
    //                         break;
    //                     default:
    //                         var base = setter.base + "style/icon_file/"
    //                         var name = file.name.split(".")
    //                         var suffix = name[name.length - 1]
    //                         var img_path = base + suffix + ".png";
    //                         img = '<img style="' + style.img + '" src="' + img_path + '">'
    //                         break;
    //                 }
    //             }

    //             var $section = $([
    //                 '<section data-index="' + index + '" style="' + style.section + '" title="' + file.name + '">',
    //                 '<i style="' + style.close + '" class="layui-icon layui-icon-close-fill"></i>',
    //                 '<i style="' + style.left + '" class="layui-icon layui-icon-left"></i>',
    //                 '<i style="' + style.right + '" class="layui-icon layui-icon-right"></i>',
    //                 img,
    //                 '<p style="' + style.p + '">' + file.name + '</p>',
    //                 '<button type="button" style="' + style.button + '" class="layui-btn layui-btn-xs layui-btn-danger">上传失败</button>',
    //                 '</section>',
    //             ].join(""));
    //             $section.find("img").on("click", function () {
    //                 $(this).on("click", function () {
    //                     layer.photos({
    //                         photos: {
    //                             "data": [{
    //                                 "src": $(this).attr("src")
    //                             }]
    //                         },
    //                     });
    //                 })
    //             })
    //             $section.find('.layui-icon-left').on('click', function () {
    //                 var that = $(this).parent();
    //                 that.insertBefore(that.prev());
    //             })
    //             $section.find('.layui-icon-right').on('click', function () {
    //                 var that = $(this).parent();
    //                 that.insertAfter(that.next());
    //             })
    //             $section.find('.layui-icon-close-fill').on('click', function () {
    //                 // try {

    //                 // } catch (error) {

    //                 // }
    //                 // delete that.files[index]; //删除对应的文件
    //                 $section.remove();
    //                 // console.log(that)
    //                 // uploadListIns.config.elem.next()[0].value = ''; //清空 input file 值，以免删除后出现同名文件不可选
    //             });

    //             $section.find('.layui-btn-danger').on('click', function () {
    //                 obj.upload(index, file);
    //             });
    //             // that.value.push()
    //             $elem.find(".file_list").append($section);
    //         },
    //         choose: function (obj) {
    //             var that = this
    //             // var files = this.files = obj.pushFile();
    //             obj.preview(function (index, file, result) {
    //                 that.file_html(index, file, result)
    //             })
    //         },
    //         done: function (res, index, upload) {
    //             if (res[0].success) {
    //                 // re.push(res.data)
    //                 // var $tr = $elem.find('tr#upload-' + index).children();
    //                 // $tr.eq(2).html('<span style="color: #5FB878;">上传成功</span>');
    //                 // $tr.eq(4).html('');
    //                 success(res[0].info)
    //                 // return delete this.files[index];
    //             } else {
    //                 error()
    //                 this.error(index, upload);
    //             }
    //         },
    //         error: function (index, upload) {
    //             $elem.find("section[data-index=" + index + "]").find(".layui-btn-danger").css({
    //                 display: "block"
    //             })
    //             // var $tr = $elem.find('tr#upload-' + index).children();
    //             // $tr.eq(2).html('<span style="color: #FF5722;">上传失败</span>');
    //             // $tr.eq(4).find('.upload-reload').removeClass('layui-hide');
    //         }
    //     })
    //     upload.render(optiopns)
    //     return optiopns
    // }

    adjfut.upload = function (option, file_list) {
        var re = []
        var elem = option.elem
        var $elem = $(elem)
        // var file_list = [];
        var style = {
            file_list: [
                "padding: 15px",
                "border: 2px dashed #e7e6e6"
            ].join(";"),
            section: [
                "width: 120px",
                "position: relative",
                "padding: 0 30px",
                "display: inline-block",
                "margin: 15px 0 0 0"
            ].join(";"),
            left: [
                "position: absolute",
                "top: 50%",
                "left: 0",
                "text-align: center",
                "font-size: 30px",
                "cursor:pointer",
            ].join(";"),
            right: [
                "position: absolute",
                "top: 50%",
                "right: 0",
                "text-align: center",
                "font-size: 30px",
                "cursor:pointer",
            ].join(";"),
            close: [
                "position: absolute",
                "top: 0",
                "right: 0",
                "font-size: 30px",
                "cursor:pointer",
            ].join(";"),
            img: [
                "width: 100%"
            ].join(";"),
            p: [
                "word-wrap:break-word",
                "word-break:break-all",
                "height: 50px",
                "overflow: hidden"
            ].join(";"),
            button: [
                "position: absolute",
                "top: 50%",
                "left: 0",
                "margin: 0 0 0 60px",
                "display: none",
            ].join(";")
        }
        var change = option.change
        if (!change) {
            change = function () {}
        }
        var success = option.success
        if (!success) {
            success = function () {}
        }
        var error = option.error
        if (!error) {
            error = function () {}
        }
        delete option.data
        delete option.elem
        delete option.choose
        delete option.done
        delete option.error
        delete option.change

        $elem.html([
            '<blockquote class="layui-elem-quote layui-quote-nm">',
            '<div class="layui-btn-group">',
            '<button type="button" class="layui-btn choose-button">选择文件上传</button>',
            // '<button type="button" class="layui-btn upload-button">文件上传</button>',
            '</div>',
            '</blockquote>',
            '<div class="file_list" style="' + style.file_list + '">',
            '</div>'
        ].join(""))
        var fun = {
            insert_img: function (src) {
                var value = src
                var img = ""
                var suffix = value.split(".")
                suffix = suffix[suffix.length - 1]
                value = adjfut.upload_path(value)
                switch (suffix) {
                    case "jpg":
                        img = '<img style="' + style.img + '" src="' + value + '">'
                        break;
                    case "png":
                        img = '<img style="' + style.img + '" src="' + value + '">'
                        break;
                    default:
                        var base = setter.base + "style/icon_file/"
                        var img_path = base + suffix + ".png";
                        img = '<img style="' + style.img + '" src="' + img_path + '">'
                        break;
                }
                var $section = $([
                    '<section data-val="' + src + '" style="' + style.section + '">',
                    '<i style="' + style.close + '" class="layui-icon layui-icon-close-fill"></i>',
                    '<i style="' + style.left + '" class="layui-icon layui-icon-left"></i>',
                    '<i style="' + style.right + '" class="layui-icon layui-icon-right"></i>',
                    img,
                    '</section>',
                ].join(""));
                $section.find("img").on("click", function () {
                    layer.photos({
                        photos: {
                            "data": [{
                                "src": $(this).attr("src")
                            }]
                        },
                    });
                })
                $section.find('.layui-icon-left').on('click', function () {
                    var that = $(this).parent();
                    that.insertBefore(that.prev());
                    change(fun.val())
                })
                $section.find('.layui-icon-right').on('click', function () {
                    var that = $(this).parent();
                    that.insertAfter(that.next());
                    change(fun.val())
                })
                $section.find('.layui-icon-close-fill').on('click', function () {
                    $section.remove();
                    change(fun.val())
                });
                $elem.find(".file_list").append($section);
            },
            val: function () {
                var $section = $elem.find(".file_list section")
                var re = []
                layui.each($section, function (_, value) {
                    re.push($(value).data("val"))
                })
                return re
            }
            // render_img: function () {
            //     $elem.find(".file_list").html("");
            //     layui.each(file_list, function (key, value) {
            //         var img = ""
            //         var suffix = value.split(".")
            //         suffix = suffix[suffix.length - 1]
            //         value = adjfut.upload_path(value)
            //         switch (suffix) {
            //             case "jpg":
            //                 img = '<img style="' + style.img + '" src="' + value + '">'
            //                 break;
            //             case "png":
            //                 img = '<img style="' + style.img + '" src="' + value + '">'
            //                 break;
            //             default:
            //                 var base = setter.base + "style/icon_file/"
            //                 var img_path = base + suffix + ".png";
            //                 img = '<img style="' + style.img + '" src="' + img_path + '">'
            //                 break;
            //         }
            //         var $section = $([
            //             '<section data-key="' + key + '" style="' + style.section + '">',
            //             '<i style="' + style.close + '" class="layui-icon layui-icon-close-fill"></i>',
            //             '<i style="' + style.left + '" class="layui-icon layui-icon-left"></i>',
            //             '<i style="' + style.right + '" class="layui-icon layui-icon-right"></i>',
            //             img,
            //             '</section>',
            //         ].join(""));
            //         $section.find("img").on("click", function () {
            //             layer.photos({
            //                 photos: {
            //                     "data": [{
            //                         "src": $(this).attr("src")
            //                     }]
            //                 },
            //             });
            //         })
            //         $section.find('.layui-icon-left').on('click', function () {
            //             var that = $(this).parent();
            //             that.insertBefore(that.prev());
            //         })
            //         $section.find('.layui-icon-right').on('click', function () {
            //             var that = $(this).parent();
            //             that.insertAfter(that.next());
            //         })
            //         $section.find('.layui-icon-close-fill').on('click', function () {

            //             $section.remove();
            //         });
            //         $elem.find(".file_list").append($section);
            //     })
            // }
        }
        var optiopns = $.extend(option, {
            elem: $elem.find(".choose-button"),
            headers: adjfut.token(),
            multiple: true,
            value: [],
            done: function (res, index, upload) {
                if (res[0].success) {
                    layer.msg("文件上传成功");
                    // success(res[0].info, fun)
                    // value.push(res[0].info.save_name)
                    fun.insert_img(res[0].info.save_name)
                    // web_resources_uploads
                    change(fun.val())
                    // fun.render_img()
                } else {
                    error()
                    this.error(index, upload);
                }
            },
            error: function (index, upload) {
                $elem.find("section[data-index=" + index + "]").find(".layui-btn-danger").css({
                    display: "block"
                })
            }
        })
        upload.render(optiopns)
        if (file_list) {
            layui.each(file_list, function (_, value) {
                fun.insert_img(value)
            })
        }
        return optiopns
    }


    adjfut.crumbs = function (data, button) {
        var elem = "#" + setter.container + "_body #crumbs";
        $(elem).html("");
        var html = "";
        html += '<div class="layui-card layadmin-header">';
        html += '<div class="layui-breadcrumb" style="display: inline-block;">';
        html += '<a href="#/zhiyun/index/index"><i class="layui-icon layui-icon-home"></i></a>'
        // html += '<div class="layui-breadcrumb" style="display: inline-block;">';
        layui.each(data, function (key, value) {
            if (data.length === key + 1) {
                if (value.url === false) {
                    html += [
                        '<a href="javascript:;">',
                        '<cite>',
                        value.name,
                        '</cite>',
                        '</a>'
                    ].join("");
                } else {
                    html += [
                        // '<a href="javascript:;" data-current_url="' + value.url + '">',
                        '<a href="' + value.url + '">',
                        '<cite>',
                        value.name,
                        '</cite>',
                        '</a>'
                    ].join("");
                }
            } else {
                if (value.url === false) {
                    html += [
                        '<a href="javascript:;">',
                        value.name,
                        '</a>'
                    ].join("");
                } else {
                    html += [
                        // '<a href="javascript:;" data-current_url="' + value.url + '">',
                        '<a href="' + value.url + '">',
                        value.name,
                        '</a>'
                    ].join("");
                }

            }
        })
        html += '</div>';
        if (button) {
            html += '<div style="display: inline-block;float:right;margin: 0 15px;">';
            layui.each(button, function (key, value) {
                html += [
                    '<button type="button" class="crumbs-btn layui-btn layui-btn-sm ' + value.btn + '">',
                    '<i style="font-size:12px !important;" class="layui-icon ' + value.icon + '"></i>',
                    value.name,
                    '</button>',
                ].join("");
            })

            html += '</div>';
        }
        html += '<div class="clear"></div>';
        html += '</div>';
        $(elem).eq(0).append(html);
        element.render("breadcrumb");
        if (button) {
            $(elem + " .crumbs-btn").each(function (key, elem) {
                $(this).bind("click", function () {
                    button[key].click();
                })
            })
        }
    }

    function form_required(that, verify) {
        var $that = $(that)
        var label = $that.parent().parent().find("label");

        var name = $that.attr("lay-reqtext") || $.trim($that.attr("lay-label") || label.html());
        if (name) {
            $that.attr("placeholder", "请输入" + name);
            $that.attr("autocomplete", "off");
            if (verify) {
                $that.attr("lay-reqtext", "请输入" + name);
            }
        }
    }

    adjfut.form_required = function () {
        var form = $(".layui-form");
        var required_input = form.find("input");
        layui.each(required_input, function (_, value) {
            var verify = $(value).attr("lay-verify");
            form_required(value, verify);
        });
        var required_textarea = form.find("textarea");
        layui.each(required_textarea, function (_, value) {
            var verify = $(value).attr("lay-verify");
            form_required(value, verify);
        });
    }

    adjfut.view_id = function (data) {
        var re = "";
        if (!data) {
            var router = layui.router();
            var path = router.path;
            if (path.length === 1) {
                re = path[0];
            } else {
                re = router.path.join("_");
            }
        } else {
            re = data.replace(/\//g, "_");
        }
        return re
    }

    adjfut.request = function (data) {
        layer.load();
        var token = adjfut.token("header");
        var url = setter.request.server;
        var success = function () {};
        var error = function () {};
        var complete = function () {};
        if (data.url) {
            url += data.url;
            delete data.url;
        }
        console.time(url);
        if (data.success) {
            success = data.success;
            delete data.success;
        }
        if (data.error) {
            error = data.error;
            delete data.error;
        }
        if (data.complete) {
            complete = data.complete;
            delete data.complete;
        }
        var response = setter.response;
        $.ajax($.extend(data, {
            url: url,
            type: "post",
            headers: token,
            success: function (res) {
                if (res[response.statusName] === response.statusCode.logout) {
                    layer.msg("登陆超时，请重新登陆", {
                        icon: 5
                    });
                    admin.exit();
                } else {
                    success(res);
                }
            },
            error: function (res) {
                error(res);
            },
            complete: function (res) {
                complete(res);
                layer.closeAll("loading");
                console.timeEnd(url);
            },
        }));
    }

    adjfut.echarts = function (elem, options) {
        if ($(elem).eq(0).length === 0) {
            console.log("elem不存在");
            return false
        }
        var canvas = echarts.init($(elem)[0], "macarons");
        // var canvas = echarts.init($(elem)[0]);
        canvas.setOption(options);
        return canvas;
    }

    exports("adjfut", adjfut);
})