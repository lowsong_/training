layui.define(["layer", "setter", "admin", "element"], function (exports) {
    "use strict";
    var element = layui.element;
    var layer = layui.layer;
    var setter = layui.setter;
    var admin = layui.admin;

    var adjfut = {};

    adjfut.url = function (data) {
        var url = setter.request.server;
        return url += data;
    }

    adjfut.token = function (type) {
        if (!type) {
            type = "data"
        }
        var table = layui.data(setter.tableName);
        var token_name = setter.request.tokenName
        var token = table[token_name];
        if (token) {
            var re = {};
            if (type === "header") {
                token_name = token_name.replace(/\_/, "-");
            }
            re[token_name] = token
            return re;
        } else {
            admin.exit();
            return false;
        }
    }

    adjfut.crumbs = function (data, button) {
        var elem = "#" + setter.container + "_body #crumbs";
        $(elem).html("");
        var html = "";
        html += '<div class="layui-card layadmin-header">';
        html += '<div class="layui-breadcrumb" style="display: inline-block;">';
        html += '<a href="#/zhiyun/index/index"><i class="layui-icon layui-icon-home"></i></a>'
        // html += '<div class="layui-breadcrumb" style="display: inline-block;">';
        layui.each(data, function (key, value) {
            if (data.length === key + 1) {
                if (value.url === false) {
                    html += [
                        '<a href="javascript:;">',
                        '<cite>',
                        value.name,
                        '</cite>',
                        '</a>'
                    ].join("");
                } else {
                    html += [
                        // '<a href="javascript:;" data-current_url="' + value.url + '">',
                        '<a href="' + value.url + '">',
                        '<cite>',
                        value.name,
                        '</cite>',
                        '</a>'
                    ].join("");
                }
            } else {
                if (value.url === false) {
                    html += [
                        '<a href="javascript:;">',
                        value.name,
                        '</a>'
                    ].join("");
                } else {
                    html += [
                        // '<a href="javascript:;" data-current_url="' + value.url + '">',
                        '<a href="' + value.url + '">',
                        value.name,
                        '</a>'
                    ].join("");
                }

            }
        })
        html += '</div>';
        if (button) {
            html += '<div style="display: inline-block;float:right;margin: 0 15px;">';
            layui.each(button, function (key, value) {
                html += [
                    '<button type="button" class="crumbs-btn layui-btn layui-btn-sm ' + value.btn + '">',
                    '<i style="font-size:12px !important;" class="layui-icon ' + value.icon + '"></i>',
                    value.name,
                    '</button>',
                ].join("");
            })

            html += '</div>';
        }
        html += '<div class="clear"></div>';
        html += '</div>';
        $(elem).eq(0).append(html);
        element.render("breadcrumb");
        if (button) {
            $(elem + " .crumbs-btn").each(function (key, elem) {
                $(this).bind("click", function () {
                    button[key].function();
                })
            })
        }
    }

    function form_required(that) {
        var label = $(that).parent().parent().find("label");
        var name = $.trim(label.html() || $(that).attr("lay-label"));
        if (name) {
            $(that).attr("placeholder", "请输入" + name);
            $(that).attr("autocomplete", "off");
            $(that).attr("lay-reqtext", "请输入" + name);
        }
    }

    adjfut.form_required = function () {
        var form = $(".layui-form");
        var required_input = form.find("input[lay-verify=required]");
        layui.each(required_input, function (_, value) {
            form_required(value);
        });
        var required_textarea = form.find("textarea[lay-verify=required]");
        layui.each(required_textarea, function (_, value) {
            form_required(value);
        });
    }

    adjfut.view_id = function (data) {
        var re = "";
        if (!data) {
            var router = layui.router();
            var path = router.path;
            if (path.length === 1) {
                re = path[0];
            } else {
                re = router.path.join("_");
            }
        } else {
            re = data.replace(/\//g, "_");
        }
        return re
    }

    adjfut.request = function (data) {
        layer.load();
        var token = adjfut.token("header");
        var url = setter.request.server;
        var success = function () { };
        var error = function () { };
        var complete = function () { };
        if (data.url) {
            url += data.url;
            delete data.url;
        }
        console.time(url);
        if (data.success) {
            success = data.success;
            delete data.success;
        }
        if (data.error) {
            error = data.error;
            delete data.error;
        }
        if (data.complete) {
            complete = data.complete;
            delete data.complete;
        }
        var response = setter.response;
        $.ajax($.extend(data, {
            url: url,
            type: "post",
            headers: token,
            success: function (res) {
                if (res[response.statusName] === response.statusCode.logout) {
                    layer.msg("登陆超时，请重新登陆", {
                        icon: 5
                    });
                    admin.exit();
                } else {
                    success(res);
                }
            },
            error: function (res) {
                error(res);
            },
            complete: function (res) {
                complete(res);
                layer.closeAll("loading");
                console.timeEnd(url);
            },
        }));
    }
    exports("adjfut", adjfut);
})