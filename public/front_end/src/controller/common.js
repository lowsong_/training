/**

 @Name：layuiAdmin 公共业务
 @Author：贤心
 @Site：http://www.layui.com/admin/
 @License：LPPL
    
 */

layui.define(function (exports) {
	var $ = layui.$,
		layer = layui.layer,
		laytpl = layui.laytpl,
		setter = layui.setter,
		view = layui.view,
		admin = layui.admin

	//公共业务的逻辑处理可以写在此处，切换任何页面都会执行
	//……



	//退出
	admin.events.logout = function () {
		//执行退出接口
		// admin.req({
		//   url: './json/user/logout.js'
		//   ,type: 'get'
		//   ,data: {}
		//   ,done: function(res){ //这里要说明一下：done 是只有 response 的 code 正常才会执行。而 succese 则是只要 http 为 200 就会执行

		//     //清空本地记录的 token，并跳转到登入页
		admin.exit();
		// }
		// });
	};

	var href = decodeURI(layui.router().href)
	var $layout = $("#layout")
	var time = new Date().getTime()

	layui.each($layout.find("#footer .foot_list"), function (_, value) {
		var $value = $(value)
		var $img = $value.find("img")
		var $p = $value.find("p")
		if ($value.data("href") === href) {
			$img.attr("src", $img.data("active-img") + "?v=" + time)
			$p.css({
				color: "#1E9FFF"
			})
		} else {
			$img.attr("src", $img.data("img") + "?v=" + time)
			$p.css({
				color: "#ccc"
			})
		}
		$value.on("click", function () {
			window.location.hash = "#" + $value.data("href")
		})
	})



	// // <div class="foot_list foot_lista"><img src="./static/images/home.png"><p>首页</p></div>
	// // <div class="foot_list foot_listb"><img src="./static/images/yuebao2.png"><p>日报</p></div>
	// // <div class="foot_list foot_listc"><img src="./static/images/qiandaob2.png"><p>签到</p></div>
	// // <div class="foot_list foot_listd"><img src="./static/images/wode2.png"><p>我的</p></div>
	// switch (href) {
	// 	case "/student/index/index":
	// 		$layout.find("#foot_list .foot_lista").find("img").attr("src", "./static/images/home2.png?v=" + time)
	// 		break;

	// 	default:
	// 		$layout.find("#foot_list .foot_lista").find("img").attr("src", "./static/images/home.png?v=" + time)
	// 		$layout.find("#foot_list .foot_listb").find("img").attr("src", "./static/images/yuebao1.png?v=" + time)
	// 		$layout.find("#foot_list .foot_listc").find("img").attr("src", "./static/images/qiandaob1.png?v=" + time)
	// 		$layout.find("#foot_list .foot_listd").find("img").attr("src", "./static/images/wode1.png?v=" + time)
	// 		break;
	// }

	// console.log(href)

	// $("#layout #back").show()


	//对外暴露的接口
	exports('common', {});
});