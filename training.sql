/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50553
Source Host           : localhost:3306
Source Database       : training

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2020-03-10 20:10:45
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `clock_in`
-- ----------------------------
DROP TABLE IF EXISTS `clock_in`;
CREATE TABLE `clock_in` (
  `clock_in_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `clock_in_time` varchar(255) DEFAULT NULL COMMENT '打卡时间',
  `clock_in_type` varchar(255) DEFAULT NULL COMMENT '正常|迟到',
  `clock_in_point` varchar(255) DEFAULT NULL COMMENT '打卡点',
  `clock_in_distance` varchar(255) DEFAULT NULL COMMENT '距离',
  `clock_in_enterprise_name` varchar(255) DEFAULT NULL,
  `clock_in_enterprise_address` varchar(255) DEFAULT NULL,
  `clock_in_enterprise_points` varchar(255) DEFAULT NULL,
  `clock_in_enterprise_radius` varchar(255) DEFAULT NULL,
  `clock_in_guid` varchar(255) NOT NULL,
  PRIMARY KEY (`clock_in_id`,`clock_in_guid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of clock_in
-- ----------------------------

-- ----------------------------
-- Table structure for `clock_in_set`
-- ----------------------------
DROP TABLE IF EXISTS `clock_in_set`;
CREATE TABLE `clock_in_set` (
  `clock_in_set_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `clock_in_set_name` varchar(255) DEFAULT NULL COMMENT '打卡设置名称',
  `clock_in_set_start` varchar(255) DEFAULT NULL COMMENT '打卡设置开始时间',
  `clock_in_set_end` varchar(255) DEFAULT NULL COMMENT '打卡设置结束时间',
  `clock_in_set_guid` varchar(255) NOT NULL,
  PRIMARY KEY (`clock_in_set_id`,`clock_in_set_guid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of clock_in_set
-- ----------------------------

-- ----------------------------
-- Table structure for `enterprise`
-- ----------------------------
DROP TABLE IF EXISTS `enterprise`;
CREATE TABLE `enterprise` (
  `enterprise_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `enterprise_logo` varchar(255) DEFAULT NULL COMMENT '企业logo',
  `enterprise_name` varchar(255) DEFAULT NULL COMMENT '企业名字',
  `enterprise_address` varchar(255) DEFAULT NULL COMMENT '企业地址',
  `enterprise_introduction` varchar(255) DEFAULT NULL COMMENT '企业简介',
  `enterprise_nature` varchar(255) DEFAULT NULL COMMENT '企业性质',
  `enterprise_phone` varchar(255) DEFAULT NULL COMMENT '企业联系电话',
  `enterprise_user_name` varchar(255) DEFAULT NULL COMMENT '企业负责人',
  `enterprise_points` varchar(255) DEFAULT NULL COMMENT '企业定位点',
  `enterprise_radius` varchar(255) DEFAULT NULL COMMENT '企业半径',
  `enterprise_status` varchar(255) NOT NULL COMMENT '审核通过|审核中|审核不通过',
  `enterprise_remark` varchar(255) DEFAULT NULL COMMENT '企业审核理由',
  `enterprise_delete` varchar(255) NOT NULL COMMENT '启用|禁用',
  `enterprise_guid` varchar(255) NOT NULL,
  `enterprise_district` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`enterprise_id`,`enterprise_guid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of enterprise
-- ----------------------------
INSERT INTO `enterprise` VALUES ('1', '20200115\\a01c04d1fb2a6c678517b155d5a86ebe.jpg', '胡锦超职业技术学校', '广东省佛山市顺德区容桂文明西路40号', 'introduction', 'nature', '12345678910', 'user_name', '113.272134,22.755631', '200', '审核不通过', 'ramark', '启用', '', null);
INSERT INTO `enterprise` VALUES ('2', '20200107\\dd616ea80034cc18b9f705d732194c3d.jpg', '12121212', '123', '123', '123', '123', '123', '1231', '23123', '审核中', null, '启用', '4dd2997f-f95c-cd67-afd8-d300463fca7a', null);
INSERT INTO `enterprise` VALUES ('3', '20200107\\f3f09b1a58d60f5b0a1c4018c893a4d1.jpg', '胡锦超职业技术学校', '广东省佛山市顺德区容桂文明西路40号', '12312', '1231', '3', '1231', '113.272391,22.756051', '100', '审核中', null, '启用', '685e17bc-1c56-86cf-bfa7-d81ec96ff64e', null);

-- ----------------------------
-- Table structure for `enterprise_position`
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_position`;
CREATE TABLE `enterprise_position` (
  `enterprise_position_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `enterprise_position_name` varchar(255) DEFAULT NULL COMMENT '企业职位名称',
  `enterprise_position_introduction` varchar(255) DEFAULT NULL COMMENT '企业职位简介',
  `enterprise_position_treatment` varchar(255) DEFAULT NULL COMMENT '企业职位待遇(用英文逗号分隔)',
  `enterprise_position_number` int(255) DEFAULT NULL COMMENT '企业职位人数',
  `enterprise_position_salary_min` float(255,2) DEFAULT NULL COMMENT '企业职位最小工资',
  `enterprise_position_salary_max` float(255,2) DEFAULT NULL COMMENT '企业职位最大工资',
  `enterprise_position_status` varchar(255) NOT NULL COMMENT '启用|禁用',
  `enterprise_position_delete` varchar(255) NOT NULL COMMENT '启用|禁用',
  `enterprise_position_guid` varchar(255) NOT NULL,
  `enterprise_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`enterprise_position_id`,`enterprise_position_guid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of enterprise_position
-- ----------------------------
INSERT INTO `enterprise_position` VALUES ('1', '工程师', '简介', '1', '1', '1.00', '1.00', '启用', '启用', '1', '3');

-- ----------------------------
-- Table structure for `enterprise_position_user`
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_position_user`;
CREATE TABLE `enterprise_position_user` (
  `enterprise_position_user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `enterprise_position_id` int(11) DEFAULT NULL COMMENT '企业职位id',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `enterprise_position_user_guid` varchar(255) NOT NULL,
  `enterprise_position_user_summary` text COMMENT '总结',
  `enterprise_position_user_evaluation` text COMMENT '评价',
  PRIMARY KEY (`enterprise_position_user_id`,`enterprise_position_user_guid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of enterprise_position_user
-- ----------------------------
INSERT INTO `enterprise_position_user` VALUES ('1', '1', '7', '1', null, '');
INSERT INTO `enterprise_position_user` VALUES ('2', '1', '11', '1', null, '');
INSERT INTO `enterprise_position_user` VALUES ('3', '1', '12', '1', null, '');
INSERT INTO `enterprise_position_user` VALUES ('4', '1', '13', '1', null, null);
INSERT INTO `enterprise_position_user` VALUES ('5', '1', '15', '1', null, null);
INSERT INTO `enterprise_position_user` VALUES ('6', null, null, '', null, null);

-- ----------------------------
-- Table structure for `menu`
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `menu_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '菜单id',
  `menu_parent_id` int(11) NOT NULL COMMENT '父id',
  `menu_name` varchar(255) DEFAULT '' COMMENT '菜单名称',
  `menu_url` varchar(255) DEFAULT NULL COMMENT '菜单路径',
  `menu_icon` varchar(255) DEFAULT NULL COMMENT '菜单图标',
  `menu_index` varchar(255) DEFAULT NULL COMMENT '菜单排序',
  `menu_status` varchar(255) NOT NULL COMMENT '启用|停用',
  `menu_delete` varchar(255) NOT NULL COMMENT '启用|停用',
  `menu_guid` varchar(200) NOT NULL,
  PRIMARY KEY (`menu_id`,`menu_guid`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES ('1', '2', '菜单管理', 'adjfut/menu/index', 'null', '1', '1', '1', '0');
INSERT INTO `menu` VALUES ('2', '0', '系统设置', null, null, null, '1', '1', '0');
INSERT INTO `menu` VALUES ('26', '25', '所有日志', 'report/report/index', '', '1', '1', '1', 'ea6dff86-837f-c208-db5c-7cd4b026b9ce');
INSERT INTO `menu` VALUES ('7', '0', '用户管理', '', '', '1', '1', '1', '229183ed-32a5-3b34-1d53-e6f1c57c0867');
INSERT INTO `menu` VALUES ('8', '7', '用户列表', 'adjfut/user/index', '', '1', '1', '1', '6cdb843f-3d92-0e0f-adb4-8c87fea5a163');
INSERT INTO `menu` VALUES ('9', '2', '角色管理', 'adjfut/role/index', '', '1', '1', '1', '3f6ddbc4-31e4-315e-8535-ec308baa5ad0');
INSERT INTO `menu` VALUES ('10', '7', '用户角色', 'adjfut/user_role/index', '', '1', '1', '1', '2a1ca034-7e40-5290-ee93-cebfe1b6d3eb');
INSERT INTO `menu` VALUES ('28', '23', '企业职位', 'enterprise/position/index', '', '1', '1', '1', '29657d41-e640-5620-2b4e-a788235614b7');
INSERT INTO `menu` VALUES ('23', '0', '企业管理', '', '', '1', '1', '1', 'e497e6bb-93dc-d164-d447-1dabedaec7ee');
INSERT INTO `menu` VALUES ('14', '11', '组织架构管理', 'adjfut/organize/index', '', '1', '1', '1', 'f9d81b80-adb2-c20c-cf5f-b54111cf945a');
INSERT INTO `menu` VALUES ('27', '7', '用户企业', 'adjfut/enterprise_position_user/index', '', '1', '1', '1', '91de4128-6e12-49cb-b08a-2f22f7d7de45');
INSERT INTO `menu` VALUES ('24', '23', '企业管理', 'enterprise/enterprise/index', '', '1', '1', '1', '733246c3-de55-d504-8830-3bf390694a14');
INSERT INTO `menu` VALUES ('25', '0', '日志管理', '', '', '1', '1', '2', '839a9706-e0c8-639b-5b95-32ab27fd1624');
INSERT INTO `menu` VALUES ('17', '7', '用户组织', 'adjfut/user_organize/index', '', '1', '1', '1', 'cd67e3e6-14d9-d2de-23eb-a5b1f4ba30bd');
INSERT INTO `menu` VALUES ('18', '2', '组织管理', 'adjfut/organize/index', '', '1', '1', '1', '5a8ee15a-83d4-aa02-8fa3-0aca60f41025');
INSERT INTO `menu` VALUES ('29', '0', '公告管理', 'notice/notice/index', '', '1', '1', '1', 'cd3cf39a-97d0-4860-97ef-2b3ffc1f0927');

-- ----------------------------
-- Table structure for `notice`
-- ----------------------------
DROP TABLE IF EXISTS `notice`;
CREATE TABLE `notice` (
  `notice_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `organize_id` int(11) NOT NULL COMMENT '组织架构id',
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `notice_title` varchar(255) DEFAULT NULL COMMENT '公告标题',
  `notice_content` varchar(255) DEFAULT NULL COMMENT '公告内容',
  `notice_file_name` varchar(255) DEFAULT NULL,
  `notice_file_path` varchar(255) DEFAULT NULL,
  `notice_time` int(255) NOT NULL,
  `notice_status` varchar(255) NOT NULL COMMENT '启用|禁用',
  `notice_delete` varchar(255) NOT NULL COMMENT '启用|禁用',
  `notice_guid` varchar(255) NOT NULL,
  PRIMARY KEY (`notice_id`,`notice_guid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of notice
-- ----------------------------
INSERT INTO `notice` VALUES ('1', '1', '1', '123123', '1231231哈哈谁离开的飞机撒两地分居', null, null, '0', '启用', '停用', '09daf499-bd1a-9d13-1a28-11deb797635e');
INSERT INTO `notice` VALUES ('11', '1', '1', '123', '123', 'test.txt', '20200116\\efb797a01af77256fb0a68a815e2c92c.txt', '1579138822', '启用', '启用', 'd4faa9a4-7767-48d5-f2b9-340e6a6cbf45');
INSERT INTO `notice` VALUES ('12', '2', '1', '123', '123', 'test.txt', '20200116\\efb797a01af77256fb0a68a815e2c92c.txt', '1579138822', '启用', '启用', 'd4faa9a4-7767-48d5-f2b9-340e6a6cbf45');
INSERT INTO `notice` VALUES ('13', '3', '1', '123', '123', 'test.txt', '20200116\\efb797a01af77256fb0a68a815e2c92c.txt', '1579138822', '启用', '启用', 'd4faa9a4-7767-48d5-f2b9-340e6a6cbf45');

-- ----------------------------
-- Table structure for `organize`
-- ----------------------------
DROP TABLE IF EXISTS `organize`;
CREATE TABLE `organize` (
  `organize_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '组织架构id',
  `organize_parent_id` int(11) DEFAULT NULL COMMENT '父ID',
  `organize_name` varchar(255) DEFAULT NULL COMMENT '机构名称',
  `organize_status` varchar(255) NOT NULL COMMENT '启用|停用',
  `organize_delete` varchar(255) NOT NULL COMMENT '启用|停用',
  `organize_guid` varchar(200) NOT NULL,
  `organize_internship_start` varchar(200) DEFAULT NULL COMMENT '开始实习时间',
  `organize_internship_end` varchar(200) DEFAULT NULL COMMENT '结束实习时间',
  `organize_note` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`organize_id`,`organize_guid`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of organize
-- ----------------------------
INSERT INTO `organize` VALUES ('1', '5', '平湖市人民政府当湖街道办事处', '1', '1', 'da0bd2cd-18d5-416b-9aba-c980b244e020', null, null, null);
INSERT INTO `organize` VALUES ('2', '1', '1901班', '1', '2', '3029f99e-0f41-50dc-4e1c-a41d6f1e5422', '2020-01-10', '2020-06-30', null);
INSERT INTO `organize` VALUES ('3', '1', '1902班', '1', '2', 'fda2a7b0-d61d-2e19-20f9-d2f4cdfd7717', null, null, null);
INSERT INTO `organize` VALUES ('4', '1', '测试', '1', '2', 'ee7c801c-c540-c207-ac40-b7177a2150f5', '2020-01-10', '2020-06-30', null);
INSERT INTO `organize` VALUES ('5', '0', '上海赫得环境科技股份有限公司', '1', '1', '1', null, null, null);
INSERT INTO `organize` VALUES ('6', '0', '设备厂商（演示专用）', '1', '1', '1340d06c-be94-51a3-fa14-a544c0ffb491', null, null, null);
INSERT INTO `organize` VALUES ('7', '0', '设备研发部门（研发专用）', '1', '1', 'a38f9f33-c59c-42bc-669d-7df507154778', null, null, null);
INSERT INTO `organize` VALUES ('8', '6', 'A深埋桶客户单位（演示专用）', '1', '1', 'cbf46df1-3009-31ea-6b6b-640781dd764e', null, null, null);
INSERT INTO `organize` VALUES ('9', '6', 'B移动箱客户单位（演示专用）', '1', '1', 'fc0a491f-5b24-7284-09c1-c640a9e1bda5', null, null, null);
INSERT INTO `organize` VALUES ('10', '7', '设备测试部门（测试专用）', '1', '1', '6130997d-7db4-f580-835b-98c7f91d41bf', null, null, null);
INSERT INTO `organize` VALUES ('11', '5', '昆山城建绿和环境科技有限公司', '1', '1', 'd487b552-0053-d816-e022-13569203ce60', null, null, null);
INSERT INTO `organize` VALUES ('12', '5', '昆山市周庄镇环境卫生管理所', '1', '1', 'efc48cfd-ecde-0e06-960e-ea68a2c83870', null, null, null);
INSERT INTO `organize` VALUES ('13', '5', '昆山市千灯镇环境卫生管理所', '1', '1', '5d794a52-63fe-c5fb-9ad6-5a8079058f38', null, null, null);

-- ----------------------------
-- Table structure for `report`
-- ----------------------------
DROP TABLE IF EXISTS `report`;
CREATE TABLE `report` (
  `report_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `report_content` varchar(255) DEFAULT NULL COMMENT '报告内容',
  `report_type` varchar(255) DEFAULT NULL COMMENT '日报|周报|月报',
  `report_time` varchar(255) DEFAULT NULL COMMENT '报告时间',
  `report_guid` varchar(255) NOT NULL,
  `report_year` int(255) DEFAULT NULL COMMENT '报告年',
  `report_month` int(255) DEFAULT NULL COMMENT '报告月',
  `report_day` int(255) DEFAULT NULL COMMENT '报告日',
  `report_week` int(255) DEFAULT NULL COMMENT '报告周',
  `report_audit` varchar(255) NOT NULL COMMENT '审核通过/审核中/审核不通过',
  `report_audit_message` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`report_id`,`report_guid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of report
-- ----------------------------
INSERT INTO `report` VALUES ('2', '7', '1. 完成日报/周报/月报\n2.完成签到\n3.完善页面\n4.玩家测试\n5.666', '日报', '1578722992', 'aecdae30-1df3-3744-d64a-f1585f77919f', '2020', '1', '11', '1', '审核中', null);
INSERT INTO `report` VALUES ('3', '7', '测试周报', '周报', '1578723476', '188d4891-f3a7-cbf1-fe35-f5206340113e', '2020', '1', '11', '1', '审核中', null);
INSERT INTO `report` VALUES ('4', '7', '测试月报', '月报', '1578723502', '8591521d-1f20-df11-b851-31b100389c70', '2020', '1', '11', '1', '审核中', null);
INSERT INTO `report` VALUES ('5', '7', '1.测试123\n2.测试321\n3.测试', '日报', '1578906813', '056995f6-31f6-39d1-9015-e20e97836861', '2020', '1', '13', '2', '审核通过', '通过');

-- ----------------------------
-- Table structure for `role`
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `role_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '角色id',
  `role_name` varchar(255) NOT NULL COMMENT '角色名称',
  `role_remark` varchar(255) DEFAULT NULL COMMENT '角色备注说明',
  `role_guid` varchar(255) NOT NULL,
  `role_status` int(11) NOT NULL,
  `role_delete` int(11) NOT NULL,
  PRIMARY KEY (`role_id`,`role_guid`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES ('1', '管理员', '备注', '0', '1', '1');
INSERT INTO `role` VALUES ('2', 'asad', '', '30a04997-dcf3-b373-7b18-7314ae21ed75', '1', '2');
INSERT INTO `role` VALUES ('3', '测试', '大萨达萨达', '7123ff4d-1bb3-483d-5c29-24a17abe2c7e', '1', '2');
INSERT INTO `role` VALUES ('4', '测试1', '测试1', '1d1c929f-5bf6-3ccd-870b-b6dfbbc80d3d', '1', '2');
INSERT INTO `role` VALUES ('5', '测试1', '测试1', '0833aac2-4a9c-e1fd-d83f-5318b01db693', '1', '2');
INSERT INTO `role` VALUES ('6', '厂商主管', '主管', 'db8107a0-ea33-e0e3-43b2-230fa536e0bb', '1', '1');
INSERT INTO `role` VALUES ('7', '厂商负责人', '负责人', 'f06bd389-3b98-0405-172d-8a3435f41c11', '1', '1');
INSERT INTO `role` VALUES ('8', '单位主管', '主管', '78e5d218-0a0c-4c7f-6de8-6a934c8ae20a', '1', '1');
INSERT INTO `role` VALUES ('9', '单位负责人', '负责人', '8128aeba-8ba7-efa9-6151-f3eb767ab064', '1', '1');
INSERT INTO `role` VALUES ('10', '收运员', '收运员', '704df12b-f9c6-5457-ba96-1e66e70a4f5f', '1', '1');

-- ----------------------------
-- Table structure for `role_menu`
-- ----------------------------
DROP TABLE IF EXISTS `role_menu`;
CREATE TABLE `role_menu` (
  `role_menu_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '角色菜单id',
  `menu_id` int(11) DEFAULT NULL COMMENT '菜单id',
  `role_id` int(11) DEFAULT NULL COMMENT '角色id',
  `role_menu_guid` varchar(255) NOT NULL,
  PRIMARY KEY (`role_menu_id`,`role_menu_guid`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=272 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of role_menu
-- ----------------------------
INSERT INTO `role_menu` VALUES ('269', '25', '1', 'cd5e2b79-4734-ef5c-29d4-f73368ae64f4');
INSERT INTO `role_menu` VALUES ('268', '24', '1', 'd8c0146f-9b22-dd8c-7196-0bc096d8b5ba');
INSERT INTO `role_menu` VALUES ('267', '28', '1', '0a544f74-9a23-6da2-4227-565fd96c4ea4');
INSERT INTO `role_menu` VALUES ('266', '23', '1', '21e112ba-68ce-149e-54ac-173d8d25e1e2');
INSERT INTO `role_menu` VALUES ('265', '17', '1', '2452be69-8ad4-5be6-7916-2f1a33d28781');
INSERT INTO `role_menu` VALUES ('264', '27', '1', '2665199b-9019-6b67-0b2a-a1c8dbcb558f');
INSERT INTO `role_menu` VALUES ('263', '10', '1', 'a1d71903-fcce-38bd-b606-52ba629822c1');
INSERT INTO `role_menu` VALUES ('262', '8', '1', 'beebf1e8-5718-0813-5502-b1c0e2983cd5');
INSERT INTO `role_menu` VALUES ('261', '7', '1', '86028f70-ec9a-3c39-06f7-17068a409270');
INSERT INTO `role_menu` VALUES ('260', '18', '1', 'eff0fee4-4acc-8e55-6c92-54930fcafe41');
INSERT INTO `role_menu` VALUES ('259', '9', '1', 'f6d14f6b-1f26-b88a-1119-50b0dca4c237');
INSERT INTO `role_menu` VALUES ('258', '1', '1', '95b34704-41f9-7961-e370-02e102f8d082');
INSERT INTO `role_menu` VALUES ('257', '2', '1', '59eb715f-00ea-8d5c-2df3-85585bb17884');
INSERT INTO `role_menu` VALUES ('270', '26', '1', '28a41926-af89-a8a1-b7a5-5354cf6456a9');
INSERT INTO `role_menu` VALUES ('271', '29', '1', '0e0c9ec3-ce99-fd5c-6862-51ae87b226a0');

-- ----------------------------
-- Table structure for `sign_in`
-- ----------------------------
DROP TABLE IF EXISTS `sign_in`;
CREATE TABLE `sign_in` (
  `sign_in_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `sign_in_time` varchar(255) DEFAULT NULL COMMENT '打卡时间',
  `sign_in_type` varchar(255) DEFAULT NULL COMMENT '正常|迟到',
  `sign_in_point` varchar(255) DEFAULT NULL COMMENT '打卡点',
  `sign_in_address` text COMMENT '地址',
  `sign_in_distance` varchar(255) DEFAULT NULL COMMENT '距离',
  `sign_in_enterprise_name` varchar(255) DEFAULT NULL,
  `sign_in_enterprise_address` varchar(255) DEFAULT NULL,
  `sign_in_enterprise_points` varchar(255) DEFAULT NULL,
  `sign_in_enterprise_radius` varchar(255) DEFAULT NULL,
  `sign_in_guid` varchar(255) NOT NULL,
  PRIMARY KEY (`sign_in_id`,`sign_in_guid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of sign_in
-- ----------------------------
INSERT INTO `sign_in` VALUES ('1', '7', '1578736820', '正常', '113.27293,22.755146', '广东省佛山市顺德区容桂街道文滘路胡锦超职业技术学校', '58', '测试企业', '123', '113.27252,22.75479', '100', '213dcd4d-f2d9-c5e6-e031-4710a845c060');
INSERT INTO `sign_in` VALUES ('3', '7', '1578869844', '正常', '113.272921,22.755176', '广东省佛山市顺德区容桂街道文滘路胡锦超职业技术学校', '60', '测试企业', '123', '113.27252,22.75479', '100', 'eb54a313-fa6f-a9db-b73e-0d09f81b1e78');
INSERT INTO `sign_in` VALUES ('4', '12', '1578962100', '正常', '113.272911,22.755152', '广东省佛山市顺德区容桂街道文滘路胡锦超职业技术学校', '57', '测试企业', '123', '113.27252,22.75479', '100', 'b2159675-4dbd-ad97-2f14-4542f267e8b0');
INSERT INTO `sign_in` VALUES ('8', '7', '1578991330', '正常', '113.273002,22.755202', '广东省佛山市顺德区容桂街道文滘路胡锦超职业技术学校', '67', '测试企业', '123', '113.27252,22.75479', '100', '61b88a54-286c-f0cb-a0d7-bcd542f19473');
INSERT INTO `sign_in` VALUES ('9', '7', '1579053942', '正常', '113.273002,22.755202', '广东省佛山市顺德区容桂街道文滘路胡锦超职业技术学校', '67', '测试企业', '123', '113.27252,22.75479', '100', 'ce599420-a6af-1614-81ad-f2b28366794a');
INSERT INTO `sign_in` VALUES ('10', '11', '1579054131', '正常', '113.272896,22.755171', '广东省佛山市顺德区容桂街道文滘路胡锦超职业技术学校', '57', '测试企业', '123', '113.27252,22.75479', '100', 'b71ebd46-67f7-bfa3-75d4-50cb17d5fd40');

-- ----------------------------
-- Table structure for `token`
-- ----------------------------
DROP TABLE IF EXISTS `token`;
CREATE TABLE `token` (
  `token_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'tokenid',
  `token_content` varchar(255) DEFAULT NULL COMMENT 'token内容',
  `token_time` varchar(255) DEFAULT NULL COMMENT '过期时间',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `token_guid` varchar(255) NOT NULL,
  PRIMARY KEY (`token_id`,`token_guid`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=120 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of token
-- ----------------------------
INSERT INTO `token` VALUES ('111', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJ1c2VyIiwiaWF0IjoxNTgzODQxMTA5LCJleHAiOjE1ODM4NDQ3MDksIm5iZiI6MTU4Mzg0MTEwOSwic3ViIjo0ODAyNSwianRpIjoiYTRjNDU3NjI5YzI0ZmNhMzE0ZTI2YzE0ZGNkZTUyYTgifQ.6XpfxcCywt5esawpWdhv0EVtXKnRAR0WZpx9CgXBzW8', '1583844709', '1', '9494765c-2525-8ea8-0ece-572f42f22009');
INSERT INTO `token` VALUES ('112', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJ1c2VyIiwiaWF0IjoxNTc3NzYwMzMzLCJleHAiOjE1Nzc3NjM5MzMsIm5iZiI6MTU3Nzc2MDMzMywic3ViIjo5MjMyMywianRpIjoiNDI0ODk1OTM2NTU4YWRmYTQ4OTdjMzliYmJkYTJkZWYifQ.lkXtor-nlaQ3CEieKbKzmWD5V1cYTY5YtCqfb53HDAg', '1577763933', '2', '21774dd3-243f-f185-5208-d416c1bdc004');
INSERT INTO `token` VALUES ('113', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJ1c2VyIiwiaWF0IjoxNTc4MDk4MjYxLCJleHAiOjE1NzgxMDE4NjEsIm5iZiI6MTU3ODA5ODI2MSwic3ViIjoyODI5NiwianRpIjoiZDRjMGY4ZjVjOThmNTY5ZTZiZmEwNmUyMjdlMDQzNWQifQ.ppKutmQ-rJJ9PQJ6Nt15G26NOnPQ81kKU3N15B0cps0', '1578101861', '3', 'c53ae006-8644-c8b2-8c91-5f26baecefe4');
INSERT INTO `token` VALUES ('114', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJ1c2VyIiwiaWF0IjoxNTc5MTU5MjUwLCJleHAiOjE1NzkxNjI4NTAsIm5iZiI6MTU3OTE1OTI1MCwic3ViIjo0MTMwOCwianRpIjoiNjVlMWI2ZjBkMjI5MmRlMmJlNTkwY2FmN2ZhYzcxZGMifQ.ROPhXvWHgEEy37ZdcrUG-3Udsw0ZP-TGcmYDJbz40WM', '1579162850', '15', 'fb33de2d-2932-5725-a2ec-227554ce7835');
INSERT INTO `token` VALUES ('115', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJ1c2VyIiwiaWF0IjoxNTgxNDcwODc0LCJleHAiOjE1ODE0NzQ0NzQsIm5iZiI6MTU4MTQ3MDg3NCwic3ViIjoyMjM2MSwianRpIjoiMTRiOTBkZGI2OGUxMmY5YTdhOWY0MDdiYmQ3YWYwZGQifQ.UIuivoxv0--5x-tCF-9WckH22nOQzbssw5ePeTtylH8', '1581474474', '7', '8fd2c518-3f01-8197-0d20-530f3ef1f54e');
INSERT INTO `token` VALUES ('116', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJ1c2VyIiwiaWF0IjoxNTc5MjQ5MTcxLCJleHAiOjE1NzkyNTI3NzEsIm5iZiI6MTU3OTI0OTE3MSwic3ViIjoyMzMyNSwianRpIjoiY2ExYjgwZTRjMTZiMTM1Mjg2NzI3MDJlM2NhMmNkNTIifQ.Z7aDRFEzmOFFp2fntKi-JRrU_uFXcSgHrx_tKLv5438', '1579252771', '10', 'bcc7fdb8-8cc2-49cf-9d3f-6e909addf931');
INSERT INTO `token` VALUES ('117', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJ1c2VyIiwiaWF0IjoxNTc5MjQ1MjE2LCJleHAiOjE1NzkyNDg4MTYsIm5iZiI6MTU3OTI0NTIxNiwic3ViIjo2OTEwMiwianRpIjoiYTA4ZTdhMWRmYjdkYzdhMzVhMGFkODllYzFjYTc3MGQifQ.nL8tjTteyAKUxZP6MgjmCG1rZek0s78I1cYXU08pVDE', '1579248816', '11', '19136be3-8dad-c153-df8a-84f1fb964efd');
INSERT INTO `token` VALUES ('118', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJ1c2VyIiwiaWF0IjoxNTc5MzM4NTU2LCJleHAiOjE1NzkzNDIxNTYsIm5iZiI6MTU3OTMzODU1Niwic3ViIjoxODYyNywianRpIjoiODRkNWQxODk0MGNlOWZlOWRhOGFkNWNhZTM4YmFkMWMifQ.xWsBhTSAdzlqeWMV2Z3ojAoucCz5iXwvqpHxj3PKxik', '1579342156', '12', 'a73d6e30-9dd4-4c7e-6386-7841374415a8');
INSERT INTO `token` VALUES ('119', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJ1c2VyIiwiaWF0IjoxNTgzODIxNDM5LCJleHAiOjE1ODM4MjUwMzksIm5iZiI6MTU4MzgyMTQzOSwic3ViIjozMTk2MCwianRpIjoiMTE5NzcxZjQ1OTc2NzRjZGZkMDM5MTY1YzdkZGVkZWUifQ.rSpUEFjnIdRpfp1JXo6EmpDLaxsfONVdAOlbgBcn9Hs', '1583825039', '131', '34107055-b2d3-5219-00c3-11c83836c13d');

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `user_name` varchar(255) DEFAULT NULL COMMENT '微信小程序openid',
  `user_headimage` varchar(255) DEFAULT NULL COMMENT '微信昵称',
  `user_password` varchar(200) DEFAULT NULL COMMENT '用户密码',
  `user_number` int(255) DEFAULT NULL COMMENT '编号',
  `user_phone` varchar(255) DEFAULT NULL COMMENT '用户手机',
  `user_identity_card` varchar(255) DEFAULT NULL COMMENT '身份证号码',
  `user_sex` varchar(200) DEFAULT NULL,
  `user_status` varchar(255) NOT NULL COMMENT '启用|停用',
  `user_delete` varchar(255) NOT NULL COMMENT '启用|停用',
  `user_guid` varchar(255) NOT NULL DEFAULT '0',
  `user_unionid` varchar(255) DEFAULT NULL,
  `user_openid_gzh` varchar(255) DEFAULT NULL,
  `user_admin` varchar(255) DEFAULT NULL COMMENT '是否超级管理员',
  `user_account` varchar(255) DEFAULT NULL,
  `user_section` varchar(255) DEFAULT NULL,
  `user_time` varchar(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`,`user_guid`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=212 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', '管理员', null, '432F45B44C432414D2F97DF0E5743818', null, '12345678910', null, '男', '2', '1', '0', null, null, null, null, null, null, null);
INSERT INTO `user` VALUES ('6', '叶问', null, '123456', '190101', '13745673214', null, '男', '1', '1', '69cece66-e961-1492-b55d-21abd65d8190', null, null, '2', null, null, null, null);
INSERT INTO `user` VALUES ('8', '北斗有七星', '', null, null, '12222222222', null, '男', '1', '1', '1b9be2c2-7596-4bc1-74bf-72bf5799d590', 'oR2y-5j-JtTTxkldftDoW1LFpGHQ', 'oM3Ms6YnuLXxa6CZ-BC4NAh9Ipqc', '2', null, null, null, null);
INSERT INTO `user` VALUES ('7', 'zhi', 'http://thirdwx.qlogo.cn/mmopen/vi_32/kQuPz4HLlnSWgo5aQ8YuaMfN8kWjBrbhVdoDgMBibNomQE0COsBL1UYKPEQgfoZZT12T5T6snogxMLCPIqvUoiaA/132', null, null, '13723568974', null, '男', '1', '1', 'e32388d7-8a9d-fb15-ab90-2ea603349531', 'oR2y-5qxQnhvpN29-gifCXUXeS1Q', 'oM3Ms6c46bZgNia-cxSKlCFAh8I0', '2', null, null, null, null);
INSERT INTO `user` VALUES ('10', 'Drav', 'http://thirdwx.qlogo.cn/mmopen/vi_32/iaJU1dZ9XeK720B4VyqQglibuHibUAcZO2XuwlKkwlrBs0fhwTEKPpDZtGJyIuA6XEcWib9CmN069M5MliawelSAs2Q/132', null, null, '13467925698', null, '男', '1', '1', 'adbd852b-59dc-7c3c-7ea5-73b8e8ddb158', 'oR2y-5l-EG6XJtEndqZowPSEx-Ng', 'oM3Ms6XyihM8Ef1FUqMmQRZJD89E', '2', null, null, null, null);
INSERT INTO `user` VALUES ('11', 'YSproject', 'http://thirdwx.qlogo.cn/mmopen/vi_32/XiaFgEcnARPwywiaGaKZJ3lRpIFyMxKJ62yb2wAmXyke4P3xqmogSBg8oBviaEiaKrXGylMezpLEZMq1PntRHPzKfA/132', null, null, '13647985426', null, '男', '1', '1', '7c0d26d3-c2b8-d71f-f800-4063de9fa4c0', 'oR2y-5v8FLE1s6ZHnFjxPwVYgCQ8', 'oM3Ms6ZUjnxLmJhb-CwoPXRIxPJ4', '2', null, null, null, null);
INSERT INTO `user` VALUES ('12', 'flashkk', 'http://thirdwx.qlogo.cn/mmopen/vi_32/NGMG9jcJ14ib9mqSrZhPuIqlV6Eb1KqJNm3Jl4VssURdwPedmUnyv77czib1Bbhmq7bwvicmu74T9WPcWE2Zknntw/132', null, null, '13923264548', null, '男', '1', '1', '5eeee72c-f27d-c192-7801-508e0144f9da', 'oR2y-5owp1RTBedNkzG_-k2NtJCE', 'oM3Ms6UwKWXogDT_LXl-O-lP_qpY', '2', null, null, null, null);
INSERT INTO `user` VALUES ('13', '阿斯顿', 'http://thirdwx.qlogo.cn/mmopen/vi_32/PiajxSqBRaEL3l82LZ531IgcsnHYjfEq8lSsJRxJcRVWQVicnia2UhLY4p4jicJRYzrxY5RMiaGHS1cNFpIlwhibH5icQ/132', '', null, '15217657216', null, '男', '1', '1', '599a5bb8-d407-22dd-4350-1b2e9039ff6b', 'oR2y-5meBBe-kyJOHR0epTKoLOvM', 'oM3Ms6Q7JkQ43PZOef1IudGdKb88', '2', null, null, null, null);
INSERT INTO `user` VALUES ('14', '五观端正三好青年', 'http://thirdwx.qlogo.cn/mmopen/vi_32/FBRibOc08xqOtI6hQqibqcjClPqxOdFZYzLxXSKWeic2XjFsnPicSPRopltxDRz3QMor8wAlkUWKqfLgs3bKibdvjrg/132', null, null, '15521448728', null, '男', '1', '1', '10838545-4e34-bb88-50b3-771828f7ad71', 'oR2y-5mSQCXpfJf4sgY0sH2M4ijk', 'oM3Ms6asp6rf_X6g6w9WeqglVsx8', '2', null, null, null, null);
INSERT INTO `user` VALUES ('15', 'Brunt', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eo4pLZAFKvngDBXZX9XDYT819Wr1mAS19ySyiawVS0RvQ5wXC4KaJViaSdSFffXAPCzmCShE2k9ZECQ/132', null, null, '15217657216', null, '男', '1', '1', 'cd9d847c-eed2-8670-8a67-99a7c76cd7e5', 'oR2y-5lfKuE05QONCnIzJ10SzQ7g', 'oM3Ms6Ut9qNqodLG-28PTzkGhCow', '2', null, null, null, null);
INSERT INTO `user` VALUES ('16', '楠 AI', 'http://thirdwx.qlogo.cn/mmopen/vi_32/p1jBkPiav9jEa334NMjV5eicibt0pXRNNHLhx4JlvcX8hiaCkfeQpxVdBvcMJuhH3xj0uQRoAq4s5lA7MWQF26ibQRQ/132', null, null, '13823427278', null, '男', '1', '1', '5295ec8f-4b9e-5af9-fca4-52631fbe77f5', 'oR2y-5k2gW84U6vNFPQNCPb2GQ3Q', 'oM3Ms6YhG8sQKt-8XaWMmDXv5rR8', '2', null, null, null, null);
INSERT INTO `user` VALUES ('17', 'abc', null, '123456', null, null, null, '男', '', '', '0', null, null, null, null, null, null, null);
INSERT INTO `user` VALUES ('131', 'Tony Tan', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTI8pc2SK68xET2BaIICbcJB3sStuP5bcebd3WpqwYJl73XR9zMg6SHPq9MdkNaibK4pbm6MbmJw3EA/132', '432F45B44C432414D2F97DF0E5743818', '1', '13564485541', null, '男', '2', '1', '7a96aab8-f555-c210-3ee3-b6cdc6a225ee', null, 'ojmae5jE1w0OZZ47kaacifRxgY-s', null, 'strg0001', 'strg', '1559045552', '谭频');
INSERT INTO `user` VALUES ('134', 'T', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKDKibF6ohESHPrqwyMO2ZKYUQfnyRWSEIDHicgpqbRrVgR6YI9rv4YNOh0WicqtDLmbWNTLwnWgibFMg/132', '9e406957d45fcb6c6f38c2ada7bace91', '2', '13962197671', null, '男', '2', '1', 'de90ad6a-2360-8b06-0cd1-d291894da857', null, 'ojmae5n57pQJSl0S8CKiGlTivHAc', null, 'strg0002', 'strg', '1559052459', '谭海');
INSERT INTO `user` VALUES ('143', 'Vincent', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJ5exQKIhibFicFMn13aM90bqRvDm1vyXdrWrXZHjTUiaVSpWrD6V5vxftJSB4kCeN22a9StJBYs1vHw/132', 'f237ac6364e1fca134f9e5216d77f645', '1', '13917615291', null, '男', '2', '1', '07fef40c-5ab7-8d4f-8e2e-899224c8442b', null, 'oA17C007sxk0Xfh8ky9IouXCh7-M', null, 'SHHED0001', 'SHHED', '1560311769', '任一帆');
INSERT INTO `user` VALUES ('144', '方平凡', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKSBPYO1L0NI1YKULqvRf95mYfkE8nVe9f6iaqliaysrPguqs6KaFOuISIneLYZ63hQ4yKcEraoszZA/132', '202cb962ac59075b964b07152d234b70', '3', '13764060268', null, '男', '2', '1', '471f079b-8b76-13f0-0dc2-b07d1223f981', null, 'ojmae5t2OUOF5vBhCEX-QRPc7ejg', null, 'strg0003', 'strg', '1560311944', '方平凡');
INSERT INTO `user` VALUES ('145', '心平气和', 'http://thirdwx.qlogo.cn/mmopen/vi_32/yWaViaGhqlWCibFUTJH7noa5t2ILB6AyQgo5ibFKBdicbgmfO2OPzJ70Fw4V5DC7JCyQ9ia8iblgY0v50Xyf7GNzYVww/132', '96dbcb525ee590dbfa7e193aeb93cb24', '4', '15026954839', null, '男', '2', '1', '64f2c96a-000d-07b5-5892-f6b3471dbbaa', null, 'ojmae5kRfxA0IzIhRgQIjjOJbTyM', null, 'STRG0004', 'STRG', '1560312770', '周海建');
INSERT INTO `user` VALUES ('146', '袁锦杰', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLdjteDdBPjt3YTqXmCcXfGY8baoibprU51B6YmBceT9BOiaHql8cCHUd71Wfbv4mF8NQZeiaCreUa2w/132', '6e36203ef1e783bbfa768610173027df', '2', '15801833015', null, '男', '2', '1', 'adef94dd-3b0c-7a58-00c7-645c554ab68f', null, 'oA17C09lRc5YhmkCNymL7glwC_l8', null, 'SHHED0002', 'SHHED', '1560313199', '袁锦杰');
INSERT INTO `user` VALUES ('147', '木木盾', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKYu6n1Xib4l2Ma6N2DLVic9tuErXcsflpn6wcVUiakZC68GQc7sIU1yIjRRHxJr141uTEbtiauxm9v1Q/132', '63352af1fef18b009be8b9a705150344', '3', '13564056927', null, '男', '2', '1', '1eed5add-03c7-4a02-278e-ea5e3be74031', null, 'oA17C0xx3F5jxz2Ma4ucVwboDNcE', null, 'SHHED0003', 'SHHED', '1560313460', '孙樑');
INSERT INTO `user` VALUES ('148', 'Ivan', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eppfacSf6rhz1Gw6Wfd7FfgrUrySOVODtuFEtPgzLE3n09yQeIibiaQ5nuU9wWF0J1QSCcoWJNS4qYQ/132', '9f99a8976e6ea0900f7d4d1c63e549ce', '4', '15802188581', null, '男', '2', '2', '14cdaa25-8415-091c-441f-b1385c5d4fdc', null, 'oA17C07PiGycpvp3Aoo_Fst_glag', null, 'SHHED0004', 'SHHED', '1560314707', '吴一凡');
INSERT INTO `user` VALUES ('149', '阿泰', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKs0tNLMmeErTNePIEBpP4EhOKYSM9VzzNTePuV0ibcAJBDQxVh9dJIrJIkr9esKcE3MmmPZr4icgXA/132', 'dfe6beaa1c73cb9cf180c83b89fcca83', '5', '15902160078', null, '男', '2', '1', '2c03c559-f485-1664-2f46-17922b46af7a', null, 'oA17C094_IbGhEHAsYvnIyflkIc4', null, 'SHHED0005', 'SHHED', '1560315002', '葛茂忠');
INSERT INTO `user` VALUES ('150', '眼神犀利', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJYyicXxjn5xNFgU3gGCb0BSeg9SfyKDIGWRibGDPPrg2UFZgnkcvmN83U5f81Y6JfX9IxDDfJyShEg/132', '51722fdb6393bc50a20766a58557ca39', '6', '13817107174', null, '男', '2', '1', 'a173831e-95ce-d1b3-d805-3e72e3c97ff7', null, 'oA17C0x6MVIdvAzXpA6jJZuySitI', null, 'SHHED0006', 'SHHED', '1560315422', '许晨');
INSERT INTO `user` VALUES ('151', '心云', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eroU7rOseaKd0SNT0fX3lYnlr6GpD3ibtibAoCRUcX2eV2fo03BVQNfsSeWBicPeuibBoR1MuzwtRRjWw/132', '162c097247a7ce7ebe1f1a7b280adb9d', '19', '13817927655', null, '男', '2', '1', '9192682f-df0a-d5ea-7fa1-bf8862372793', null, 'oA17C0zN9lXPmOxIjEacimjoa_Bc', null, 'SHHED0019', 'SHHED', '1560321861', '刘渊');
INSERT INTO `user` VALUES ('154', 'Vincent', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJwuwg8JZsWopNCic6LLAt3IDdrLPdg2ECZX4xNCUCbFdJ37yI51U8Nplkicvvwam3nS0VV2GVUObYA/132', 'b17b3e7a0cb80721f0b32e18f47c5cb6', '8', '17702187117', null, '男', '2', '1', '5a3ede6f-7c66-1d49-76aa-6307573c36ad', null, 'oA17C0_-WpWLeMB7hghGxDawqf2o', null, 'SHHED0008', 'SHHED', '1560436026', '任二帆');
INSERT INTO `user` VALUES ('155', '东东', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLDbVia3sYSJfpNVUuOtI7t9gxbhHAia1XfScpQBJAaibyv6PnZJwIQVtUxzZehC0azCp3bibMibEx3sOg/132', '04c2d8dbc16f4e32d0d600aef67e6fb1', '1', '15050233125', null, '男', '2', '1', 'bc103747-4664-905e-239b-df341b3cb67e', null, 'oJpgz1VUj5WSyMwPv-V7xIkMGuQg', null, 'cjlh0001', 'cjlh', '1560489193', '吕明东');
INSERT INTO `user` VALUES ('156', '阿本', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIvXDg8cwGSAR1Dkib6pdMI2dRxQY3Wic4TjwnqmfOiag54icZXwcPniarINib6uZ8uCwme2Uf0iaYZonxpQ/132', 'e10adc3949ba59abbe56e057f20f883e', '2', '13801506506', null, '男', '2', '1', 'd8c4e34c-b609-f96b-acff-7ccba0cb78dc', null, 'oJpgz1ZclWIhcUo602LWGFSx9r7I', null, 'CJLH0002', 'CJLH', '1560490136', '袁华斌');
INSERT INTO `user` VALUES ('157', 'Gu_jianq', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eqgZPReoEiavltiamgicQDoegXPP0ox0gzOC5FazZ31yThv71o8yP22QeIUyWqYjr5jyaDraz0tV8F3g/132', '0f60d452b16f50da371ca68ef0f386f5', '3', '17625126688', null, '男', '2', '1', '8b956d38-ea3b-7cee-1fe0-decd6717d2a2', null, 'oJpgz1c6sM7VfvyOA5BddbB2qVHA', null, 'cjlh0003', 'cjlh', '1560490154', '顾健强');
INSERT INTO `user` VALUES ('158', '微笑着生活', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJiao0HeNUmialJzNg3BVqFl2mg0exaib0Zqclq0Ft16PBO07YpPuFSy2ZmB7qOWW93RwEERdMPKypIg/132', 'e807f1fcf82d132f9bb018ca6738a19f', '4', '15851943633', null, '男', '2', '1', '2398e5be-0c58-8f74-87bf-ea02f846cd38', null, 'oJpgz1dbOR3E2wi57dT3IZRQllM8', null, 'CJLH0004', 'CJLH', '1560491002', '许美杰');
INSERT INTO `user` VALUES ('159', '周庆中13962684485', 'http://thirdwx.qlogo.cn/mmopen/vi_32/wrK88GbrwEHSgUz2RG1nXIYKSGQwBnDlEhbhiaAI7J6vDGQEjl8G3eqhk7ibaBSQLWiasAONXcYne8c73micmC0q9g/132', '25f9e794323b453885f5181f1b624d0b', '5', '13962684485', null, '男', '2', '1', '3e68c6b2-8a25-659a-6dd0-15fb54ff432b', null, 'oJpgz1WVE1_1uy4-e0Rbfxa2fIsA', null, 'CJLH0005', 'CJLH', '1560491058', '周庆中');
INSERT INTO `user` VALUES ('160', '问剑', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83epH27gfpQykPULdMicEtrm9osl4CqDFhzgfobbibuH1PWDvJmRoqkKv4OpY3w4lNXmlHsvictQLNk21g/132', '25ecd63631d4ab12b131ac199a00f11b', '6', '13511623951', null, '男', '2', '1', 'fa02b757-4c55-162a-178c-c4fe31185dce', null, 'oJpgz1RKW_bfoBt0y-sY9JQGHOuo', null, 'cjlh0006', 'cjlh', '1560494805', '徐文剑');
INSERT INTO `user` VALUES ('161', '随遇而安', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIvXiaf15aT4fjoO3iaRSna6kFRxLQX62AibIwAKJjwbvibc5sYSgjU4RnnYRwlcdkicyfM1MYbLGSDt9Q/132', '202cb962ac59075b964b07152d234b70', '5', '13641983934', null, '男', '2', '1', '3c9271b6-5004-8ed7-155d-d7a27e532c8a', null, 'ojmae5imshh9KtdKw6NI9ePdM1M0', null, 'BKHDW0005', 'BKHDW', '1560502192', '左工');
INSERT INTO `user` VALUES ('162', 'D.Star芮', 'http://thirdwx.qlogo.cn/mmopen/vi_32/GKmwHTO2J3cL7d0EEtq0Peb3XicNx6EnVJiaB8TDvLyt7jwhXDbCyZF2LYfRpCMngqoVC9dnoPYQvk6JLiaJq4ayA/132', '9a30dcd34d9ce6886d1b2165162607a2', '7', '15906261156', null, '男', '2', '1', '60c6a885-4d13-1e48-4bd6-b714241cbe5e', null, 'oJpgz1SFDyPULINC-Se1r0zgi-K8', null, 'CJLH0007', 'CJLH', '1560644462', '芮星星');
INSERT INTO `user` VALUES ('163', '任光源', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJyY9OiaQiaf61iaVScQXyVgahWOXL2n6dWGCopdvJCpu3JDrBTTSWw4BC9KkMTnG45cArkMKwEYiaCYw/132', '501ef56f7c0302e7ab68101ebc661a1a', '9', '13901654926', null, '男', '2', '1', '850b70a5-6437-8a66-4ceb-2e82f89e0c8f', null, 'oA17C0zcF4v-P0TbmBBGyTqouUXQ', null, 'SHHED0009', 'SHHED', '1560752727', '任光源');
INSERT INTO `user` VALUES ('164', 'Aaron-iftoif', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q3auHgzwzM4PWWaRfuPemaNNNrxprEFHrUY4nOMpx4Wy5RWEOCAOAaEh8HMywxib0NUIaNgM749L9WvRuTmU0ibQ/132', '8df3d756b707a7d0c0d73b8bfbf100ea', '11', '18602112707', null, '男', '2', '1', 'df6f382b-796c-567e-8b66-71282bca04aa', null, 'oA17C04gZ4VybxRAmyZ_2a7QBeTs', null, 'SHHED0011', 'SHHED', '1561454286', '陆渊');
INSERT INTO `user` VALUES ('165', '子和', 'http://thirdwx.qlogo.cn/mmopen/vi_32/5RK3eNhQaDUib0UVYYFbKzAHCFEV8C0Y52jicuTdibxoyAjUpVZR9KTVaL9pT8SVLIpicYODAKTrFGBKl51aiavLIoA/132', 'bae03a60dccf027d686c6b98ecba1723', '10', '15857102320', null, '男', '2', '1', '5df380a6-56c2-d22e-9ee9-7fb1ced00690', null, 'oA17C0-uhqwSaLttzo98ugEhlNDg', null, 'SHHED0010', 'SHHED', '1561510457', '章书恒');
INSERT INTO `user` VALUES ('166', '愚者', 'http://thirdwx.qlogo.cn/mmopen/vi_32/ctUv1iaj0AGZMPUCqW8hxjdU4Xw8lGMLsvVFsMbSK27bxEZnDHqta4Gib3AJ3z7HHDMqsAmsedyicArDtsmPhI6wg/132', '0c75ea2fa6390d134c6442f168c295e8', '8', '13255152218', null, '男', '2', '1', '25a3bc0a-e6b8-a2d6-fc52-e71bb4fb02d3', null, 'oJpgz1esKhBE7veJCFVScTPUL5uc', null, 'CJLH0008', 'CJLH', '1561533558', '王士刚');
INSERT INTO `user` VALUES ('172', '三子', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKWfTv7XfrRqbzlQb4BghORXrYlDAoKicqZIIqTtFE2JaAotYT6LpichSANmjzicYqGiaNxVuAcDFqt1A/132', 'ca16db08df748450cd2f3a452624bce8', '9', '13655230555', null, '男', '2', '1', '78a3c49c-1ca3-be48-c372-d71300a13056', null, 'oJpgz1doqsJ618Yjc9U3YIgmBvsY', null, 'CJLH0009', 'CJLH', '1561716238', '李兴仁');
INSERT INTO `user` VALUES ('173', '天空二号', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIeSQHjODezicuKel9iaknjAMnC8gqicicPPADMibV7kXfQkEJibJOgW9HtHaia2m4jqD4OxZOmRaRbQpl5w/132', '334930f6237bf19668609cf3673fe3f5', '10', '15370773714', null, '男', '2', '1', 'f1132e39-caa3-90d6-27ac-83ca2b7bc5e8', null, 'oJpgz1cSS82hFZZBT3YTAM7tGpZs', null, 'CJLH0010', 'CJLH', '1561716476', '周修军');
INSERT INTO `user` VALUES ('174', '快乐时光', 'http://thirdwx.qlogo.cn/mmopen/vi_32/iatia2cT76F0yic3eptSu9PkHcLxL4Ru7WUu31pRefPtQ5VIUte07tibgk4pEocAkZmX4k7dmI7G07CjOLaITeuTag/132', '8d7fd75a2821e4dd4991b1d4f28dff1b', '11', '18361049053', null, '男', '2', '1', '5ade8c45-de14-11bb-0052-c38c54556881', null, 'oJpgz1ZVevOM5xjekM69f3TY4P7s', null, 'CJLH0011', 'CJLH', '1561716491', '李玉远');
INSERT INTO `user` VALUES ('175', '陈鑫', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83epwA8Tjh9HLQqW7qypuLm26viagbqucFAlmFiaHkmNsy69ClVunKrfB79BLKbc5z85vjUWaoffAl9cg/132', 'e5092e21f3aafa92b44488d79e5cb1b9', '12', '13050982234', null, '男', '2', '1', '8db5fcd1-cd98-09f3-7555-9995a0ed976e', null, 'oJpgz1Y8cRa1xC2NgYF_EDSZSxSo', null, 'CJLH0012', 'CJLH', '1561768674', '陈艳闯');
INSERT INTO `user` VALUES ('176', '灿烂人生', 'http://thirdwx.qlogo.cn/mmopen/vi_32/R5sUP6Rjm4qMy4opC81wiactz7GlOnJGpjcdk9vEXHcj9RC7gM0Bt38arsQblJF2GvSTHYto6StHXngkicMTGS7Q/132', '56e9d9ac7f2e8963a9fb0b9c8826b1df', '13', '13484852556', null, '男', '2', '1', 'f7534aa3-6689-d22a-c9a1-5442f7ba4ff6', null, 'oJpgz1fnSf9EhMhsrEBBR7AU31JU', null, 'CJLH0013', 'CJLH', '1561768726', '代承明');
INSERT INTO `user` VALUES ('177', '努力才是幸福', 'http://thirdwx.qlogo.cn/mmopen/vi_32/WK96XyMe47QdeqX2DGyib8ceNcxTtmAaWhUBUoRS99D8LicETwtKg3wg2PM5jd74yfa4R4fVWic9QgD2rKEE0rgyQ/132', 'abd964a84fbe18d1ba46ee7addc7daf8', '14', '18862259907', null, '男', '2', '1', 'e7636455-6337-063a-9691-86c9d9b9dec4', null, 'oJpgz1dMxH_ipPdZ3X8rt0YOnSyE', null, 'CJLH0014', 'CJLH', '1561798993', '曹刚');
INSERT INTO `user` VALUES ('178', '彩云追月', 'http://thirdwx.qlogo.cn/mmopen/vi_32/IXA4J6HGS5pmIbxV6sxf2pTeNDB6vSGpUHkPalBfJWCq2vo7TRnGJy35qIQPeDGCFgto8ll5cXnZm8toYA4FNQ/132', '6ac6a680a5085122894efce632779a6f', '15', '13862666996', null, '男', '2', '1', 'c1277b58-2a76-2f9c-2ffa-f896c2b83d28', null, 'oJpgz1WzIQTSsgEm2R5jFB-b9i54', null, 'CJLH0015', 'CJLH', '1562198610', '徐长根');
INSERT INTO `user` VALUES ('179', 'antony', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTICjbibo7VVyzmlpvvHlPTmllpDGRMGFoV4KfUh5kCe53CdWzpfvnsGmwRNVCkcm8Eib1mibTv2mokPA/132', '783d10480fd261c3119cabb0c06f810a', '16', '13405198848', null, '男', '2', '1', '815f1d83-9526-547f-339f-36df6d2488ff', null, 'oJpgz1ZKFcHhlU1fAFb-ksw_QP8Q', null, 'CJLH0016', 'CJLH', '1562226419', '姚建平');
INSERT INTO `user` VALUES ('183', '老谭无敌', 'http://thirdwx.qlogo.cn/mmopen/vi_32/r0q6m2RhmHozntXyVKvYygFYzRealyUCpI4OOvp3hxa3SCVF9yicb5fu4oafDvnOOwXNH5tguzVxxUVnuNBoyKw/132', '202cb962ac59075b964b07152d234b70', '2', '12345678901', null, '男', '2', '1', '2d2ad187-8481-2b3a-d2a1-145139625ea5', null, 'ojmae5is83zUa6CkQh5E3VCTWwek', null, 'BKHDW0002', 'BKHDW', '1563878431', '测试1');
INSERT INTO `user` VALUES ('184', '欧阳', 'http://thirdwx.qlogo.cn/mmopen/vi_32/ZXLibyRlJXxFd0wMSicgqAQAwBRLX3BWXNIicr8xTVHmez1p2KFxicFdU9QG0ibiaXoKlvaIGgyehF4XVZicibyauGsgoA/132', '202cb962ac59075b964b07152d234b70', '3', '12345674567', null, '男', '2', '1', '1b5172be-6d8d-6211-f82b-9fd67ebb6037', null, 'ojmae5mM-etksfGVagjU8hPpqam4', null, 'BKHDW0003', 'BKHDW', '1563890259', '测1');
INSERT INTO `user` VALUES ('185', '谭志奎', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83ery55YvGJtHgUsRB0RicSk0qSjGj8gCWgRzcdbeuIDRibYd0famUmATPrqiaTCJ4pVt1QzPMyTGrYosQ/132', '202cb962ac59075b964b07152d234b70', '4', '13345674567', null, '男', '2', '1', '6b6079dd-08e2-b09f-62be-3bd1cc256d61', null, 'ojmae5jA-TxGh8hhxbEACYqwf34o', null, 'BKHDW0004', 'BKHDW', '1563890715', '测2');
INSERT INTO `user` VALUES ('186', 'Cici', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKovibrgjqfh7Z0NqLhcAGz5jMLrXQCxw8QAic1iatTFAicLtkCPlx1dPGbPjoDQJOtbUeiaAt2IyqFkTw/132', 'a555a1826a739507478459b1dc22b73d', '12', '18616592237', null, '男', '2', '1', '339e4144-24f2-c2d4-1666-c4918e66a295', null, 'oA17C0yAnlBcB9swD9FbOxVMYXoI', null, 'SHHED0012', 'SHHED', '1566782578', '丁燕');
INSERT INTO `user` VALUES ('187', '阿唐', 'http://thirdwx.qlogo.cn/mmopen/vi_32/f6SU9YPLpKETicNzOeHK3KzoSjoWlRjTLjhmiaibmyuUKupdmHHDIHW9bUCO9zhSFjgw6zjibuq8bbCmfrAcnUeqDg/132', '670b14728ad9902aecba32e22fa4f6bd', '1', '13951182809', null, '男', '2', '1', 'c9cdf3d8-851d-db91-3d49-d72820138b09', null, 'oA17C0xu0EUbe1qezE9XN_U__2nU', null, 'ZZHW0001', 'ZZHW', '1568688674', '唐永生');
INSERT INTO `user` VALUES ('188', 'Lydia', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTICZfgP1aCJE2Vib0rJ1ow7IdAOEGKedlXcvMJMMOibM2FLUq4VDYK6UeTysp4w8wgOf7F3HIpvx9ZQ/132', '26b61f3fdf6c08f7d4c65f0737953d3c', '13', '13901696959', null, '男', '2', '1', 'df2205f0-18bc-dbb4-d8b9-2921b2ce6a39', null, 'oA17C05khQsCPayfNwHnFnZYBXtc', null, 'SHHED0013', 'SHHED', '1568693432', '罗宁');
INSERT INTO `user` VALUES ('189', 'lily', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83erYUwe91zYWvXPKlEZ2tRdLWCRZzvJEPOaD6TuGRnffh6doib930CEtXGAiamN2frJMibZ1LhtEK1Wkg/132', '6ca7858c1aebe661840e7e53f9fcd2ef', '14', '13601634337', null, '男', '2', '1', '5c848063-f49f-6063-a718-0d9bd3d3ba4e', null, 'oA17C0w_u8tSRB5ya33mJl159D9I', null, 'SHHED0014', 'SHHED', '1568693708', '秦历');
INSERT INTO `user` VALUES ('190', '杨金文', 'http://thirdwx.qlogo.cn/mmopen/vi_32/fNdFohH1MEkffNTJVcqtFAicxcQUZ2InsE1Uqs1cNRzRaMWzA0hYcnO2iaDReReOXobmh0z4ZkznS7ekrgcibic3Tg/132', 'b17b3e7a0cb80721f0b32e18f47c5cb6', '15', '15921043948', null, '男', '2', '1', '1b419946-dbc3-636d-5da6-aafe62ec3d4a', null, 'oA17C0-1pHqbxrBfYpS-eA_VvQhg', null, 'SHHED0015', 'SHHED', '1568693900', '杨金文');
INSERT INTO `user` VALUES ('191', '赵方荣（环卫所）', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIRgiaadlyQvR7I1icNKja6RicwccGEn52HHDCOTw9dgjiaicmoicVqRo9JfSeLsz9vZratwvQNYoyXiar2w/132', 'e10adc3949ba59abbe56e057f20f883e', '1', '13806260859', null, '男', '2', '1', '2035dd30-295e-373a-4b82-272cabeaee8c', null, 'oA17C04iT4fyVhCxlmVdMAIyzfGw', null, 'QDHW0001', 'QDHW', '1568781272', '赵方荣');
INSERT INTO `user` VALUES ('192', '国良', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJnRV11GshCnCuicbnj7piaiabXHjsCUhDIcJggDSDDzKcA3p3Tde3zq9uUdrO9LNEmENqeM8RugdhSg/132', 'f05a6cfe00ad146785e88b86032557b5', '2', '13952458858', null, '男', '2', '2', '40a82536-8bd0-b1e6-e547-cb68f867703a', null, 'oA17C0zDWPZPt3BDLgaC1vKwbVyc', null, 'ZZHW0002', 'ZZHW', '1568881298', '朱国良');
INSERT INTO `user` VALUES ('193', '大奔，', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Re0nmqHc9MP1oDUNiaMkmQlZFKyD72A79Y8HZWVTnBlAH77RiaJEc9BeiaB8Ev36DttIZb3etVJYKAxDOoUiaQ56Rg/132', 'e10adc3949ba59abbe56e057f20f883e', '3', '13584957331', null, '男', '2', '1', 'b7bcc0e3-52fa-ff24-09a1-536ec3484981', null, 'oA17C02IEQ0b_F3DUyWSCkoY5uBs', null, 'ZZHW0003', 'ZZHW', '1568942173', '徐根生');
INSERT INTO `user` VALUES ('194', '吴学根', 'http://thirdwx.qlogo.cn/mmopen/vi_32/KvIK1VGbVMCu2hmZvuS0q8obJTWJjrURZkON1LTuR86mb0QL6aWTUA6ZzibF3icxUTwycllGnxBRpxHjialva0pag/132', 'e10adc3949ba59abbe56e057f20f883e', '4', '13776057948', null, '男', '2', '1', 'ab6b7339-5cec-e026-94f8-7d70ed6b3d47', null, 'oA17C0w_Dr-s7LhLcgdAANad_OBc', null, 'ZZHW0004', 'ZZHW', '1569550903', '吴学根');
INSERT INTO `user` VALUES ('195', '随缘', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLOZMbesUKuqxX6Qfh0e2nuWWia7mJ1wMJWpicxeHe2iarBZWsZ3bZ0nXnlibicAEwfKwFXd3KtSKg1apA/132', '158c0de34273f3d25afe39b92b36a92f', '16', '13818573638', null, '男', '2', '2', '18559e24-7969-cdcf-7383-ec666bca1900', null, 'oA17C09nZ8Xo4DlGxCPTckG8AWUY', null, 'SHHED0016', 'SHHED', '1569554284', '陈彩军');
INSERT INTO `user` VALUES ('196', '影天涯', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJODlibAibvic0jzQI5NrLDt92Iia2DtHR4icfhmhx3cM8ZPFYbBjyCaLJOJUYD4Lia1F3M5Fvsprzcsbjw/132', 'a692a8ab43b74160c619cdbe1625525e', '17', '18801732326', null, '男', '2', '1', '422ab664-7130-484a-72e9-4c4105bb0a36', null, 'oA17C0znM1Nhv9cHnhUzW6CgvVls', null, 'SHHED0017', 'SHHED', '1572852789', '吴真伟');
INSERT INTO `user` VALUES ('197', '郭飞', 'http://thirdwx.qlogo.cn/mmopen/vi_32/BrSGgVJ4cQJueicZpRZPIshTQhFLCzNlkEqFJRFUIpRpOiagkOcvVSLJGiaJVtJ6vcf2IlaP5O8Cx038ZuKUSXfww/132', 'e10adc3949ba59abbe56e057f20f883e', '1', '19884342030', null, '男', '2', '1', 'c89cb2bd-49c3-1c4d-1ca1-8b06993fd183', null, 'oA17C0zcOUvVW5xeBHfcBtvdmsrE', null, 'PHDHJD0001', 'PHDHJD', '1575250571', '郭飞');
INSERT INTO `user` VALUES ('198', '没心没肺，活着不累', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIuqOuvZLWMJyDIovOLFP1umaWdaT9FicnDhNDASIcL3TDBbETibOeicibrQVDzDWclc7sMRR7utSscxg/132', 'f54741c39a4b942825e3c68fb8aacded', '2', '13511286863', null, '男', '2', '1', 'dfd150b4-f00f-77ff-dea5-b3060c29aec8', null, 'oA17C029ZU2rxFzBOmUOfnuNiah4', null, 'PHDHJD0002', 'PHDHJD', '1575250585', '胡美玲');
INSERT INTO `user` VALUES ('199', '樱桃', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJUr75QrJOjCdfPvZYcd4KA1icTmquINJKO7KZIorSdr5MYsicnB4bLtWEnibRibpqqWI0FzNOWGO1FOw/132', '7d4baee42b6f8a2609c1dd243b25c68d', '3', '13567315233', null, '男', '2', '1', 'd9f17c09-be6b-33e9-f795-3738459be394', null, 'oA17C00McOY8uoWAvC-1eGYGPRUA', null, 'PHDHJD0003', 'PHDHJD', '1575250603', '殷爱芳(滨湖新村广场)');
INSERT INTO `user` VALUES ('200', '陈敏洁', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83epzAGNjbntQegFcbTTKBKxcuKxia986olW4ialbeFyCBULicod2RHy0Lte3ljcYLhnhkvjJPZEo4ibm0g/132', 'e10adc3949ba59abbe56e057f20f883e', '4', '18618273331', null, '男', '2', '1', '9da98f23-619c-e621-fe9b-165756bf1b76', null, 'oA17C03Y-mwBu8IT81LJb2OsMpLQ', null, 'PHDHJD0004', 'PHDHJD', '1575250606', '陈敏洁');
INSERT INTO `user` VALUES ('201', '(谭彪)', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83erfDE0N5p3kR0iacG9gLj95nZmiarfVROFsDxLJxT6XUs9hXfUmahVZcww4378u5RuMh6DJua8NGZrA/132', '04a4aa65f0017ee85dfd2dfc7fe45b7e', '5', '13806721213', null, '男', '2', '1', 'e9394dc8-5aec-a5c9-f5a4-9c07b3a1abb5', null, 'oA17C0wE4rGBrw7bLFnKVddqXuP4', null, 'PHDHJD0005', 'PHDHJD', '1575250665', '谭彪（东升南区）');
INSERT INTO `user` VALUES ('202', '沈金龙', 'http://thirdwx.qlogo.cn/mmopen/vi_32/5nagwiaoh5AqYS3FRb5197ptAQVrfia3oD5Rslu31IZEA3of5MX2ib3aoPX5EnRKGOOd4tFHGSxsrF5Ty9J2txQibQ/132', 'c13ed6ac872669a72792efa1571994dc', '6', '13567314895', null, '男', '2', '1', '1897e192-fa87-7215-8874-c3024a908444', null, 'oA17C0xJIWtL7Bc9Xk4D7buypyLw', null, 'PHDHJD0006', 'PHDHJD', '1575250807', '沈金龙 兴阳花苑');
INSERT INTO `user` VALUES ('203', '似冷非冰^', 'http://thirdwx.qlogo.cn/mmopen/vi_32/WFcMMkjhJEqib73mjdxppjI5aMyeJQD4YCWhULIArOfrtQXolQapGt5ff20DXEiaPib4kpuwIQZw90DKRyRvVLoLQ/132', 'e10adc3949ba59abbe56e057f20f883e', '7', '15167311005', null, '男', '2', '1', '8382fe70-4f10-e94b-454f-f405b248f7da', null, 'oA17C08e1imjUCI3kuY3RM2NHuT8', null, 'PHDHJD0007', 'PHDHJD', '1575250819', '倪勇锋（司机）');
INSERT INTO `user` VALUES ('204', '马超', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83epM3T6J4Qj0smUdHianUjicfhfob1diclJW8w2RvlT5H9fwhmV5yNiaDdcfic2uP9wgvCVu90SvmNumW6Q/132', '153b361bd4f4b99ec6da30b23532833d', '8', '13967311456', null, '男', '2', '1', 'e4804318-61fb-036b-87e3-a67ec6c3e1ac', null, 'oA17C02YfcMdehySbhFsdDTxAkD8', null, 'PHDHJD0008', 'PHDHJD', '1575250858', '马超');
INSERT INTO `user` VALUES ('205', '迎客松', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoicSDlgbUlsPzblEal6rBczDPEuicKiaayZceGHPbcOxaicyjtevo1zrqqNSjVk9Rt056kyRhl3M9HwA/132', 'e10adc3949ba59abbe56e057f20f883e', '9', '15167327112', null, '男', '2', '1', '0bf77011-9161-ee3e-3e3c-adbe424f8cb6', null, 'oA17C06oS6qyxTVke91SPJ7aLoks', null, 'PHDHJD0009', 'PHDHJD', '1575251428', '张根其 大桥小区');
INSERT INTO `user` VALUES ('206', 'WHYO_o', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoOLTicyFroI2MnEqEzYaeeAavUq9Je1LUmWOzaLexORgAMCXUquUyufUCRTCtic1qice4Aiceqm06LhQ/132', 'e10adc3949ba59abbe56e057f20f883e', '10', '18913524087', null, '男', '1', '1', '3a48812e-e350-9214-5d04-1b000af5981d', null, 'oA17C04O9gfSIlU_XooPd4SQOHdA', null, 'PHDHJD0010', 'PHDHJD', '1575251521', '吴红燕');
INSERT INTO `user` VALUES ('207', 'aummyaummy', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLANMmtu2m6yGLLwAwsicGtTETm3jC7RibY9MSibMzsBQfg53V0mnvViaOgaWr0D4sht58D6nJ8c7lSTQ/132', '01b46b85f41f71e000aa580c98d08b45', '18', '13901787339', null, '男', '2', '1', 'd0465e65-14ff-cdd6-d843-e02750638622', null, 'oA17C0y1xo4DE3MJ-2LoUe2Xi_rg', null, 'SHHED0018', 'SHHED', '1575252357', '孙飞');
INSERT INTO `user` VALUES ('208', 'Ted', '', '9e406957d45fcb6c6f38c2ada7bace91', '6', '12333334444', null, '男', '2', '1', 'c375e21e-d2c0-ba85-8043-fa08ee893330', null, 'oA17C0_C8vJVyHcISK7fbC7Uzm34', null, 'BKHDW0006', 'BKHDW', '1576064397', 'tt');
INSERT INTO `user` VALUES ('209', '田大壮', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJrptJd4DQulKVIQH1saagQQn2THtV0z1ksDT9oBkucYEOep8vTXan9URNZnI25BceLLPZPnecd7Q/132', '59b658b86f405cf44906838b1beb6fbc', '20', '13916999659', null, '男', '2', '1', '4b29d099-a0f5-8fb0-36fc-e313bed8a074', null, 'oA17C08t9Ox3_59sUkp16CWVWlbY', null, 'SHHED0020', 'SHHED', '1576067420', '田雪');
INSERT INTO `user` VALUES ('210', 'Drav', 'http://thirdwx.qlogo.cn/mmopen/vi_32/5X1AnImVo7rroHyrejHapDonqdCm4QQicdwtGp0Cib11RcD1Sz7wtiaiaxr1pN4PLSnJqn92j8WW0kyQEW4oRNQOXg/132', '202cb962ac59075b964b07152d234b70', '1', '13642312678', null, '男', '2', '1', 'ec51e6fe-c5df-2829-0646-73ba538498ea', null, 'oA17C05hWPvPH2UJ4mM9D-mtWrtw', null, 'STRGCS0001', 'STRGCS', '1576245724', 'zeng');
INSERT INTO `user` VALUES ('211', '白雪峰', 'http://thirdwx.qlogo.cn/mmopen/vi_32/gUiaHhHdWMGoFaYiaMzibmfwRd4b84YvInWTfCPnWJQlNYhBS1RKVJNvK9L7YIFJKMyvgs2OZdsMrfjvxreYUkYAg/132', '298c45101713b14f43e5f29479f70edc', '21', '17521674203', null, '男', '2', '1', '3d2cb7ef-d4a5-1427-63b1-f75b3b95d690', null, 'oA17C07tm4d4ADN265GtztxrSpdc', null, 'SHHED0021', 'SHHED', '1576311462', '白雪峰');

-- ----------------------------
-- Table structure for `user_organize`
-- ----------------------------
DROP TABLE IF EXISTS `user_organize`;
CREATE TABLE `user_organize` (
  `user_organize_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户组织架构id',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `organize_id` int(11) DEFAULT NULL COMMENT '组织架构id',
  `user_organize_guid` varchar(255) NOT NULL,
  PRIMARY KEY (`user_organize_id`,`user_organize_guid`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=64 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of user_organize
-- ----------------------------
INSERT INTO `user_organize` VALUES ('31', '197', '1', 'fdd42343-0ead-6c34-5f7f-fcfe03535c99');
INSERT INTO `user_organize` VALUES ('2', '2', '1', 'cd3a217d-6501-dfd5-b278-596b13c77bda');
INSERT INTO `user_organize` VALUES ('3', '7', '2', '20dad8c8-54ef-4d9d-a6ab-e72bd57405f4');
INSERT INTO `user_organize` VALUES ('4', '8', '3', '41fd3d75-0cf9-d286-737e-2e126c25ec2b');
INSERT INTO `user_organize` VALUES ('5', '10', '2', '32b54605-6dcd-c91c-9604-f7ead78bd9fc');
INSERT INTO `user_organize` VALUES ('6', '11', '2', 'c91e3e15-962e-9df7-0e53-2c00e7c67400');
INSERT INTO `user_organize` VALUES ('7', '12', '2', '0cd76dcb-a780-272a-deda-5e33c8020862');
INSERT INTO `user_organize` VALUES ('8', '14', '2', '927e1f32-c88a-db13-9937-6c7f410bfb06');
INSERT INTO `user_organize` VALUES ('9', '15', '2', '39b3fb76-0b2a-e022-e4a7-1525b250fb51');
INSERT INTO `user_organize` VALUES ('10', '13', '2', '5020d207-9df9-960c-a0db-1dc754a0503e');
INSERT INTO `user_organize` VALUES ('11', '16', '2', 'a35ae2e6-9296-04b3-4fd2-ea5688ec64e6');
INSERT INTO `user_organize` VALUES ('12', '143', '5', 'f076c969-b6c2-97ab-b3f7-28292a411fe8');
INSERT INTO `user_organize` VALUES ('13', '146', '5', '2b315ac4-00dd-91ce-0e3d-3aa0101a5722');
INSERT INTO `user_organize` VALUES ('14', '147', '5', 'd22f1fe6-a08f-1bc1-30c6-17538c6319d8');
INSERT INTO `user_organize` VALUES ('15', '149', '5', 'f7998cfb-767f-356a-bc02-fc7a8cccdfc7');
INSERT INTO `user_organize` VALUES ('16', '150', '5', 'c848b64d-cce1-83ad-4a4b-53e31396b85d');
INSERT INTO `user_organize` VALUES ('17', '151', '5', '9c396d68-1ffe-ec96-f4f0-3d634ef1e712');
INSERT INTO `user_organize` VALUES ('18', '154', '5', 'e93cea2a-f5e7-c848-b872-8523d2b48d45');
INSERT INTO `user_organize` VALUES ('19', '209', '5', '3c2b5327-6164-b0d5-43b5-1f94bccadef4');
INSERT INTO `user_organize` VALUES ('20', '211', '5', '5fa58244-ce5a-61ab-9a90-31147b2225f3');
INSERT INTO `user_organize` VALUES ('21', '186', '5', '312dfa84-ddec-d08a-ba59-e419b1eaab9a');
INSERT INTO `user_organize` VALUES ('22', '188', '5', '0be6e48d-ff1e-4e4c-a7a3-a782bc4a8b9c');
INSERT INTO `user_organize` VALUES ('23', '189', '5', '75eba88d-b531-9b04-6211-5aee93713ed9');
INSERT INTO `user_organize` VALUES ('24', '190', '5', '26e1417c-5e92-1ca2-f892-b7d934f027d2');
INSERT INTO `user_organize` VALUES ('25', '196', '5', '7ba9b0fa-9920-31ee-96bc-f52ca76ece89');
INSERT INTO `user_organize` VALUES ('26', '163', '5', '06172a24-9518-24b9-4b82-c3ef86611ffc');
INSERT INTO `user_organize` VALUES ('27', '164', '5', 'e1349a7a-120d-8696-8575-02cd0a855c78');
INSERT INTO `user_organize` VALUES ('28', '165', '5', '558dec44-f647-984d-0467-34893878eef4');
INSERT INTO `user_organize` VALUES ('29', '210', '10', '164e5726-a923-a150-7dad-12ce28c34846');
INSERT INTO `user_organize` VALUES ('30', '208', '9', 'f7c19656-adf3-e045-4a88-92d48db2c91d');
INSERT INTO `user_organize` VALUES ('32', '198', '1', '0bc1db43-e5bd-a77b-a25e-419e4482d62e');
INSERT INTO `user_organize` VALUES ('33', '199', '1', '1bb884ca-cdc5-a60b-3530-346456021625');
INSERT INTO `user_organize` VALUES ('34', '200', '1', '2b5194a3-9613-bebc-f6ee-041145d8d6fc');
INSERT INTO `user_organize` VALUES ('35', '201', '1', '4e43366b-cd72-71f1-397a-da43894957ba');
INSERT INTO `user_organize` VALUES ('36', '202', '1', '0b531310-7dff-9411-dc0d-1dd1522a2c83');
INSERT INTO `user_organize` VALUES ('37', '203', '1', '03a9291e-2349-bab6-d04c-0da0dc4e4933');
INSERT INTO `user_organize` VALUES ('38', '204', '1', '40899228-e37a-4df9-cc14-b1efcd63ec59');
INSERT INTO `user_organize` VALUES ('39', '205', '1', '2c64e2d8-0ec1-5a20-59d8-d9d0e1d8c7b1');
INSERT INTO `user_organize` VALUES ('40', '193', '12', '5579ae2d-60c3-7cf0-4d66-bc02069d216a');
INSERT INTO `user_organize` VALUES ('41', '194', '12', 'e46616fb-95c4-1e62-1487-c369783f6d59');
INSERT INTO `user_organize` VALUES ('42', '187', '12', 'ce0c12ea-447c-34b2-4706-d2e7ba2a01ef');
INSERT INTO `user_organize` VALUES ('43', '191', '13', '8ddca483-f00c-3c50-233e-dfc53635f118');
INSERT INTO `user_organize` VALUES ('44', '166', '11', '708c0ad9-9e0c-e0f4-8d87-17fd65c20451');
INSERT INTO `user_organize` VALUES ('45', '172', '11', 'cd12ef9e-a69b-7275-20b9-8d6426a0bcc6');
INSERT INTO `user_organize` VALUES ('46', '173', '11', '7b4908f8-a36c-efa6-b7d5-ec957690d8d3');
INSERT INTO `user_organize` VALUES ('47', '174', '11', '32ab2290-d873-b5e4-20a0-d4872c82818d');
INSERT INTO `user_organize` VALUES ('48', '175', '11', '02d8449e-2d9e-7e2b-73a7-31f57cea0f1b');
INSERT INTO `user_organize` VALUES ('49', '176', '11', '82f8683b-5761-7bb0-196b-ae013cf0ff26');
INSERT INTO `user_organize` VALUES ('50', '177', '11', '807dfeae-b1a2-ee9f-3ba5-4e329e45fafa');
INSERT INTO `user_organize` VALUES ('51', '178', '11', '2326464d-e6d0-5c45-a9e5-50ca1b799d8c');
INSERT INTO `user_organize` VALUES ('52', '179', '11', '3dd0571f-b626-f10c-773f-6fd04e78163b');
INSERT INTO `user_organize` VALUES ('53', '155', '11', '36ea1123-d43c-0b25-3431-6743ae6f1370');
INSERT INTO `user_organize` VALUES ('54', '156', '11', '2fef0688-6129-a8f8-ec34-38643a74da79');
INSERT INTO `user_organize` VALUES ('55', '157', '11', '2afa5e05-0a3b-2986-fce5-c28757983caf');
INSERT INTO `user_organize` VALUES ('56', '158', '11', 'deff1dca-0669-9a1e-98c2-ee1c4d42f7a7');
INSERT INTO `user_organize` VALUES ('57', '159', '11', '21af4fc4-3660-1109-a755-0459222aa22e');
INSERT INTO `user_organize` VALUES ('58', '160', '11', '343d22e2-aea5-7537-31cf-c683223cb151');
INSERT INTO `user_organize` VALUES ('59', '162', '11', 'b13713f7-3d0e-a542-862e-2976b26e6794');
INSERT INTO `user_organize` VALUES ('60', '161', '9', '85ea936c-c7b0-87ee-d8d8-b0e7f789492b');
INSERT INTO `user_organize` VALUES ('61', '183', '9', '4ebabace-de61-331f-3dc9-46ac6b4f5b8d');
INSERT INTO `user_organize` VALUES ('62', '184', '9', 'd944f6f7-7e3b-20e3-dca4-d42ab4f9c835');
INSERT INTO `user_organize` VALUES ('63', '185', '9', '29802bfb-b3e6-979d-15ef-239813e886ef');

-- ----------------------------
-- Table structure for `user_role`
-- ----------------------------
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role` (
  `user_role_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户角色id',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `role_id` int(11) DEFAULT NULL COMMENT '角色id',
  `user_role_guid` varchar(255) NOT NULL,
  PRIMARY KEY (`user_role_id`,`user_role_guid`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=73 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of user_role
-- ----------------------------
INSERT INTO `user_role` VALUES ('1', '1', '1', '1');
INSERT INTO `user_role` VALUES ('2', '0', '3', '90e1ef35-d6f4-bf53-ddde-2a4639ddf215');
INSERT INTO `user_role` VALUES ('4', '1', '3', 'c557f45b-91eb-bbe3-b85f-9dc29e97411b');
INSERT INTO `user_role` VALUES ('5', '7', '6', 'fe0f2cad-6ec7-dff4-125d-31e1e3c6f12f');
INSERT INTO `user_role` VALUES ('6', '8', '6', 'a2bbf5c5-1041-150a-e6f5-8a92f4f5697f');
INSERT INTO `user_role` VALUES ('7', '10', '7', '13134e50-85e1-1302-12a9-ae5626fcc100');
INSERT INTO `user_role` VALUES ('16', '11', '7', 'fcaf3434-5770-4f47-89de-8abe732a24a2');
INSERT INTO `user_role` VALUES ('10', '12', '6', '7f835e63-e7a0-fa40-796d-9904d3748f41');
INSERT INTO `user_role` VALUES ('12', '14', '6', 'ecb65606-5f66-5dc5-b777-c593bd13c1cf');
INSERT INTO `user_role` VALUES ('13', '15', '6', '7712e9c0-5df4-a8e8-a4a2-808a09724cca');
INSERT INTO `user_role` VALUES ('15', '13', '6', '102e9f56-1757-c57d-25a2-38724c8ea0f1');
INSERT INTO `user_role` VALUES ('17', '16', '6', '9117f2fb-2898-7b5c-c3ae-74de7a5fd3aa');
INSERT INTO `user_role` VALUES ('18', '131', '1', '4cfd8081-4b5f-4270-293b-1239690ba027');
INSERT INTO `user_role` VALUES ('19', '134', '1', 'e42168ae-bc79-470a-f56b-a7df1db356e9');
INSERT INTO `user_role` VALUES ('20', '196', '6', '6f9cb9d2-a66d-dd84-7df1-4cf220935b47');
INSERT INTO `user_role` VALUES ('21', '207', '6', 'a0ba16d5-66b9-8018-de9a-7761e4d2a802');
INSERT INTO `user_role` VALUES ('22', '209', '6', '863bb424-3005-e08f-d5b2-5451f9b00226');
INSERT INTO `user_role` VALUES ('23', '211', '6', '34d33d32-8fae-622f-90df-d46ba19166a0');
INSERT INTO `user_role` VALUES ('24', '186', '6', 'aa9234a2-aca7-f4f9-2d3b-e1e9937199e5');
INSERT INTO `user_role` VALUES ('25', '188', '6', '5f168413-ead9-ba53-2ce3-450626884bf1');
INSERT INTO `user_role` VALUES ('26', '189', '6', 'e2b222ac-12e0-6863-0c83-f499ffca46fe');
INSERT INTO `user_role` VALUES ('27', '146', '6', '16e6570d-8c89-7cdd-292e-ba71307117c8');
INSERT INTO `user_role` VALUES ('28', '149', '6', 'ee6a7973-5cc8-4963-87d6-7f3f4db9008e');
INSERT INTO `user_role` VALUES ('29', '150', '6', '233d017b-8444-8a1e-674b-2d091b03de51');
INSERT INTO `user_role` VALUES ('30', '151', '6', 'c0e6c45b-67ca-da28-3b93-7a2f455e5429');
INSERT INTO `user_role` VALUES ('31', '163', '6', 'd7f99a13-c0a9-85ee-0231-208789aa005a');
INSERT INTO `user_role` VALUES ('32', '164', '6', 'fa3e0905-843a-fe5b-6c60-430341db453d');
INSERT INTO `user_role` VALUES ('33', '165', '6', '9138b840-fff0-c1e0-71b5-bc2ec9ff6043');
INSERT INTO `user_role` VALUES ('34', '143', '7', '53313271-230b-071d-ca2a-a966595009cf');
INSERT INTO `user_role` VALUES ('35', '147', '7', '008a9299-3dd5-f046-81e4-db311143b3c3');
INSERT INTO `user_role` VALUES ('36', '154', '7', '26849b5b-1406-4ad2-82cb-9d033ec05960');
INSERT INTO `user_role` VALUES ('37', '197', '8', '7bbf1901-1026-625c-c474-72aefd0348d2');
INSERT INTO `user_role` VALUES ('38', '198', '8', 'd4ec1772-f8fb-4019-d1c7-d1405c0b6669');
INSERT INTO `user_role` VALUES ('39', '199', '8', 'cd619a27-246d-6443-0376-974aa8dfb3d9');
INSERT INTO `user_role` VALUES ('40', '200', '8', 'deb63b67-85c0-9f69-0e1a-a78af90fda42');
INSERT INTO `user_role` VALUES ('41', '201', '8', '6648e6fd-9334-ccd6-c1c1-1cb0db055a9a');
INSERT INTO `user_role` VALUES ('42', '202', '8', '701b021f-eb47-3ed6-76a9-fdcd3ea93e9c');
INSERT INTO `user_role` VALUES ('43', '205', '8', '3283a89c-07da-a5d0-b026-e7b841385e19');
INSERT INTO `user_role` VALUES ('44', '155', '8', '856eb4a7-8dc7-bdac-bf80-edec01fc392f');
INSERT INTO `user_role` VALUES ('45', '156', '8', '9eeb4810-03aa-9dfb-9fca-73f9de632df9');
INSERT INTO `user_role` VALUES ('46', '157', '8', 'de44f984-b975-5a96-d4cd-8742d88eff72');
INSERT INTO `user_role` VALUES ('47', '158', '8', 'e26bb077-c1c8-b594-25d0-12d51c35ddab');
INSERT INTO `user_role` VALUES ('48', '159', '8', '7b7b3022-99c8-dd8c-ad00-0f11503f563e');
INSERT INTO `user_role` VALUES ('49', '160', '9', '637476c7-90ba-dd3b-f6bf-b4e7fe4d0567');
INSERT INTO `user_role` VALUES ('50', '161', '9', '84a8cfed-956f-526e-203a-f0efac3dce4f');
INSERT INTO `user_role` VALUES ('51', '162', '9', '7a3decd5-bf73-bc23-fb5c-ac3a64497a5b');
INSERT INTO `user_role` VALUES ('52', '185', '9', '2b34f2cb-1b5b-08a7-19bd-1bc5ddf9631a');
INSERT INTO `user_role` VALUES ('53', '187', '9', '88b3af66-1d2b-60df-5e4d-bf5f8452f4d7');
INSERT INTO `user_role` VALUES ('54', '191', '9', 'dfa6a609-d531-01e7-d9a0-70a0576792a2');
INSERT INTO `user_role` VALUES ('55', '204', '9', '14b85937-ab2a-b9db-b2d8-8789c2debd77');
INSERT INTO `user_role` VALUES ('56', '166', '10', 'b2c638f7-d589-0611-6596-816e6043653c');
INSERT INTO `user_role` VALUES ('57', '172', '10', 'bad21972-2e88-8737-48a1-00edb65d4414');
INSERT INTO `user_role` VALUES ('58', '173', '10', '9b3054be-9016-8f76-da5d-8bea7df5dd88');
INSERT INTO `user_role` VALUES ('59', '174', '10', 'd8b4ae29-c085-1fac-55d3-2825e7ca5898');
INSERT INTO `user_role` VALUES ('60', '175', '10', '3b54a141-8873-de2b-378d-3794150ef84b');
INSERT INTO `user_role` VALUES ('61', '176', '10', '34f232a9-88aa-aa98-3958-04d971527c61');
INSERT INTO `user_role` VALUES ('62', '177', '10', 'aa64d896-2f47-ac70-968f-673bd9eb129f');
INSERT INTO `user_role` VALUES ('63', '178', '10', '3c896da6-1187-912d-1fc3-e15adcb14705');
INSERT INTO `user_role` VALUES ('64', '179', '10', 'd74d5b6f-0f9c-b02c-1a29-dedf45a65d9b');
INSERT INTO `user_role` VALUES ('65', '183', '10', '97dfcc60-7a04-a6da-8705-d92cf846e82c');
INSERT INTO `user_role` VALUES ('66', '184', '10', '3490b2d7-7489-54b9-05c4-207c71beec25');
INSERT INTO `user_role` VALUES ('67', '190', '10', 'ab9b414c-c959-6607-5430-775e329cdc26');
INSERT INTO `user_role` VALUES ('68', '193', '10', '5bdae52c-dd3f-c021-d58f-56c20ba3b384');
INSERT INTO `user_role` VALUES ('69', '194', '10', '1addef1f-5924-6447-34ef-b1895dfaf903');
INSERT INTO `user_role` VALUES ('70', '203', '10', 'ab4cc0a0-2e66-0658-2b66-43a4c7a8cbf2');
INSERT INTO `user_role` VALUES ('71', '208', '10', '9d76ea0b-450f-e85f-8d36-63f1a0c0db4e');
INSERT INTO `user_role` VALUES ('72', '210', '10', '19b5a6a3-8425-9223-7da2-b2cdc5d6504d');

-- ----------------------------
-- Table structure for `wx_token`
-- ----------------------------
DROP TABLE IF EXISTS `wx_token`;
CREATE TABLE `wx_token` (
  `wx_token_id` int(11) NOT NULL AUTO_INCREMENT,
  `wx_token_content` varchar(200) NOT NULL,
  `wx_token_time` int(200) NOT NULL,
  `wx_token_guid` varchar(200) NOT NULL,
  `wx_token_type` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`wx_token_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of wx_token
-- ----------------------------
INSERT INTO `wx_token` VALUES ('1', '{\"access_token\":\"29_gFefoxFY9XkgomTZuTjgmpdugTn194oW05EksYYbd6Bmik4VzjzEVh4rQXgOpMa72DRk9rCv4Hak6WBZarsxS7z5U4wkzIxPJrWRBZ9FRzQ7JMFz9vU0J-4xwbObqU4jYW3V3h8brr4DKS6zBCMjAFAMNX\",\"expires_time\":157914171', '1579138072', 'cdd5e3bf-8b24-99d2-05b4-f024695513b4', 'token');
INSERT INTO `wx_token` VALUES ('2', '{\"ticket\":\"O3SMpm8bG7kJnF36aXbe82we3yEfr5TeQJhnNe4yEJovdsGChMehFPC7vl3JyB5zb1WsX7EGG3RLYpMKnUuIZQ\",\"expires_time\":1579141714}', '1579138114', '93090be1-76b1-a52a-00cd-1fc0dabccd47', 'ticket');

-- ----------------------------
-- View structure for `v_enterprise_position_user`
-- ----------------------------
DROP VIEW IF EXISTS `v_enterprise_position_user`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_enterprise_position_user` AS select `user`.`user_id` AS `user_id`,`user`.`user_name` AS `user_name`,`user`.`user_headimage` AS `user_headimage`,`user`.`user_password` AS `user_password`,`user`.`user_number` AS `user_number`,`user`.`user_phone` AS `user_phone`,`user`.`user_identity_card` AS `user_identity_card`,`user`.`user_sex` AS `user_sex`,`user`.`user_status` AS `user_status`,`user`.`user_delete` AS `user_delete`,`user`.`user_guid` AS `user_guid`,`user`.`user_unionid` AS `user_unionid`,`user`.`user_openid_gzh` AS `user_openid_gzh`,`user`.`user_admin` AS `user_admin`,`enterprise_position_user`.`enterprise_position_user_id` AS `enterprise_position_user_id`,`enterprise_position_user`.`enterprise_position_id` AS `enterprise_position_id`,`enterprise_position_user`.`enterprise_position_user_guid` AS `enterprise_position_user_guid`,`enterprise_position`.`enterprise_position_name` AS `enterprise_position_name`,`enterprise_position`.`enterprise_position_introduction` AS `enterprise_position_introduction`,`enterprise_position`.`enterprise_position_treatment` AS `enterprise_position_treatment`,`enterprise_position`.`enterprise_position_number` AS `enterprise_position_number`,`enterprise_position`.`enterprise_position_salary_min` AS `enterprise_position_salary_min`,`enterprise_position`.`enterprise_position_salary_max` AS `enterprise_position_salary_max`,`enterprise_position`.`enterprise_position_status` AS `enterprise_position_status`,`enterprise_position`.`enterprise_position_delete` AS `enterprise_position_delete`,`enterprise_position`.`enterprise_position_guid` AS `enterprise_position_guid`,`enterprise_position`.`enterprise_id` AS `enterprise_id`,`enterprise`.`enterprise_logo` AS `enterprise_logo`,`enterprise`.`enterprise_name` AS `enterprise_name`,`enterprise`.`enterprise_address` AS `enterprise_address`,`enterprise`.`enterprise_introduction` AS `enterprise_introduction`,`enterprise`.`enterprise_nature` AS `enterprise_nature`,`enterprise`.`enterprise_phone` AS `enterprise_phone`,`enterprise`.`enterprise_user_name` AS `enterprise_user_name`,`enterprise`.`enterprise_points` AS `enterprise_points`,`enterprise`.`enterprise_radius` AS `enterprise_radius`,`enterprise`.`enterprise_status` AS `enterprise_status`,`enterprise`.`enterprise_remark` AS `enterprise_remark`,`enterprise`.`enterprise_delete` AS `enterprise_delete`,`enterprise`.`enterprise_guid` AS `enterprise_guid` from (((`user` join `enterprise_position_user` on((`enterprise_position_user`.`user_id` = `user`.`user_id`))) join `enterprise_position` on((`enterprise_position`.`enterprise_position_id` = `enterprise_position_user`.`enterprise_position_id`))) join `enterprise` on((`enterprise`.`enterprise_id` = `enterprise_position`.`enterprise_id`))) ;

-- ----------------------------
-- View structure for `v_user_organize`
-- ----------------------------
DROP VIEW IF EXISTS `v_user_organize`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_user_organize` AS select `user_organize`.`user_organize_guid` AS `user_organize_guid`,`user`.`user_id` AS `user_id`,`user`.`user_name` AS `user_name`,`user`.`user_headimage` AS `user_headimage`,`user`.`user_password` AS `user_password`,`user`.`user_number` AS `user_number`,`user`.`user_phone` AS `user_phone`,`user`.`user_identity_card` AS `user_identity_card`,`user`.`user_sex` AS `user_sex`,`user`.`user_status` AS `user_status`,`user`.`user_delete` AS `user_delete`,`user`.`user_guid` AS `user_guid`,`user`.`user_unionid` AS `user_unionid`,`user`.`user_openid_gzh` AS `user_openid_gzh`,`user`.`user_admin` AS `user_admin`,`user_organize`.`user_organize_id` AS `user_organize_id`,`organize`.`organize_id` AS `organize_id`,`organize`.`organize_parent_id` AS `organize_parent_id`,`organize`.`organize_name` AS `organize_name`,`organize`.`organize_status` AS `organize_status`,`organize`.`organize_delete` AS `organize_delete`,`organize`.`organize_guid` AS `organize_guid`,`organize`.`organize_internship_start` AS `organize_internship_start`,`organize`.`organize_internship_end` AS `organize_internship_end` from ((`user_organize` join `user` on((`user`.`user_id` = `user_organize`.`user_id`))) join `organize` on((`organize`.`organize_id` = `user_organize`.`organize_id`))) ;

-- ----------------------------
-- View structure for `v_user_role`
-- ----------------------------
DROP VIEW IF EXISTS `v_user_role`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_user_role` AS select `user_role`.`user_role_id` AS `user_role_id`,`user_role`.`user_role_guid` AS `user_role_guid`,`role`.`role_id` AS `role_id`,`role`.`role_name` AS `role_name`,`role`.`role_remark` AS `role_remark`,`role`.`role_guid` AS `role_guid`,`role`.`role_status` AS `role_status`,`role`.`role_delete` AS `role_delete`,`user`.`user_id` AS `user_id`,`user`.`user_name` AS `user_name`,`user`.`user_headimage` AS `user_headimage`,`user`.`user_password` AS `user_password`,`user`.`user_number` AS `user_number`,`user`.`user_phone` AS `user_phone`,`user`.`user_identity_card` AS `user_identity_card`,`user`.`user_sex` AS `user_sex`,`user`.`user_status` AS `user_status`,`user`.`user_delete` AS `user_delete`,`user`.`user_guid` AS `user_guid`,`user`.`user_unionid` AS `user_unionid`,`user`.`user_openid_gzh` AS `user_openid_gzh`,`user`.`user_admin` AS `user_admin` from ((`user_role` join `user` on((`user`.`user_id` = `user_role`.`user_id`))) join `role` on((`role`.`role_id` = `user_role`.`role_id`))) ;
