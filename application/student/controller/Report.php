<?php

namespace app\student\controller;

use Adjfut\Common;
use Adjfut\Tool\Tool;
use think\Db;
use app\student\validate\Report as Validate;
class Report extends Common
{
    public function user_organize_tree()
    {
        $re = [];
        try {
            $con = [];
            $con["organize_status"] = 1;
            $con["organize_delete"] = 1;
            $con["organize_id"] = [
                "in", $this->UserInfo["user_organize"],
            ];
            $field = [];
            $field = [];
            $select_organize = Db::name("organize")->where($con)->field($field)->select();
            if ($select_organize) {
                $children = function (array $data, int $parent_id, $children) {
                    $re = [];
                    foreach ($data as $parent_data_key => $parent_data_value) {
                        if ($parent_data_value["organize_parent_id"] == $parent_id) {
                            $temporary = [
                                "title" => $parent_data_value["organize_name"],
                                "id" => $parent_data_value["organize_id"],
                                "data" => $parent_data_value,
                                "children" => [],
                                "spread" => true,
                            ];
                            foreach ($data as $children_data_key => $children_data_value) {
                                if ($children_data_value["organize_parent_id"] == $parent_data_value["organize_id"]) {
                                    $temporary["children"] = $children($data, $parent_data_value["organize_id"], $children);
                                }
                            }
                            $re[] = $temporary;
                        }
                    }
                    return $re;
                };
                $re["code"] = 0;
                $re["data"] = [[
                    "title" => "用户组织",
                    "id" => time(),
                    "spread" => true,
                    "data" => [
                        "organize_id" => 0,
                    ],
                    "children" => $children($select_organize, $this->UserInfo["organize_id"], $children),
                ]];
                $re["msg"] = "ok";
            } else {
                $re["code"] = 40000;
                $re["msg"] = "暂无数据";
            }
        } catch (\Throwable $th) {
            $re["code"] = 40004;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function user_organize_list()
    {
        $re = [];
        try {
            $page = input("page");
            $limit = input("limit");
            $organize_id = input("organize_id");
            $user_name = input('user_name');
            $report_audit = input('report_audit');

            $join = [];
            $join = [
                ["v_enterprise_position_user", "v_enterprise_position_user.user_id = v_user_organize.user_id"],
                ["report", "report.user_id = v_enterprise_position_user.user_id"],
                ["user","user.user_id = report.user_id"]
            ];
            $con = [];
            //搜索
            if ($user_name) {
                $con['user.user_name'] = ['like', '%' . $user_name . '%']; //用户名称
            }
            if($report_audit){
                $con['report.report_audit'] = ['like', '%' . $report_audit . '%']; //审核状态
            }
            //
            if ($organize_id) {
                $con["v_user_organize.organize_id"] = $organize_id;
            } else {
                $con["v_user_organize.organize_id"] = [
                    "in", $this->UserInfo["user_organize"],
                ];
            }
            $con["v_user_organize.user_status"] = 2;
            $con["v_user_organize.user_delete"] = 1;
            $con["v_user_organize.organize_status"] = 1;
            $con["v_user_organize.organize_delete"] = 1;
            $con["v_enterprise_position_user.enterprise_position_status"] = "启用";
            $con["v_enterprise_position_user.enterprise_position_delete"] = "启用";
            $con["v_enterprise_position_user.enterprise_delete"] = "启用";
            $field = [];
            $field = [];
            $select_v_user_organize = Db::name("v_user_organize")->join($join)->where($con)->field($field)->page($page, $limit)->select();
            for($i = 0; $i < count($select_v_user_organize); $i++){
                $select_v_user_organize[$i]['report_teacher_score'] = $select_v_user_organize[$i]['report_teacher_score'] ?:'暂无';
            }
            $count_v_user_organize = Db::name("v_user_organize")->join($join)->where($con)->count();
            if ($select_v_user_organize && $count_v_user_organize) {
                // foreach ($select_v_user_organize as &$select_v_user_organize_value) {
                //     $v_user_organize_register_time = $select_v_user_organize_value["v_user_organize_register_time"];
                // }
                $re["code"] = 0;
                $re["data"] = $select_v_user_organize;
                $re["count"] = $count_v_user_organize;
                $re["msg"] = "ok";
            } else {
                $re["code"] = 40000;
                $re["msg"] = "暂无数据";
            }
        } catch (\Throwable $th) {
            $re["code"] = 40004;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }
    //评价分数
    public function report_data()
    {
        $re = [];
        try {
            $system = Db::name('system');
            $field = [];
            $field = [
                'system_evaluation_excellent_score',
                'system_evaluation_good_score',
                'system_evaluation_middle_score',
                'system_evaluation_common_score',
                'system_evaluation_poor_score',
            ];
            $find_system = $system->field($field)->find();
            if($find_system){
                $re['code'] = '0';
                $re['excellent'] = $find_system['system_evaluation_excellent_score']; //优
                $re['good'] = $find_system['system_evaluation_good_score']; //良
                $re['middle'] = $find_system['system_evaluation_middle_score']; //中
                $re['common'] = $find_system['system_evaluation_common_score']; //一般
                $re['poor'] = $find_system['system_evaluation_poor_score']; //差
                $re['msg'] = 'ok';
            }else{
                $re['code'] = 40000;
                $re['msg'] = '暂无数据';
            }
        } catch (\Throwable $th) {
            $re["code"] = 40004;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }
	//日报审核
	public function report_approval()
	{
		$re = [];
		try {
			$post = Tool::Input(input("post."));
			$Validate = new Validate();
			if ($Validate->scene("id")->check($post)) {
				$report_id = $post["report_id"];
				$update_report_audit = Db::name("report")
					->where([
						"report_id" => $report_id,
						"report_audit" => "审核中",
					])
					->update([
						"report_audit" => "审核通过",
					]);
				if ($update_report_audit === false) {
					$re["code"] = 40000;
					$re["msg"] = "审核失败";
				} else {
					$re["code"] = 0;
					$re["msg"] = "审核成功";
				}

			} else {
				$re["code"] = 40001;
				$re["msg"] = $Validate->getError();
			}
		} catch (\Throwable $th) {
			$re["code"] = 40004;
			$re["msg"] = $th->getMessage();
		}
		return json($re);
    }
    //驳回日报
	public function report_reject()
	{
		$re = [];
		try {
			$post = Tool::Input(input("post."));
			$Validate = new Validate();
			if ($Validate->scene("id")->check($post)) {
				$report_id = $post["report_id"];
				$update_report_audit = Db::name("report")
					->where([
						"report_id" => $report_id,
						"report_audit" => "审核中",
					])
					->update([
						"report_audit" => "审核不通过",
					]);
				if ($update_report_audit === false) {
					$re["code"] = 40000;
					$re["msg"] = "驳回失败";
				} else {
					$re["code"] = 0;
					$re["msg"] = "您已驳回该用户提交的报告";
				}

			} else {
				$re["code"] = 40001;
				$re["msg"] = $Validate->getError();
			}
		} catch (\Throwable $th) {
			$re["code"] = 40004;
			$re["msg"] = $th->getMessage();
		}
		return json($re);
    }
    //日报打分
    public function report_score()
    {
        $re = [];
        try {
            $post = input("post.");
            $validate = new validate();
            if ($validate->scene("id")->check($post)) {
                $update_report_score = Db::name("report")->where([
                    "report_id" => $post["report_id"],
                ])->update($post);
                if ($update_report_score === false) {
                    $re["code"] = 40000;
                    $re["msg"] = "评分失败";
                } else {
                    $re["code"] = 0;
                    $re["msg"] = "评分成功";
                }
            } else {
                $re["code"] = 40001;
                $re["msg"] = $validate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40004;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

}
