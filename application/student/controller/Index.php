<?php

namespace app\student\controller;

use Adjfut\Common;
use Adjfut\Tool\Tool;
use app\student\validate\IndexValidate;
use think\Db;

class Index extends Common
{
    public function user_organize_tree()
    {
        $re = [];
        try {
            $con = [];
            $con["organize_status"] = 1;
            $con["organize_delete"] = 1;
            $con["organize_id"] = [
                "in", $this->UserInfo["user_organize"],
            ];
            $field = [];
            $field = [];
            $select_organize = Db::name("organize")->where($con)->field($field)->select();
            if ($select_organize) {
                $children = function (array $data, int $parent_id, $children) {
                    $re = [];
                    foreach ($data as $parent_data_key => $parent_data_value) {
                        if ($parent_data_value["organize_parent_id"] == $parent_id) {
                            $temporary = [
                                "title" => $parent_data_value["organize_name"],
                                "id" => $parent_data_value["organize_id"],
                                "data" => $parent_data_value,
                                "children" => [],
                                "spread" => true,
                            ];
                            foreach ($data as $children_data_key => $children_data_value) {
                                if ($children_data_value["organize_parent_id"] == $parent_data_value["organize_id"]) {
                                    $temporary["children"] = $children($data, $parent_data_value["organize_id"], $children);
                                }
                            }
                            $re[] = $temporary;
                        }
                    }
                    return $re;
                };
                $re["code"] = 0;
                $re["data"] = [[
                    "title" => "用户组织",
                    "id" => time(),
                    "spread" => true,
                    "data" => [
                        "organize_id" => 0,
                    ],
                    "children" => $children($select_organize, $this->UserInfo["organize_id"], $children),
                ]];
                $re["msg"] = "ok";
            } else {
                $re["code"] = 40000;
                $re["msg"] = "暂无数据";
            }
        } catch (\Throwable $th) {
            $re["code"] = 40004;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function user_organize_list()
    {
        $re = [];
        try {
            $page = input("page");
            $limit = input("limit");
            $organize_id = input("organize_id");
            $join = [];
            $join = [
                ["v_enterprise_position_user", "v_enterprise_position_user.user_id = v_user_organize.user_id"],
            ];
            $con = [];
            if ($organize_id) {
                $con["v_user_organize.organize_id"] = $organize_id;
            } else {
                $con["v_user_organize.organize_id"] = [
                    "in", $this->UserInfo["user_organize"],
                ];
            }
            $con["v_user_organize.user_status"] = 2;
            $con["v_user_organize.user_delete"] = 1;
            $con["v_user_organize.organize_status"] = 1;
            $con["v_user_organize.organize_delete"] = 1;
            $con["v_enterprise_position_user.enterprise_position_status"] = "启用";
            $con["v_enterprise_position_user.enterprise_position_delete"] = "启用";
            $con["v_enterprise_position_user.enterprise_delete"] = "启用";
            $field = [];
            $field = [];
            $select_v_user_organize = Db::name("v_user_organize")->join($join)->where($con)->field($field)->page($page, $limit)->select();
            $count_v_user_organize = Db::name("v_user_organize")->join($join)->where($con)->count();
            if ($select_v_user_organize && $count_v_user_organize) {
                // foreach ($select_v_user_organize as &$select_v_user_organize_value) {
                //     $v_user_organize_register_time = $select_v_user_organize_value["v_user_organize_register_time"];
                // }
                $re["code"] = 0;
                $re["data"] = $select_v_user_organize;
                $re["count"] = $count_v_user_organize;
                $re["msg"] = "ok";
            } else {
                $re["code"] = 40000;
                $re["msg"] = "暂无数据";
            }
        } catch (\Throwable $th) {
            $re["code"] = 40004;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function sign_in()
    {
        $re = [];
        try {
            $post = Tool::Input(input("par"));
            $IndexValidate = new IndexValidate();
            if ($IndexValidate->scene("sign_in")->check($post)) {
                $select_sign_in = Db::name("sign_in")->where([
                    "user_id" => $post["user_id"],
                ])->select();
            } else {
                $re["code"] = 40001;
                $re["msg"] = $IndexValidate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }
}
