<?php
namespace app\student\validate;

use think\Validate;

class Report extends Validate
{
    protected $scene = [
        "sign_in" => ["user_id"],
        "id" => ['report_id'],
    ];

}
