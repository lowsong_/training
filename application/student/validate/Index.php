<?php
namespace app\student\validate;

use think\Validate;

class Index extends Validate
{
    protected $rule = [
        "user_id|用户" => "require",
    ];

    protected $message = [
        // "organize_price.number" =>
    ];

    protected $scene = [
        "sign_in" => ["user_id"],
        "id" => ['report_id'],
    ];

}
