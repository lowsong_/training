<?php

namespace app\report\controller;

use Adjfut\Common;
use Adjfut\Tool\Tool;
use app\report\validate\Report as ReportValidate;
use think\Db;
use think\Config;
use think\Request;

class Report extends Common
{
	// public function report_tree()
	// {
	// 	$re = [];
	// 	try {
	// 		$con = [];
	// 		$con["organize_status"] = 1;
	// 		$con["organize_delete"] = 1;
	// 		$field = [];
	// 		$field = [
	// 		];
	// 		$select_organize = Db::name("organize")->where($con)->field($field)->select();
	// 		if ($select_organize) {
	// 			$children = function (array $data, int $parent_id, $children) {
	// 				$re = [];
	// 				foreach ($data as $parent_data_key => $parent_data_value) {
	// 					if ($parent_data_value["organize_parent_id"] == $parent_id) {
	// 						$temporary = [
	// 							"title" => $parent_data_value["organize_name"],
	// 							"id" => $parent_data_value["organize_id"],
	// 							"data" => $parent_data_value,
	// 							"children" => [],
	// 							"spread" => true,
	// 						];
	// 						foreach ($data as $children_data_key => $children_data_value) {
	// 							if ($children_data_value["organize_parent_id"] == $parent_data_value["organize_id"]) {
	// 								$temporary["children"] = $children($data, $parent_data_value["organize_id"], $children);
	// 							}
	// 						}
	// 						$re[] = $temporary;
	// 					}
	// 				}
	// 				return $re;
	// 			};
	// 			$re["code"] = 0;
	// 			$re["data"] = [[
	// 				"title" => "用户组织",
	// 				"id" => time(),
	// 				"spread" => true,
	// 				"data" => [
	// 					"organize_id" => 0,
	// 				],
	// 				"children" => $children($select_organize, 0, $children),
	// 			]];
	// 			$re["msg"] = "ok";
	// 		} else {
	// 			$re["code"] = 40000;
	// 			$re["msg"] = "暂无数据";
	// 		}
	// 	} catch (\Throwable $th) {
	// 		$re["code"] = 40004;
	// 		$re["msg"] = $th->getMessage();
	// 	}
	// 	return json($re);
	// }

	public function get_time($time_range)
	{

		


		$start_time = '';
		$end_time = '';
		$week = date('w');
		//如果是周日回返回零，若一周从周日开始则不需要该语句
		if($week == 0){
			$week = 7;
		}
		if($time_range == 0){
			//今天时间戳
			$start_time = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
			$end_time = mktime(0, 0, 0, date('m'), date('d') + 1, date('Y')) - 1;
		}else if($time_range == 1){
			//昨天时间戳
			$start_time = mktime(0, 0, 0, date('m'), date('d') - 1, date('Y'));
			$end_time = mktime(0, 0, 0, date('m'), date('d'), date('Y')) - 1;
		}else if($time_range == 2){
			//本周时间戳
			$start_time = mktime(0, 0, 0, date('m'), date('d') - $week + 1 - 0, date('Y'));
			$end_time = mktime(23, 59, 59, date('m'), date('d') - $week + 7 - 0, date('Y'));
		}else if($time_range == 3){
			//上周时间戳
			$start_time = mktime(0, 0, 0, date('m'), date('d') - $week + 1 - 7, date('Y'));
			$end_time = mktime(23, 59, 59, date('m'), date('d') - $week + 7 - 7, date('Y'));
		}else if($time_range == 4){
			//本月时间戳
			$start_time = mktime(0, 0, 0, date('m'), 1, date('Y'));
			$end_time = mktime(23, 59, 59, date('m'), date('t'), date('Y'));
		}else if($time_range == 5){
			//上月时间戳
			$start_time = mktime(0, 0, 0, date('m') - 1, 1, date('Y'));
			$end_time = mktime(23, 59, 59, date('m') - 1, cal_days_in_month(CAL_GREGORIAN, date('m') - 1, date('Y')), date('Y'));
		}else{
			$arr = explode(" - ",$time_range);
			$start_time = strtotime($arr[0]);
			$end_time = strtotime($arr[1]);
		}

		if($start_time && $end_time !== ''){
			$t = $end_time-$start_time;
	        $arr = [];
			$day = intval($t/86400);//计算时间段的天数
			$arr['day'] = $day;
			if($day == 0){
				$day = 1;
			}

			$time_arr = array(
				'start_time' => $start_time,
				'end_time' => $end_time,
				'day' => $day
			);
			return $time_arr;
		}else{
			return false;
		}
	}

	public function report_day()
	{
		$re = [];
		$get = Tool::Input(input('get.'));
		$ReportValidate = new ReportValidate();
		$UserInfo = $this->UserInfo;
		if ($ReportValidate->scene("choose_time")->check($get)) {
			try {
				$sub_count = 0;
				$not_sub_count = 0;

				$page = input("page");
				$limit = input("limit");
				$time_range = $get['time_range'];

				$time_arr = $this->get_time($time_range);
				if($time_arr == false){
					$re["code"] = 40003;
					$re["msg"] = "查询失败";
				}else{
					$con = [];
					$con = [
						"enterprise_status" => "审核通过",
						"enterprise_delete" => "启用"
					];

					$field = [];
					$field = [
						"enterprise_name",
						"enterprise_id"
					];
					$select_enterprise = Db::name("enterprise")->where($con)->field($field)->page($page, $limit)->select();//查询企业
					$count_enterprise = Db::name("enterprise")->where($con)->count();

					foreach ($select_enterprise as $select_enterprise_key => &$select_enterprise_value) {
						$join = [];
						$join = [
							["enterprise", "enterprise_position.enterprise_id = enterprise.enterprise_id"],
							["enterprise_position_user", "enterprise_position_user.enterprise_position_id = enterprise_position.enterprise_position_id"],
							["user", "user.user_id = enterprise_position_user.user_id"],
							["user_organize", "user.user_id = user_organize.user_id"],
							["organize", "user_organize.organize_id = organize.organize_id"],
							["report", "user_organize.user_id = report.user_id"],
						];
						$con_report = [];
						$con_report = [
							"enterprise.enterprise_id" => $select_enterprise_value['enterprise_id'],
							"enterprise_position.enterprise_position_status" => "启用",
							"enterprise_position.enterprise_position_delete" => "启用",
							"user.user_status" => 2
							"user.user_delete" => 1,
							"report.report_type" => "日报",
							"report.report_audit" => "审核通过"
						];
						$con_report["organize.organize_id"] = [
							"in", $this->UserInfo["user_organize"],
						];
						
						$count_sub_report = Db::name("enterprise_position")->join($join)->where($con_report)->where('report.report_time', 'between time', [$time_arr['start_time'], $time_arr['end_time']])->count();//查询审核通过的日报数量
						$sub_count += $count_sub_report;//计算全校提交日报数量

						$join_e_p_u = [];
						$join_e_p_u = [
							["enterprise_position", "enterprise_position.enterprise_position_id = enterprise_position_user.enterprise_position_id"],
							["enterprise", "enterprise_position.enterprise_id = enterprise.enterprise_id"],
							["user", "user.user_id = enterprise_position_user.user_id"],
							["user_organize", "user.user_id = user_organize.user_id"],
							["organize", "user_organize.organize_id = organize.organize_id"],
						];
						$con_e_p_u = [];
						$con_e_p_u = [
							"enterprise.enterprise_id" => $select_enterprise_value['enterprise_id'],
							"enterprise_position.enterprise_position_status" => "启用",
							"enterprise_position.enterprise_position_delete" => "启用",
							"user.user_status" => 2
							"user.user_delete" => 1,
						];
						$con_e_p_u["organize.organize_id"] = [
							"in", $this->UserInfo["user_organize"],
						];
						$count_enterprise_position_user = Db::name("enterprise_position_user")->join($join_e_p_u)->where($con_e_p_u)->count();
						//查询当前学校-当前公司有多少实习生
						$all_need_report = $count_enterprise_position_user*$time_arr['day'];//实习生人数*天数

						$select_enterprise_value['count_enterprise_position_user'] = $count_enterprise_position_user;
						$select_enterprise_value['all_need_report'] = $all_need_report;//所需提交日报数量
						$select_enterprise_value['count_sub_report'] = $count_sub_report;//已提交日报数量

						$count_unsub_report = $all_need_report-$count_sub_report;
						
						$select_enterprise_value['count_unsub_report'] = $count_unsub_report;//未提交日报数量
						if($all_need_report == 0){
							$select_enterprise_value['count_sub_rate'] = 0;
						}else{
							$select_enterprise_value['count_sub_rate'] = $count_sub_report/$all_need_report*100;
							$not_sub_count += $count_unsub_report;
						}

						$school_report_count = array(
							"sub_count" => $sub_count,
							"not_sub_count" => $not_sub_count
						);
					}
					if ($select_enterprise && $count_enterprise) {
						$re["code"] = 0;
						$re["data"] = $select_enterprise;
						$re["count"] = $count_enterprise;
						$re['data_school'] = $school_report_count;
						$re["msg"] = "ok";
					} else {
						$re["code"] = 40002;
						$re["msg"] = "暂无数据";
					}
				}
			} catch (\Throwable $th) {
				$re["code"] = 40001;
				$re["msg"] = $th->getMessage();
			}
		} else {
		$re["code"] = 40001;
		$re["msg"] = $ReportValidate->getError();
		}
		return json($re);
	}

	public function report_week()
	{
		$re = [];
		$get = Tool::Input(input('get.'));
		$ReportValidate = new ReportValidate();
		$UserInfo = $this->UserInfo;
		if ($ReportValidate->scene("choose_time")->check($get)) {
			try {
				$sub_count = 0;
				$not_sub_count = 0;

				$page = input("page");
				$limit = input("limit");
				$time_range = $get['time_range'];

				$time_arr = $this->get_time($time_range);
				if($time_arr == false){
					$re["code"] = 40003;
					$re["msg"] = "查询失败";
				}else{
					$con = [];
					$con = [
						"enterprise_status" => "审核通过",
						"enterprise_delete" => "启用"
					];
					$field = [];
					$field = [
						"enterprise_name",
						"enterprise_id"
					];
					$select_enterprise = Db::name("enterprise")->where($con)->field($field)->page($page, $limit)->select();//查询企业
					$count_enterprise = Db::name("enterprise")->where($con)->count();

					foreach ($select_enterprise as $select_enterprise_key => &$select_enterprise_value) {
						$join = [];
						$join = [
							["enterprise", "enterprise_position.enterprise_id = enterprise.enterprise_id"],
							["enterprise_position_user", "enterprise_position_user.enterprise_position_id = enterprise_position.enterprise_position_id"],
							["user", "user.user_id = enterprise_position_user.user_id"],
							["user_organize", "user.user_id = user_organize.user_id"],
							["organize", "user_organize.organize_id = organize.organize_id"],
							["report", "user_organize.user_id = report.user_id"],
						];
						$con_report = [];
						$con_report = [
							"enterprise.enterprise_id" => $select_enterprise_value['enterprise_id'],
							"enterprise_position.enterprise_position_status" => "启用",
							"enterprise_position.enterprise_position_delete" => "启用",
							"user.user_status" => 2
							"user.user_delete" => 1,
							"report.report_type" => "周报",
							"report.report_audit" => "审核通过"
						];
						$con_report["organize.organize_id"] = [
							"in", $this->UserInfo["user_organize"],
						];
						
						$count_sub_report = Db::name("enterprise_position")->join($join)->where($con_report)->where('report.report_time', 'between time', [$time_arr['start_time'], $time_arr['end_time']])->count();//查询审核通过的周报数量
						$sub_count += $count_sub_report;//计算全校提交周报数量

						$join_e_p_u = [];
						$join_e_p_u = [
							["enterprise_position", "enterprise_position.enterprise_position_id = enterprise_position_user.enterprise_position_id"],
							["enterprise", "enterprise_position.enterprise_id = enterprise.enterprise_id"],
							["user", "user.user_id = enterprise_position_user.user_id"],
							["user_organize", "user.user_id = user_organize.user_id"],
							["organize", "user_organize.organize_id = organize.organize_id"],
						];
						$con_e_p_u = [];
						$con_e_p_u = [
							"enterprise.enterprise_id" => $select_enterprise_value['enterprise_id'],
							"enterprise_position.enterprise_position_status" => "启用",
							"enterprise_position.enterprise_position_delete" => "启用",
							"user.user_status" => 2
							"user.user_delete" => 1,
						];
						$con_e_p_u["organize.organize_id"] = [
							"in", $this->UserInfo["user_organize"],
						];
						$count_enterprise_position_user = Db::name("enterprise_position_user")->join($join_e_p_u)->where($con_e_p_u)->count();
						//查询当前学校-当前公司有多少实习生
						
						$all_need_report = $count_enterprise_position_user;//每日需要提交的周报数量*天数

						$select_enterprise_value['count_enterprise_position_user'] = $count_enterprise_position_user;
						$select_enterprise_value['all_need_report'] = $all_need_report;//所需提交周报数量
						$select_enterprise_value['count_sub_report'] = $count_sub_report;//已提交周报数量

						$count_unsub_report = $all_need_report-$count_sub_report;
						$select_enterprise_value['count_unsub_report'] = $count_unsub_report;//未提交周报数量
						if($all_need_report == 0){
							$select_enterprise_value['count_sub_rate'] = 0;
						}else{
							$select_enterprise_value['count_sub_rate'] = $count_sub_report/$all_need_report*100;
							$not_sub_count += $count_unsub_report;
						}

						$school_report_count = array(
							"sub_count" => $sub_count,
							"not_sub_count" => $not_sub_count
						);
					}
					if ($select_enterprise && $count_enterprise) {
						$re["code"] = 0;
						$re["data"] = $select_enterprise;
						$re["count"] = $count_enterprise;
						$re['data_school'] = $school_report_count;
						$re["msg"] = "ok";
					} else {
						$re["code"] = 40002;
						$re["msg"] = "暂无数据";
					}
				}
			} catch (\Throwable $th) {
				$re["code"] = 40001;
				$re["msg"] = $th->getMessage();
			}
		} else {
		$re["code"] = 40001;
		$re["msg"] = $ReportValidate->getError();
		}
		return json($re);
	}

	public function report_month()
	{
		$re = [];
		$get = Tool::Input(input('get.'));
		$ReportValidate = new ReportValidate();
		$UserInfo = $this->UserInfo;
		if ($ReportValidate->scene("choose_time")->check($get)) {
			try {
				$sub_count = 0;
				$not_sub_count = 0;

				$page = input("page");
				$limit = input("limit");
				$time_range = $get['time_range'];

				$time_arr = $this->get_time($time_range);
				if($time_arr == false){
					$re["code"] = 40003;
					$re["msg"] = "查询失败";
				}else{
					$con = [];
					$con = [
						"enterprise_status" => "审核通过",
						"enterprise_delete" => "启用"
					];
					$field = [];
					$field = [
						"enterprise_name",
						"enterprise_id"
					];
					$select_enterprise = Db::name("enterprise")->where($con)->field($field)->page($page, $limit)->select();//查询企业
					$count_enterprise = Db::name("enterprise")->where($con)->count();

					foreach ($select_enterprise as $select_enterprise_key => &$select_enterprise_value) {
						$join = [];
						$join = [
							["enterprise", "enterprise_position.enterprise_id = enterprise.enterprise_id"],
							["enterprise_position_user", "enterprise_position_user.enterprise_position_id = enterprise_position.enterprise_position_id"],
							["user", "user.user_id = enterprise_position_user.user_id"],
							["user_organize", "user.user_id = user_organize.user_id"],
							["organize", "user_organize.organize_id = organize.organize_id"],
							["report", "user_organize.user_id = report.user_id"],
						];
						$con_report = [];
						$con_report = [
							"enterprise.enterprise_id" => $select_enterprise_value['enterprise_id'],
							"enterprise_position.enterprise_position_status" => "启用",
							"enterprise_position.enterprise_position_delete" => "启用",
							"user.user_status" => 2
							"user.user_delete" => 1,
							"report.report_type" => "月报",
							"report.report_audit" => "审核通过"
						];
						$con_report["organize.organize_id"] = [
							"in", $this->UserInfo["user_organize"],
						];
						
						$count_sub_report = Db::name("enterprise_position")->join($join)->where($con_report)->where('report.report_time', 'between time', [$time_arr['start_time'], $time_arr['end_time']])->count();//查询审核通过的月报数量
						$sub_count += $count_sub_report;//计算全校提交月报数量

						$join_e_p_u = [];
						$join_e_p_u = [
							["enterprise_position", "enterprise_position.enterprise_position_id = enterprise_position_user.enterprise_position_id"],
							["enterprise", "enterprise_position.enterprise_id = enterprise.enterprise_id"],
							["user", "user.user_id = enterprise_position_user.user_id"],
							["user_organize", "user.user_id = user_organize.user_id"],
							["organize", "user_organize.organize_id = organize.organize_id"],
						];
						$con_e_p_u = [];
						$con_e_p_u = [
							"enterprise.enterprise_id" => $select_enterprise_value['enterprise_id'],
							"enterprise_position.enterprise_position_status" => "启用",
							"enterprise_position.enterprise_position_delete" => "启用",
							"user.user_status" => 2
							"user.user_delete" => 1,
						];
						$con_e_p_u["organize.organize_id"] = [
							"in", $this->UserInfo["user_organize"],
						];
						$count_enterprise_position_user = Db::name("enterprise_position_user")->join($join_e_p_u)->where($con_e_p_u)->count();
						//查询当前学校-当前公司有多少实习生

						$all_need_report = $count_enterprise_position_user;//每日需要提交的月报数量

						$select_enterprise_value['count_enterprise_position_user'] = $count_enterprise_position_user;
						$select_enterprise_value['all_need_report'] = $all_need_report;//所需提交月报数量
						$select_enterprise_value['count_sub_report'] = $count_sub_report;//已提交月报数量

						$count_unsub_report = $all_need_report-$count_sub_report;
						$select_enterprise_value['count_unsub_report'] = $count_unsub_report;//未提交月报数量
						if($all_need_report == 0){
							$select_enterprise_value['count_sub_rate'] = 0;
						}else{
							$select_enterprise_value['count_sub_rate'] = $count_sub_report/$all_need_report*100;
							$not_sub_count += $count_unsub_report;
						}

						$school_report_count = array(
							"sub_count" => $sub_count,
							"not_sub_count" => $not_sub_count
						);
					}
					if ($select_enterprise && $count_enterprise) {
						$re["code"] = 0;
						$re["data"] = $select_enterprise;
						$re["count"] = $count_enterprise;
						$re['data_school'] = $school_report_count;
						$re["msg"] = "ok";
					} else {
						$re["code"] = 40002;
						$re["msg"] = "暂无数据";
					}
				}
			} catch (\Throwable $th) {
				$re["code"] = 40001;
				$re["msg"] = $th->getMessage();
			}
		} else {
		$re["code"] = 40001;
		$re["msg"] = $ReportValidate->getError();
		}
		return json($re);
	}

	public function report_list()
	{
		$re = [];
		// $post = Tool::Input(input('post.'));
		$get = Tool::Input(input('get.'));
		$ReportValidate = new ReportValidate();
		$UserInfo = $this->UserInfo;
		if ($ReportValidate->scene("report_list")->check($get)) {
			try {
				$page = input("page");
				$limit = input("limit");

				$time_range = $get['time_range'];
				$enterprise_id = $get['enterprise_id'];
				$report_type = $get['report_type'];

				if($report_type == 1){
					$report_type = '日报';
				}else if($report_type == 2){
					$report_type = '周报';
				}else if($report_type == 3){
					$report_type = '月报';
				}

				$time_arr = $this->get_time($time_range);
				if($time_arr == false){
					$re["code"] = 40003;
					$re["msg"] = "查询失败";
				}else{
					$join = [];
					$join = [
						["user","report.user_id = user.user_id"],
						["enterprise_position_user","user.user_id = enterprise_position_user.user_id"],
						["enterprise_position","enterprise_position_user.enterprise_position_id = enterprise_position.enterprise_position_id"],
						["enterprise","enterprise.enterprise_id = enterprise_position.enterprise_id"],
						["user_organize","user_organize.user_id = user.user_id"],
						["organize","user_organize.organize_id = organize.organize_id"],
					];

					$con = [];
					$con = [
						"enterprise.enterprise_id" => $enterprise_id,
						"enterprise_position.enterprise_position_status" => "启用",
						"enterprise_position.enterprise_position_delete" => "启用",
						"user_status" => 2,
						"user_delete" => 1,
						"organize_status" => 1,
						"organize_delete" => 1,
						"report.report_audit" => "审核通过",
						"report.report_type" => $report_type
					];
					$con["organize.organize_id"] = [
						"in", $this->UserInfo["user_organize"],
					];

					// $field = [];
					// $field = [
					// 	"enterprise_position_name",
					// 	"enterprise_id",
					// 	"organize_name",
					// 	"user_id",
					// 	"user_name",
					// 	"report_id",
					// 	"report_time",
					// ];
					$select_report = Db::name("report")->join($join)->where($con)->where('report.report_time', 'between time', [$time_arr['start_time'], $time_arr['end_time']])->page($page, $limit)->select();//查询日报
					$count_report = Db::name("report")->join($join)->where($con)->where('report.report_time', 'between time', [$time_arr['start_time'], $time_arr['end_time']])->count();
					if ($select_report && $count_report) {
						foreach ($select_report as $select_report_key => &$select_report_value) {
							$select_report_value['report_time'] = date('Y-m-d H:i:s',$select_report_value['report_time']);
						}

						$re["code"] = 0;
						$re["data"] = $select_report;
						$re["count"] = $count_report;
						$re["msg"] = "ok";
					} else {
						$re["code"] = 40002;
						$re["msg"] = "暂无数据";
					}
				}
			} catch (\Throwable $th) {
				$re["code"] = 40001;
				$re["msg"] = $th->getMessage();
			}
		} else {
		$re["code"] = 40001;
		$re["msg"] = $ReportValidate->getError();
		}
		return json($re);
	}
}
