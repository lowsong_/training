<?php
namespace app\report\validate;

use think\Validate;

class Report extends Validate
{
    protected $rule = [
        "time_range|日期" => "require",
        "enterprise_id|id" => "require",
        "report_type|报告类型" => "require",
    ];

    protected $message = [
        // "role_price.number" =>
    ];

    protected $scene = [
        'choose_time' => ['time_range'],
        'report_list' => ['time_range','enterprise_id','report_type']
    ];

}
