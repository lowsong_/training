<?php
namespace app\api\validate;

use think\Validate;

class User extends Validate
{
    protected $rule = [
        "report_id|报告ID" => "require",
        "report_content|报告内容" => "require",
        "report_type|报告类型" => "require|in:日报,周报,月报",
        "report_time|报告日期" => "require|dateFormat:y-m-d",
        "user_phone|手机号" => "require|length:11" 
    ];

    protected $message = [
        // "role_price.number" =>
    ];

    protected $scene = [
        "report_add" => ["report_content", "report_type", "report_time"],
        "report_edit" => ["report_content", "report_id"],
        "user_bind" => ["user_phone"]
    ];

}
