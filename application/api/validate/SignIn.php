<?php
namespace app\api\validate;

use think\Validate;

class SignIn extends Validate
{
    protected $rule = [
        "sign_in_distance|签到距离" => "require",
        "sign_in_point|签到点" => "require",
        "sign_in_address|签到地址" => "require",
    ];

    protected $message = [
        
    ];

    protected $scene = [
        "sign_in" => ["sign_in_distance", "sign_in_point"],
    ];

}
