<?php

namespace app\api\validate;

use think\Validate;

class Gzh extends Validate
{
    protected $rule = [
        "phone|手机号" => "require|length:11",
        "code|验证码" => "require",
    ];

    protected $message = [];

    protected $scene = [
        "bind_phone" => ["phone", "code"],
        "send_sms" => ["phone"]
    ];
}
