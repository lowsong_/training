<?php

namespace app\api\controller;

use Adjfut\Common;
use app\api\validate\UserValidate;
use think\Db;

class User extends Common
{
    public function user_bind(){
        $re = [];
        try {
            $post = Tool::Input(input('post.'));
            $UserValidate = new UserValidate();
            if ($UserValidate->scene("user_bind")->check($post)) {
                // $report = Db::name("report");
                // $post["report_guid"] = $this->get_guid();
                // $post["user_id"] = $this->UserInfo["user_id"];
                // $insert_report = $report->insert($post);
                // if ($insert_report) {
                //     $re["code"] = 10000;
                //     $re["msg"] = "保存失败";
                // } else {
                //     $re["code"] = 40002;
                //     $re["msg"] = "保存失败";
                // }
            } else {
                $re["code"] = 40001;
                $re["msg"] = $UserValidate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function report_list()
    {
        $re = [];
        try {
            $userinfo = $this->UserInfo;
            $page = input("page");
            $limit = input("limit");
            $join = [];
            $join = [
                ["user", "user.user_id = report.user_id"],
                ["user_organize", "user_organize.user_id = user.user_id"],
                ["organize", "organize.organize_id = user_organize.organize_id"],
            ];
            $con = [];
            $con = [
                "organize.organize_id" => [
                    "in", $userinfo["user_organize"],
                ],
                "organize.organize_status" => 1,
                "organize.organize_delete" => 1,
                "user.user_status" => 2,
                "user.user_delete" => 1,
            ];
            $field = [];
            $field = [
                "organize.organize_name",
                "user.user_name",
                "report.report_content",
                "report.report_type",
                "report.report_time",
            ];
            $select_report = Db::name("report")->join($join)->where($con)->field($field)->page($page, $limit)->select();
            $count_report = Db::name("report")->join($join)->where($con)->count();
            if ($select_report && $count_report) {
                $re["code"] = 0;
                $re["data"] = $select_report;
                $re["count"] = $count_report;
                $re["msg"] = "ok";
            } else {
                $re["code"] = 40002;
                $re["msg"] = "暂无数据";
            }
        } catch (\Throwable $th) {
            $re["code"] = 40001;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function report_add()
    {
        $re = [];
        try {
            $post = Tool::Input(input('post.'));
            $LoginValidate = new ReportValidate();
            if ($LoginValidate->scene("report_add")->check($post)) {
                $report = Db::name("report");
                $post["report_guid"] = $this->get_guid();
                $post["user_id"] = $this->UserInfo["user_id"];
                $insert_report = $report->insert($post);
                if ($insert_report) {
                    $re["code"] = 10000;
                    $re["msg"] = "保存失败";
                } else {
                    $re["code"] = 40002;
                    $re["msg"] = "保存失败";
                }
            } else {
                $re["code"] = 40001;
                $re["msg"] = $LoginValidate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function report_edit()
    {
        $re = [];
        try {
            $post = Tool::Input(input('post.'));
            $LoginValidate = new ReportValidate();
            if ($LoginValidate->scene("report_edit")->check($post)) {
                $report = Db::name("report");
                $post["report_guid"] = $this->get_guid();
                $post["user_id"] = $this->UserInfo["user_id"];
                $insert_report = $report->insert($post);
                if ($insert_report) {
                    $re["code"] = 10000;
                    $re["msg"] = "保存失败";
                } else {
                    $re["code"] = 40002;
                    $re["msg"] = "保存失败";
                }
            } else {
                $re["code"] = 40001;
                $re["msg"] = $LoginValidate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    // protected function
}
