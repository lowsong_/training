<?php

namespace app\api\controller;

use Adjfut\Common;

// use app\api\validate\ReportValidate;
use think\Db;

class ClockIn extends Common
{
    public function my_clock_in_log()
    {
        $re = [];
        try {
            $select_clock_in = Db::name("clock_in")->where([

            ])->select();
            $re["u"] = $this->UserInfo;
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function clock_in_time(){
        $re = [];
        try {
            $userinfo = $this->UserInfo;
            $select_clock_in_set = Db::name("clock_in_set")->where([
                "enterprise_id" => $userinfo["enterprise_id"]
            ])->field([
                "clock_in_set_name",
                "clock_in_set_start",
                "clock_in_set_end",
            ])->select();
            $re["a"] = $select_clock_in_set;
            if($select_clock_in_set){
                $date = date("Y/m/d ");
                $time = time();
                foreach ($select_clock_in_set as &$select_clock_in_set_value) {
                    $start_time = strtotime($date.$select_clock_in_set_value["clock_in_set_start"]);
                    $end_time = strtotime($date.$select_clock_in_set_value["clock_in_set_end"]);

                    $select_clock_in_set_value["clock_in"] = false;

                    if($start_time >= $time && $time <= $end_time){
                        $select_clock_in_set_value["clock_in"] = true;
                    }
                }
                $re["code"] = 10000;
                $re["enterprise"] = [
                    "enterprise_points" => $userinfo["enterprise_points"],
                    "enterprise_radius" => $userinfo["enterprise_radius"],
                ];
                $re["data"] = $select_clock_in_set;
                $re["msg"] = "ok";
            }else{
                $re["code"] = 40001;
                $re["msg"] = "企业未设置打卡时间";
            }

            
            $re["u"] = $this->UserInfo;
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

}
