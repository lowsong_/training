<?php

namespace app\api\controller;

use Adjfut\Common;
use Adjfut\Tool\Tool;
use app\adjfut\controller\Login;
use app\api\validate\SignIn as SignInValidate;
use think\Db;

class SignIn extends Common
{
    public function map()
    {
        $re = [];
        try {

            $con = [];
            $user_id = input("user_id");
            if ($user_id) {
                $user = (new Login)->user($user_id);
                $userinfo = $user["data"];
            } else {
                $userinfo = $this->UserInfo;
            }
            $con["user_id"] = $userinfo["user_id"];

            $select_sign_in = Db::name("sign_in")->where($con)->select();
            $today_sign_in = false;
            $today = [
                "start" => strtotime(date("Y/m/d 00:00:00")),
                "end" => strtotime(date("Y/m/d 23:59:59")),
            ];
            // $time = time();
            foreach ($select_sign_in as $select_sign_in_value) {
                $sign_in_time = $select_sign_in_value["sign_in_time"];
                if ($today["start"] <= $sign_in_time && $sign_in_time <= $today["end"]) {
                    $today_sign_in = true;
                    break;
                }
            }
            $re["code"] = 10000;
            $re["msg"] = "ok";
            $re["data"]["today_sign_in"] = $today_sign_in;
            $re["data"]["sign_in"] = $select_sign_in;
            $re["data"]["enterprise"] = [
                "enterprise_points" => $userinfo["enterprise_points"],
                "enterprise_radius" => $userinfo["enterprise_radius"],
            ];
            // $re["sign_in"] =
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function sign_in()
    {
        $re = [];
        try {
            $post = Tool::Input(input('post.'));
            $SignInValidate = new SignInValidate();
            if ($SignInValidate->scene("sign_in")->check($post)) {
                $userinfo = $this->UserInfo;
                $sign_in = Db::name("sign_in");

                $find_sign_in = $sign_in->where([
                    "sign_in_time" => ["between", [
                        strtotime(date("Y/m/d 00:00:00")),
                        strtotime(date("Y/m/d 23:59:59")),
                    ]],
                    "user_id" => $userinfo["user_id"],
                ])->find();
                if ($find_sign_in) {
                    $re["code"] = 40002;
                    $re["msg"] = "您以签到过，不能重复签到";
                } else {
                    $data = [];
                    if ($post["sign_in_distance"] <= $userinfo["enterprise_radius"]) {
                        $data["sign_in_type"] = "正常";
                    } else {
                        $data["sign_in_type"] = "异常";
                    }
                    $data["user_id"] = $userinfo["user_id"];
                    $data["sign_in_time"] = time();
                    $data["sign_in_point"] = $post["sign_in_point"];
                    $data["sign_in_address"] = $post["sign_in_address"];
                    $data["sign_in_distance"] = $post["sign_in_distance"];
                    $data["sign_in_enterprise_name"] = $userinfo["enterprise_name"];
                    $data["sign_in_enterprise_address"] = $userinfo["enterprise_address"];
                    $data["sign_in_enterprise_points"] = $userinfo["enterprise_points"];
                    $data["sign_in_enterprise_radius"] = $userinfo["enterprise_radius"];
                    $data["sign_in_guid"] = $this->get_guid();
                    $insert_sign_in = $sign_in->insert($data);
                    if ($insert_sign_in) {
                        $re["code"] = 10000;
                        $re["msg"] = "签到成功";
                    } else {
                        $re["code"] = 40003;
                        $re["msg"] = "签到失败，请检查网络连接";
                    }
                }
            } else {
                $re["code"] = 40001;
                $re["msg"] = $SignInValidate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    // public function student_list(){
    //     $re = [];
    //     try {
    //         $page = input("page");
    //         $limit = input("limit");
    //         $type = input("type");
    //         $userinfo = $this->UserInfo;
    //         $join = [];
    //         $join = [
    //             ["user_organize","user_organize.user_id = user.user_id"],
    //             ["organize","organize.organize_id = user_organize.organize_id"],
    //             ["user_role","user_role.user_id = user.user_id"],
    //             ["role","role.role_id = user_role.role_id"],
    //             ["sign_in","sign_in.user_id = user.user_id","left"]
    //         ];
    //         $con = [];
    //         $con = [
    //             "organize.organize_id" => [
    //                 "in",$userinfo["user_organize"]
    //             ],
    //             "role.role_name" => "学生",
    //             "sign_in.sign_in_time" => [
    //                 ["between",[
    //                     strtotime(date("Y/m/d 00:00:00")),
    //                     strtotime(date("Y/m/d 23:59:59")),
    //                 ]],["null",""],"or"
    //             ],
    //         ];
    //         // if($type){

    //         // }
    //         if($type == "正常" || $type == "异常"){
    //             $con["sign_in_type"] = $type;
    //         }
    //         $select_user = Db::name("user")->join($join)->where($con)->field([
    //             "user.*",
    //             "sign_in.*",
    //         ])->page($page,$limit)->select();
    //         $count_user = Db::name("user")->join($join)->where($con)->count();

    //         $re["sql"] = Db::name("user")->join($join)->where($con)->field([
    //             "user.*",
    //             "sign_in.*",
    //         ])->page($page,$limit)->select(false);

    //         // $data = [];
    //         // $count = 0;
    //         // if($type == "未签到"){

    //         // }else{
    //         //     $data = $select_user;
    //         //     $count = $count_user;
    //         // }

    //         // $select_sign_in = Db::name("sign_in")->where([
    //         //     "user_id" => [
    //         //         "in",array_map(function($value){
    //         //             return $value["user_id"];
    //         //         },$select_user)
    //         //     ]
    //         // ])->page($page,$limit)->select();
    //         // $data = [];
    //         // foreach ($select_sign_in as $select_sign_in_value) {
    //         //     $sign_in_type = $select_sign_in_value["sign_in_type"];
    //         //     switch ($type) {
    //         //         case '已签到':
    //         //             array_push($data,$select_sign_in_value);
    //         //             break;
    //         //         case '未签到':
    //         //             foreach ($select_user as $select_user_value) {
    //         //                 # code...
    //         //             }
    //         //             break;
    //         //         case '正常':
    //         //             # code...
    //         //             break;
    //         //         case '异常':
    //         //             # code...
    //         //             break;
    //         //     }
    //         // }
    //         $re["code"] = 10000;
    //         $re["data"] = $select_user;
    //         $re["count"] = $count_user;
    //         $re["msg"] = "ok";
    //     } catch (\Throwable $th) {
    //         $re["code"] = 40000;
    //         $re["msg"] = $th->getMessage();
    //     }
    //     return json($re);
    // }
    public function student_list()
    {
        $re = [];
        try {
            $page = input("page");
            $limit = input("limit");
            $type = input("type");
            $date = input("date");
            $userinfo = $this->UserInfo;
            $join = [];
            $join = [
                ["user_organize", "user_organize.user_id = user.user_id"],
                ["organize", "organize.organize_id = user_organize.organize_id"],
                ["user_role", "user_role.user_id = user.user_id"],
                ["role", "role.role_id = user_role.role_id"],
                ["v_enterprise_position_user", "v_enterprise_position_user.user_id = user.user_id"],
            ];
            $con = [];
            $con = [
                "organize.organize_id" => [
                    "in", $userinfo["user_organize"],
                ],
                "role.role_name" => "学生",
            ];
            $buildSql_user = Db::name("user")->join($join)->where($con)->field([
                "user.user_id",
                "user.user_name",
                "user.user_headimage",
                "user.user_headimage",
                "v_enterprise_position_user.enterprise_name",
                "v_enterprise_position_user.enterprise_address",
            ])->buildSql();

            if ($date) {
                $today = [
                    strtotime(date("$date 00:00:00")),
                    strtotime(date("$date 23:59:59")),
                ];
            } else {
                $today = [
                    strtotime(date("Y/m/d 00:00:00")),
                    strtotime(date("Y/m/d 23:59:59")),
                ];
            }

            if ($type == "未签到") {
                $select_user = Db::query($buildSql_user);
                $sign_in = Db::name("sign_in");
                foreach ($select_user as $select_user_key => $select_user_value) {
                    $find_sign_in = $sign_in->where([
                        "sign_in_time" => ["between", $today],
                        "user_id" => $select_user_value["user_id"],
                    ])->find();
                    if ($find_sign_in) {
                        array_splice($select_user, $select_user_key, 1);
                    }
                }
                $count_user = count($select_user);
            } else {
                $join = [];
                $join = [
                    ["sign_in", [
                        "sign_in.user_id = user.user_id",
                        "sign_in.sign_in_time BETWEEN " . join(" AND ", $today),
                    ], "left"],
                ];
                $con = [];

                // $con["sign_in.sign_in_time"] = [
                //     ["between", $today], ["not null", ""], "and",
                // ];
                if ($type == "已签到") {
                    $con["sign_in_type"] = [
                        ["eq", "正常"], ["eq", "异常"], "or",
                    ];
                }

                if ($type == "正常" || $type == "异常") {
                    $con["sign_in_type"] = $type;
                }
                $select_user = Db::table($buildSql_user . " user")->join($join)->where($con)->page($page, $limit)->field([
                    "user.*",
                    "sign_in.sign_in_address",
                    "sign_in.sign_in_time",
                    "sign_in.sign_in_type",
                    "sign_in.sign_in_enterprise_name",
                    "sign_in.sign_in_enterprise_address",
                    "sign_in.sign_in_type",
                ])->select();
                $count_user = Db::table($buildSql_user . " user")->join($join)->where($con)->count();
            }

            $statistical_data = Db::table($buildSql_user . " user")->join([
                ["sign_in", [
                    "sign_in.user_id = user.user_id",
                    "sign_in.sign_in_time BETWEEN " . join(" AND ", $today),
                ], "left"],
            ])->where([
                // "sign_in.sign_in_time" => [
                //     ["between", $today], ["null", ""], "or",
                // ],
            ])->field([
                "sign_in.sign_in_type",
            ])->select();
            // $re["a"] = $statistical_data;
            $statistical = [];
            $statistical["已签到"] = 0;
            $statistical["未签到"] = 0;
            $statistical["正常"] = 0;
            $statistical["异常"] = 0;
            $statistical["全部"] = count($statistical_data);
            foreach ($statistical_data as $statistical_data_value) {
                $sign_in_type = $statistical_data_value["sign_in_type"];
                switch ($sign_in_type) {
                    case null:
                        $statistical["未签到"]++;
                        break;
                    case "正常":
                        $statistical["正常"]++;
                        $statistical["已签到"]++;
                        break;
                    case "异常":
                        $statistical["异常"]++;
                        $statistical["已签到"]++;
                        break;
                }
            }
            foreach ($select_user as &$select_user_value) {
                if (isset($select_user_value["sign_in_time"])) {
                    $select_user_value["sign_in_time"] = date("m/d H:i", $select_user_value["sign_in_time"]);
                }

                if (empty($select_user_value["sign_in_type"])) {
                    $select_user_value["sign_in_type"] = "未签到";
                }
            }

            $re["code"] = 10000;
            $re["data"] = $select_user;
            $re["count"] = $count_user;
            $re["statistical"] = $statistical;
            $re["msg"] = "ok";
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }
}
