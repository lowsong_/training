<?php

namespace app\api\controller;

use Adjfut\Common;
use app\api\validate\Report as ReportValidate;
use think\Db;

class Report extends Common
{
    public function report_list()
    {
        $re = [];
        try {
            $userinfo = $this->UserInfo;
            $page = input("page");
            $limit = input("limit");
            $type = input("type");
            $join = [];
            $join = [
                ["user", "user.user_id = report.user_id"],
                ["user_organize", "user_organize.user_id = user.user_id"],
                ["organize", "organize.organize_id = user_organize.organize_id"],
            ];
            $con = [];
            $con = [
                "organize.organize_id" => [
                    "in", $userinfo["user_organize"],
                ],
                "organize.organize_status" => 1,
                "organize.organize_delete" => 1,
                "user.user_id" => $userinfo["user_id"],
                "user.user_status" => 2,
                "user.user_delete" => 1,
            ];
            if($type){
                $con["report_type"] = $type;
            }
            $field = [];
            $field = [
                "organize.organize_name",
                "user.user_name",
                "user.user_headimage",
                "report.report_id",
                "report.report_content",
                "report.report_type",
                "report.report_time",
                "report.report_week",
                "report.report_audit",
            ];
            $select_report = Db::name("report")->join($join)->where($con)->field($field)->page($page, $limit)->select();
            $count_report = Db::name("report")->join($join)->where($con)->count();
            if ($select_report && $count_report) {
                foreach ($select_report as &$select_report_value) {
                    $report_time = $select_report_value["report_time"];
                    $select_report_value["report_time"] = date("m/d H:i",$report_time);
                }
                $re["code"] = 10000;
                $re["data"] = $select_report;
                $re["count"] = $count_report;
                $re["msg"] = "ok";
            } else {
                $re["code"] = 40002;
                $re["msg"] = "暂无数据";
            }
        } catch (\Throwable $th) {
            $re["code"] = 40001;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function report_add()
    {
        $re = [];
        try {
            $post = input('post.');
            $ReportValidate = new ReportValidate();
            if ($ReportValidate->scene("report_add")->check($post)) {
                $userinfo = $this->UserInfo;
                $report_type = $post["report_type"];
                list($year,$mounth,$day) = explode("-",date("Y-m-d"));
                $week = $this->week_list(date("Ymd"));
                if($week){
                    $report = Db::name("report");
                    $con = [];
                    $con["user_id"] = $userinfo["user_id"];
                    $con["report_type"] = $report_type;
                    switch ($report_type) {
                        case '日报':
                            $con["report_year"] = $year;
                            $con["report_month"] = $mounth;
                            $con["report_day"] = $day;
                            break;
                        case '周报':
                            $con["report_year"] = $year;
                            $con["report_month"] = $mounth;
                            $con["report_week"] = $week;
                            break;
                        case '月报':
                            $con["report_year"] = $year;
                            $con["report_month"] = $mounth;
                            break;
                    }
                    $find_report = $report->where($con)->find();
                    if($find_report){
                        $re["code"] = 40002;
                        $re["msg"] = "您已经写过$report_type";
                    }else{
                        $report = Db::name("report");
                        $post["report_time"] = time();
                        $post["report_guid"] = $this->get_guid();
                        $post["user_id"] = $userinfo["user_id"];
                        $post["report_year"] = $year;
                        $post["report_month"] = $mounth;
                        $post["report_day"] = $day;
                        $post["report_week"] = $week;
                        $post["report_audit"] = "审核中";
                        $insert_report = $report->insert($post);
                        if ($insert_report) {
                            $re["code"] = 10000;
                            $re["msg"] = "添加成功";
                        } else {
                            $re["code"] = 40002;
                            $re["msg"] = "添加失败";
                        }
                    }
                }else{
                    $re["code"] = 40001;
                    $re["msg"] = "实习未开始，请开始实习后再提交$report_type";
                }
            } else {
                $re["code"] = 40001;
                $re["msg"] = $ReportValidate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function report_edit()
    {
        $re = [];
        try {
            $post = input('post.');
            $ReportValidate = new ReportValidate();
            if ($ReportValidate->scene("report_edit")->check($post)) {
                $report = Db::name("report");
                $post["report_audit"] = "审核中";
                $update_report = $report->where([
                    "report_id" => $post["report_id"]
                ])->update($post);
                if ($update_report === false) {
                    $re["code"] = 40002;
                    $re["msg"] = "修改失败";
                } else {
                    $re["code"] = 10000;
                    $re["msg"] = "修改成功";
                }      
            } else {
                $re["code"] = 40001;
                $re["msg"] = $ReportValidate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function student_list(){
        $re = [];
        try {
            $page = input("page");
            $limit = input("limit");
            $type = input("type");
            $date = input("date");

            $userinfo = $this->UserInfo;
            $join = [];
            $join = [
                ["user_organize","user_organize.user_id = user.user_id"],
                ["organize","organize.organize_id = user_organize.organize_id"],
                ["user_role","user_role.user_id = user.user_id"],
                ["role","role.role_id = user_role.role_id"],
                ["v_enterprise_position_user","v_enterprise_position_user.user_id = user.user_id"]
            ];
            $con = [];
            $con = [
                "organize.organize_id" => [
                    "in",$userinfo["user_organize"]
                ],
                "role.role_name" => "学生",
            ];
            $buildSql_user = Db::name("user")->join($join)->where($con)->field([
                "user.user_id",
                "user.user_name",
                "user.user_headimage",
                "user.user_headimage",
                "v_enterprise_position_user.enterprise_name",
                "v_enterprise_position_user.enterprise_address"
            ])->buildSql();
            
            $join = [];
            $join = [
                ["report","report.user_id = user.user_id","left"]
            ];
            $con = [];
            if($date){
                list($year,$mounth,$day) = explode("-",$date);
            }else{
                list($year,$mounth,$day) = explode("-",date("Y-m-d"));
            }
            $con["report.report_type"] = $type;
            
            switch ($type) {
                case '日报':
                    $con["report.report_year"] = $year;
                    $con["report.report_month"] = $mounth;
                    $con["report.report_day"] = $day;
                    break;
                case '周报':
                    if($date){
                        $week = $this->week_list(date("Ymd",strtotime($date)));
                    }else{
                        $week = $this->week_list(date("Ymd"));
                    }
                    $con["report.report_year"] = $year;
                    $con["report.report_month"] = $mounth;
                    $con["report.report_week"] = $week;
                    break;
                case '月报':
                    $con["report.report_year"] = $year;
                    $con["report.report_month"] = $mounth;
                    break;
            }
            $select_user = Db::table($buildSql_user." user")->join($join)->where($con)->page($page,$limit)->field([
                "user.*",
                "report.*",
            ])->select();
            $count_user = Db::table($buildSql_user." user")->join($join)->where($con)->count();
            foreach ($select_user as &$select_user_value) {
                $report_time = $select_user_value["report_time"];
                $select_user_value["report_time"] = date("m/d H:i",$report_time);
            }

            // $statistical_data = Db::table($buildSql_user." user")->join($join)->where($con)->field([
            //     "report.report_audit",
            // ])->select();
            // $statistical = [];
            // $statistical["审核通过"] = 0;
            // $statistical["审核不通过"] = 0;
            // $statistical["审核中"] = 0;
            // $statistical["未提交"] = 0;
            // $statistical["全部"] = count($statistical_data);
            // foreach ($statistical_data as $statistical_data_value) {
            //     $report_audit = $statistical_data_value["report_audit"];
            //     switch ($report_audit) {
            //         case null:
            //             $statistical["未签到"]++;
            //             break;
            //         case "正常":
            //             $statistical["正常"]++;
            //             $statistical["已签到"]++;
            //             break;
            //         case "异常":
            //             $statistical["异常"]++;
            //             $statistical["已签到"]++;
            //             break;
            //     }
            // }

            $re["code"] = 10000;
            $re["data"] = $select_user;
            $re["count"] = $count_user;
            $re["msg"] = "ok";
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function report_audit()
    {
        $re = [];
        try {
            $post = input('post.');
            $ReportValidate = new ReportValidate();
            if ($ReportValidate->scene("report_audit")->check($post)) {
                $report = Db::name("report");
                $update_report = $report->where([
                    "report_id" => $post["report_id"]
                ])->update($post);
                if ($update_report === false) {
                    $re["code"] = 40002;
                    $re["msg"] = "审核失败";
                } else {
                    $re["code"] = 10000;
                    $re["msg"] = "审核成功";
                }      
            } else {
                $re["code"] = 40001;
                $re["msg"] = $ReportValidate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function summary_save(){
        $re = [];
        try {
            $post_value = input('post.value');
            $userinfo = $this->UserInfo;
            $update_enterprise_position_user = Db::name("enterprise_position_user")->where([
                "user_id" => $userinfo["user_id"],
                "enterprise_position_id" => $userinfo["enterprise_position_id"]
            ])->update([
                "enterprise_position_user_summary" => $post_value
            ]);
            if($update_enterprise_position_user === false){
                $re["code"] = 40001;
                $re["msg"] = "保存失败";
            }else{
                $re["code"] = 10000;
                $re["msg"] = "保存成功";
            }
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    protected function week_list(int $now){
        $userinfo = $this->UserInfo;
        $start_day = date("Ymd",strtotime($userinfo["organize_internship_start"]));
        $end_day = date("Ymd",strtotime($userinfo["organize_internship_end"]));
        // $end_day   = date('Ymd', strtotime("{$start_day} + 1 month -1 day"));
        $date = range($start_day, $end_day, 1);
        $data = [];
        $data_index = 1;
        foreach ($date as $date_key => $date_value) {
            // if(($date_key % 7) === 0){
            //     $data_index++;
            // }
            // dump(date("w",strtotime($date_value)));
            if(date("w",strtotime($date_value)) == 1){
                $data_index++;
            }
            if(!isset($data[$data_index])){
                $data[$data_index] = [];
            }
            array_push($data[$data_index],$date_value);
        }
        $re = "";
        // dump(in_array($now,[20200110,20200111,20200112]));
        foreach ($data as $data_key => $data_value) {
            if(in_array($now,$data_value)){
                $re = $data_key;
                break;
            }
        }
        return $re;
        // dump($start_day);
        // dump($end_day);
    }
    
}
