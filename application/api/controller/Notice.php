<?php

namespace app\api\controller;

use Adjfut\Common;
use Adjfut\Tool\Tool;

use app\api\validate\Notice as NoticeValidate;
use think\Db;

class Notice extends Common
{
    public function notice_list(){
        $re = [];
        try {
            $notice_id = input("notice_id");
            $userinfo = $this->UserInfo;
            $con = [];
            $join = [];
            $join = [
                ["user","user.user_id = notice.user_id"]
            ];
            if($notice_id){
                $con["notice_id"] = $notice_id;
            }
            $con["notice.notice_status"] = "启用";
            $con["notice.notice_delete"] = "启用";
            $con["notice.organize_id"] = [
                "in",$userinfo["user_organize"]
            ];
            $field = [];
            $field = [
                "user.user_name",
                "user.user_headimage",
                "notice.notice_id",
                "notice.notice_title",
                "notice.notice_content",
                "notice.notice_time",
            ];
            $select_notice = Db::name("notice")->join($join)->where($con)->field($field)->select();
            $count_notice = Db::name("notice")->join($join)->where($con)->count();
            if($select_notice){
                foreach ($select_notice as &$select_notice_value) {
                    $notice_time = $select_notice_value["notice_time"];
                    $select_notice_value["notice_time"] = date("m/d H:i",$notice_time);
                }
                $re["code"] = 10000;
                $re["data"] = $select_notice;
                $re["count"] = $count_notice;
                $re["msg"] = "ok";
            }else{
                $re["code"] = 40001;
                $re["msg"] = "暂无数据";
            }
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }
}
