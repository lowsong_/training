<?php

namespace app\api\controller;

use Adjfut\Common;
use Adjfut\WeChat\Gzh as WechatGzh;
use app\adjfut\controller\Login;
use app\api\validate\Gzh as GzhValidate;
use think\Db;
use think\Request;
use think\Session;
use think\View;

// use think\Session;

class Gzh extends Common
{
    public function local($user_id)
    {
        $find_user = Db::name("user")->where([
            "user_id" => $user_id,
        ])->find();
        if ($find_user["user_status"] == 2) {
            $request = Request::instance();
            // $url = str_replace("index.php", "wx_gzh.html", $request->root(true));
            $url = str_replace("index.php", "", $request->root(true)) . "front_end/start/#/token";
            $token = (new Login)->token($find_user["user_id"]);
            $url .= "/" . http_build_query([
                "TOKEN" => $token,
            ]);
            return redirect($url);
        } else {
            // $view = new View();
            // $view->assign('data', [
            //     "msg" => "您已被禁止登陆",
            // ]);
            // return $view->fetch();

            echo "您已被禁止登陆";
        }
    }
    /*
    array(14) {
    ["openid"] => string(28) "oM3Ms6c46bZgNia-cxSKlCFAh8I0"
    ["nickname"] => string(3) "zhi"
    ["sex"] => int(1)
    ["language"] => string(5) "zh_CN"
    ["city"] => string(6) "佛山"
    ["province"] => string(6) "广东"
    ["country"] => string(6) "中国"
    ["headimgurl"] => string(129) "http://thirdwx.qlogo.cn/mmopen/vi_32/kQuPz4HLlnSWgo5aQ8YuaMfN8kWjBrbhVdoDgMBibNomQE0COsBL1UYKPEQgfoZZT12T5T6snogxMLCPIqvUoiaA/132"
    ["privilege"] => array(0) {
    }
    ["unionid"] => string(28) "oR2y-5qxQnhvpN29-gifCXUXeS1Q"
    ["access_token"] => string(89) "29_xCtBb38ds4exQH5WYDpS6DXv3VKEr6eJC9Osb5hQxnJFYibMGlJpksq3jOj4NezTv01_xXJhANIxzPok_RSpbQ"
    ["expires_in"] => int(7200)
    ["refresh_token"] => string(89) "29_fAvkryoxIAA6ZKa-T68-z-qtWq-cG-Fl1mtfDI6XCmKlj0bgR-Nkej88mNLS-EVrBZ0PC7axmJu44EoWxqz3fg"
    ["scope"] => string(15) "snsapi_userinfo"
    }
     */
    public function authorize()
    {
        return (new WechatGzh)->authorize([
            "scope" => "snsapi_userinfo",
            // "redirect_uri" => "https://mini.dszjjt.com/training/public/index.php/api/gzh/authorize"
        ], function ($userinfo) {
            $sex_data = ["", "男", "女"];
            $unionid = isset($userinfo["unionid"]) ? $userinfo["unionid"] : "";
            $openid = isset($userinfo["openid"]) ? $userinfo["openid"] : "";
            $sex = isset($userinfo["sex"]) ? $userinfo["sex"] : "";

            $user_headimage = isset($userinfo["headimgurl"]) ? $userinfo["headimgurl"] : "";
            $nickname = isset($userinfo["nickname"]) ? $userinfo["nickname"] : "";

            $user = Db::name("user");
            $find_user = Db::name("user")->where([
                "user_openid_gzh" => $openid,
                // "user_unionid" => $unionid,
                "user_delete" => 1,
            ])->field([
                "user_id",
                "user_status",
            ])->find();
            if ($find_user) {
                if ($find_user["user_status"] == 2) {
                    $request = Request::instance();
                    // $url = str_replace("index.php", "wx_gzh.html", $request->root(true));
                    $url = str_replace("index.php", "", $request->root(true)) . "front_end/start/#/token";
                    $token = (new Login)->token($find_user["user_id"]);
                    $url .= "/" . http_build_query([
                        "TOKEN" => $token,
                    ]);
                    return redirect($url);
                } else {
                    // $view = new View();
                    // $view->assign('data', [
                    //     "msg" => "您已被禁止登陆",
                    // ]);
                    // return $view->fetch();
                    echo "您已被禁止登陆";
                }
            } else {
                $request = Request::instance();
                $data = [];
                $data["user_name"] = $nickname;
                $data["user_headimage"] = $user_headimage;
                $data["user_openid_gzh"] = $openid;
                $data["user_unionid"] = $unionid;
                $data["user_guid"] = $this->get_guid();
                $data["user_status"] = 2;
                $data["user_delete"] = 1;
                $data["user_admin"] = 2;
                if ($sex) {
                    $data["user_sex"] = $sex_data[$sex];
                }
                Session::set("userinfo", $data);
                $url = str_replace("index.php", "", $request->root(true)) . "front_end/start/#/bind_phone";
                return redirect($url);
                // $insertGetId_user = $user->insertGetId($data);
                // if ($insertGetId_user) {
                //     $request = Request::instance();
                //     // $url = str_replace("index.php", "wx_gzh.html", $request->root(true));
                //     $url = str_replace("index.php", "", $request->root(true)) . "front_end/start/#/token";
                //     $url .= "/" . http_build_query([
                //         "TOKEN" => (new Login)->token($insertGetId_user),
                //     ]);
                //     return redirect($url);
                // } else {
                //     $view = new View();
                //     $view->assign('data', [
                //         "msg" => "添加用户失败",
                //     ]);
                //     return $view->fetch();
                // }
            }
        });
    }

    public function jssdk()
    {
        return (new WechatGzh)->Jssdk(input("url"));
    }

    public function token()
    {
        $re = [];
        try {
            $userinfo = $this->UserInfo;
            if ($userinfo["user_phone"] && $userinfo["user_openid_gzh"]) {
                if ($userinfo["role_id"] && $userinfo["organize_id"]) {
                    switch ($userinfo["role_name"]) {
                        case '老师':
                            $re["code"] = 10000;
                            $re["action"] = "teacher";
                            $re["msg"] = "ok";
                            break;
                        case '学生':
                            $re["code"] = 10000;
                            $re["action"] = "student";
                            $re["msg"] = "ok";
                            break;
                        default:
                            $re["code"] = 10000;
                            $re["action"] = "error";
                            $re["msg"] = "角色错误" . $userinfo["role_name"];
                            break;
                    }
                    $re["userinfo"] = [
                        "user_name" => $userinfo["user_name"],
                        "user_headimage" => $userinfo["user_headimage"],
                        "role_name" => $userinfo["role_name"],
                        // "enterprise_position_name" => $userinfo["enterprise_position_name"],
                    ];
                } else {
                    $re["code"] = 10000;
                    $re["action"] = "connect_admin";
                    $re["msg"] = "请联系系统管理员绑定角色和组织";
                }
            } else {
                $re["code"] = 10000;
                $re["action"] = "bind_phone";
                $re["msg"] = "请绑定手机号";
            }
            $re["u"] = $userinfo;
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function send_sms()
    {
        $re = [];
        try {
            $post = input("param.");
            $GzhValidate = new GzhValidate();
            if ($GzhValidate->scene("send_sms")->check($post)) {
                $sms_code = mt_rand(1000, 9999);
                $time = time();
                $data = [];
                $data["sms_phone"] = $post["phone"];
                $data["sms_code"] = $sms_code;
                $data["sms_guid"] = $this->get_guid();
                $data["sms_time"] = $time + (60 * 5);
                $insert_sms = Db::name("sms")->insert($data);
                if ($insert_sms) {
                    $re["code"] = 10000;
                    $re["msg"] = "短信发送成功";
                } else {
                    $re["code"] = 40002;
                    $re["msg"] = "短信发送失败";
                }
            } else {
                $re["code"] = 40001;
                $re["msg"] = $GzhValidate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function bind_phone()
    {
        $re = [];
        try {
            $post = input("param.");
            $GzhValidate = new GzhValidate();
            if ($GzhValidate->scene("bind_phone")->check($post)) {
                if ($this->check_sms($post["phone"], $post["code"])) {
                    $user_id = Db::name("user")->where([
                        "user_phone" => $post["phone"]
                    ])->value("user_id");
                    if (!$user_id) {
                        $re["code"] = 40003;
                        $re["msg"] = "手机号不存在，请联系管理原添加";
                    } else {
                        $userinfo = Session::get("userinfo");
                        dump($userinfo);
                        $update_user = Db::name("user")->where([
                            "user_id" => $user_id,
                        ])->update(array_merge($userinfo, [
                            "user_phone" => $post["phone"],
                        ]));
                        if ($update_user === false) {
                            $re["code"] = 40003;
                            $re["msg"] = "注册失败，请稍后重试";
                        } else {
                            Session::delete("userinfo");
                            $re["code"] = 10000;
                            $re["msg"] = "注册成功";
                        }
                    }
                } else {
                    $re["code"] = 40002;
                    $re["msg"] = "验证码错误";
                }
            } else {
                $re["code"] = 40001;
                $re["msg"] = $GzhValidate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function userinfo()
    {
        $re = [];
        try {
            $userinfo = $this->UserInfo;
            $re["code"] = 10000;
            $data = [];
            switch ($userinfo["role_name"]) {
                case '学生':
                    $teacher_name = Db::name("user")->join([
                        ["user_role", "user_role.user_id = user.user_id"],
                        ["role", "role.role_id = user_role.role_id"],

                        ["user_organize", "user_organize.user_id = user.user_id"],
                        ["organize", "organize.organize_id = user_organize.organize_id"],
                    ])->where([
                        "role.role_name" => "老师",
                        "organize.organize_id" => $userinfo["organize_id"],
                    ])->value("user_name");

                    $data["top_organize_name"] = Db::name("organize")->where([
                        "organize_id" => $userinfo["top_organize_id"],
                    ])->value("organize_name");

                    $find_sign_in = Db::name("sign_in")->where([
                        "sign_in_time" => ["between", [
                            strtotime(date("Y/m/d 00:00:00")),
                            strtotime(date("Y/m/d 23:59:59")),
                        ]],
                        "user_id" => $userinfo["user_id"],
                    ])->find();

                    $data["today_sign_in"] = $find_sign_in ? true : false;
                    $data["teacher_name"] = $teacher_name;
                    $data["user_name"] = $userinfo["user_name"];
                    $data["user_headimage"] = $userinfo["user_headimage"];
                    $data["user_number"] = $userinfo["user_number"];

                    $data["organize_name"] = $userinfo["organize_name"];
                    $data["organize_note"] = $userinfo["organize_note"];
                    $data["organize_internship_start"] = $userinfo["organize_internship_start"];
                    $data["organize_internship_end"] = $userinfo["organize_internship_end"];

                    $data["enterprise_name"] = $userinfo["enterprise_name"];
                    $data["enterprise_district"] = $userinfo["enterprise_district"];
                    $data["enterprise_address"] = $userinfo["enterprise_district"];

                    $data["enterprise_position_user_summary"] = $userinfo["enterprise_position_user_summary"];
                    $data["enterprise_position_name"] = $userinfo["enterprise_position_name"];

                    break;

                default:
                    # code...
                    break;
            }
            $re["data"] = $data;
            $re["u"] = $userinfo;
            $re["msg"] = "ok";
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    protected function check_sms($phone, $code)
    {
        if ($code == "0258") {
            return true;
        } else {
            $sms_code = Db::name("sms")->where([
                "sms_phone" => $phone,
                "sms_time" => [
                    ">=", time()
                ]
            ])->value("sms_code");
            return $sms_code == $code;
        }
    }
}
