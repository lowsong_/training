<?php
namespace app\notice\validate;

use think\Validate;

class Notice extends Validate
{
    protected $rule = [
        "notice_id|id" => "require",
        "notice_title|公告标题" => "require",
        "notice_content|公告内容" => "require",
    ];

    protected $message = [
    ];

    protected $scene = [
        'add' => ['notice_title','notice_content'],
        'edit' => ['notice_id','notice_title','notice_content'],
        'status' => ['notice_id'],
        'delete' => ['notice_id'],
    ];

}
