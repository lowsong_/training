<?php

namespace app\notice\controller;

use Adjfut\Common;
use Adjfut\Tool\Tool;
use app\notice\validate\Notice as validate;
use think\Db;
use think\Config;
use think\Request;

class Notice extends Common
{

    // public function notice_upload(){
    //     return json(Tool::Upload());
    // }
    public function notice_upload()
    {
        $re = [];
        $request = Request::instance();
        $file = $request->file();
        $uploads_path = Config::get("uploads_path");
        $web_resources_uploads = Config::get("web_resources_uploads");
        foreach ($file as $file_value) {
            if ($file_value) {
                $file_name = $file_value->getInfo()['name'];
                $info = $file_value->move($uploads_path);
                if ($info) {
                    $save_name = $info->getSaveName();
                    array_push($re, [
                        "success" => true,
                        "info" => [
                            "save_name" => $save_name,
                            "file_name" => $file_name,
                            "web_resources_uploads" => $web_resources_uploads,
                        ],
                        "msg" => "上传成功",
                    ]);
                } else {
                    array_push($re, [
                        "success" => false,
                        "msg" => $file->getError(),
                    ]);
                }
            }
        }
        return $re;
    }

    public function notice()
    {
        $re = [];
        try {
            $page = input("page");
            $limit = input("limit");
            $notice_title = input('get.notice_title');
            $join = [
                ["user","user.user_id = notice.user_id"],
                ['organize','organize.organize_id = notice.organize_id']
            ];
            $con = [];
            $con["notice_delete"] = "启用";
            $con['organize.organize_id'] = $this->UserInfo['top_organize_id'];
            if ($notice_title) {
                $con['notice_title'] = ['like', '%' . $notice_title . '%']; //公告标题
            }
            $field = [];
            $field = [
                "notice_id","notice_title","notice_content","notice_file_name","notice_file_path","notice_status","user.user_name"
            ];
            $select_notice = Db::name("notice")->join($join)->where($con)->field($field)->page($page, $limit)->select();
            $count_notice = Db::name("notice")->join($join)->where($con)->page($page, $limit)->count();
            if ($select_notice && $count_notice) {
                $re["code"] = 0;
                $re["data"] = $select_notice;
                $re['userinfo'] = $this->UserInfo;
                $re["count"] = $count_notice;
                $re["msg"] = "ok";
            } else {
                $re["code"] = 40000;
                $re["msg"] = "暂无数据";
            }
        } catch (\Throwable $th) {
            $re["code"] = 40004;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function notice_add()
    {
        $re = [];
        try {
            $post = input("post.");
            $validate = new validate();
            if ($validate->scene("add")->check($post)) {
                $post["user_id"] = $this->UserInfo["user_id"];
                $post["notice_status"] = "启用";
                $post["notice_delete"] = "启用";
                $post["notice_time"] = time();
                $post["notice_guid"] = $this->get_guid();
                // foreach($this->UserInfo["user_organize"] as $val){
                //     $post["organize_id"] = $val;
                //     $insert_notice = Db::name("notice")->insert($post);
                // }
                $post['organize_id'] = $this->UserInfo['top_organize_id'];
                $insert_notice = Db::name("notice")->insert($post);
                if ($insert_notice === false) {
                    $re["code"] = 40000;
                    $re["msg"] = "发布失败";
                } else {
                    $re["code"] = 0;
                    $re["msg"] = "发布成功";
                }
            } else {
                $re["code"] = 40001;
                $re["msg"] = $validate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40004;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function notice_edit()
    {
        $re = [];
        try {
            $post = input("post.");
            $validate = new validate();
            if ($validate->scene("edit")->check($post)) {
                $update_notice = Db::name("notice")->where([
                    "notice_id" => $post["notice_id"],
                ])->update($post);
                if ($update_notice === false) {
                    $re["code"] = 40000;
                    $re["msg"] = "更新失败";
                } else {
                    $re["code"] = 0;
                    $re["msg"] = "更新成功";
                }
            } else {
                $re["code"] = 40001;
                $re["msg"] = $validate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40004;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function notice_status()
    {
        $re = [];
        try {
            $post = Tool::Input(input("post."));
            $validate = new validate();
            if ($validate->scene("status")->check($post)) {
                $notice_id = $post["notice_id"];

                $update_notice = Db::name("notice")->where([
                        "notice_id" => $notice_id,
                        "notice_delete" => "启用",
                    ])->exp('notice_status', 'IF(notice_status = "启用","停用","启用")')
                    ->update();
                if ($update_notice === false) {
                    $re["code"] = 40000;
                    $re["msg"] = "修改状态失败";
                } else {
                    $re["code"] = 0;
                    $re["msg"] = "修改状态成功";
                }
            } else {
                $re["code"] = 40001;
                $re["msg"] = $validate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40004;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function notice_delete()
    {
        $re = [];
        try {
            $post = Tool::Input(input("post."));
            $validate = new validate();
            if ($validate->scene("status")->check($post)) {
                $notice_id = $post["notice_id"];

                $update_notice = Db::name("notice")
                    ->where([
                        "notice_id" => $notice_id,
                        "notice_delete" => "启用",
                    ])
                    ->update([
                        "notice_delete" => "停用",
                    ]);
                if ($update_notice === false) {
                    $re["code"] = 40000;
                    $re["msg"] = "删除失败";
                } else {
                    $re["code"] = 0;
                    $re["msg"] = "删除成功";
                }
            } else {
                $re["code"] = 40001;
                $re["msg"] = $validate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40004;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }
}
