<?php

namespace app\sign_in\controller;

use Adjfut\Common;
use Adjfut\Tool\Tool;
use app\sign_in\validate\SignIn as SignInValidate;
use think\Db;
use think\Config;
use think\Request;

class SignIn extends Common
{
	public function get_time($time_range)
	{
		$start_time = '';
		$end_time = '';
		$week = date('w');
		//如果是周日回返回零，若一周从周日开始则不需要该语句
		if($week == 0){
			$week = 7;
		}
		if($time_range == 0){
			//今天时间戳
			$start_time = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
			$end_time = mktime(0, 0, 0, date('m'), date('d') + 1, date('Y')) - 1;
		}else if($time_range == 1){
			//昨天时间戳
			$start_time = mktime(0, 0, 0, date('m'), date('d') - 1, date('Y'));
			$end_time = mktime(0, 0, 0, date('m'), date('d'), date('Y')) - 1;
		}else if($time_range == 2){
			//本周时间戳
			$start_time = mktime(0, 0, 0, date('m'), date('d') - $week + 1 - 0, date('Y'));
			$end_time = mktime(23, 59, 59, date('m'), date('d') - $week + 7 - 0, date('Y'));
		}else if($time_range == 3){
			//上周时间戳
			$start_time = mktime(0, 0, 0, date('m'), date('d') - $week + 1 - 7, date('Y'));
			$end_time = mktime(23, 59, 59, date('m'), date('d') - $week + 7 - 7, date('Y'));
		}else if($time_range == 4){
			//本月时间戳
			$start_time = mktime(0, 0, 0, date('m'), 1, date('Y'));
			$end_time = mktime(23, 59, 59, date('m'), date('t'), date('Y'));
		}else if($time_range == 5){
			//上月时间戳
			$start_time = mktime(0, 0, 0, date('m') - 1, 1, date('Y'));
			$end_time = mktime(23, 59, 59, date('m') - 1, cal_days_in_month(CAL_GREGORIAN, date('m') - 1, date('Y')), date('Y'));
		}else{
			$arr = explode(" - ",$time_range);
			$start_time = strtotime($arr[0]);
			$end_time = strtotime($arr[1]);
		}

		if($start_time && $end_time !== ''){
			$time = $end_time - $start_time;
			$day = $time/(3600*24);//计算时间段的天数

			$time_arr = array(
				'start_time' => $start_time,
				'end_time' => $end_time,
				'day' => $day
			);
			return $time_arr;
		}else{
			return false;
		}
	}

	public function sign_enterprise_list()
	{
		$re = [];
		$get = Tool::Input(input('get.'));
		$SignInValidate = new SignInValidate();
		$UserInfo = $this->UserInfo;
		if ($SignInValidate->scene("choose_time")->check($get)) {
			try {
				$sign_count = 0;
				$not_sign_count = 0;

				$page = input("page");
				$limit = input("limit");

				if(array_key_exists("time_range",$get)){
					$time_range = $get["time_range"];
				}else{
					$time_range = 0;
				}
				$time_arr = $this->get_time($time_range);
				if($time_arr == false){
					$re["code"] = 40003;
					$re["msg"] = "查询失败";
				}else{
					$con = [];
					$con = [
						"enterprise_status" => "审核通过",
						"enterprise_delete" => "启用"
					];
					$field = [];
					$field = [
						"enterprise_name",
						"enterprise_id"
					];
					$select_enterprise = Db::name("enterprise")->where($con)->field($field)->page($page, $limit)->select();//查询企业
					$count_enterprise = Db::name("enterprise")->where($con)->count();

					foreach ($select_enterprise as $select_enterprise_key => &$select_enterprise_value) {
						$join = [];
						$join = [
							["enterprise", "enterprise_position.enterprise_id = enterprise.enterprise_id"],
							["enterprise_position_user", "enterprise_position_user.enterprise_position_id = enterprise_position.enterprise_position_id"],
							["user", "user.user_id = enterprise_position_user.user_id"],
							["user_organize", "user.user_id = user_organize.user_id"],
							["organize", "user_organize.organize_id = organize.organize_id"],
							["sign_in", "user_organize.user_id = sign_in.user_id"],
						];
						$con_sign_in = [];
						$con_sign_in = [
							"enterprise.enterprise_id" => $select_enterprise_value['enterprise_id'],
							"enterprise_position.enterprise_position_status" => "启用",
							"enterprise_position.enterprise_position_delete" => "启用",
							"user.user_status" => 2,
							"user.user_delete" => 1
						];
						$con_sign_in["organize.organize_id"] = [
							"in", $this->UserInfo["user_organize"],
						];
						
						$count_sign_in = Db::name("enterprise_position")->join($join)->where($con_sign_in)->where('sign_in.sign_in_time', 'between time', [$time_arr['start_time'], $time_arr['end_time']])->count();//查询打卡的数量
						$sign_count += $count_sign_in;//计算全校打卡数量

						$join = [];
						$join = [
							["enterprise", "enterprise_position.enterprise_id = enterprise.enterprise_id"],
							["enterprise_position_user", "enterprise_position_user.enterprise_position_id = enterprise_position.enterprise_position_id"],
							["user", "user.user_id = enterprise_position_user.user_id"],
							["sign_in", "user.user_id = sign_in.user_id"],
						];
						$con_count_sign_in = [];
						$con_count_sign_in = [
							"enterprise.enterprise_id" => $select_enterprise_value['enterprise_id'],
							"enterprise_position.enterprise_position_status" => "启用",
							"enterprise_position.enterprise_position_delete" => "启用",
							"user.user_status" => 2,
							"user.user_delete" => 1
						];
						$count_enterprise_position = Db::name("enterprise_position")->join($join)->where($con_count_sign_in)->count();
						$all_need_count_sign_in = $count_enterprise_position*$time_arr['day'];//每日需要打卡数量*天数(每天一次)

						$select_enterprise_value['count_enterprise_position'] = $count_enterprise_position;
						$select_enterprise_value['all_need_count_sign_in'] = $all_need_count_sign_in;//所需打卡数量
						$select_enterprise_value['count_sign_in'] = $count_sign_in;//已打卡数量

						$count_not_sign_in = $all_need_count_sign_in-$count_sign_in;
						$select_enterprise_value['count_not_sign_in'] = $count_not_sign_in;//未打卡数量
						if($all_need_count_sign_in == 0){
							$select_enterprise_value['count_sign_rate'] = 0;
						}else{
							$select_enterprise_value['count_sign_rate'] = $count_sign_in/$all_need_count_sign_in*100;
							$not_sign_count += $count_not_sign_in;
						}

						$school_sign_in_count = array(
							"sign_count" => $sign_count,
							"not_sign_count" => $not_sign_count
						);
					}
					if ($select_enterprise && $count_enterprise) {
						$re["code"] = 0;
						$re["data"] = $select_enterprise;
						$re["count"] = $count_enterprise;
						$re['data_school'] = $school_sign_in_count;
						$re["msg"] = "ok";
					} else {
						$re["code"] = 40002;
						$re["msg"] = "暂无数据";
					}
				}
			} catch (\Throwable $th) {
				$re["code"] = 40001;
				$re["msg"] = $th->getMessage();
			}
		} else {
		$re["code"] = 40001;
		$re["msg"] = $SignInValidate->getError();
		}
		return json($re);
	}

	public function sign_in_list(){
		$re = [];
		// $post = Tool::Input(input('post.'));
		$get = Tool::Input(input('get.'));
		$SignInValidate = new SignInValidate();
		if ($SignInValidate->scene("sign_in_list")->check($get)) {
			try {
				$page = input("page");
				$limit = input("limit");

				$join = [];
				$join = [
					["user","sign_in.user_id = user.user_id"],
					["enterprise_position_user","user.user_id = enterprise_position_user.user_id"],
					["enterprise_position","enterprise_position_user.enterprise_position_id = enterprise_position.enterprise_position_id"],
					["enterprise","enterprise.enterprise_id = enterprise_position.enterprise_id"],
					["user_organize","user_organize.user_id = user.user_id"],
					["organize","user_organize.organize_id = organize.organize_id"],
				];

				$con = [];
				$con = [
					"enterprise.enterprise_id" => $get['enterprise_id'],
					"enterprise_position.enterprise_position_status" => "启用",
					"enterprise_position.enterprise_position_delete" => "启用",
					"user_status" => 2,
					"user_delete" => 1,
					"organize_status" => 1,
					"organize_delete" => 1,
				];
				$con["organize.organize_id"] = [
					"in", $this->UserInfo["user_organize"],
				];
				$field = [];
				$field = [
					"enterprise_position_name",
					"enterprise_id",
					"organize_name",
					"user_id",
					"user_name",
					"sign_in_id",
					"sign_in_time",
				];
				$select_sign_in = Db::name("sign_in")->join($join)->where($con)->page($page, $limit)->select();//查询签到表
				$count_sign_in = Db::name("sign_in")->join($join)->where($con)->count();
				if ($select_sign_in && $count_sign_in) {
					$re["code"] = 0;
					$re["data"] = $select_sign_in;
					$re["count"] = $count_sign_in;
					$re["msg"] = "ok";
				} else {
					$re["code"] = 40002;
					$re["msg"] = "暂无数据";
				}
			} catch (\Throwable $th) {
				$re["code"] = 40001;
				$re["msg"] = $th->getMessage();
			}
		} else {
		$re["code"] = 40001;
		$re["msg"] = $SignInValidate->getError();
		}
		return json($re);
	}
}
