<?php
namespace app\sign_in\validate;

use think\Validate;

class SignIn extends Validate
{
    protected $rule = [
        "time_range|日期" => "require",
        "enterprise_id|id" => "require"
    ];

    protected $message = [
        // "role_price.number" =>
    ];

    protected $scene = [
        'choose_time' => ['time_range'],
        'sign_in_list' => ['enterprise_id']
    ];

}
