<?php

namespace app\enterprise\controller;

use Adjfut\Common;
use Adjfut\Tool\Excel;
use Adjfut\Tool\Tool;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use app\enterprise\validate\Enterprise as validate;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use think\Config;
use think\Db;

class Enterprise extends Common
{

	public function enterprise_upload(){
		return json(Tool::Upload());
	}

	public function enterprise()
	{
		$re = [];
		try {
			$page = input("page");
			$limit = input("limit");
			$con = [];
			input("enterprise_name")?$con["enterprise_name"] = ["like", "%".input('enterprise_name')."%"]:'';
			input("enterprise_status")?$con["enterprise_status"] = input("enterprise_status"):'';
			$con["enterprise_delete"] = "启用";
			$field = [];
			$field = [
				"enterprise_id","enterprise_logo","enterprise_name","enterprise_address","enterprise_introduction","enterprise_nature","enterprise_phone","enterprise_user_name","enterprise_points","enterprise_radius","enterprise_status","enterprise_remark"
			];
			$select_enterprise = Db::name("enterprise")->where($con)->field($field)->page($page, $limit)->select();
			$count_enterprise = Db::name("enterprise")->where($con)->count();
			if ($select_enterprise && $count_enterprise) {
				$re["code"] = 0;
				$re["data"] = $select_enterprise;
				$re["count"] = $count_enterprise;
				$re["msg"] = "ok";
			} else {
				$re["code"] = 40000;
				$re["msg"] = "暂无数据";
			}
		} catch (\Throwable $th) {
			$re["code"] = 40004;
			$re["msg"] = $th->getMessage();
		}
		return json($re);
	}

	public function enterprise_add()
	{
		$re = [];
		try {
			$post = input("post.");
			$validate = new validate();
			if ($validate->scene("add")->check($post)) {
				$con = [];
				$con['enterprise_name'] = $post['enterprise_name'];
				$con['enterprise_delete'] = '启用';
				$find_enterprise = Db::name("enterprise")->where($con)->find();
				if($find_enterprise){
					$re["code"] = 40002;
					$re["msg"] = "已存在该企业";
				}else{
					$post["enterprise_status"] = "审核中";
					$post["enterprise_delete"] = "启用";
					$post["enterprise_guid"] = $this->get_guid();
					$insert_enterprise = Db::name("enterprise")->insert($post);
					if ($insert_enterprise === false) {
						$re["code"] = 40000;
						$re["msg"] = "添加失败";
					} else {
						$re["code"] = 0;
						$re["msg"] = "添加成功";
					} 
				}
			} else {
				$re["code"] = 40001;
				$re["msg"] = $validate->getError();
			}
		} catch (\Throwable $th) {
			$re["code"] = 40004;
			$re["msg"] = $th->getMessage();
		}
		return json($re);
	}

	public function enterprise_edit()
	{
		$re = [];
		try {
			$post = input("post.");
			$validate = new validate();
			if ($validate->scene("edit")->check($post)) {
				$update_enterprise = Db::name("enterprise")->where([
					"enterprise_id" => $post["enterprise_id"],
				])->update($post);
				if ($update_enterprise === false) {
					$re["code"] = 40000;
					$re["msg"] = "更新失败";
				} else {
					$re["code"] = 0;
					$re["msg"] = "更新成功";
				}
			} else {
				$re["code"] = 40001;
				$re["msg"] = $validate->getError();
			}
		} catch (\Throwable $th) {
			$re["code"] = 40004;
			$re["msg"] = $th->getMessage();
		}
		return json($re);
	}

	public function load()
	{
		$re = [];
		try {
			$post = input("post.");
			$validate = new validate();
			if ($validate->scene("id")->check($post)) {
				$find_enterprise = Db::name("enterprise")->where([
					"enterprise_id" => $post["enterprise_id"],
				])->find();
				if ($find_enterprise) {
					$re["code"] = 0;
					$re["data"] = $find_enterprise;
					$re["msg"] = "ok";
				} else {
					$re["code"] = 40000;
					$re["msg"] = "暂无数据";
				}
			} else {
				$re["code"] = 40001;
				$re["msg"] = $validate->getError();
			}
		} catch (\Throwable $th) {
			$re["code"] = 40004;
			$re["msg"] = $th->getMessage();
		}
		return json($re);
	}

	// public function enterprise_status()
	// {
	//     $re = [];
	//     try {
	//         $post = Tool::Input(input("post."));
	//         $validate = new validate();
	//         if ($validate->scene("id")->check($post)) {
	//             $enterprise_id = $post["enterprise_id"];

	//             $update_enterprise = Db::name("enterprise")->where([
	//                     "enterprise_id" => $enterprise_id,
	//                     "enterprise_delete" => "启用",
	//                 ])->exp('enterprise_status', 'IF(enterprise_status = "启用","禁用","启用")')
	//                 ->update();
	//             if ($update_enterprise === false) {
	//                 $re["code"] = 40000;
	//                 $re["msg"] = "修改状态失败";
	//             } else {
	//                 $re["code"] = 0;
	//                 $re["msg"] = "修改状态成功";
	//             }
	//         } else {
	//             $re["code"] = 40001;
	//             $re["msg"] = $validate->getError();
	//         }
	//     } catch (\Throwable $th) {
	//         $re["code"] = 40004;
	//         $re["msg"] = $th->getMessage();
	//     }
	//     return json($re);
	// }

	public function enterprise_delete()
	{
		$re = [];
		try {
			$post = Tool::Input(input("post."));
			$validate = new validate();
			if ($validate->scene("id")->check($post)) {
				$enterprise_id = $post["enterprise_id"];
				//如果实习单位已有实习生则不允许删除
				$join = [];
				$join = [
					['enterprise_position','enterprise_position.enterprise_id = enterprise.enterprise_id'],
					['enterprise_position_user','enterprise_position_user.enterprise_position_id = enterprise_position.enterprise_position_id']
				];
				$con = [];
				$con['enterprise.enterprise_id'] = $enterprise_id;
				$find_stu = Db::name('enterprise')->alias('enterprise')->where($con)->join($join)->find();
				if($find_stu){
					$re['code'] = '400002';
					$re['msg'] = '该实习单位已存在实习生';
				}else{
					$update_enterprise = Db::name("enterprise")
						->where([
							"enterprise_id" => $enterprise_id,
							"enterprise_delete" => "启用",
						])
						->update([
							"enterprise_delete" => "禁用",
						]);
					if ($update_enterprise === false) {
						$re["code"] = 40000;
						$re["msg"] = "删除失败";
					} else {
						$re["code"] = 0;
						$re["msg"] = "删除成功";
					}
				}
			} else {
				$re["code"] = 40001;
				$re["msg"] = $validate->getError();
			}
		} catch (\Throwable $th) {
			$re["code"] = 40004;
			$re["msg"] = $th->getMessage();
		}
		return json($re);
	}
	//企业导入模板
	public function enterprise_template()
    {
        try {
            $data = [
                ["企业名字", "企业半径","企业定位点", "企业区域", "企业地址","企业简介", "企业性质", "联系电话","企业负责人"],
                ["xx有限公司", "200","113.272828,22.758876", "广东省佛山市顺德区", "容桂街道文明路40号","本公司位于...", "私营企业", "13516526182","林某",],
            ];
            return Excel::export("企业导入数据模板", $data, function ($worksheet, $data) {
                $end_key = Coordinate::stringFromColumnIndex(count($data[0]));
                $worksheet->getStyle('A:' . $end_key)->applyFromArray([
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                        'wrapText' => true,
                    ],
                ]);
                $worksheet->getDefaultColumnDimension()->setWidth(25);
                $worksheet->getDefaultRowDimension()->setRowHeight(25);
                return $worksheet;
            });
        } catch (\Throwable $th) {
            echo $th->getMessage();
        }
	}
	public function enterprise_import()
    {
        $re = [];
        try {
            $file = input("file.file");
            if ($file) {
                $info = $file->validate([
                    'size' => Config::get("upload.size"),
                    'ext' => 'xlsx',
                ])->move(Config::get("upload.save_path"));
                if ($info) {
                    $save_path = $info->getRealPath();
                    $spreadsheet = IOFactory::load($save_path);
                    $worksheet = $spreadsheet->getActiveSheet();
                    $excel_array = $worksheet->toarray();
                    $judge_title = Excel::judge_title($excel_array, [
						"企业名字", "企业半径", "企业定位点", "企业区域", "企业地址",
						"企业简介","企业性质","联系电话","企业负责人"
                    ]);
                    if ($judge_title) {
                        array_shift($excel_array);
                        $excel_array = Excel::clear_empty_line($excel_array);
                        list($error, $data) = Excel::judge_field($excel_array, [
                            "enterprise_name" => [
                                "validate" => [
                                    "rule" => "require",
                                    "msg" => [
                                        "require" => "企业名字不能为空",
                                    ],
                                ],
                            ],
                            "enterprise_radius" => [
                                "validate" => [
                                    "rule" => "require",
                                    "msg" => [
                                        "require" => "企业半径不能为空",
                                    ],
                                ],
                            ],
                            "enterprise_points" => [
                                "validate" => [
                                    "rule" => "require",
                                    "msg" => [
                                        "require" => "企业定位点不能为空",
                                    ],
                                ],
							],
							"enterprise_district" => [
                                "validate" => [
                                    "rule" => "require",
                                    "msg" => [
                                        "require" => "企业区域不能为空",
                                    ],
                                ],
							],
							"enterprise_address" => [
                                "validate" => [
                                    "rule" => "require",
                                    "msg" => [
                                        "require" => "企业地址不能为空",
                                    ],
                                ],
							],
							"enterprise_introduction" => [
                                "validate" => [
                                    "rule" => "require",
                                    "msg" => [
                                        "require" => "企业简介不能为空",
                                    ],
                                ],
							],
							"enterprise_nature" => [
                                "validate" => [
                                    "rule" => "require",
                                    "msg" => [
                                        "require" => "企业性质不能为空",
                                    ],
                                ],
							],
							"enterprise_phone" => [
                                "validate" => [
                                    "rule" => "require|length:11",
                                    "msg" => [
                                        "require" => "企业联系电话不能为空",
                                        "length" => "手机号长度错误",
                                    ],
                                ],
                            ],
							"enterprise_user_name" => [
                                "validate" => [
                                    "rule" => "require",
                                    "msg" => [
                                        "require" => "企业负责人不能为空",
                                    ],
                                ],
							],
                        ]);
                        if (count($error) === 0) {
                            Db::startTrans();
                            $enterprise = Db::name("enterprise");
                            foreach ($data as $data_value) {
                                //企业
                                $data_value["enterprise_id"] = $enterprise->where([
                                    "enterprise_name" => $data_value["enterprise_name"],
                                ])->value("enterprise_id");
                                if (!$data_value["enterprise_id"]) {
                                    $data_value["enterprise_id"] = $enterprise->insertGetId([
                                        "enterprise_name" => $data_value["enterprise_name"],
										"enterprise_radius" => $data_value["enterprise_radius"],
										"enterprise_points" => $data_value["enterprise_points"],
										"enterprise_district" => $data_value["enterprise_district"],
										"enterprise_address" => $data_value["enterprise_address"],
										"enterprise_introduction" => $data_value["enterprise_introduction"],
										"enterprise_nature" => $data_value["enterprise_nature"],
										"enterprise_phone" => $data_value["enterprise_phone"],
										"enterprise_user_name" => $data_value["enterprise_user_name"],
										"enterprise_delete" => '启用',
                                        "enterprise_guid" => $this->get_guid()
                                    ]);
                                }
                            }
                            Db::commit();
							$re["code"] = 10000;
							$re['msg'] = '导入成功';
                        } else {
                            $re["code"] = 40005;
                            $re["msg"] = join("<br>", $error);
                        }
                    } else {
                        $re["code"] = 40004;
                        $re["msg"] = "表格错误";
                    }
                } else {
                    $re["code"] = 40003;
                    $re["msg"] = $file->getError();
                }
            } else {
                $re["code"] = 40002;
                $re["msg"] = "请上传文件";
            }
        } catch (\Throwable $th) {
            $re["code"] = 40001;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }
    public function download()
    {
        $enterprise = Db::name('enterprise');

        $con = [];
        $con['enterprise_delete'] = '启用';

        $select_enterprise = $enterprise->where($con)->select();

        if ($select_enterprise) {
            $file = ROOT_PATH . "public" . DS . 'layui' . DS . 'start' . DS . 'static' . DS . 'excel' . DS . "enterprise.xlsx";

            $spreadsheet = IOFactory::load($file);

            $worksheet = $spreadsheet->getActiveSheet();

            $key = 2;
            $index = 1;

            foreach ($select_enterprise as $select_enterprise_key => $select_enterprise_value) {
                $worksheet->getCell("A" . ($select_enterprise_key + $key))->setValue($select_enterprise_value['enterprise_name']); //企业名字
                $worksheet->getCell("B" . ($select_enterprise_key + $key))->setValue($select_enterprise_value['enterprise_radius']); //企业半径
                $worksheet->getCell("C" . ($select_enterprise_key + $key))->setValue($select_enterprise_value['enterprise_points']); //企业定位点
                $worksheet->getCell("D" . ($select_enterprise_key + $key))->setValue($select_enterprise_value['enterprise_district']); //企业区域
                $worksheet->getCell("E" . ($select_enterprise_key + $key))->setValue($select_enterprise_value['enterprise_address']); //企业地址
                $worksheet->getCell("F" . ($select_enterprise_key + $key))->setValue($select_enterprise_value['enterprise_introduction']); //企业简介
                $worksheet->getCell("G" . ($select_enterprise_key + $key))->setValue($select_enterprise_value['enterprise_nature']); //企业性质
                $worksheet->getCell("H" . ($select_enterprise_key + $key))->setValue($select_enterprise_value['enterprise_phone']); //企业联系电话
                $worksheet->getCell("I" . ($select_enterprise_key + $key))->setValue($select_enterprise_value['enterprise_user_name']); //企业负责人
                $index++;
            }

            $worksheet->getStyle("A1:I1")->applyFromArray([
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                ],
            ]);

            $worksheet->getStyle("A$key:D$index")->applyFromArray([
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                ],
            ]);

            $writer = IOFactory::createWriter($spreadsheet, "Xls");
            header("Content-Type: application/force-download");
            header("Content-Type: application/octet-stream");
            header("Content-Type: application/download");
            header('Content-Disposition:inline;filename="企业资料导出表.xlsx"');
            $writer->save('php://output');
            if ($writer) {
                $re['code'] = 10000;
                $re['msg'] = '导出成功';
            } else {
                $re['code'] = 40006;
                $re['msg'] = '导出失败';
            }
            exit;
        } else {
            $re['code'] = 101;
            $re['msg'] = '暂无数据';
        }
    }
	
}
