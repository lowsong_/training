<?php

namespace app\enterprise\controller;

use Adjfut\Common;
use Adjfut\Tool\Tool;
use app\enterprise\validate\Position as validate;
use think\Db;

class Position extends Common
{

    public function position_upload()
    {
        return json(Tool::Upload());
    }

    public function enterprise_tree()
    {
        $re = [];
        try {
            $select_enterprise = Db::name("enterprise")->where([
                // "enterprise_status" => 1,
                "enterprise_delete" => "启用",
            ])->field([])->select();
            if ($select_enterprise) {
                $data = [];
                foreach ($select_enterprise as $select_enterprise_value) {
                    array_push($data, [
                        "id" => $select_enterprise_value["enterprise_id"],
                        "title" => $select_enterprise_value["enterprise_name"],
                        "data" => $select_enterprise_value,
                    ]);
                }
                $re["code"] = 0;
                $re["data"] = [[
                    "title" => "企业列表",
                    "id" => time(),
                    "data" => [],
                    "spread" => true,
                    "children" => $data,
                ]];
            } else {
                $re["code"] = 400001;
                $re["msg"] = "暂无企业";
            }
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function position()
    {
        $re = [];
        try {
            $page = input("page");
            $limit = input("limit");
            $join = [];
            $join = [
                ['enterprise', 'enterprise_position.enterprise_id = enterprise.enterprise_id'],
            ];
            $con = [];
            input("enterprise_id") ? $con["enterprise_position.enterprise_id"] = input("enterprise_id") : '';
            input("enterprise_position_name") ? $con["enterprise_position_name"] = ["like", "%" . input('enterprise_position_name') . "%"] : '';
            $con["enterprise_delete"] = "启用";
            $con["enterprise_position_delete"] = "启用";
            $field = [];
            $field = [
                "enterprise_position_id", "enterprise_position_name", "enterprise_position_introduction", "enterprise_position_treatment", "enterprise_position_number", "enterprise_position_salary_min", "enterprise_position_salary_max", "enterprise_position_status", "enterprise_position.enterprise_id", "enterprise_name"
            ];
            $select_position = Db::name("enterprise_position")->join($join)->where($con)->field($field)->page($page, $limit)->select();
            $count_position = Db::name("enterprise_position")->join($join)->where($con)->count();
            if ($select_position && $count_position) {
                $con = [];
                $con["enterprise_delete"] = "启用";
                $field = [];
                $field = [
                    "enterprise_id", "enterprise_name"
                ];
                $select_enterprise = Db::name("enterprise")->where($con)->field($field)->page($page, $limit)->select();
                $re["code"] = 0;
                $re["data"] = $select_position;
                $re["enterprise"] = $select_enterprise;
                $re["count"] = $count_position;
                $re["msg"] = "ok";
            } else {
                $re["code"] = 40000;
                $re["msg"] = "暂无数据";
            }
        } catch (\Throwable $th) {
            $re["code"] = 40004;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }
    //企业名称查询
    public function enterprise_data(){
        $re = [];
        try {
            $con = [];
            $con['enterprise_delete'] = '启用';
            $field = [];
            $field = [
                'enterprise_id',
                'enterprise_name'
            ];
            $select_enterprise = Db::name("enterprise")->where($con)->field($field)->select();
            if ($select_enterprise) {
                $re["code"] = 0;
                $re["data"] = $select_enterprise;
                $re["msg"] = "ok";
            } else {
                $re["code"] = 40000;
                $re["msg"] = "暂无数据";
            }
        } catch (\Throwable $th) {
            $re["code"] = 40001;
            $re["msg"] = $th->getMessage();
        }
        return json($re);    
    }
    public function position_add()
    {
        $re = [];
        try {
            $post = input("post.");
            $validate = new validate();
            if ($validate->scene("add")->check($post)) {
                $con = [];
                $con['enterprise_position_name'] = $post['enterprise_position_name'];
                $con['enterprise_position_delete'] = '启用';
                $find_enterprise_position = Db::name('enterprise_position')->where($con)->find();
                if($find_enterprise_position){
                    $re['code'] = '400002';
                    $re['msg'] = '该企业已有相同职位';
                }else{
                    $post["enterprise_position_status"] = "启用";
                    $post["enterprise_position_delete"] = "启用";
                    $post["enterprise_position_guid"] = $this->get_guid();
                    $insert_position = Db::name("enterprise_position")->insert($post);
                    if ($insert_position === false) {
                        $re["code"] = 40000;
                        $re["msg"] = "新增职位失败";
                    } else {
                        $re["code"] = 0;
                        $re["msg"] = "新增职位成功";
                    }
                }
            } else {
                $re["code"] = 40001;
                $re["msg"] = $validate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40004;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function position_edit()
    {
        $re = [];
        try {
            $post = input("post.");
            $validate = new validate();
            if ($validate->scene("edit")->check($post)) {
                $update_position = Db::name("enterprise_position")->where([
                    "enterprise_position_id" => $post["enterprise_position_id"],
                ])->update($post);
                if ($update_position === false) {
                    $re["code"] = 40000;
                    $re["msg"] = "更新失败";
                } else {
                    $re["code"] = 0;
                    $re["msg"] = "更新成功";
                }
            } else {
                $re["code"] = 40001;
                $re["msg"] = $validate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40004;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function position_status()
    {
        $re = [];
        try {
            $post = Tool::Input(input("post."));
            $validate = new validate();
            if ($validate->scene("id")->check($post)) {
                $position_id = $post["enterprise_position_id"];

                $update_position = Db::name("enterprise_position")->where([
                    "enterprise_position_id" => $position_id,
                    "enterprise_position_delete" => "启用",
                ])->exp('enterprise_position_status', 'IF(enterprise_position_status = "启用","禁用","启用")')
                    ->update();
                if ($update_position === false) {
                    $re["code"] = 40000;
                    $re["msg"] = "修改状态失败";
                } else {
                    $re["code"] = 0;
                    $re["msg"] = "修改状态成功";
                }
            } else {
                $re["code"] = 40001;
                $re["msg"] = $validate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40004;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function position_delete()
    {
        $re = [];
        try {
            $post = Tool::Input(input("post."));
            $validate = new validate();
            if ($validate->scene("id")->check($post)) {
                $position_id = $post["enterprise_position_id"];

                $update_position = Db::name("enterprise_position")
                    ->where([
                        "enterprise_position_id" => $position_id,
                        "enterprise_position_delete" => "启用",
                    ])
                    ->update([
                        "enterprise_position_delete" => "禁用",
                    ]);
                if ($update_position === false) {
                    $re["code"] = 40000;
                    $re["msg"] = "删除失败";
                } else {
                    $re["code"] = 0;
                    $re["msg"] = "删除成功";
                }
            } else {
                $re["code"] = 40001;
                $re["msg"] = $validate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40004;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }
}
