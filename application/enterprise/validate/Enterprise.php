<?php
namespace app\enterprise\validate;

use think\Validate;

class Enterprise extends Validate
{
    protected $rule = [
        "enterprise_id|id" => "require",
        "enterprise_name|企业名字" => "require",
        "enterprise_logo|企业logo" => "require",
        "enterprise_address|企业地址" => "require",
        "enterprise_introduction|企业简介" => "require|max:250",
        "enterprise_nature|企业性质" => "require",
        "enterprise_phone|联系电话" => "require",
        "enterprise_user_name|企业负责人" => "require",
        "enterprise_points|企业定位点" => "require",
        "enterprise_radius|企业半径" => "require",
    ];

    protected $message = [
    ];

    protected $scene = [
        'add' => ['enterprise_name','enterprise_logo','enterprise_address','enterprise_introduction','enterprise_nature','enterprise_phone','enterprise_user_name','enterprise_points','enterprise_radius'],
        'edit' => ['enterprise_id','enterprise_name','enterprise_logo','enterprise_address','enterprise_introduction','enterprise_nature','enterprise_phone','enterprise_user_name','enterprise_points','enterprise_radius'],
        'id' => ['enterprise_id'],
    ];
}
