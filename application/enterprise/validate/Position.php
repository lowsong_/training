<?php
namespace app\enterprise\validate;

use think\Validate;

class Position extends Validate
{
    protected $rule = [
        "enterprise_position_id|id" => "require",
        "enterprise_id|企业名字" => "require",
        "enterprise_position_name|职位名称" => "require",
        "enterprise_position_introduction|职位简介" => "require",
        "enterprise_position_treatment|职位待遇" => "require|max:250",
        "enterprise_position_number|职位人数" => "require",
        "enterprise_position_salary_min|最小工资" => "require|float",
        "enterprise_position_salary_max|最大工资" => "require|float",
    ];

    protected $message = [
    ];

    protected $scene = [
        'add' => ['enterprise_id','enterprise_position_name','enterprise_position_introduction','enterprise_position_treatment','enterprise_position_number','enterprise_position_salary_min','enterprise_position_salary_max'],
        'edit' => ['enterprise_position_id','enterprise_id','enterprise_position_name','enterprise_position_introduction','enterprise_position_treatment','enterprise_position_number','enterprise_position_salary_min','enterprise_position_salary_max'],
        'id' => ['enterprise_position_id'],
    ];
}
