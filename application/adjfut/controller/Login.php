<?php

namespace app\adjfut\controller;

use Adjfut\Common;
use Adjfut\Jwt;
use Adjfut\Tool\Tool;
use app\adjfut\validate\Login as LoginValidate;
use think\Config;
use think\Db;
use think\Request;

class Login
{
    public $login = false;

    public function login()
    {
        $re = [];
        $post = Tool::Input(input('post.'));
        $LoginValidate = new LoginValidate();
        if ($LoginValidate->scene("login")->check($post)) {
            $account = $post['account'];
            $password = $post['password'];
            $password = self::password_encryption($password);
            $con = [];
            $con['user_account|user_name|user_number'] = $account;
            $find_user = Db::name('user')->where($con)->find();
            if ($find_user) {
                $user_id = $find_user['user_id'];
                $data = $this->user($user_id, $password);
                if ($data['code'] === 0) {
                    $access_token = $this->token($user_id);
                    $re['code'] = 0;
                    $re['data']['token'] = $access_token;
                    $re['msg'] = '登录成功';
                } else {
                    $re = $data;
                }
            } else {
                $re['code'] = 100;
                $re['msg'] = '用户不存在';
            }
        } else {
            $re["code"] = 40001;
            $re["msg"] = $LoginValidate->getError();
        }
        return json($re);
    }

    public function token(int $id)
    {
        $request = Request::instance();
        $token_exp_time = Config::get('token_exp_time');
        $time = time();
        $token = Db::name('token');
        $con = [];
        $con['user_id'] = $id;
        $find_token = $token->where($con)->find();

        if ($find_token) {
            if ($find_token["token_time"] > $time) {
                $token_content = $find_token["token_content"];
            } else {
                $token_content = Jwt::getToken([
                    'iss' => 'user',
                    'iat' => $time,
                    'exp' => $time + $token_exp_time,
                    'nbf' => $time,
                    'sub' => mt_rand(10000, 99999),
                    'jti' => md5(uniqid(Tool::random_str("3")) . $time),
                ]);
                $con = [];
                $con['token_id'] = $find_token['token_id'];
                $data = [];
                $data['token_content'] = $token_content;
                $data['token_time'] = $time + $token_exp_time;
                $update_token = $token->where($con)->update($data);
                if (!$update_token) {
                    throw new Exception('更新token失败', 500);
                }
            }
        } else {
            $token_content = Jwt::getToken([
                'iss' => 'user',
                'iat' => $time,
                'exp' => $time + $token_exp_time,
                'nbf' => $time,
                'sub' => mt_rand(10000, 99999),
                'jti' => md5(uniqid(Tool::random_str("3")) . $time),
            ]);
            $data = [];
            $data['token_content'] = $token_content;
            $data['token_time'] = $time + $token_exp_time;
            $data['user_id'] = $id;
            $data['token_guid'] = (new Common)->get_guid();
            $insert_token = $token->insert($data);
            if (!$insert_token) {
                throw new Exception('添加token失败', 500);
            }
        }
        return $token_content;
    }

    public static function password_encryption(string $string)
    {
        $password = strtoupper(md5($string));
        return $password;
    }

    public function user(int $user_id, string $password = '')
    {
        $re = [];
        $con = [];
        $con['user.user_id'] = $user_id;
        $con['user.user_delete'] = 1;
        // $con['organize.organize_status'] = 1;
        // $con['organize.organize_delete'] = 1;
        $find_user = Db::name('user')->join([
            ["user_organize", "user_organize.user_id = user.user_id", "left"],
            ["organize", [
                "organize.organize_id = user_organize.organize_id",
                "organize.organize_status = 1",
                "organize.organize_delete = 1",
            ], "left"],
            ["user_role", "user_role.user_id = user.user_id", "left"],
            ["role", [
                "role.role_id = user_role.role_id",
                "role.role_status = 1",
                "role.role_delete = 1",
            ], "left"],
            ["enterprise_position_user", "enterprise_position_user.user_id = user.user_id", "left"],
            ["enterprise_position", [
                "enterprise_position.enterprise_position_id = enterprise_position_user.enterprise_position_id",
                "enterprise_position.enterprise_position_status = '启用'",
                "enterprise_position.enterprise_position_delete = '启用'",
            ], "left"],
            ["enterprise", "enterprise_position.enterprise_id = enterprise.enterprise_id", "left"],
        ])->where($con)->field([
            "user.*",
            "organize.*",
            "role.*",
            "enterprise_position.*",
            "enterprise_position_user.enterprise_position_user_summary",
            "enterprise.*",
        ])->find();
        if (!$find_user) {
            $re['code'] = 101;
            $re['msg'] = '用户不存在';
            return $re;
        }
        if ($password) {
            if (!($find_user['user_password'] == $password)) {
                $re['code'] = 102;
                $re['msg'] = '密码错误';
                return $re;
            }
        }
        if (!($find_user['user_status'] == 2)) {
            $re['code'] = 103;
            $re['msg'] = '您已被禁止登录';
            return $re;
        }
        if ($find_user["organize_parent_id"]) {
            $organize_id = null;
            // $parent_id = null;
            $parent_id = $find_user["organize_parent_id"];
            $organize = Db::name("organize");
            do {
                $find_organize = $organize->where([
                    "organize_id" => $parent_id,
                ])->find();
                // if ($find_organize) {
                $organize_id = $find_organize["organize_id"];
                $parent_id = $find_organize["organize_parent_id"];
                // }
            } while ($parent_id != 0);
            $find_user["top_organize_id"] = $organize_id;
            unset($organize_id);
            unset($parent_id);
        }
        // $select_user_role = Db::name("user_role")->join([
        //     ["role", "role.role_id = user_role.role_id"],
        // ])->where([
        //     "user_role.user_id" => $user_id,
        //     "role.role_status" => 1,
        //     "role.role_delete" => 1,
        // ])->select();
        // $find_user["user_role"] = $select_user_role;

        // $find_user_organize = Db::name("user_organize")->join([
        //     ["organize", "organize.organize_id = user_organize.organize_id"],
        // ])->where([
        //     "user_organize.user_id" => $user_id,
        //     "organize.organize_status" => 1,
        //     "organize.organize_delete" => 1,
        // ])->find();
        // $find_user["organize"] = $find_user_organize;
        $find_user["user_organize"] = [];
        $select_organize = Db::name("organize")->where([
            "organize_status" => 1,
            "organize_delete" => 1,
        ])->select();
        if ($find_user["user_admin"] == 1) {
            $find_user["user_organize"] = array_map(function ($value) {
                return $value["organize_id"];
            }, $select_organize);
        } else {
            if ($find_user["organize_id"]) {
                $this->user_organize_children(
                    $select_organize,
                    $find_user["organize_id"],
                    $find_user["user_organize"]
                );
            }
        }
        // if (!in_array($find_user["top_organize_id"], $find_user["user_organize"])) {
        //     array_push($find_user["user_organize"], $find_user["top_organize_id"]);
        // }

        $re['code'] = 0;
        $re['data'] = $find_user;
        $re['msg'] = 'ok';
        return $re;
    }

    protected function user_organize_children(array $data, int $parent_id, array &$return)
    {
        foreach ($data as $data_value) {
            $organize_parent_id = $data_value["organize_parent_id"];
            $organize_id = $data_value["organize_id"];
            if ($organize_id == $parent_id) {
                if (!in_array($parent_id, $return)) {
                    array_push($return, $parent_id);
                }
            }
            if ($organize_parent_id == $parent_id) {
                if (!in_array($parent_id, $return)) {
                    array_push($return, $parent_id);
                }
                if (!in_array($organize_id, $return)) {
                    array_push($return, $organize_id);
                }
                $this->user_organize_children($data, $organize_id, $return);
            }
        }
    }
}
