<?php
namespace app\user\controller;

use Adjfut\Common;
use Adjfut\Tool\Tool;
use think\Db;

class Info extends Common
{
    public function personal(){
        $userinfo = $this->UserInfo;
        return json([
            "code" => 0,
            "data" => [
                "admin_name" => $userinfo["admin_name"],
                "admin_account" => $userinfo["admin_account"],
            ],
            "msg" => "ok"
        ]);
    }
}