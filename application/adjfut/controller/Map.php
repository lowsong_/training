<?php

namespace app\adjfut\controller;

use Adjfut\Common;
use Adjfut\Tool\Tool;
use think\Db;

class Map extends Common
{
    public function index_ok(){
        $user = $this->UserInfo;
        try {
            if($user['role_level'] == 8){
                $con = [];
                $con['equipment_delete'] = 1;
                $select_equipment = Db::name('equipment')->where($con)->select();
                $count_equipment = Db::name('equipment')->where($con)->count();
                if($select_equipment && $count_equipment){
                    $re["code"] = 0;
                    $re["data"] = $select_equipment;
                    $re["count"] = $count_equipment;
                    $re["msg"] = "ok";
                }else {
                    $re["code"] = 40000;
                    $re["msg"] = "暂无数据";
                }
            }
        } catch (\Throwable $th) {
            $re["code"] = 40001;
            $re["msg"] = $th->getMessage();
        }
        return $re;
        
        
    }
}
