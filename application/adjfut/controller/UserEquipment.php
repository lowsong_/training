<?php

namespace app\adjfut\controller;

use Adjfut\Common;
use Adjfut\Tool\Tool;
use Adjfut\Tool\Traverse;
use app\adjfut\validate\UserEquipment as validate;
use think\Db;

class UserEquipment extends Common
{
    public function user_equipment_tree()
    {
        $user = $this->UserInfo;
        $re = [];
        try {
            $con = [];
            $con["organize_status"] = 1;
            $con["organize_delete"] = 1;
            $con["organize_delete"] = 1;
            $field = [];
            $field = [
            ];
            $select_organize = Db::name("organize")->where($con)->field($field)->select();
            if ($select_organize) {
                $children = function (array $data, int $parent_id, $children) {
                    $re = [];
                    foreach ($data as $parent_data_key => $parent_data_value) {
                        if ($parent_data_value["organize_parent_id"] == $parent_id) {
                            $temporary = [
                                "title" => $parent_data_value["organize_name"],
                                "id" => $parent_data_value["organize_id"],
                                "data" => $parent_data_value,
                                "children" => [],
                                "spread" => true,
                            ];
                            foreach ($data as $children_data_key => $children_data_value) {
                                if ($children_data_value["organize_parent_id"] == $parent_data_value["organize_id"]) {
                                    $temporary["children"] = $children($data, $parent_data_value["organize_id"], $children);
                                }
                            }
                            $re[] = $temporary;
                        }
                    }
                    return $re;
                };
                $re["code"] = 0;
                $re["data"] = [[
                    "title" => "用户组织",
                    "id" => time(),
                    "spread" => true,
                    "data" => [
                        "organize_id" => 0,
                    ],
                    "children" => $children($select_organize, 0, $children),
                ]];
                $re["msg"] = "ok";
            } else {
                $re["code"] = 40000;
                $re["msg"] = "暂无数据";
            }
        } catch (\Throwable $th) {
            $re["code"] = 40004;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function user_equipment_list()
    {
        $re = [];
        try {
            $page = input("page");
            $limit = input("limit");
            $organize_id = input("organize_id");
            // dump($organize_id);
            $con = [];
            if ($organize_id) {
                //有organize_id
                $con["organize.organize_id"] = $organize_id;
                $select_organize = Db::name("organize")->where([
                    "organize_status" => 1,
                    "organize_delete" => 1,
                ])->field([
                    "organize_id",
                    "organize_parent_id",
                ])->select();
                
                $child_index = (new Traverse("organize_id","organize_parent_id"))->next($select_organize,$organize_id,true);
                array_push($child_index,(int)$organize_id);
                $con["organize.organize_id"] = [
                    "in",$child_index,
                ];
                $join = [];
                $join = [
                    ["organize", "organize.organize_id = user_equipment.organize_id"],
                    ["equipment", "equipment.equipment_id = user_equipment.equipment_id"],
                ];
                $select_user_organize = Db::name("user_equipment")->join($join)->where($con)->page($page, $limit)->select();
                $count_user_organize = Db::name("user_equipment")->join($join)->where($con)->count();
            }else{
                //没有organize_id
                $con = [];
                $select_user_organize = Db::name("equipment")->where($con)->page($page, $limit)->select();
                $count_user_organize = Db::name("equipment")->where($con)->count();
            }
            
            if ($select_user_organize && $count_user_organize) {
                $re["code"] = 0;
                $re["data"] = $select_user_organize;
                $re["count"] = $count_user_organize;
                $re["msg"] = "ok";
            } else {
                $re["code"] = 40000;
                $re["msg"] = "暂无数据";
            }
        } catch (\Throwable $th) {
            $re["code"] = 40004;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function user_equipment_menu()
    {
        $re = [];
        try {
            $post = Tool::Input(input("post."));
            $validate = new validate();
            if ($validate->scene("menu")->check($post)) {
                $select_organize_menu = Db::name("organize_menu")->where([
                    "organize_id" => $post["organize_id"],
                ])->select();
                $menu_id = array_map(function ($value) {
                    return $value["menu_id"];
                }, $select_organize_menu);
                $select_menu = Db::name("menu")->field([
                    "menu_name",
                    "menu_parent_id",
                    "menu_id",
                ])->where([
                    "menu_status" => 1,
                    "menu_delete" => 1,
                ])->select();
                $children = function ($data, $parent_id, $checked, $children) {
                    $re = [];
                    foreach ($data as $parent_data_key => $parent_data_value) {
                        if ($parent_data_value["menu_parent_id"] == $parent_id) {
                            $temporary = [
                                "title" => $parent_data_value["menu_name"],
                                "id" => $parent_data_value["menu_id"],
                                "data" => $parent_data_value,
                                "children" => [],
                                // "checked" => in_array($parent_data_value["menu_id"], $checked),
                                // "a" => $checked,
                                "spread" => true,
                            ];
                            foreach ($data as $children_data_key => $children_data_value) {
                                if ($children_data_value["menu_parent_id"] == $parent_data_value["menu_id"]) {
                                    $temporary["children"] = $children($data, $parent_data_value["menu_id"], $checked, $children);
                                }
                            }
                            $re[] = $temporary;
                        }
                    }
                    return $re;
                };

                $tree = $children($select_menu, 0, $menu_id, $children);
                $re["code"] = 0;
                $re["data"] = [[
                    "title" => "菜单管理",
                    "children" => $tree,
                    "spread" => true,
                ]];
                $re["checked"] = $menu_id;
                $re["msg"] = "ok";
            } else {
                $re["code"] = 40001;
                $re["msg"] = $validate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function user_equipment_power()
    {
        $re = [];
        try {
            $post = Tool::Input(input("post."));
            $validate = new validate();
            if ($validate->scene("power")->check($post)) {
                $organize_id = $post["organize_id"];
                $info = $post["info"];
                $info = htmlspecialchars_decode($info);
                $info = explode(",", $info);
                Db::startTrans();
                try {
                    $organize_menu = Db::name("organize_menu");
                    $delete_organize_menu = $organize_menu->where([
                        "organize_id" => $organize_id,
                    ])->delete();
                    $data = [];
                    foreach ($info as $info_value) {
                        array_push($data, [
                            "organize_id" => $organize_id,
                            "menu_id" => $info_value,
                            "organize_menu_guid" => $this->get_guid(),
                        ]);
                    }
                    $insertAll_organize_menu = $organize_menu->insertAll($data);
                    if ($insertAll_organize_menu !== false && $delete_organize_menu !== false) {
                        Db::commit();
                        $re["code"] = 0;
                        $re["msg"] = "授权成功";
                    } else {
                        Db::rollback();
                        $re["code"] = 40002;
                        $re["msg"] = "授权失败";
                    }
                } catch (\Exception $e) {
                    Db::rollback();
                    $re["code"] = 40003;
                    $re["data"] = $e->getMessage();
                    $re["msg"] = "服务器错误";
                }
            } else {
                $re["code"] = 40001;
                $re["msg"] = $validate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function user_equipment_info()
    {
        $re = [];
        try {
            $post = Tool::Input(input("post."));
            $validate = new validate();
            if ($validate->scene("info")->check($post)) {
                $find_user_organize = Db::name("user_organize")->where([
                    "user_organize_id" => $post["user_organize_id"],
                    "user_organize_delete" => 1,
                ])->find();
                if ($find_user_organize) {
                    $re["code"] = 0;
                    $re["data"] = $find_user_organize;
                    $re["msg"] = "ok";
                } else {
                    $re["code"] = 40002;
                    $re["msg"] = "数据不存在";
                }
            } else {
                $re["code"] = 40001;
                $re["msg"] = $validate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function user_equipment_add()
    {
        $re = [];
        try {
            $post = Tool::Input(input("post."));
            $validate = new validate();
            if ($validate->scene("add")->check($post)) {
                $user_equipment = Db::name("user_equipment");
                $organize_id = $post["organize_id"];
                $info = $post["info"];
                $info = htmlspecialchars_decode($info);
                $info = explode(",", $info);
                $data = [];
                $select_user_equipment = $user_equipment->where([
                    "organize_id" => $organize_id,
                ])->select();
                $equipment_id = array_map(function ($value) {
                    return $value["equipment_id"];
                }, $select_user_equipment);
                foreach ($info as $info_value) {
                    if (!in_array($info_value, $equipment_id)) {
                        array_push($data, [
                            "equipment_id" => $info_value,
                            "organize_id" => $organize_id,
                            "user_equipment_guid" => $this->get_guid(),
                        ]);
                    }
                }
                foreach ($info as $info_value) {
                    if (!in_array($info_value, $equipment_id)) {
                        $admin_con['equipment_id'] = $info_value;
                        // dump($admin_con);
                        $data_admin['equipment_admin'] = 2;
                        $update_equipment = Db::name("equipment")->where($admin_con)->update($data_admin);
                    }
                }
                $insertAll_user_equipment = $user_equipment->insertAll($data);
                if ($insertAll_user_equipment === false) {
                    $re["code"] = 40002;
                    $re["msg"] = "添加失败";
                } else {
                    $re["code"] = 0;
                    $re["msg"] = "添加成功";
                }
            } else {
                $re["code"] = 40001;
                $re["msg"] = $validate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function user_equipment_delete()
    {
        $re = [];
        try {
            $post = Tool::Input(input("post."));
            $validate = new validate();
            if ($validate->scene("delete")->check($post)) {
                $delete_user_organize = Db::name("user_organize")->where([
                    "user_organize_id" => $post["user_organize_id"],
                ])->delete();
                if ($delete_user_organize === false) {
                    $re["code"] = 40002;
                    $re["msg"] = "删除失败";
                } else {
                    $re["code"] = 0;
                    $re["msg"] = "删除成功";
                }
            } else {
                $re["code"] = 40001;
                $re["msg"] = $validate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function user_equipment_bind()
    {
        $re = [];
        try {
            $post = Tool::Input(input(""));
            $page = input("page");
            $limit = input("limit");
            $validate = new validate();
            if ($validate->scene("bind")->check($post)) {
                $parent_con['organize_id'] = $post['organize_id'];
                $find_organize = Db::name('organize')->where($parent_con)->find();
                if($find_organize['organize_parent_id'] == 0){
                    $con=[];
                    $con['equipment_delete'] = 1;
                    $con['equipment_admin'] = 1;
                    $select_equipment = Db::name("equipment")->where($con)->page($page, $limit)->order('equipment_id desc')->select();
                    $count_user = Db::name("equipment")->where($con)->count();
                }else{
                    $con=[];
                    $con['equipment_delete'] = 1;
                    $con['user_equipment.organize_id'] = $find_organize['organize_parent_id'];
                    $join = [];
                    $join = [
                        ["user_equipment", "user_equipment.equipment_id = equipment.equipment_id",'right'],
                    ];
                    $select_equipment = Db::name("equipment")->alias('equipment')->join($join)->where($con)->page($page, $limit)->order('equipment.equipment_id desc')->select();
                    $count_user = Db::name("equipment")->join($join)->where($con)->count();
                }
                
                if ($select_equipment && $count_user) {
                    $re["code"] = 0;
                    $re["data"] = $select_equipment;
                    $re["count"] = $count_user;
                    $re["msg"] = "ok";
                } else {
                    $re["code"] = 40002;
                    $re["msg"] = "暂无数据";
                }
            } else {
                $re["code"] = 40001;
                $re["msg"] = $validate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function user_equipment_view()
    {
        $re = [];
        try {
            $post = Tool::Input(input(""));
            $validate = new validate();
            if ($validate->scene("view")->check($post)) {
                $join = [];
                $join = [
                    ["user", "user.user_id = user_organize.user_id"],
                ];
                $con = [];
                $con["user.user_status"] = 2;
                $con["user.user_delete"] = 1;
                $con['user_organize.organize_id'] = $post["organize_id"];
                $select_user_organize = Db::name("user_organize")->join($join)->where($con)->page(input("page"), input("limit"))->select();
                $count_user_organize = Db::name("user_organize")->join($join)->where($con)->count();
                if ($select_user_organize && $count_user_organize) {
                    $re["code"] = 0;
                    $re["data"] = $select_user_organize;
                    $re["count"] = $count_user_organize;
                    $re["msg"] = "ok";
                } else {
                    $re["code"] = 40002;
                    $re["msg"] = "暂无数据";
                }
            } else {
                $re["code"] = 40001;
                $re["msg"] = $validate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }
    public function info(){
        $user = $this->UserInfo;
        $post = Tool::Input(input(""));
        $organize_id = $post['user_id']['organize_id'];

        $con = [];
        if ($organize_id) {
            $con["organize_id"] = $organize_id;
            $select_organize = Db::name("organize")->where([
                "organize_status" => 1,
                "organize_delete" => 1,
            ])->field([
                "organize_id",
                "organize_parent_id",
            ])->select();
            
            $child_index = (new Traverse("organize_id","organize_parent_id"))->next($select_organize,$organize_id,true);
            array_push($child_index,(int)$organize_id);
            $con["organize_id"] = [
                "in",$child_index,
            ];
        }
        $con['role_level'] = 1;
        $join = [];
        $join = [
            ["v_user_role","v_user_role.user_id = user_organize.user_id"],
        ];
        $select_organize = Db::name('user_organize')->alias('user_organize')->join($join)->where($con)->select();
        if ($select_organize) {
            $re["code"] = 0;
            $re["data"] = $select_organize;
            $re["msg"] = "ok";
        } else {
            $re["code"] = 40002;
            $re["msg"] = "暂无数据";
        }
        return json($re);  


    }
    public function user_equipment_edit(){
        $re = [];
        try {
            $post = Tool::Input(input(""));
            $validate = new validate();
            if ($validate->scene("edit")->check($post)) {
                $con['equipment_id'] = $post['user_id']['equipment_id'];
                $data['user_id'] = $post['user_ids'];
                $update_equipment = Db::name('equipment')->where($con)->update($data);
                if($update_equipment){
                    $re["code"] = 0;
                    $re["msg"] = "绑定成功";
                }
            }else{
                $re["code"] = 40001;
                $re["msg"] = $validate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
        
    }
}
