<?php

namespace app\adjfut\controller;

use Adjfut\Common;
use Adjfut\Tool\Tool;
use app\adjfut\validate\Menu as validate;
use think\Config;
use think\Db;

class Menu extends Common
{
    public function menu()
    {
        $re = [];
        $user_info = $this->UserInfo;
        $menu = [];
        $join = [];
        $join = [
            ['role_menu', 'role_menu.role_id = user_role.role_id'],
            ['menu', 'menu.menu_id = role_menu.menu_id'],
        ];
        $con = [];
        if ($user_info["user_admin"] == 1) {
            $select_menu = Db::name("menu")->where([
                "menu_status" => 1,
                "menu_delete" => 1,
            ])->order([
                "menu_index"
            ])->select();
            $menu = $this->menu_tree($select_menu, 0, "menu");
        } else {
            $con['user_role.user_id'] = $user_info['user_id'];
            $con['menu.menu_status'] = 1;
            $con['menu.menu_delete'] = 1;
            $select_user_role = Db::name('user_role')->join($join)->where($con)->order([
                "menu_index"
            ])->select();
            $menu = $this->menu_tree($select_user_role, 0, "menu");
        }
        $re["code"] = 0;
        $re["data"] = $menu;
        $re["uif"] = $this->UserInfo;
        $re["msg"] = "ok";
        return json($re);
    }

    public function menu_tree(array $data, int $parent_id, string $type = "tree")
    {
        $re = [];
        $web_resources_uploads = Config::get('web_resources_uploads');
        foreach ($data as $parent_data_key => $parent_data_value) {
            if ($parent_data_value["menu_parent_id"] == $parent_id) {
                switch ($type) {
                    case 'tree':
                        $temporary = [
                            "title" => $parent_data_value["menu_name"],
                            "id" => $parent_data_value["menu_id"],
                            "data" => $parent_data_value,
                            "children" => [],
                            "spread" => true,
                        ];
                        break;
                    case 'menu':
                        $temporary = [
                            "title" => $parent_data_value["menu_name"],
                            "jump" => $parent_data_value["menu_url"],
                            "id" => $parent_data_value["menu_id"],
                            "data" => $parent_data_value,
                            "list" => [],
                        ];
                        break;
                }

                if ($parent_data_value["menu_icon"]) {
                    $temporary["icon"] = $web_resources_uploads . $parent_data_value["menu_icon"];
                }
                foreach ($data as $children_data_key => $children_data_value) {
                    if ($children_data_value["menu_parent_id"] == $parent_data_value["menu_id"]) {
                        switch ($type) {
                            case 'tree':
                                $temporary["children"] = $this->menu_tree($data, $parent_data_value["menu_id"], $type);
                                break;
                            case 'menu':
                                $temporary["list"] = $this->menu_tree($data, $parent_data_value["menu_id"], $type);
                                break;
                        }
                        break;
                    }
                }
                $re[] = $temporary;
            }
        }
        return $re;
    }

    public function menu_list()
    {
        $re = [];
        $menu = Db::name("menu");
        $con = [];
        $con["menu.menu_delete"] = 1;
        $field = [];
        $field = [
            "menu.menu_name",
            "menu.menu_url",
            "menu.menu_id",
            "menu.menu_parent_id",
            "menu.menu_icon",
            "menu.menu_status",
            "menu.menu_index",
        ];
        $select_menu = $menu->where($con)->field($field)
            ->order("menu.menu_id desc")->select();
        $data = $this->menu_tree($select_menu, 0, "tree");
        $re["code"] = 0;
        $re["data"] = [
            [
                "title" => "导航分类",
                "data" => [
                    "menu_id" => 0,
                ],
                "children" => $data,
                "spread" => true,
            ],
        ];
        $re["msg"] = "ok";
        return json($re);
    }

    public function menu_add()
    {
        $re = [];
        try {
            $post = Tool::Input(input("post."), [
                'menu_url',
            ]);
            $validate = new validate();
            if ($validate->scene("add")->check($post)) {
                $post["menu_delete"] = 1;
                $post["menu_guid"] = $this->get_guid();
                $insert_menu = Db::name("menu")->insert($post);
                if ($insert_menu === false) {
                    $re["code"] = 40002;
                    $re["msg"] = "添加失败";
                } else {
                    $re["code"] = 0;
                    $re["msg"] = "添加成功";
                }
            } else {
                $re["code"] = 40001;
                $re["msg"] = $validate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function menu_edit()
    {
        $re = [];
        try {
            $post = Tool::Input(input("post."), [
                'menu_url',
            ]);
            $validate = new validate();
            if ($validate->scene("edit")->check($post)) {
                $update_menu = Db::name("menu")->where([
                    "menu_id" => $post["menu_id"],
                ])->update($post);
                if ($update_menu === false) {
                    $re["code"] = 40002;
                    $re["msg"] = "更新失败";
                } else {
                    $re["code"] = 0;
                    $re["msg"] = "更新成功";
                }
            } else {
                $re["code"] = 40001;
                $re["msg"] = $validate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function menu_delete()
    {

        $re = [];
        try {
            $post = Tool::Input(input("post."));
            $validate = new validate();
            if ($validate->scene("delete")->check($post)) {
                $update_menu = Db::name("menu")
                    ->where([
                        "menu_id" => $post["menu_id"],
                        "menu_delete" => 1,
                    ])
                    ->update([
                        "menu_delete" => 2,
                    ]);
                if ($update_menu === false) {
                    $re["code"] = 40000;
                    $re["msg"] = "删除失败";
                } else {
                    $re["code"] = 0;
                    $re["msg"] = "删除成功";
                }
            } else {
                $re["code"] = 40001;
                $re["msg"] = $validate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }
}
