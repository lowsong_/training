<?php

namespace app\adjfut\controller;

use Adjfut\Common;
use Adjfut\Tool\Tool;
use Adjfut\Tool\Traverse;
use app\adjfut\validate\UserOrganize as validate;
use think\Db;

class UserOrganize extends Common
{
    public function user_organize_tree()
    {
        $user = $this->UserInfo;
        $re = [];
        try {
            $con = [];
            $con["organize_status"] = 1;
            $con["organize_delete"] = 1;
            $con["organize_delete"] = 1;
            $field = [];
            $field = [
            ];
            $select_organize = Db::name("organize")->where($con)->field($field)->select();
            if ($select_organize) {
                $children = function (array $data, int $parent_id, $children) {
                    $re = [];
                    foreach ($data as $parent_data_key => $parent_data_value) {
                        if ($parent_data_value["organize_parent_id"] == $parent_id) {
                            $temporary = [
                                "title" => $parent_data_value["organize_name"],
                                "id" => $parent_data_value["organize_id"],
                                "data" => $parent_data_value,
                                "children" => [],
                                "spread" => true,
                            ];
                            foreach ($data as $children_data_key => $children_data_value) {
                                if ($children_data_value["organize_parent_id"] == $parent_data_value["organize_id"]) {
                                    $temporary["children"] = $children($data, $parent_data_value["organize_id"], $children);
                                }
                            }
                            $re[] = $temporary;
                        }
                    }
                    return $re;
                };
                $re["code"] = 0;
                $re["data"] = [[
                    "title" => "用户组织",
                    "id" => time(),
                    "spread" => true,
                    "data" => [
                        "organize_id" => 0,
                    ],
                    "children" => $children($select_organize, 0, $children),
                ]];
                $re["msg"] = "ok";
            } else {
                $re["code"] = 40000;
                $re["msg"] = "暂无数据";
            }
        } catch (\Throwable $th) {
            $re["code"] = 40004;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function user_organize_list()
    {
        $re = [];
        try {
            $page = input("page");
            $limit = input("limit");
            $organize_id = input("organize_id");
            // dump($organize_id);
            $join = [];
            $join = [
                ["user", "user.user_id = user_organize.user_id"],
                ["organize", "organize.organize_id = user_organize.organize_id"],
            ];
            $con = [];
            if ($organize_id) {
                $con["organize.organize_id"] = $organize_id;
                $select_organize = Db::name("organize")->where([
                    "organize_status" => 1,
                    "organize_delete" => 1,
                ])->field([
                    "organize_id",
                    "organize_parent_id",
                ])->select();
                
                $child_index = (new Traverse("organize_id","organize_parent_id"))->next($select_organize,$organize_id,true);
                array_push($child_index,(int)$organize_id);
                $con["organize.organize_id"] = [
                    "in",$child_index,
                ];
            }
            $con["user.user_status"] = 2;
            $con["user.user_delete"] = 1;
            $con["organize.organize_status"] = 1;
            $con["organize.organize_delete"] = 1;
            $field = [];
            $field = [
            ];
            $select_user_organize = Db::name("user_organize")->join($join)->where($con)->field($field)->page($page, $limit)->select();
            $count_user_organize = Db::name("user_organize")->join($join)->where($con)->count();
            if ($select_user_organize && $count_user_organize) {
                $re["code"] = 0;
                $re["data"] = $select_user_organize;
                $re["count"] = $count_user_organize;
                $re["msg"] = "ok";
            } else {
                $re["code"] = 40000;
                $re["msg"] = "暂无数据";
            }
        } catch (\Throwable $th) {
            $re["code"] = 40004;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function user_organize_menu()
    {
        $re = [];
        try {
            $post = Tool::Input(input("post."));
            $validate = new validate();
            if ($validate->scene("menu")->check($post)) {
                $select_organize_menu = Db::name("organize_menu")->where([
                    "organize_id" => $post["organize_id"],
                ])->select();
                $menu_id = array_map(function ($value) {
                    return $value["menu_id"];
                }, $select_organize_menu);
                $select_menu = Db::name("menu")->field([
                    "menu_name",
                    "menu_parent_id",
                    "menu_id",
                ])->where([
                    "menu_status" => 1,
                    "menu_delete" => 1,
                ])->select();
                $children = function ($data, $parent_id, $checked, $children) {
                    $re = [];
                    foreach ($data as $parent_data_key => $parent_data_value) {
                        if ($parent_data_value["menu_parent_id"] == $parent_id) {
                            $temporary = [
                                "title" => $parent_data_value["menu_name"],
                                "id" => $parent_data_value["menu_id"],
                                "data" => $parent_data_value,
                                "children" => [],
                                // "checked" => in_array($parent_data_value["menu_id"], $checked),
                                // "a" => $checked,
                                "spread" => true,
                            ];
                            foreach ($data as $children_data_key => $children_data_value) {
                                if ($children_data_value["menu_parent_id"] == $parent_data_value["menu_id"]) {
                                    $temporary["children"] = $children($data, $parent_data_value["menu_id"], $checked, $children);
                                }
                            }
                            $re[] = $temporary;
                        }
                    }
                    return $re;
                };

                $tree = $children($select_menu, 0, $menu_id, $children);
                $re["code"] = 0;
                $re["data"] = [[
                    "title" => "菜单管理",
                    "children" => $tree,
                    "spread" => true,
                ]];
                $re["checked"] = $menu_id;
                $re["msg"] = "ok";
            } else {
                $re["code"] = 40001;
                $re["msg"] = $validate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function user_organize_power()
    {
        $re = [];
        try {
            $post = Tool::Input(input("post."));
            $validate = new validate();
            if ($validate->scene("power")->check($post)) {
                $organize_id = $post["organize_id"];
                $info = $post["info"];
                $info = htmlspecialchars_decode($info);
                $info = explode(",", $info);
                Db::startTrans();
                try {
                    $organize_menu = Db::name("organize_menu");
                    $delete_organize_menu = $organize_menu->where([
                        "organize_id" => $organize_id,
                    ])->delete();
                    $data = [];
                    foreach ($info as $info_value) {
                        array_push($data, [
                            "organize_id" => $organize_id,
                            "menu_id" => $info_value,
                            "organize_menu_guid" => $this->get_guid(),
                        ]);
                    }
                    $insertAll_organize_menu = $organize_menu->insertAll($data);
                    if ($insertAll_organize_menu !== false && $delete_organize_menu !== false) {
                        Db::commit();
                        $re["code"] = 0;
                        $re["msg"] = "授权成功";
                    } else {
                        Db::rollback();
                        $re["code"] = 40002;
                        $re["msg"] = "授权失败";
                    }
                } catch (\Exception $e) {
                    Db::rollback();
                    $re["code"] = 40003;
                    $re["data"] = $e->getMessage();
                    $re["msg"] = "服务器错误";
                }
            } else {
                $re["code"] = 40001;
                $re["msg"] = $validate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function user_organize_info()
    {
        $re = [];
        try {
            $post = Tool::Input(input("post."));
            $validate = new validate();
            if ($validate->scene("info")->check($post)) {
                $find_user_organize = Db::name("user_organize")->where([
                    "user_organize_id" => $post["user_organize_id"],
                    "user_organize_delete" => 1,
                ])->find();
                if ($find_user_organize) {
                    $re["code"] = 0;
                    $re["data"] = $find_user_organize;
                    $re["msg"] = "ok";
                } else {
                    $re["code"] = 40002;
                    $re["msg"] = "数据不存在";
                }
            } else {
                $re["code"] = 40001;
                $re["msg"] = $validate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function user_organize_add()
    {
        $re = [];
        try {
            $post = Tool::Input(input("post."));
            $validate = new validate();
            if ($validate->scene("add")->check($post)) {
                $user_organize = Db::name("user_organize");
                $organize_id = $post["organize_id"];
                $info = $post["info"];
                $info = htmlspecialchars_decode($info);
                $info = explode(",", $info);
                $data = [];
                $select_user_organize = $user_organize->where([
                    "organize_id" => $organize_id,
                ])->select();
                $user_id = array_map(function ($value) {
                    return $value["user_id"];
                }, $select_user_organize);
                foreach ($info as $info_value) {
                    if (!in_array($info_value, $user_id)) {
                        array_push($data, [
                            "user_id" => $info_value,
                            "organize_id" => $organize_id,
                            "user_organize_guid" => $this->get_guid(),
                        ]);
                    }
                }
                $insertAll_user_organize = $user_organize->insertAll($data);
                if ($insertAll_user_organize === false) {
                    $re["code"] = 40002;
                    $re["msg"] = "添加失败";
                } else {
                    $re["code"] = 0;
                    $re["msg"] = "添加成功";
                }
            } else {
                $re["code"] = 40001;
                $re["msg"] = $validate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function user_organize_delete()
    {
        $re = [];
        try {
            $post = Tool::Input(input("post."));
            $validate = new validate();
            if ($validate->scene("delete")->check($post)) {
                $delete_user_organize = Db::name("user_organize")->where([
                    "user_organize_id" => $post["user_organize_id"],
                ])->delete();
                if ($delete_user_organize === false) {
                    $re["code"] = 40002;
                    $re["msg"] = "删除失败";
                } else {
                    $re["code"] = 0;
                    $re["msg"] = "删除成功";
                }
            } else {
                $re["code"] = 40001;
                $re["msg"] = $validate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function user_organize_bind()
    {
        $re = [];
        try {
            $post = Tool::Input(input(""));
            $validate = new validate();
            if ($validate->scene("bind")->check($post)) {
                $user_user_organize = Db::name("user_organize")->where([
                    "organize_id" => $post["organize_id"],
                ])->select();
                $con = [];
                $con["user_status"] = 2;
                $con["user_delete"] = 1;
                $con["user_id"] = [
                    "not in", array_map(function ($value) {
                        return $value["user_id"];
                    }, $user_user_organize),
                ];
                $select_user = Db::name("user")->where($con)->page(input("page"), input("limit"))->select();
                $count_user = Db::name("user")->where($con)->count();
                if ($select_user && $count_user) {
                    $re["code"] = 0;
                    $re["data"] = $select_user;
                    $re["count"] = $count_user;
                    $re["msg"] = "ok";
                } else {
                    $re["code"] = 40002;
                    $re["msg"] = "暂无数据";
                }
            } else {
                $re["code"] = 40001;
                $re["msg"] = $validate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function user_organize_view()
    {
        $re = [];
        try {
            $post = Tool::Input(input(""));
            $validate = new validate();
            if ($validate->scene("view")->check($post)) {
                $join = [];
                $join = [
                    ["user", "user.user_id = user_organize.user_id"],
                ];
                $con = [];
                $con["user.user_status"] = 2;
                $con["user.user_delete"] = 1;
                $con['user_organize.organize_id'] = $post["organize_id"];
                $select_user_organize = Db::name("user_organize")->join($join)->where($con)->page(input("page"), input("limit"))->select();
                $count_user_organize = Db::name("user_organize")->join($join)->where($con)->count();
                if ($select_user_organize && $count_user_organize) {
                    $re["code"] = 0;
                    $re["data"] = $select_user_organize;
                    $re["count"] = $count_user_organize;
                    $re["msg"] = "ok";
                } else {
                    $re["code"] = 40002;
                    $re["msg"] = "暂无数据";
                }
            } else {
                $re["code"] = 40001;
                $re["msg"] = $validate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }
}
