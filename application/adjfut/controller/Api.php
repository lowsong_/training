<?php

namespace app\adjfut\controller;

use Adjfut\Common;
use Adjfut\WeChat\Gzh;
use think\Db;

class Api extends Common
{
    protected $login = false;
    public function user_info()
    {
        $user_info = $this->UserInfo;
        return json([
            "code" => 0,
            "data" => [
                "user_name" => $user_info["user_account"],
            ],
            "msg" => "ok",
        ]);
    }

    public function test()
    {
        (new Gzh)->authorize([
            "state" => "测试",
        ], function ($info, $state) {
            dump($info);
            dump($state);
        });
    }

    public function v_user_role()
    {
        return Db::name("user_role")->join([
            ["user", "user.user_id = user_role.user_id"],
            ["role", "role.role_id = user_role.role_id"],
        ])->select(false);
    }

    public function v_user_organize()
    {
        return Db::name("user_organize")->join([
            ["user", "user.user_id = user_organize.user_id"],
            ["organize", "organize.organize_id = user_organize.organize_id"],
        ])->select(false);
    }

    public function v_enterprise_position_user()
    {
        return Db::name("user")->join([
            ["enterprise_position_user", "enterprise_position_user.user_id = user.user_id"],
            ["enterprise_position", "enterprise_position.enterprise_position_id = enterprise_position_user.enterprise_position_id"],
            ["enterprise", "enterprise.enterprise_id = enterprise_position.enterprise_id"],
        ])->select(false);
    }

}
