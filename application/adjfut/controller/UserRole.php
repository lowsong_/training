<?php

namespace app\adjfut\controller;

use Adjfut\Common;
use Adjfut\Tool\Tool;
use app\adjfut\validate\UserRole as validate;
use think\Db;

class UserRole extends Common
{
    public function user_role_tree()
    {
        $re = [];
        try {
            $con = [];
            $con["role_status"] = 1;
            $con["role_delete"] = 1;
            $field = [];
            $field = [
            ];
            $select_role = Db::name("role")->where($con)->field($field)->select();
            if ($select_role) {
                $re["code"] = 0;
                $re["data"] = [[
                    "title" => "用户角色",
                    "id" => time(),
                    "spread" => true,
                    "data" => [
                        "role_id" => 0,
                    ],
                    "children" => array_map(function ($value) {
                        return [
                            "title" => $value["role_name"],
                            "data" => $value,
                        ];
                    }, $select_role),
                ]];
                $re["msg"] = "ok";
            } else {
                $re["code"] = 40000;
                $re["msg"] = "暂无数据";
            }
        } catch (\Throwable $th) {
            $re["code"] = 40004;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function user_role_list()
    {
        $re = [];
        try {
            $page = input("page");
            $limit = input("limit");
            $role_id = input("role_id");
            $join = [];
            $join = [
                ["user", "user.user_id = user_role.user_id"],
                ["role", "role.role_id = user_role.role_id"],
            ];
            $con = [];
            if ($role_id) {
                $con["role.role_id"] = $role_id;
            }
            $con["user.user_status"] = 2;
            $con["user.user_delete"] = 1;
            $con["role.role_status"] = 1;
            $con["role.role_delete"] = 1;
            $field = [];
            $field = [
            ];
            $select_user_role = Db::name("user_role")->join($join)->where($con)->field($field)->page($page, $limit)->select();
            $count_user_role = Db::name("user_role")->join($join)->where($con)->page($page, $limit)->count();
            if ($select_user_role && $count_user_role) {
                $re["code"] = 0;
                $re["data"] = $select_user_role;
                $re["count"] = $count_user_role;
                $re["msg"] = "ok";
            } else {
                $re["code"] = 40000;
                $re["msg"] = "暂无数据";
            }
        } catch (\Throwable $th) {
            $re["code"] = 40004;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function user_role_menu()
    {
        $re = [];
        try {
            $post = Tool::Input(input("post."));
            $validate = new validate();
            if ($validate->scene("menu")->check($post)) {
                $select_role_menu = Db::name("role_menu")->where([
                    "role_id" => $post["role_id"],
                ])->select();
                $menu_id = array_map(function ($value) {
                    return $value["menu_id"];
                }, $select_role_menu);
                $select_menu = Db::name("menu")->field([
                    "menu_name",
                    "menu_parent_id",
                    "menu_id",
                ])->where([
                    "menu_status" => 1,
                    "menu_delete" => 1,
                ])->select();
                // $children = function ($data, $parent_id, $checked, $children) {
                //     $re = [];
                //     foreach ($data as $parent_data_key => $parent_data_value) {
                //         if ($parent_data_value["menu_parent_id"] == $parent_id) {
                //             $temporary = [
                //                 "title" => $parent_data_value["menu_name"],
                //                 "id" => $parent_data_value["menu_id"],
                //                 "data" => $parent_data_value,
                //                 "children" => [],
                //                 // "checked" => in_array($parent_data_value["menu_id"], $checked),
                //                 // "a" => $checked,
                //                 "spread" => true,
                //             ];
                //             foreach ($data as $children_data_key => $children_data_value) {
                //                 if ($children_data_value["menu_parent_id"] == $parent_data_value["menu_id"]) {
                //                     $temporary["children"] = $children($data, $parent_data_value["menu_id"], $checked, $children);
                //                 }
                //             }
                //             $re[] = $temporary;
                //         }
                //     }
                //     return $re;
                // };

                // $tree = $children($select_menu, 0, $menu_id, $children);
                $re["code"] = 0;
                // $re["data"] = [[
                //     "title" => "菜单管理",
                //     "children" => $tree,
                //     "spread" => true,
                // ]];
                $re["tree"] = $select_menu;
                $re["checked"] = $menu_id;
                $re["msg"] = "ok";
            } else {
                $re["code"] = 40001;
                $re["msg"] = $validate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function user_role_power()
    {
        $re = [];
        try {
            $post = Tool::Input(input("post."));
            $validate = new validate();
            if ($validate->scene("power")->check($post)) {
                $role_id = $post["role_id"];
                $info = $post["info"];
                $info = htmlspecialchars_decode($info);
                $info = explode(",", $info);
                Db::startTrans();
                try {
                    $role_menu = Db::name("role_menu");
                    $delete_role_menu = $role_menu->where([
                        "role_id" => $role_id,
                    ])->delete();
                    $data = [];
                    foreach ($info as $info_value) {
                        array_push($data, [
                            "role_id" => $role_id,
                            "menu_id" => $info_value,
                            "role_menu_guid" => $this->get_guid(),
                        ]);
                    }
                    $insertAll_role_menu = $role_menu->insertAll($data);
                    if ($insertAll_role_menu !== false && $delete_role_menu !== false) {
                        Db::commit();
                        $re["code"] = 0;
                        $re["msg"] = "授权成功";
                    } else {
                        Db::rollback();
                        $re["code"] = 40002;
                        $re["msg"] = "授权失败";
                    }
                } catch (\Exception $e) {
                    Db::rollback();
                    $re["code"] = 40003;
                    $re["data"] = $e->getMessage();
                    $re["msg"] = "服务器错误";
                }
            } else {
                $re["code"] = 40001;
                $re["msg"] = $validate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function user_role_info()
    {
        $re = [];
        try {
            $post = Tool::Input(input("post."));
            $validate = new validate();
            if ($validate->scene("info")->check($post)) {
                $find_user_role = Db::name("user_role")->where([
                    "user_role_id" => $post["user_role_id"],
                    "user_role_delete" => 1,
                ])->find();
                if ($find_user_role) {
                    $re["code"] = 0;
                    $re["data"] = $find_user_role;
                    $re["msg"] = "ok";
                } else {
                    $re["code"] = 40002;
                    $re["msg"] = "数据不存在";
                }
            } else {
                $re["code"] = 40001;
                $re["msg"] = $validate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function user_role_add()
    {
        $re = [];
        try {
            $post = Tool::Input(input("post."));
            $validate = new validate();
            if ($validate->scene("add")->check($post)) {
                $user_role = Db::name("user_role");
                $role_id = $post["role_id"];
                $info = $post["info"];
                $info = htmlspecialchars_decode($info);
                $info = explode(",", $info);
                $data = [];
                $select_user_role = $user_role->where([
                    "role_id" => $role_id,
                ])->select();
                $user_id = array_map(function ($value) {
                    return $value["user_id"];
                }, $select_user_role);
                foreach ($info as $info_value) {
                    if (!in_array($info_value, $user_id)) {
                        array_push($data, [
                            "user_id" => $info_value,
                            "role_id" => $role_id,
                            "user_role_guid" => $this->get_guid(),
                        ]);
                    }
                }
                $insertAll_user_role = $user_role->insertAll($data);
                if ($insertAll_user_role === false) {
                    $re["code"] = 40002;
                    $re["msg"] = "添加失败";
                } else {
                    $re["code"] = 0;
                    $re["msg"] = "添加成功";
                }
            } else {
                $re["code"] = 40001;
                $re["msg"] = $validate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function user_role_delete()
    {
        $re = [];
        try {
            $post = Tool::Input(input("post."));
            $validate = new validate();
            if ($validate->scene("delete")->check($post)) {
                $delete_user_role = Db::name("user_role")->where([
                    "user_role_id" => $post["user_role_id"],
                ])->delete();
                if ($delete_user_role === false) {
                    $re["code"] = 40002;
                    $re["msg"] = "删除失败";
                } else {
                    $re["code"] = 0;
                    $re["msg"] = "删除成功";
                }
            } else {
                $re["code"] = 40001;
                $re["msg"] = $validate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function user_role_bind()
    {
        $re = [];
        try {
            $post = Tool::Input(input(""));
            $validate = new validate();
            if ($validate->scene("bind")->check($post)) {
                $user_user_role = Db::name("user_role")->where([
                    // "role_id" => $post["role_id"],
                ])->select();
                $con = [];
                $con["user_status"] = 2;
                $con["user_delete"] = 1;
                $con["user_id"] = [
                    "not in", array_map(function ($value) {
                        return $value["user_id"];
                    }, $user_user_role),
                ];
                $select_user = Db::name("user")->where($con)->page(input("page"), input("limit"))->select();
                $count_user = Db::name("user")->where($con)->count();
                if ($select_user && $count_user) {
                    $re["code"] = 0;
                    $re["data"] = $select_user;
                    $re["count"] = $count_user;
                    $re["msg"] = "ok";
                } else {
                    $re["code"] = 40002;
                    $re["msg"] = "暂无数据";
                }
            } else {
                $re["code"] = 40001;
                $re["msg"] = $validate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function user_role_view()
    {
        $re = [];
        try {
            $post = Tool::Input(input(""));
            $validate = new validate();
            if ($validate->scene("view")->check($post)) {
                $join = [];
                $join = [
                    ["user", "user.user_id = user_role.user_id"],
                ];
                $con = [];
                $con["user.user_status"] = 2;
                $con["user.user_delete"] = 1;
                $con['user_role.role_id'] = $post["role_id"];
                $select_user_role = Db::name("user_role")->join($join)->where($con)->page(input("page"), input("limit"))->select();
                $count_user_role = Db::name("user_role")->join($join)->where($con)->count();
                if ($select_user_role && $count_user_role) {
                    $re["code"] = 0;
                    $re["data"] = $select_user_role;
                    $re["count"] = $count_user_role;
                    $re["msg"] = "ok";
                } else {
                    $re["code"] = 40002;
                    $re["msg"] = "暂无数据";
                }
            } else {
                $re["code"] = 40001;
                $re["msg"] = $validate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }
}
