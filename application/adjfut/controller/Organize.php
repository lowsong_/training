<?php

namespace app\adjfut\controller;

use Adjfut\Common;
use Adjfut\Tool\Tool;
use app\adjfut\validate\Organize as OrganizeValidate;
use think\Db;

class Organize extends Common
{
    public function organize()
    {
        $re = [];
        $user_info = $this->UserInfo;
        $organize = [];
        $join = [];
        $join = [
            ['role_organize', 'role_organize.role_id = user_role.role_id'],
            ['organize', 'organize.organize_id = role_organize.organize_id'],
        ];
        $con = [];
        $con['user_role.user_id'] = $user_info['user_id'];
        $con['organize.organize_status'] = 1;
        $con['organize.organize_delete'] = 1;
        $select_user_role = Db::name('user_role')->join($join)->where($con)->select();
        $organize = $this->organize_tree($select_user_role, 0, "organize");
        $re["code"] = 0;
        $re["data"] = $organize;
        $re["msg"] = "ok";
        return json($re);
    }

    public function organize_tree(array $data, int $parent_id, string $type = "tree")
    {
        $re = [];
        foreach ($data as $parent_data_key => $parent_data_value) {
            if ($parent_data_value["organize_parent_id"] == $parent_id) {
                $temporary = [
                    "title" => $parent_data_value["organize_name"],
                    "id" => $parent_data_value["organize_id"],
                    "data" => $parent_data_value,
                    "children" => [],
                    "spread" => true,
                ];
                foreach ($data as $children_data_key => $children_data_value) {
                    if ($children_data_value["organize_parent_id"] == $parent_data_value["organize_id"]) {
                        $temporary["children"] = $this->organize_tree($data, $parent_data_value["organize_id"]);
                    }
                }
                $re[] = $temporary;
            }
        }
        return $re;
    }

    public function organize_list()
    {
        $re = [];
        $user = $this->UserInfo;
        // dump($user);
        $organize = Db::name("organize");
        $con = [];
        $con["organize.organize_delete"] = 1;
        if($user['role_level'] == 8){
            $select_organize = Db::name("organize")->where($con)->select();
            // dump($select_organize);
        }else{
            if($user['role_level'] == 7){
                $con['organize_id'] = $user['organize_id'];
            }else if ($user['role_level'] == 6) {
                $con['organize_parent_id'] = $user['organize_id'];
                // $con['organize_id'] = $user['organize_id'];
                // dump($con);
                $select_organize = $organize->where($con)->order("organize.organize_id desc")->select();
                // dump($select_organize);
            }else if ($user['role_level'] == 5) {
                $con['v_user_role.role_level'] = ["lt", '5'];
            }else if ($user['role_level'] == 4) {
                $con['v_user_role.role_level'] = ["lt", '4'];
            }else if ($user['role_level'] == 3) {
                $con['v_user_role.role_level'] = ["lt", '3'];
            }else if ($user['role_level'] == 2) {
                $con['v_user_role.role_level'] = ["lt", '2'];
            }
        }
        
        $data = $this->organize_tree($select_organize, 0);
        $re["code"] = 0;
        $re["data"] = [
            [
                "title" => "组织管理",
                "data" => [
                    "organize_id" => 0,
                    "organize_level" => 0,
                ],
                "children" => $data,
                "spread" => true,
            ],
        ];
        $re["msg"] = "ok";
        return json($re);
    }

    public function organize_info()
    {
        $re = [];
        try {
            $post = Tool::Input(input("post."));
            $OrganizeValidate = new OrganizeValidate();
            if ($OrganizeValidate->scene("info")->check($post)) {
                $find_organize = Db::name("organize")->where([
                    "organize_id" => $post["organize_id"],
                    "organize_delete" => 1,
                ])->find();
                if ($find_organize) {
                    $re["code"] = 0;
                    $re["msg"] = "ok";
                    $re["data"] = $find_organize;
                } else {
                    $re["code"] = 40002;
                    $re["msg"] = "暂无数据";
                }
            } else {
                $re["code"] = 40001;
                $re["msg"] = $OrganizeValidate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }
    public function organize_add()
    {
        $re = [];
        try {
            $post = Tool::Input(input("post."));
            $OrganizeValidate = new OrganizeValidate();
            if ($OrganizeValidate->scene("organize_add")->check($post)) {
                $post["organize_delete"] = 1;
                $post["organize_guid"] = $this->get_guid();
                $insert_organize = Db::name("organize")->insert($post);
                if ($insert_organize === false) {
                    $re["code"] = 40002;
                    $re["msg"] = "添加失败";
                } else {
                    $re["code"] = 0;
                    $re["msg"] = "添加成功";
                }
            } else {
                $re["code"] = 40001;
                $re["msg"] = $OrganizeValidate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function organize_edit()
    {
        $re = [];
        try {
            $post = Tool::Input(input("post."));
            // dump($post);
            $OrganizeValidate = new OrganizeValidate();
            if ($OrganizeValidate->scene("organize_edit")->check($post)) {
                $update_organize = Db::name("organize")->where([
                    "organize_id" => $post["organize_id"],
                ])->update($post);
                if ($update_organize === false) {
                    $re["code"] = 40002;
                    $re["msg"] = "编辑失败";
                } else {
                    $re["code"] = 0;
                    $re["msg"] = "编辑成功";
                }
            } else {
                $re["code"] = 40001;
                $re["msg"] = $OrganizeValidate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function organize_delete()
    {
        $re = [];
        try {
            $post = Tool::Input(input("post."));
            $OrganizeValidate = new OrganizeValidate();
            if ($OrganizeValidate->scene("organize_delete")->check($post)) {
                $update_organize = Db::name("organize")->where([
                    "organize_id" => $post["organize_id"],
                ])->update([
                    "organize_delete" => 2,
                ]);
                if ($update_organize === false) {
                    $re["code"] = 40002;
                    $re["msg"] = "删除失败";
                } else {
                    $re["code"] = 0;
                    $re["msg"] = "删除成功";
                }
            } else {
                $re["code"] = 40001;
                $re["msg"] = $OrganizeValidate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function organize_children(int $organize_id)
    {
        $re = [];
        $organize = Db::name("organize");
        $select_organize = $organize->where([
            "organize_parent_id" => $organize_id,
            "organize_status" => 1,
            "organize_delete" => 1,
        ])->field([
            "organize_id",
            "organize_parent_id",
        ])->select();
        foreach ($select_organize as $select_organize_value) {
            $organize_id = $select_organize_value["organize_id"];
            array_push($re, $organize_id);
            $re = array_merge($re,$this->organize_children($organize_id));
        }
        return $re;
    }
}
