<?php

namespace app\adjfut\controller;

use Adjfut\Common;
use Adjfut\Tool\Tool;
use app\adjfut\controller\Menu;
use app\adjfut\validate\Role as validate;
use think\Db;

class Role extends Common
{

    public function role_list()
    {
        $re = [];
        try {
            $role = Db::name("role");
            $page = input("page");
            $limit = input("limit");
            $role_name = input("role_name");
            $con = [];
            if ($role_name) {
                $con['role_name'] = [
                    "like", "%$role_name%",
                ];
            }
            $con["role_delete"] = 1;
            $field = [];
            $field = [
            ];
            $select_role_all = Db::name("v_user_role")->where([
                "role_delete" => 1,
                "user_delete" => 1,
            ])->field([
                "role_id",
                "user_name",
            ])->select();
            $select_role = $role->where($con)->field($field)->page($page, $limit)->select();
            $count_role = $role->where($con)->page($page, $limit)->count();
            if ($select_role && $count_role) {
                foreach ($select_role as &$select_role_value) {
                    $select_role_value["view"] = [];

                    foreach ($select_role_all as $select_role_all_value) {
                        if ($select_role_value["role_id"] === $select_role_all_value["role_id"]) {
                            array_push($select_role_value["view"], $select_role_all_value);
                        }
                    }
                }
                $re["code"] = 0;
                $re["data"] = $select_role;
                // dump($select_role);
                $re["count"] = $count_role;
                $re["msg"] = "ok";
            } else {
                $re["code"] = 40000;
                $re["msg"] = "暂无数据";
            }
        } catch (\Throwable $th) {
            $re["code"] = 40004;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function role_info()
    {
        $re = [];
        try {
            $post = Tool::Input(input("post."));
            $validate = new validate();
            if ($validate->scene("info")->check($post)) {
                $find_role = Db::name("role")->where([
                    "role_id" => $post["role_id"],
                    "role_delete" => 1,
                ])->find();
                $select_menu = Db::name("menu")->where([
                    "menu_status" => 1,
                    "menu_delete" => 1,
                ])->select();
                if ($find_role) {
                    $re["code"] = 0;
                    $re["data"] = $find_role;
                    $re["tree"] = [[
                        "title" => "菜单管理",
                        "spread" => true,
                        "id" => 0,
                        "children" => (new Menu)->menu_tree($select_menu, 0, "tree"),
                    ]];
                    $re["msg"] = "ok";
                } else {
                    $re["code"] = 40002;
                    $re["msg"] = "数据不存在";
                }
            } else {
                $re["code"] = 40001;
                $re["msg"] = $validate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function role_add()
    {
        $re = [];
        try {
            $post = Tool::Input(input("post."));
            $validate = new validate();
            if ($validate->scene("add")->check($post)) {
                $post["role_status"] = 1;
                $post["role_delete"] = 1;
                $post["role_guid"] = $this->get_guid();
                $insert_role = Db::name("role")->insert($post);
                if ($insert_role === false) {
                    $re["code"] = 40002;
                    $re["msg"] = "添加失败";
                } else {
                    $re["code"] = 0;
                    $re["msg"] = "添加成功";
                }
            } else {
                $re["code"] = 40001;
                $re["msg"] = $validate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function role_edit()
    {
        $re = [];
        try {
            $post = Tool::Input(input("post."));
            $validate = new validate();
            if ($validate->scene("edit")->check($post)) {
                $update_role = Db::name("role")->where([
                    "role_id" => $post["role_id"],
                ])->update($post);
                if ($update_role === false) {
                    $re["code"] = 40002;
                    $re["msg"] = "修改失败";
                } else {
                    $re["code"] = 0;
                    $re["msg"] = "修改成功";
                }
            } else {
                $re["code"] = 40001;
                $re["msg"] = $validate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function role_status()
    {
        $re = [];
        try {
            $post = Tool::Input(input("post."));
            $validate = new validate();
            if ($validate->scene("status")->check($post)) {
                $update_role = Db::name("role")->where([
                    "role_id" => $post["role_id"],
                    "role_delete" => 1,
                ])->exp('role_status', 'IF(role_status = 1,2,1)')
                    ->update();
                if ($update_role === false) {
                    $re["code"] = 40002;
                    $re["msg"] = "更新失败";
                } else {
                    $re["code"] = 0;
                    $re["msg"] = "更新成功";
                }
            } else {
                $re["code"] = 40001;
                $re["msg"] = $validate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function role_delete()
    {
        $re = [];
        try {
            $post = Tool::Input(input("post."));
            $validate = new validate();
            if ($validate->scene("delete")->check($post)) {
                $update_role = Db::name("role")->where([
                    "role_id" => $post["role_id"],
                    "role_delete" => 1,
                ])->update([
                    "role_delete" => 2,
                ]);
                if ($update_role === false) {
                    $re["code"] = 40002;
                    $re["msg"] = "删除失败";
                } else {
                    $re["code"] = 0;
                    $re["msg"] = "删除成功";
                }
            } else {
                $re["code"] = 40001;
                $re["msg"] = $validate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }
}
