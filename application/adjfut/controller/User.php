<?php

namespace app\adjfut\controller;

use Adjfut\Common;
use Adjfut\Tool\Excel;
use Adjfut\Tool\Tool;
use app\adjfut\controller\Login;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use app\adjfut\validate\User as UserValidate;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use think\Config;
use think\Db;

// use think\Validate;

class User extends Common
{
    public function user_list()
    {
        //负责人比主管高级
        $user = $this->UserInfo;
        
        $re = [];
        try {
            $page = input("page");
            $limit = input("limit");
            $con = [];
            $join = [];
            $join = [
                ['user', 'user.user_id = v_user_organize.user_id'],
                ['v_user_role','v_user_role.user_id = user.user_id']
            ];
            if($user['role_level'] == 8){
                $order = [
                    "user.user_id desc"
                ];
                $con["v_user_organize.user_delete"] = 1;
                $select_user = Db::name("v_user_organize")->alias('v_user_organize')->join($join)->where($con)->page($page, $limit)->order($order)->select();
                $count_user = Db::name("v_user_organize")->where($con)->count();
            }else{
                $con = [];
                if($user['role_level'] == 7){
                    $con['v_user_role.role_level'] = ["lt", '7'];
                }else if ($user['role_level'] == 6) {
                    $con['v_user_role.role_level'] = ["lt", '6'];
                }else if ($user['role_level'] == 5) {
                    $con['v_user_role.role_level'] = ["lt", '5'];
                }else if ($user['role_level'] == 4) {
                    $con['v_user_role.role_level'] = ["lt", '4'];
                }else if ($user['role_level'] == 3) {
                    $con['v_user_role.role_level'] = ["lt", '3'];
                }else if ($user['role_level'] == 2) {
                    $con['v_user_role.role_level'] = ["lt", '2'];
                }
                $order = [
                    "user.user_id desc"
                ];
                $select_user = [];
                $count = [];
                foreach ($user['user_organize'] as $key => $value) {
                    $select = [];
                    $con['v_user_organize.organize_id'] = $value;
                    $con["v_user_organize.user_delete"] = 1;
                    $select = Db::name("v_user_organize")->alias('v_user_organize')->join($join)->where($con)->page($page, $limit)->order($order)->select();
                    $select_1 = Db::name("v_user_organize")->alias('v_user_organize')->join($join)->where($con)->select();
                    if($select && $select_1){
                        foreach ($select as $select_key => $select_value) {
                            $select_user[] = $select_value;

                        }
                        foreach ($select_1 as $select_1_key => $select_1_value) {
                            $count[] = $select_1_value;
                        }
                    }

                }
                $count_user = count($count);
            }
            if ($select_user && $count_user) {
                foreach ($select_user as &$select_user_value) {
                    $user_register_time = $select_user_value["user_time"];
                    $select_user_value["user_time"] = date("Y-m-d", $user_register_time);
                    // $user_openid_xcx = $select_user_value["user_openid_xcx"];
                    // if ($user_openid_xcx) {
                    //     $select_user_value["user_openid_xcx"] = "已绑定";
                    // } else {
                    //     $select_user_value["user_openid_xcx"] = "未绑定";
                    // }
                }
                $re["code"] = 0;
                $re["data"] = $select_user;
                $re["count"] = $count_user;
                $re["msg"] = "ok";
            } else {
                $re["code"] = 40000;
                $re["msg"] = "暂无数据";
            }
        } catch (\Throwable $th) {
            // dump($th);
            $re["code"] = 40001;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function user_info()
    {
        $re = [];
        try {
            $post = Tool::Input(input("post."));
            $UserValidate = new UserValidate();
            $join = [];
            $join = [
                ["v_user_role", "v_user_role.user_id = user.user_id"],
                ["v_user_organize", "v_user_organize.user_id = v_user_role.user_id"],
            ];
            if ($UserValidate->scene("user_info")->check($post)) {
                $find_user = Db::name("user")
                ->join($join)
                ->where([
                    "user.user_id" => $post["user_id"],
                    "user.user_delete" => 1,
                ])->field([
                    "user.user_name",
                    "user.user_phone",
                    "user.user_sex",
                    "user.user_number",
                    "user.user_identity_card",
                    "role_name",
                    "organize_name",
                ])->find();
                if ($find_user) {
                    $re["code"] = 10000;
                    $re["data"] = $find_user;
                    $re["msg"] = "ok";
                } else {
                    $re["code"] = 40002;
                    $re["msg"] = "数据不存在";
                }
            } else {
                $re["code"] = 40001;
                $re["msg"] = $UserValidate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function user_add()
    {
        $re = [];
        try {
            $post = Tool::Input(input("post."));
            $UserValidate = new UserValidate();
            if ($UserValidate->scene("user_add")->check($post)) {
                $user = Db::name("user");

                $this->user_recheck($post);

                if ($post["user_password"]) {
                    $post["user_password"] = Login::password_encryption($post["user_password"]);
                }
                $post["user_status"] = 2;
                $post["user_delete"] = 1;
                $post["user_guid"] = $this->get_guid();
                $insert_user = Db::name("user")->insert($post);
                if ($insert_user === false) {
                    $re["code"] = 40002;
                    $re["msg"] = "添加失败";
                } else {
                    $re["code"] = 0;
                    $re["msg"] = "添加成功";
                }
            } else {
                $re["code"] = 40001;
                $re["msg"] = $UserValidate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function user_edit()
    {
        $re = [];
        try {
            $post = Tool::Input(input("post."));
            $UserValidate = new UserValidate();
            if ($UserValidate->scene("user_edit")->check($post)) {
                $user = Db::name("user");
                $user_id = $post["user_id"];
                $this->user_recheck($post, $user_id);

                if ($post["user_password"]) {
                    $post["user_password"] = Login::password_encryption($post["user_password"]);
                }
                $update_user = Db::name("user")->where([
                    "user_id" => $user_id,
                ])->update($post);
                if ($update_user === false) {
                    $re["code"] = 40002;
                    $re["msg"] = "更新失败";
                } else {
                    $re["code"] = 0;
                    $re["msg"] = "更新成功";
                }
            } else {
                $re["code"] = 40001;
                $re["msg"] = $UserValidate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function user_delete()
    {
        $re = [];
        $post = Tool::Input(input("post."));
        $judge = Tool::Judge($post, [
            "user_id",
        ]);

        if ($judge["code"] === 0) {
            $user_id = $post["user_id"];

            $update_user = Db::name("user")
                ->where([
                    "user_id" => $user_id,
                    "user_delete" => 1,
                ])
                ->update([
                    "user_delete" => 2,
                ]);
            if ($update_user === false) {
                $re["code"] = 40000;
                $re["msg"] = "删除失败";
            } else {
                $re["code"] = 0;
                $re["msg"] = "删除成功";
            }
        } else {
            $re = $judge;
        }
        return json($re);
    }

    public function template()
    {
        try {
            $data = [
                ["名称", "密码", "编号", "手机号", "性别"],
                ["叶问", "123456", "190101", "13745673214", "男/女"],
            ];
            return Excel::export("用户导入模板", $data);
        } catch (\Throwable $th) {
            echo $th->getMessage();
        }
    }

    public function import()
    {
        $re = [];
        try {
            $file = input("file.file");
            if ($file) {
                $info = $file->validate([
                    'size' => Config::get("upload.size"),
                    'ext' => 'xlsx',
                ])->move(Config::get("upload.save_path"));
                if ($info) {
                    $save_path = $info->getRealPath();
                    $spreadsheet = IOFactory::load($save_path);
                    $worksheet = $spreadsheet->getActiveSheet();
                    $excel_array = $worksheet->toarray();
                    // dump($excel_array);
                    $judge_title = Excel::judge_title($excel_array, [
                        "名称", "密码", "编号", "手机号", "性别",
                    ]);
                    if ($judge_title) {
                        array_shift($excel_array);
                        $excel_array = Excel::clear_empty_line($excel_array);
                        $error = [];
                        $data = Excel::judge_field($excel_array, [
                            "user_name" => [
                                "validate" => [
                                    "rule" => "require",
                                    "msg" => [
                                        "require" => "名称不能为空",
                                    ],
                                ],
                            ],
                            "user_password" => [
                                "default" => 123456,
                            ],
                            "user_number" => [
                                "validate" => [
                                    "rule" => "require",
                                    "msg" => [
                                        "require" => "编号不能为空",
                                    ],
                                ],
                            ],
                            "user_phone" => [
                                "validate" => [
                                    "rule" => "require|length:11",
                                    "msg" => [
                                        "require" => "手机号不能为空",
                                        "length" => "手机号长度错误",
                                    ],
                                ],
                            ],
                            "user_sex" => [
                                "validate" => [
                                    "rule" => "require|in:男,女",
                                    "msg" => [
                                        "require" => "性别不能为空",
                                        "in" => "性别值错误（男/女）",
                                    ],
                                ],
                            ],
                        ], $error);
                        if (count($error) === 0) {
                            $user = Db::name("user");
                            $select_user = $user->where([
                                "user_delete" => 1,
                            ])->select();
                            $user_phone = [];
                            foreach ($select_user as $select_user_value) {
                                array_push($user_phone, $select_user_value["user_phone"]);
                            }
                            foreach ($data as $data_value) {
                                $data_value["user_password"] = Login::password_encryption($data_value["user_password"]);
                                $data_value = array_merge($data_value, [
                                    "user_status" => 2,
                                    "user_delete" => 1,
                                    "user_admin" => 2,
                                    "user_guid" => $this->get_guid(),
                                ]);
                                if (!in_array($data_value["user_phone"], $user_phone)) {
                                    $insert_user = $user->insert($data_value);
                                    if ($insert_user) {
                                        array_push($user_phone, $data_value["user_phone"]);
                                    } else {
                                        Db::rollback();
                                        $re["code"] = 40006;
                                        $re["msg"] = "保存失败";
                                    }
                                }
                            }
                            Db::commit();
                            $re["code"] = 10000;
                            $re["msg"] = "保存成功：";
                        } else {
                            $re["code"] = 40005;
                            $re["msg"] = join("<br>", $error);
                        }
                    } else {
                        $re["code"] = 40004;
                        $re["msg"] = "表格错误";
                    }
                } else {
                    $re["code"] = 40003;
                    $re["msg"] = $file->getError();
                }
            } else {
                $re["code"] = 40002;
                $re["msg"] = "请上传文件";
            }
        } catch (\Throwable $th) {
            $re["code"] = 40001;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function synchronization_template()
    {
        try {
            $data = [
                [
                    "序号", "学校名称",
                    "学生手机号", "学生姓名", "学生编号",
                    "班主任手机号", "班主任姓名", "班主任编号",
                    "年级名称", "班级名称", "班级专业", "班级实习开始时间", "班级实习结束时间",
                    "企业名称", "企业职位", "企业指导人", "企业指导人手机号"
                ],
                [
                    "1", "胡锦超职业技术学校",
                    "13763219980", "叶某", "160907",
                    "13763219981", "黄某", "100001",
                    "2016级", "1609", "数媒", "2020-01-01", "2020-07-01",
                    "广东某某网络科技有限公司", "品牌文案策划", "梁某", "13974461368"
                ],
            ];
            return Excel::export("用户导入数据模板", $data, function ($worksheet, $data) {
                $end_key = Coordinate::stringFromColumnIndex(count($data[0]));
                $worksheet->getStyle('A:' . $end_key)->applyFromArray([
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                        'wrapText' => true,
                    ],
                ]);
                $worksheet->getDefaultColumnDimension()->setWidth(25);
                $worksheet->getDefaultRowDimension()->setRowHeight(25);
                return $worksheet;
            });
        } catch (\Throwable $th) {
            echo $th->getMessage();
        }
    }

    public function synchronization()
    {
        $re = [];
        try {
            $file = input("file.file");
            if ($file) {
                $info = $file->validate([
                    'size' => Config::get("upload.size"),
                    'ext' => 'xlsx',
                ])->move(Config::get("upload.save_path"));
                if ($info) {
                    $save_path = $info->getRealPath();
                    $spreadsheet = IOFactory::load($save_path);
                    $worksheet = $spreadsheet->getActiveSheet();
                    $excel_array = $worksheet->toarray();
                    $judge_title = Excel::judge_title($excel_array, [
                        "序号", "学校名称",
                        "学生手机号", "学生姓名", "学生编号",
                        "班主任手机号", "班主任姓名", "班主任编号",
                        "年级名称", "班级名称", "班级专业", "班级实习开始时间", "班级实习结束时间",
                        "企业名称", "企业职位", "企业指导人", "企业指导人手机号"
                    ]);
                    if ($judge_title) {
                        array_shift($excel_array);
                        $excel_array = Excel::clear_empty_line($excel_array);
                        list($error, $data) = Excel::judge_field($excel_array, [
                            "index" => [
                                "index" => true,
                                "validate" => [
                                    "rule" => "require",
                                    "msg" => [
                                        "require" => "序号不能为空",
                                    ],
                                ],
                            ],
                            "school_name" => [
                                "validate" => [
                                    "rule" => "require",
                                    "msg" => [
                                        "require" => "学校不能为空",
                                    ],
                                ],
                            ],
                            "student_phone" => [
                                "validate" => [
                                    "rule" => "require|length:11",
                                    "msg" => [
                                        "require" => "学生手机号不能为空",
                                        "length" => "学生手机号长度错误",
                                    ],
                                ],
                            ],
                            "student_name" => [
                                "validate" => [
                                    "rule" => "require",
                                    "msg" => [
                                        "require" => "学生姓名不能为空",
                                    ],
                                ],
                            ],
                            "student_number" => [
                                "validate" => [
                                    "rule" => "require",
                                    "msg" => [
                                        "require" => "学生编号不能为空",
                                    ],
                                ],
                            ],
                            "teacher_phone" => [
                                "validate" => [
                                    "rule" => "require|length:11",
                                    "msg" => [
                                        "require" => "班主任手机号不能为空",
                                        "length" => "班主任手机号长度错误",
                                    ],
                                ],
                            ],
                            "teacher_name" => [
                                "validate" => [
                                    "rule" => "require",
                                    "msg" => [
                                        "require" => "班主任姓名不能为空",
                                    ],
                                ],
                            ],
                            "teacher_number" => [
                                "validate" => [
                                    "rule" => "require",
                                    "msg" => [
                                        "require" => "班主任编号不能为空",
                                    ],
                                ],
                            ],
                            "grade_name" => [
                                "validate" => [
                                    "rule" => "require",
                                    "msg" => [
                                        "require" => "年级名称不能为空",
                                    ],
                                ],
                            ],
                            "class_name" => [
                                "validate" => [
                                    "rule" => "require",
                                    "msg" => [
                                        "require" => "班级名称不能为空",
                                    ],
                                ],
                            ],
                            "class_note" => [
                                "validate" => [
                                    "rule" => "require",
                                    "msg" => [
                                        "require" => "班级专业不能为空",
                                    ],
                                ],
                            ],
                            "organize_internship_start" => [
                                "validate" => [
                                    "rule" => "require|date",
                                    "msg" => [
                                        "require" => "班级实习开始时间不能为空",
                                        "date" => "班级实习开始时间格式错误",
                                    ],
                                ],
                            ],
                            "organize_internship_end" => [
                                "validate" => [
                                    "rule" => "require|date",
                                    "msg" => [
                                        "require" => "班级实习结束时间不能为空",
                                        "date" => "班级实习结束时间格式错误",
                                    ],
                                ],
                            ],
                            "enterprise_name" => [
                                "validate" => [
                                    "rule" => "require",
                                    "msg" => [
                                        "require" => "企业名称不能为空",
                                    ],
                                ],
                            ],
                            "enterprise_position_name" => [
                                "validate" => [
                                    "rule" => "require",
                                    "msg" => [
                                        "require" => "企业职位不能为空",
                                    ],
                                ],
                            ],
                            "enterprise_head_user_name" => [
                                "validate" => [
                                    "rule" => "require",
                                    "msg" => [
                                        "require" => "企业指导人不能为空",
                                    ],
                                ],
                            ],
                            "enterprise_head_user_phone" => [
                                "validate" => [
                                    "rule" => "require|length:11",
                                    "msg" => [
                                        "require" => "企业指导人手机号不能为空",
                                        "length" => "企业指导人手机号长度错误",
                                    ],
                                ],
                            ],
                        ]);
                        if (count($error) === 0) {
                            Db::startTrans();
                            $user = Db::name("user");
                            $user_role = Db::name("user_role");
                            $organize = Db::name("organize");
                            $user_organize = Db::name("user_organize");
                            $enterprise = Db::name("enterprise");
                            $enterprise_position = Db::name("enterprise_position");
                            $enterprise_position_user = Db::name("enterprise_position_user");
                            foreach ($data as &$data_value) {
                                $line = $data_value["index"];
                                $error_msg = "第{$line}行：";

                                //学校
                                $data_value["school_id"] = $organize->where([
                                    "organize_name" => $data_value["school_name"],
                                    "organize_status" => 1,
                                    "organize_delete" => 1,
                                ])->value("organize_id");
                                if (!$data_value["school_id"]) {
                                    $data_value["school_id"] = $organize->insertGetId([
                                        "organize_parent_id" => 0,
                                        "organize_name" => $data_value["school_name"],
                                        "organize_internship_start" => "",
                                        "organize_internship_end" => "",
                                        "organize_note" => "",
                                        "organize_status" => 1,
                                        "organize_delete" => 1,
                                        "organize_guid" => $this->get_guid()
                                    ]);
                                }

                                //学生
                                $data_value["student_id"] = $user->where([
                                    "user_phone" => $data_value["student_phone"]
                                ])->value("user_id");
                                if ($data_value["student_id"]) {
                                    array_push($error, $error_msg . "学生已存在");
                                } else {
                                    $data_value["student_id"] = $user->insertGetId([
                                        "user_name" => $data_value["student_name"],
                                        "user_number" => $data_value["student_number"],
                                        "user_phone" => $data_value["student_phone"],
                                        "user_identity_card" => "",
                                        "user_sex" => "",
                                        "user_unionid" => "",
                                        "user_openid_gzh" => "",
                                        "user_admin" => 2,
                                        "user_status" => 2,
                                        "user_delete" => 1,
                                        "user_guid" => $this->get_guid()
                                    ]);
                                    array_push($error, $error_msg . "学生添加成功");
                                }

                                //学生角色
                                $data_value["student_user_role_id"] = $user_role->where([
                                    "user_id" => $data_value["student_id"],
                                    "role_id" => 6,
                                ])->value("user_role_id");
                                if (!$data_value["student_user_role_id"]) {
                                    $data_value["student_user_role_id"] = $user_role->insertGetId([
                                        "user_id" => $data_value["student_id"],
                                        "role_id" => 6,
                                        "user_role_guid" => $this->get_guid()
                                    ]);
                                }

                                //班主任
                                $data_value["teacher_id"] = $user->where([
                                    "user_phone" => $data_value["teacher_phone"]
                                ])->value("user_id");
                                if ($data_value["teacher_id"]) {
                                    array_push($error, $error_msg . "班主任已存在");
                                } else {
                                    $data_value["teacher_id"] = $user->insertGetId([
                                        "user_name" => $data_value["teacher_name"],
                                        "user_number" => $data_value["teacher_number"],
                                        "user_phone" => $data_value["teacher_phone"],
                                        "user_identity_card" => "",
                                        "user_sex" => "",
                                        "user_unionid" => "",
                                        "user_openid_gzh" => "",
                                        "user_admin" => 2,
                                        "user_status" => 2,
                                        "user_delete" => 1,
                                        "user_guid" => $this->get_guid()
                                    ]);
                                    array_push($error, $error_msg . "班主任已存在");
                                }

                                //教师角色
                                $data_value["teacher_user_role_id"] = $user_role->where([
                                    "user_id" => $data_value["teacher_id"],
                                    "role_id" => 7,
                                ])->value("user_role_id");
                                if (!$data_value["teacher_user_role_id"]) {
                                    $data_value["teacher_user_role_id"] = $user_role->insertGetId([
                                        "user_id" => $data_value["teacher_id"],
                                        "role_id" => 7,
                                        "user_role_guid" => $this->get_guid()
                                    ]);
                                }

                                //年级
                                $data_value["grade_id"] = $organize->where([
                                    "organize_parent_id" => $data_value["school_id"],
                                    "organize_name" => $data_value["grade_name"],
                                    "organize_status" => 1,
                                    "organize_delete" => 1,
                                ])->value("organize_id");
                                if (!$data_value["grade_id"]) {
                                    $data_value["grade_id"] = $organize->insertGetId([
                                        "organize_parent_id" => $data_value["school_id"],
                                        "organize_name" => $data_value["grade_name"],
                                        "organize_internship_start" => "",
                                        "organize_internship_end" => "",
                                        "organize_note" => "",
                                        "organize_status" => 1,
                                        "organize_delete" => 1,
                                        "organize_guid" => $this->get_guid()
                                    ]);
                                    array_push($error, $error_msg . "年级添加成功");
                                }

                                //班级
                                $data_value["class_id"] = $organize->where([
                                    "organize_parent_id" => $data_value["grade_id"],
                                    "organize_name" => $data_value["class_name"],
                                    "organize_status" => 1,
                                    "organize_delete" => 1,
                                ])->value("organize_id");
                                if (!$data_value["class_id"]) {
                                    $data_value["class_id"] = $organize->insertGetId([
                                        "organize_parent_id" => $data_value["grade_id"],
                                        "organize_name" => $data_value["class_name"],
                                        "organize_note" => $data_value["class_note"],
                                        "organize_internship_start" => date("Y-m-d", strtotime($data_value["organize_internship_start"])),
                                        "organize_internship_end" => date("Y-m-d", strtotime($data_value["organize_internship_end"])),
                                        "organize_status" => 1,
                                        "organize_delete" => 1,
                                        "organize_guid" => $this->get_guid()
                                    ]);
                                    array_push($error, $error_msg . "班级添加成功");
                                }

                                //学生组织架构
                                $data_value["student_user_organize_id"] = $user_organize->where([
                                    "user_id" => $data_value["student_id"],
                                    "organize_id" => $data_value["class_id"],
                                ])->value("user_organize_id");
                                if (!$data_value["student_user_organize_id"]) {
                                    $data_value["student_user_organize_id"] = $user_organize->insertGetId([
                                        "user_id" => $data_value["student_id"],
                                        "organize_id" => $data_value["class_id"],
                                        "user_organize_guid" => $this->get_guid()
                                    ]);
                                }

                                //教师组织架构
                                $data_value["teacher_user_organize_id"] = $user_organize->where([
                                    "user_id" => $data_value["teacher_id"],
                                    "organize_id" => $data_value["class_id"],
                                ])->value("user_organize_id");
                                if (!$data_value["teacher_user_organize_id"]) {
                                    $data_value["teacher_user_organize_id"] = $user_organize->insertGetId([
                                        "user_id" => $data_value["teacher_id"],
                                        "organize_id" => $data_value["class_id"],
                                        "user_organize_guid" => $this->get_guid()
                                    ]);
                                }


                                //企业指导人
                                $data_value["enterprise_head_user_id"] = $user->where([
                                    "user_phone" => $data_value["enterprise_head_user_phone"]
                                ])->value("user_id");
                                if (!$data_value["enterprise_head_user_id"]) {
                                    $data_value["enterprise_head_user_id"] = $user->insertGetId([
                                        "user_name" => $data_value["enterprise_head_user_name"],
                                        "user_phone" => $data_value["enterprise_head_user_phone"],
                                        "user_number" => "",
                                        "user_identity_card" => "",
                                        "user_sex" => "",
                                        "user_unionid" => "",
                                        "user_openid_gzh" => "",
                                        "user_admin" => 2,
                                        "user_status" => 2,
                                        "user_delete" => 1,
                                        "user_guid" => $this->get_guid()
                                    ]);
                                }

                                //企业指导人角色
                                $data_value["enterprise_head_user_role_id"] = $user_role->where([
                                    "user_id" => $data_value["enterprise_head_user_id"],
                                    "role_id" => 9,
                                ])->value("user_role_id");
                                if (!$data_value["enterprise_head_user_role_id"]) {
                                    $data_value["enterprise_head_user_role_id"] = $user_role->insertGetId([
                                        "user_id" => $data_value["enterprise_head_user_id"],
                                        "role_id" => 9,
                                        "user_role_guid" => $this->get_guid()
                                    ]);
                                }

                                //企业
                                $data_value["enterprise_id"] = $enterprise->where([
                                    "enterprise_name" => $data_value["enterprise_name"]
                                ])->value("enterprise_id");
                                if ($data_value["enterprise_id"]) {
                                    //企业职位
                                    $data_value["enterprise_position_id"] = $enterprise_position->where([
                                        "enterprise_id" => $data_value["enterprise_id"],
                                        "enterprise_position_name" => $data_value["enterprise_position_name"],
                                    ])->value("enterprise_position_id");
                                    if ($data_value["enterprise_position_id"]) {
                                        //学生企业职位
                                        $data["enterprise_position_user_id"] = $enterprise_position_user->where([
                                            "enterprise_position_id" => $data_value["enterprise_position_id"],
                                            "user_id" => $data_value["student_id"],
                                        ])->value("enterprise_position_user_id");
                                        if (!$data["enterprise_position_user_id"]) {
                                            $data_value["enterprise_position_user_id"] = $enterprise_position_user->insertGetId([
                                                "enterprise_position_id" => $data_value["enterprise_position_id"],
                                                "user_id" => $data_value["student_id"],
                                                "enterprise_position_user_summary" => "",
                                                "enterprise_position_user_evaluation" => "",
                                                "enterprise_position_user_head_user_id" => $data_value["enterprise_head_user_id"],
                                                "enterprise_position_user_guid" => $this->get_guid()
                                            ]);
                                        }
                                    } else {
                                        array_push($error, $error_msg . "企业职位不存在【" . $data_value["enterprise_position_name"] . "】");
                                    }
                                } else {
                                    array_push($error, $error_msg . "企业不存在【" . $data_value["enterprise_name"] . "】");
                                }
                            }
                            Db::commit();
                            // Db::rollback();
                            $re["code"] = 10000;
                            $re["msg"] = join("<br>", $error);
                        } else {
                            $re["code"] = 40005;
                            $re["msg"] = join("<br>", $error);
                        }
                    } else {
                        $re["code"] = 40004;
                        $re["msg"] = "表格错误";
                    }
                } else {
                    $re["code"] = 40003;
                    $re["msg"] = $file->getError();
                }
            } else {
                $re["code"] = 40002;
                $re["msg"] = "请上传文件";
            }
        } catch (\Throwable $th) {
            $re["code"] = 40001;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    /**
     *用户查重
     * @param array $data 用户数据
     * @return error
     */
    protected function user_recheck(array $data, int $id = null)
    {
        $user = Db::name("user");
        $con = [];
        if ($id) {
            $con["user_id"] = [
                "neq", $id,
            ];
        }
        $con["user_delete"] = 1;
        if (isset($data["user_phone"])) {
            $con["user_phone"] = $data["user_phone"];
            $find_user = $user->where($con)->find();
            if ($find_user) {
                throw new \Exception("手机号重复", 1);
            }
        }

        // return $find_user ? true : false;
    }
}
