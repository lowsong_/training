<?php

namespace app\adjfut\controller;

use Adjfut\Common;
use Adjfut\Tool\Tool;
use app\adjfut\validate\EnterprisePositionUser as EnterprisePositionUserValidate;
use think\Db;

class EnterprisePositionUser extends Common
{
    public function enterprise_position_user_tree()
    {
        $re = [];
        try {
            // $select_enterprise = Db::name("enterprise")->join([
            //     ["enterprise_position", "enterprise_position.enterprise_id = enterprise.enterprise_id"],
            // ])->where([
            //     // "enterprise.enterprise_status" => "启用",
            //     "enterprise.enterprise_delete" => "启用",
            //     "enterprise_position.enterprise_position_status" => "启用",
            //     "enterprise_position.enterprise_position_delete" => "启用",
            // ])->field([
            //     "enterprise.enterprise_name",
            //     "enterprise.enterprise_id",
            //     "enterprise_position.enterprise_position_name",
            //     "enterprise_position.enterprise_position_id",
            // ])->select();
            $select_enterprise = Db::name("enterprise")->where([
                // "enterprise.enterprise_status" => "启用",
                "enterprise.enterprise_delete" => "启用",
            ])->field([
                "enterprise.enterprise_name",
                "enterprise.enterprise_id",
            ])->select();
            $data = [];
            if ($select_enterprise) {
                foreach ($select_enterprise as &$select_enterprise_value) {
                    $enterprise_id = $select_enterprise_value["enterprise_id"];
                    if (!isset($data[$enterprise_id])) {
                        $a = $select_enterprise_value;
                        $a["enterprise_position_id"] = "";
                        $data[$enterprise_id] = [
                            "title" => $select_enterprise_value["enterprise_name"],
                            "id" => $enterprise_id,
                            "data" => $a,
                            "children" => [],
                            "spread" => true,
                        ];
                    }
                }
                $select_enterprise_position = Db::name("enterprise_position")->where([
                    "enterprise_position.enterprise_id" => [
                        "in", array_keys($data)
                    ],
                    "enterprise_position.enterprise_position_status" => "启用",
                    "enterprise_position.enterprise_position_delete" => "启用",
                ])->field([
                    "enterprise_position.enterprise_id",
                    "enterprise_position.enterprise_position_name",
                    "enterprise_position.enterprise_position_id",
                ])->select();
                foreach ($select_enterprise_position as $select_enterprise_position_value) {
                    $enterprise_id = $select_enterprise_position_value["enterprise_id"];
                    array_push($data[$enterprise_id]["children"], [
                        "title" => $select_enterprise_position_value["enterprise_position_name"],
                        "id" => $select_enterprise_position_value["enterprise_position_id"],
                        "data" => $select_enterprise_position_value,
                    ]);
                }
                $re["code"] = 0;
                $re["data"] = [[
                    "title" => "企业列表",
                    "id" => time(),
                    "spread" => true,
                    "children" => array_values($data),
                ]];
                $re["msg"] = "ok";
            } else {
                $re["code"] = 40001;
                $re["msg"] = "暂无数据";
            }
        } catch (\Throwable $th) {
            $re["code"] = 40004;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function enterprise_position_user_list()
    {
        $re = [];
        try {
            $enterprise_id = input("enterprise_id");
            $enterprise_position_id = input("enterprise_position_id");
            $page = input("page");
            $limit = input("limit");
            $join = [];
            $join = [
                ["v_user_role", "v_user_role.user_id = enterprise_position_user.user_id"],
                ["v_user_organize", "v_user_organize.user_id = enterprise_position_user.user_id"],
                // ["user", "user.user_id = enterprise_position_user.user_id"],
                // ["user_role", "user_role.user_id = user.user_id"],
                // ["role", "role.role_id = user_role.role_id"],
                // ["user_role","user_role.user_id = user.user_id"],
                ["enterprise_position", "enterprise_position.enterprise_position_id = enterprise_position_user.enterprise_position_id"],
                ["enterprise", "enterprise.enterprise_id = enterprise_position.enterprise_id"],
            ];
            $con = [];
            if ($enterprise_id) {
                $con["enterprise_position.enterprise_id"] = $enterprise_id;
            }
            if ($enterprise_position_id) {
                $con["enterprise_position.enterprise_position_id"] = $enterprise_position_id;
            }
            $con["v_user_role.user_status"] = 2;
            $con["v_user_role.user_delete"] = 1;
            $con["v_user_role.role_status"] = 1;
            $con["v_user_role.role_delete"] = 1;
            // $con["v_user_organize.user_status"] = 1;
            // $con["v_user_organize.user_delete"] = 1;
            $con["v_user_organize.organize_status"] = 1;
            $con["v_user_organize.organize_delete"] = 1;

            $con["v_user_role.role_name"] = "学生";
            $con["enterprise_position.enterprise_position_status"] = "启用";
            $con["enterprise_position.enterprise_position_delete"] = "启用";
            // $con["enterprise.enterprise_status"] = "启用";
            $con["enterprise.enterprise_delete"] = "启用";
            $field = [];
            $field = [
                "v_user_role.user_name",
                "v_user_organize.organize_name",
                "enterprise_position.enterprise_position_name",
                "enterprise.enterprise_name",
                "v_user_role.user_number",
                "v_user_role.user_phone",
            ];
            $select_enterprise_position_user = Db::name("enterprise_position_user")->join($join)->where($con)->field($field)->page($page, $limit)->select();
            $count_enterprise_position_user = Db::name("enterprise_position_user")->join($join)->where($con)->count();
            if ($select_enterprise_position_user && $count_enterprise_position_user) {
                // foreach ($select_enterprise_position_user as &$select_enterprise_position_user_value) {
                //     $enterprise_position_user_register_time = $select_enterprise_position_user_value["enterprise_position_user_register_time"];
                // }
                $re["code"] = 0;
                $re["data"] = $select_enterprise_position_user;
                $re["count"] = $count_enterprise_position_user;
                $re["msg"] = "ok";
            } else {
                $re["code"] = 40000;
                $re["msg"] = "暂无数据";
            }
        } catch (\Throwable $th) {
            $re["code"] = 40004;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function enterprise_position_user_power()
    {
        $re = [];
        try {
            $post = Tool::Input(input("post."));
            $EnterprisePositionUserValidate = new EnterprisePositionUserValidate();
            if ($EnterprisePositionUserValidate->scene("power")->check($post)) {
                $organize_id = $post["organize_id"];
                $info = $post["info"];
                $info = htmlspecialchars_decode($info);
                $info = explode(",", $info);
                Db::startTrans();
                try {
                    $organize_menu = Db::name("organize_menu");
                    $delete_organize_menu = $organize_menu->where([
                        "organize_id" => $organize_id,
                    ])->delete();
                    $data = [];
                    foreach ($info as $info_value) {
                        array_push($data, [
                            "organize_id" => $organize_id,
                            "menu_id" => $info_value,
                            "organize_menu_guid" => $this->get_guid(),
                        ]);
                    }
                    $insertAll_organize_menu = $organize_menu->insertAll($data);
                    if ($insertAll_organize_menu !== false && $delete_organize_menu !== false) {
                        Db::commit();
                        $re["code"] = 0;
                        $re["msg"] = "授权成功";
                    } else {
                        Db::rollback();
                        $re["code"] = 40002;
                        $re["msg"] = "授权失败";
                    }
                } catch (\Exception $e) {
                    Db::rollback();
                    $re["code"] = 40003;
                    $re["data"] = $e->getMessage();
                    $re["msg"] = "服务器错误";
                }
            } else {
                $re["code"] = 40001;
                $re["msg"] = $EnterprisePositionUserValidate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function enterprise_position_user_info()
    {
        $re = [];
        try {
            $post = Tool::Input(input("post."));
            $EnterprisePositionUserValidate = new EnterprisePositionUserValidate();
            if ($EnterprisePositionUserValidate->scene("info")->check($post)) {
                $find_enterprise_position_user = Db::name("enterprise_position_user")->where([
                    "enterprise_position_user_id" => $post["enterprise_position_user_id"],
                    "enterprise_position_user_delete" => 1,
                ])->find();
                if ($find_enterprise_position_user) {
                    $re["code"] = 0;
                    $re["data"] = $find_enterprise_position_user;
                    $re["msg"] = "ok";
                } else {
                    $re["code"] = 40002;
                    $re["msg"] = "数据不存在";
                }
            } else {
                $re["code"] = 40001;
                $re["msg"] = $EnterprisePositionUserValidate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function enterprise_position_user_add()
    {
        $re = [];
        try {
            $post = Tool::Input(input("post."));
            $EnterprisePositionUserValidate = new EnterprisePositionUserValidate();
            if ($EnterprisePositionUserValidate->scene("enterprise_position_user_add")->check($post)) {
                $enterprise_position_user = Db::name("enterprise_position_user");
                $enterprise_position_id = $post["enterprise_position_id"];
                $enterprise_id = $post["enterprise_id"];
                $info = $post["info"];
                $info = htmlspecialchars_decode($info);
                $info = explode(",", $info);
                $data = [];
                $select_enterprise_position_user = $enterprise_position_user->where([
                    "enterprise_position_id" => $enterprise_position_id,
                    // "enterprise_id" => $enterprise_id,
                ])->select();
                $user_id = array_map(function ($value) {
                    return $value["user_id"];
                }, $select_enterprise_position_user);
                foreach ($info as $info_value) {
                    if (!in_array($info_value, $user_id)) {
                        array_push($data, [
                            "user_id" => $info_value,
                            "enterprise_position_id" => $enterprise_position_id,
                            "enterprise_position_user_guid" => $this->get_guid(),
                        ]);
                    }
                }
                $insertAll_enterprise_position_user = $enterprise_position_user->insertAll($data);
                if ($insertAll_enterprise_position_user === false) {
                    $re["code"] = 40002;
                    $re["msg"] = "添加失败";
                } else {
                    $re["code"] = 0;
                    $re["msg"] = "添加成功";
                }
            } else {
                $re["code"] = 40001;
                $re["msg"] = $EnterprisePositionUserValidate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function enterprise_position_user_delete()
    {
        $re = [];
        try {
            $post = Tool::Input(input("post."));
            $EnterprisePositionUserValidate = new EnterprisePositionUserValidate();
            if ($EnterprisePositionUserValidate->scene("delete")->check($post)) {
                $delete_enterprise_position_user = Db::name("enterprise_position_user")->where([
                    "enterprise_position_user_id" => $post["enterprise_position_user_id"],
                ])->delete();
                if ($delete_enterprise_position_user === false) {
                    $re["code"] = 40002;
                    $re["msg"] = "删除失败";
                } else {
                    $re["code"] = 0;
                    $re["msg"] = "删除成功";
                }
            } else {
                $re["code"] = 40001;
                $re["msg"] = $EnterprisePositionUserValidate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function enterprise_position_user_bind()
    {
        $re = [];
        try {
            $post = Tool::Input(input(""));
            $EnterprisePositionUserValidate = new EnterprisePositionUserValidate();
            if ($EnterprisePositionUserValidate->scene("enterprise_position_user_bind")->check($post)) {
                $v_user_role = Db::name("v_user_role")->where([
                    "role_name" => [
                        "neq", "学生",
                    ],
                ])->field([
                    "user_id",
                ])->select();
                $v_enterprise_position_user = Db::name("v_enterprise_position_user")->where([])->field([
                    "user_id",
                ])->select();
                $user = [];
                foreach ($v_user_role as $v_user_role_value) {
                    array_push($user, $v_user_role_value["user_id"]);
                }
                foreach ($v_enterprise_position_user as $v_enterprise_position_user_value) {
                    array_push($user, $v_enterprise_position_user_value["user_id"]);
                }
                // $user_user = Db::name("user")->join([
                //     ["v_enterprise_position_user","v_enterprise_position_user.user_id = user.user_id","left"]
                // ])->select();
                // $user_v_enterprise_position_user = Db::name("v_enterprise_position_user")->join([
                //     ["v_user_role","v_user_role.user_id = v_enterprise_position_user.user_id"]
                // ])->where([
                //     "v_user_role.role_name" => "学生"
                //     // "enterprise_id" => $post["enterprise_id"],
                //     // "enterprise_position_id" => $post["enterprise_position_id"],
                // ])->select();
                $con = [];
                $con["user_status"] = 2;
                $con["user_delete"] = 1;
                $con["user_id"] = [
                    "not in", $user,
                ];
                $select_user = Db::name("user")->where($con)->page(input("page"), input("limit"))->select();
                $count_user = Db::name("user")->where($con)->count();
                if ($select_user && $count_user) {
                    $re["code"] = 0;
                    $re["data"] = $select_user;
                    $re["count"] = $count_user;
                    $re["msg"] = "ok";
                } else {
                    $re["code"] = 40002;
                    $re["msg"] = "暂无数据";
                }
            } else {
                $re["code"] = 40001;
                $re["msg"] = $EnterprisePositionUserValidate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }

    public function enterprise_position_user_view()
    {
        $re = [];
        try {
            $post = Tool::Input(input(""));
            $EnterprisePositionUserValidate = new EnterprisePositionUserValidate();
            if ($EnterprisePositionUserValidate->scene("view")->check($post)) {
                $join = [];
                $join = [
                    ["user", "user.user_id = enterprise_position_user.user_id"],
                ];
                $con = [];
                $con["user.user_status"] = 2;
                $con["user.user_delete"] = 1;
                $con['enterprise_position_user.organize_id'] = $post["organize_id"];
                $select_enterprise_position_user = Db::name("enterprise_position_user")->join($join)->where($con)->page(input("page"), input("limit"))->select();
                $count_enterprise_position_user = Db::name("enterprise_position_user")->join($join)->where($con)->count();
                if ($select_enterprise_position_user && $count_enterprise_position_user) {
                    $re["code"] = 0;
                    $re["data"] = $select_enterprise_position_user;
                    $re["count"] = $count_enterprise_position_user;
                    $re["msg"] = "ok";
                } else {
                    $re["code"] = 40002;
                    $re["msg"] = "暂无数据";
                }
            } else {
                $re["code"] = 40001;
                $re["msg"] = $EnterprisePositionUserValidate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40000;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }
}
