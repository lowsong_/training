<?php
namespace app\adjfut\validate;

use think\Validate;

class Organize extends Validate
{
    protected $rule = [
        "organize_id|id" => "require|number",
        "organize_parent_id|id" => "require|number",
        "organize_name|名称" => "require",
        "organize_number|单位编码" => "require",
        "organize_status|状态" => "require|number",
    ];

    protected $message = [
        // "role_price.number" =>
    ];

    protected $scene = [
        'organize_add' => [
            'organize_parent_id', 'organize_name', 'organize_number', 'organize_status',
        ],
        'organize_edit' => [
            'organize_id', 'organize_name', 'organize_number','organize_status',
        ],
        'organize_delete' => ['organize_id'],
        'info' => ['organize_id'],
    ];

}
