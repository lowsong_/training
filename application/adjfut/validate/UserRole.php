<?php
namespace app\adjfut\validate;

use think\Validate;

class UserRole extends Validate
{
    protected $rule = [
        "user_role_id|id" => "require",
        "role_id|角色" => "require",
        "info" => "require",
        // "role_id|角色" => "require",
    ];

    protected $message = [
        // "role_price.number" =>
    ];

    protected $scene = [
        'delete' => ['user_role_id'],
        'menu' => ['role_id'],
        'power' => ['role_id', 'info'],
        'view' => ['role_id'],
        'bind' => ['role_id'],
        'add' => ['role_id','info'],
    ];

}
