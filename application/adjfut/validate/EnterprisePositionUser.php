<?php
namespace app\adjfut\validate;

use think\Validate;

class EnterprisePositionUser extends Validate
{
    protected $rule = [
        "enterprise_position_user_id|id" => "require",
        "enterprise_position_id|组织" => "require",
        "info" => "require",
        "enterprise_id|企业" => "require",
        "enterprise_position_id|职位" => "require",
    ];

    protected $message = [
        // "organize_price.number" =>
    ];

    protected $scene = [
        'delete' => ['enterprise_position_user_id'],
        'power' => ['enterprise_position_id', 'info'],
        'view' => ['enterprise_position_id'],
        'enterprise_position_user_bind' => ['enterprise_id', 'enterprise_position_id'],
        'enterprise_position_user_add' => ['enterprise_id', 'enterprise_position_id', 'info'],
    ];

}
