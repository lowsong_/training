<?php
namespace app\adjfut\validate;

use think\Validate;

class User extends Validate
{
    protected $rule = [
        "user_id|ID" => "require",
        "user_name|名称" => "require",
        "user_phone|手机号" => "require|length:11",
        "user_sex|性别" => "require|in:男,女",
    ];

    protected $message = [
        // "role_price.number" =>
    ];

    protected $scene = [
        'user_add' => ['user_name', 'user_phone', 'user_sex'],
        'user_edit' => ['user_id', 'user_name', 'user_phone', 'user_sex'],
        'user_info' => ['user_id'],
    ];

}
