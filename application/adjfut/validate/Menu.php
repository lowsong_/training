<?php
namespace app\adjfut\validate;

use think\Validate;

class Menu extends Validate
{
    protected $rule = [
        "menu_id|id" => "require",
        "menu_name|名称" => "require",
        "menu_status|状态" => "require",
        "menu_index|排序" => "require|number",
        "menu_icon|图标" => "require",
        "menu_url|url" => "require",
    ];

    protected $message = [
        // "role_price.number" =>
    ];

    protected $scene = [
        'add' => ['menu_name', 'menu_status', 'menu_index'],
        'edit' => ['menu_id', 'menu_name', 'menu_status', 'menu_index'],
        'delete' => ['menu_id'],
    ];

}
