<?php
namespace app\adjfut\validate;

use think\Validate;

class Role extends Validate
{
    protected $rule = [
        "role_id|id" => "require",
        "user_id|id" => "require",
        "role_name|名称" => "require",
    ];

    protected $message = [
        // "role_price.number" =>
    ];

    protected $scene = [
        'add' => [
            'role_name',
        ],
        'edit' => [
            'role_id', 'role_name',
        ],
        'info' => ['role_id'],
        'status' => ['role_id'],
        'delete' => ['role_id'],
        'bind_user' => ['role_id'],
        'user_role_delete' => ['user_id', 'role_id'],
    ];

}
