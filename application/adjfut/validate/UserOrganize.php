<?php
namespace app\adjfut\validate;

use think\Validate;

class UserOrganize extends Validate
{
    protected $rule = [
        "user_organize_id|id" => "require",
        "organize_id|组织" => "require",
        "info" => "require",
        // "organize_id|角色" => "require",
    ];

    protected $message = [
        // "organize_price.number" =>
    ];

    protected $scene = [
        'delete' => ['user_organize_id'],
        'menu' => ['organize_id'],
        'power' => ['organize_id', 'info'],
        'view' => ['organize_id'],
        'bind' => ['organize_id'],
        'add' => ['organize_id', 'info'],
    ];

}
