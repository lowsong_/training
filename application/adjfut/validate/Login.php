<?php
namespace app\adjfut\validate;

use think\Validate;

class Login extends Validate
{
    protected $rule = [
        "account|账号" => "require",
        "password|密码" => "require",
    ];

    protected $message = [
        // "role_price.number" =>
    ];

    protected $scene = [
        'login' => ['account', 'password'],
        
    ];

}
