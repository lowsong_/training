<?php
namespace app\system\validate;

use think\Validate;

class System extends Validate
{
    protected $rule = [
        "system_id|ID" => "require",
        //日报
        "system_day_report_status|日报 启用|不启用" => "require",
        "system_day_report_max|日报最大补写天数" => "require",
        //周报
        "system_week_report_status|周报 启用|不启用" => "require",
        "system_week_report_max|周报最大补写天数" => "require",
        //月报
        "system_month_report_status|月报 启用|不启用" => "require",
        "system_month_report_max|月报最大补写天数" => "require",
    ];

    protected $message = [

    ];

    protected $scene = [
        'system_edit' => ['system_id','system_day_report_status','system_day_report_max','system_week_report_status','system_week_report_max','system_month_report_status','system_month_report_max'],
    ];

}
