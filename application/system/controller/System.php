<?php

namespace app\system\controller;

use Adjfut\Common;
use Adjfut\Tool\Tool;
use app\system\validate\System as Validate;
use think\Db;

class System extends Common
{
    public function system_list(){
        $re = [];
        try {
            $find_system = Db::name("system")->find();
            if ($find_system) {
                $re["code"] = 0;
                $re["data"] = $find_system;
                $re["msg"] = "ok";
            } else {
                $re["code"] = 40000;
                $re["msg"] = "暂无数据";
            }
        } catch (\Throwable $th) {
            $re["code"] = 40001;
            $re["msg"] = $th->getMessage();
        }
        return json($re);        
    }
    public function system_edit()
    {
        $re = [];
        try {
            $post = input("post.");
            $validate = new validate();
            if ($validate->scene("edit")->check($post)) {
                $post["system_update_time"] = time();
                $update_system = Db::name("system")->where([
                    "system_id" => $post["system_id"],
                ])->update($post);
                if ($update_system === false) {
                    $re["code"] = 40000;
                    $re["msg"] = "保存设置失败";
                } else {
                    $re["code"] = 0;
                    $re["msg"] = "保存设置成功";
                }
            } else {
                $re["code"] = 40001;
                $re["msg"] = $validate->getError();
            }
        } catch (\Throwable $th) {
            $re["code"] = 40004;
            $re["msg"] = $th->getMessage();
        }
        return json($re);
    }
}
