/*
 Navicat Premium Data Transfer

 Source Server         : 120.237.13.91
 Source Server Type    : MySQL
 Source Server Version : 50562
 Source Host           : 120.237.13.91:23306
 Source Schema         : training

 Target Server Type    : MySQL
 Target Server Version : 50562
 File Encoding         : 65001

 Date: 05/03/2020 09:49:55
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for clock_in
-- ----------------------------
DROP TABLE IF EXISTS `clock_in`;
CREATE TABLE `clock_in`  (
  `clock_in_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NULL DEFAULT NULL COMMENT '用户id',
  `clock_in_time` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打卡时间',
  `clock_in_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '正常|迟到',
  `clock_in_point` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打卡点',
  `clock_in_distance` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '距离',
  `clock_in_enterprise_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `clock_in_enterprise_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `clock_in_enterprise_points` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `clock_in_enterprise_radius` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `clock_in_guid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`clock_in_id`, `clock_in_guid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for clock_in_set
-- ----------------------------
DROP TABLE IF EXISTS `clock_in_set`;
CREATE TABLE `clock_in_set`  (
  `clock_in_set_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `clock_in_set_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打卡设置名称',
  `clock_in_set_start` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打卡设置开始时间',
  `clock_in_set_end` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打卡设置结束时间',
  `clock_in_set_guid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`clock_in_set_id`, `clock_in_set_guid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for enterprise
-- ----------------------------
DROP TABLE IF EXISTS `enterprise`;
CREATE TABLE `enterprise`  (
  `enterprise_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `enterprise_logo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '企业logo',
  `enterprise_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '企业名字',
  `enterprise_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '企业地址',
  `enterprise_introduction` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '企业简介',
  `enterprise_nature` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '企业性质',
  `enterprise_phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '企业联系电话',
  `enterprise_user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '企业负责人',
  `enterprise_points` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '企业定位点',
  `enterprise_radius` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '企业半径',
  `enterprise_status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '审核通过|审核中|审核不通过',
  `enterprise_remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '企业审核理由',
  `enterprise_delete` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '启用|禁用',
  `enterprise_guid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `enterprise_district` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`enterprise_id`, `enterprise_guid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of enterprise
-- ----------------------------
INSERT INTO `enterprise` VALUES (2, '20200107\\dd616ea80034cc18b9f705d732194c3d.jpg', '12121212', '123', '123', '123', '123', '123', '1231', '23123', '审核中', NULL, '启用', '4dd2997f-f95c-cd67-afd8-d300463fca7a', NULL);
INSERT INTO `enterprise` VALUES (3, '20200107\\f3f09b1a58d60f5b0a1c4018c893a4d1.jpg', '胡锦超职业技术学校', '广东省佛山市顺德区容桂文明西路40号', '12312', '1231', '3', '1231', '113.272391,22.756051', '100', '审核中', NULL, '启用', '685e17bc-1c56-86cf-bfa7-d81ec96ff64e', NULL);
INSERT INTO `enterprise` VALUES (4, '20200218\\f70bc76c358a0826d3208733aa95511a.png', '千禧广场', '振华路与景福路交叉口西北100米', '千禧广场企业简介', '千禧广场企业性质', '千禧广场联系电话', '千禧广场企业负责人', '113.274315,22.767463', '200', '审核中', NULL, '启用', '13495a72-0a39-2392-d415-135a7e64dbc5', '广东省佛山市顺德区');

-- ----------------------------
-- Table structure for enterprise_position
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_position`;
CREATE TABLE `enterprise_position`  (
  `enterprise_position_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `enterprise_position_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '企业职位名称',
  `enterprise_position_introduction` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '企业职位简介',
  `enterprise_position_treatment` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '企业职位待遇(用英文逗号分隔)',
  `enterprise_position_number` int(255) NULL DEFAULT NULL COMMENT '企业职位人数',
  `enterprise_position_salary_min` float(255, 2) NULL DEFAULT NULL COMMENT '企业职位最小工资',
  `enterprise_position_salary_max` float(255, 2) NULL DEFAULT NULL COMMENT '企业职位最大工资',
  `enterprise_position_status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '启用|禁用',
  `enterprise_position_delete` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '启用|禁用',
  `enterprise_position_guid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `enterprise_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`enterprise_position_id`, `enterprise_position_guid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of enterprise_position
-- ----------------------------
INSERT INTO `enterprise_position` VALUES (1, '工程师', '简介', '1', 1, 1.00, 1.00, '启用', '启用', '1', 3);

-- ----------------------------
-- Table structure for enterprise_position_user
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_position_user`;
CREATE TABLE `enterprise_position_user`  (
  `enterprise_position_user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `enterprise_position_id` int(11) NULL DEFAULT NULL COMMENT '企业职位id',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '用户id',
  `enterprise_position_user_guid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `enterprise_position_user_summary` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '总结',
  `enterprise_position_user_evaluation` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '评价',
  `enterprise_position_user_head_user_id` int(200) NULL DEFAULT NULL COMMENT '导师id',
  PRIMARY KEY (`enterprise_position_user_id`, `enterprise_position_user_guid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of enterprise_position_user
-- ----------------------------
INSERT INTO `enterprise_position_user` VALUES (1, 1, 7, '1', NULL, '', 0);
INSERT INTO `enterprise_position_user` VALUES (2, 1, 11, '1', NULL, '', 0);
INSERT INTO `enterprise_position_user` VALUES (3, 1, 12, '1', NULL, '', 0);
INSERT INTO `enterprise_position_user` VALUES (4, 1, 13, '1', NULL, NULL, 0);
INSERT INTO `enterprise_position_user` VALUES (5, 1, 15, '1', NULL, NULL, 0);
INSERT INTO `enterprise_position_user` VALUES (6, NULL, NULL, '', NULL, NULL, 0);
INSERT INTO `enterprise_position_user` VALUES (7, 1, 6, '3f89b38d-e16f-c6f2-f8a6-20bec8df7e8c', NULL, NULL, 0);
INSERT INTO `enterprise_position_user` VALUES (11, 1, 32, '685f021f-73e9-293d-6c9f-d9bfcc2af37c', '', '', 34);
INSERT INTO `enterprise_position_user` VALUES (13, 1, 36, '01ac263b-fa7b-36c7-0278-ee5d734b2455', '', '', 34);
INSERT INTO `enterprise_position_user` VALUES (17, 1, 40, 'bd8a6391-ee56-0766-502d-ed379b717eee', '', '', 34);
INSERT INTO `enterprise_position_user` VALUES (18, 1, 8, '113877ca-f8bb-d87b-5cf6-640e159adc3b', NULL, NULL, 0);

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu`  (
  `menu_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '菜单id',
  `menu_parent_id` int(11) NOT NULL COMMENT '父id',
  `menu_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '菜单名称',
  `menu_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单路径',
  `menu_icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单图标',
  `menu_index` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单排序',
  `menu_status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '启用|停用',
  `menu_delete` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '启用|停用',
  `menu_guid` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`menu_id`, `menu_guid`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 34 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES (1, 2, '菜单管理', 'adjfut/menu/index', 'null', '1', '1', '1', '0');
INSERT INTO `menu` VALUES (2, 0, '系统设置', NULL, NULL, NULL, '1', '1', '0');
INSERT INTO `menu` VALUES (26, 25, '日报管理', 'report/report/report_day', '', '1', '1', '1', 'ea6dff86-837f-c208-db5c-7cd4b026b9ce');
INSERT INTO `menu` VALUES (7, 0, '用户管理', '', '', '1', '1', '1', '229183ed-32a5-3b34-1d53-e6f1c57c0867');
INSERT INTO `menu` VALUES (8, 7, '用户列表', 'adjfut/user/index', '', '1', '1', '1', '6cdb843f-3d92-0e0f-adb4-8c87fea5a163');
INSERT INTO `menu` VALUES (9, 2, '角色管理', 'adjfut/role/index', '', '1', '1', '1', '3f6ddbc4-31e4-315e-8535-ec308baa5ad0');
INSERT INTO `menu` VALUES (10, 7, '用户角色', 'adjfut/user_role/index', '', '1', '1', '1', '2a1ca034-7e40-5290-ee93-cebfe1b6d3eb');
INSERT INTO `menu` VALUES (30, 2, '系统参数', 'system/system/index', '', '1', '1', '1', '21de87c1-c044-5908-e2cb-d8126d33ac5d');
INSERT INTO `menu` VALUES (28, 23, '企业职位', 'enterprise/position/index', '', '1', '1', '1', '29657d41-e640-5620-2b4e-a788235614b7');
INSERT INTO `menu` VALUES (23, 0, '企业管理', '', '', '1', '1', '1', 'e497e6bb-93dc-d164-d447-1dabedaec7ee');
INSERT INTO `menu` VALUES (14, 11, '组织架构管理', 'adjfut/organize/index', '', '1', '1', '1', 'f9d81b80-adb2-c20c-cf5f-b54111cf945a');
INSERT INTO `menu` VALUES (27, 7, '用户企业', 'adjfut/enterprise_position_user/index', '', '1', '1', '1', '91de4128-6e12-49cb-b08a-2f22f7d7de45');
INSERT INTO `menu` VALUES (24, 23, '企业管理', 'enterprise/enterprise/index', '', '1', '1', '1', '733246c3-de55-d504-8830-3bf390694a14');
INSERT INTO `menu` VALUES (25, 0, '日志管理', '', '', '1', '1', '1', '839a9706-e0c8-639b-5b95-32ab27fd1624');
INSERT INTO `menu` VALUES (17, 7, '用户组织', 'adjfut/user_organize/index', '', '1', '1', '1', 'cd67e3e6-14d9-d2de-23eb-a5b1f4ba30bd');
INSERT INTO `menu` VALUES (18, 2, '组织管理', 'adjfut/organize/index', '', '1', '1', '1', '5a8ee15a-83d4-aa02-8fa3-0aca60f41025');
INSERT INTO `menu` VALUES (29, 0, '公告管理', 'notice/notice/index', '', '1', '1', '1', 'cd3cf39a-97d0-4860-97ef-2b3ffc1f0927');
INSERT INTO `menu` VALUES (31, 0, '学生管理', '', '', '1', '1', '1', 'e9199b90-3cab-83a3-461c-9d8010040685');
INSERT INTO `menu` VALUES (32, 31, '学生管理', 'student/index/index', '', '1', '1', '1', '41f8bd9c-6705-8f3c-5c34-d8f067d72d98');
INSERT INTO `menu` VALUES (33, 31, '日志管理', 'student/report/index', '', '1', '1', '1', '68928e94-6822-4b7a-647d-8c73d68d0b37');

-- ----------------------------
-- Table structure for menu_copy
-- ----------------------------
DROP TABLE IF EXISTS `menu_copy`;
CREATE TABLE `menu_copy`  (
  `menu_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '菜单id',
  `menu_parent_id` int(11) NOT NULL COMMENT '父id',
  `menu_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '菜单名称',
  `menu_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单路径',
  `menu_icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单图标',
  `menu_index` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单排序',
  `menu_status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '启用|停用',
  `menu_delete` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '启用|停用',
  `menu_guid` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`menu_id`, `menu_guid`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 33 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of menu_copy
-- ----------------------------
INSERT INTO `menu_copy` VALUES (1, 2, '菜单管理', 'adjfut/menu/index', 'null', '1', '1', '1', '0');
INSERT INTO `menu_copy` VALUES (2, 0, '系统设置', NULL, NULL, NULL, '1', '1', '0');
INSERT INTO `menu_copy` VALUES (26, 25, '日报管理', 'report/report/report_day', '', '1', '1', '1', 'ea6dff86-837f-c208-db5c-7cd4b026b9ce');
INSERT INTO `menu_copy` VALUES (7, 0, '用户管理', '', '', '1', '1', '1', '229183ed-32a5-3b34-1d53-e6f1c57c0867');
INSERT INTO `menu_copy` VALUES (8, 7, '用户列表', 'adjfut/user/index', '', '1', '1', '1', '6cdb843f-3d92-0e0f-adb4-8c87fea5a163');
INSERT INTO `menu_copy` VALUES (9, 2, '角色管理', 'adjfut/role/index', '', '1', '1', '1', '3f6ddbc4-31e4-315e-8535-ec308baa5ad0');
INSERT INTO `menu_copy` VALUES (10, 7, '用户角色', 'adjfut/user_role/index', '', '1', '1', '1', '2a1ca034-7e40-5290-ee93-cebfe1b6d3eb');
INSERT INTO `menu_copy` VALUES (30, 2, '系统参数', 'system/system/index', '', '1', '1', '1', '21de87c1-c044-5908-e2cb-d8126d33ac5d');
INSERT INTO `menu_copy` VALUES (28, 23, '企业职位', 'enterprise/position/index', '', '1', '1', '1', '29657d41-e640-5620-2b4e-a788235614b7');
INSERT INTO `menu_copy` VALUES (23, 0, '企业管理', '', '', '1', '1', '1', 'e497e6bb-93dc-d164-d447-1dabedaec7ee');
INSERT INTO `menu_copy` VALUES (14, 11, '组织架构管理', 'adjfut/organize/index', '', '1', '1', '1', 'f9d81b80-adb2-c20c-cf5f-b54111cf945a');
INSERT INTO `menu_copy` VALUES (27, 7, '用户企业', 'adjfut/enterprise_position_user/index', '', '1', '1', '1', '91de4128-6e12-49cb-b08a-2f22f7d7de45');
INSERT INTO `menu_copy` VALUES (24, 23, '企业管理', 'enterprise/enterprise/index', '', '1', '1', '1', '733246c3-de55-d504-8830-3bf390694a14');
INSERT INTO `menu_copy` VALUES (25, 0, '日志管理', '', '', '1', '1', '1', '839a9706-e0c8-639b-5b95-32ab27fd1624');
INSERT INTO `menu_copy` VALUES (17, 7, '用户组织', 'adjfut/user_organize/index', '', '1', '1', '1', 'cd67e3e6-14d9-d2de-23eb-a5b1f4ba30bd');
INSERT INTO `menu_copy` VALUES (18, 2, '组织管理', 'adjfut/organize/index', '', '1', '1', '1', '5a8ee15a-83d4-aa02-8fa3-0aca60f41025');
INSERT INTO `menu_copy` VALUES (29, 0, '公告管理', 'notice/notice/index', '', '1', '1', '1', 'cd3cf39a-97d0-4860-97ef-2b3ffc1f0927');
INSERT INTO `menu_copy` VALUES (31, 0, '学生管理', '', '', '1', '1', '1', 'e9199b90-3cab-83a3-461c-9d8010040685');
INSERT INTO `menu_copy` VALUES (32, 31, '学生管理', 'student/index/index', '', '1', '1', '1', '41f8bd9c-6705-8f3c-5c34-d8f067d72d98');

-- ----------------------------
-- Table structure for notice
-- ----------------------------
DROP TABLE IF EXISTS `notice`;
CREATE TABLE `notice`  (
  `notice_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `organize_id` int(11) NOT NULL COMMENT '组织架构id',
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `notice_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公告标题',
  `notice_content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '公告内容',
  `notice_file_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `notice_file_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `notice_time` int(255) NOT NULL,
  `notice_status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '启用|禁用',
  `notice_delete` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '启用|禁用',
  `notice_guid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`notice_id`, `notice_guid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of notice
-- ----------------------------
INSERT INTO `notice` VALUES (1, 1, 1, '123123', '1231231哈哈谁离开的飞机撒两地分居', NULL, NULL, 0, '启用', '停用', '09daf499-bd1a-9d13-1a28-11deb797635e');
INSERT INTO `notice` VALUES (11, 1, 1, '123', '123', 'test.txt', '20200116\\efb797a01af77256fb0a68a815e2c92c.txt', 1579138822, '启用', '启用', 'd4faa9a4-7767-48d5-f2b9-340e6a6cbf45');
INSERT INTO `notice` VALUES (12, 2, 1, '123', '123', 'test.txt', '20200116\\efb797a01af77256fb0a68a815e2c92c.txt', 1579138822, '启用', '启用', 'd4faa9a4-7767-48d5-f2b9-340e6a6cbf45');
INSERT INTO `notice` VALUES (13, 3, 1, '123', '123', 'test.txt', '20200116\\efb797a01af77256fb0a68a815e2c92c.txt', 1579138822, '启用', '启用', 'd4faa9a4-7767-48d5-f2b9-340e6a6cbf45');
INSERT INTO `notice` VALUES (14, 5, 1, '测试公告', '公告内容', '', '', 1582789184, '启用', '启用', '747ee308-c240-c8ad-3d17-84c207613e4f');

-- ----------------------------
-- Table structure for organize
-- ----------------------------
DROP TABLE IF EXISTS `organize`;
CREATE TABLE `organize`  (
  `organize_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '组织架构id',
  `organize_parent_id` int(11) NULL DEFAULT NULL COMMENT '父ID',
  `organize_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '机构名称',
  `organize_status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '启用|停用',
  `organize_delete` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '启用|停用',
  `organize_guid` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `organize_internship_start` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '开始实习时间',
  `organize_internship_end` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '结束实习时间',
  `organize_note` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`organize_id`, `organize_guid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of organize
-- ----------------------------
INSERT INTO `organize` VALUES (1, 5, '2019级', '1', '1', 'da0bd2cd-18d5-416b-9aba-c980b244e020', NULL, NULL, NULL);
INSERT INTO `organize` VALUES (2, 1, '1901班', '1', '1', '3029f99e-0f41-50dc-4e1c-a41d6f1e5422', '2020-01-10', '2020-06-30', NULL);
INSERT INTO `organize` VALUES (3, 1, '1902班', '1', '1', 'fda2a7b0-d61d-2e19-20f9-d2f4cdfd7717', NULL, NULL, NULL);
INSERT INTO `organize` VALUES (4, 1, '测试', '1', '2', 'ee7c801c-c540-c207-ac40-b7177a2150f5', '2020-01-10', '2020-06-30', NULL);
INSERT INTO `organize` VALUES (5, 0, '胡锦超职业技术学校', '1', '1', '1', NULL, NULL, NULL);
INSERT INTO `organize` VALUES (14, 5, '2016级', '1', '1', '38dab4f4-b5f8-5fbd-6d56-4a38a9c43f7c', '', '', '');
INSERT INTO `organize` VALUES (15, 14, '1609', '1', '1', '7e3b08a7-1ac6-5b4b-5921-38de813b8184', '2020-01-01', '2020-07-01', '数媒');

-- ----------------------------
-- Table structure for report
-- ----------------------------
DROP TABLE IF EXISTS `report`;
CREATE TABLE `report`  (
  `report_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NULL DEFAULT NULL COMMENT '用户id',
  `report_content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '报告内容',
  `report_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '日报|周报|月报',
  `report_time` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '报告时间',
  `report_guid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `report_year` int(255) NULL DEFAULT NULL COMMENT '报告年',
  `report_month` int(255) NULL DEFAULT NULL COMMENT '报告月',
  `report_day` int(255) NULL DEFAULT NULL COMMENT '报告日',
  `report_week` int(255) NULL DEFAULT NULL COMMENT '报告周',
  `report_audit` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '审核通过/审核中/审核不通过',
  `report_audit_message` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `report_teacher_score` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '教师评分',
  `report_enterprise_score` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '企业评分',
  PRIMARY KEY (`report_id`, `report_guid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of report
-- ----------------------------
INSERT INTO `report` VALUES (2, 7, '1. 完成日报/周报/月报\n2.完成签到\n3.完善页面\n4.玩家测试\n5.666', '日报', '1578722992', 'aecdae30-1df3-3744-d64a-f1585f77919f', 2020, 1, 11, 1, '审核通过', NULL, '75', NULL);
INSERT INTO `report` VALUES (3, 7, '测试周报', '周报', '1578723476', '188d4891-f3a7-cbf1-fe35-f5206340113e', 2020, 1, 11, 1, '审核通过', NULL, '', NULL);
INSERT INTO `report` VALUES (4, 7, '测试月报', '月报', '1578723502', '8591521d-1f20-df11-b851-31b100389c70', 2020, 1, 11, 1, '审核中', NULL, NULL, NULL);
INSERT INTO `report` VALUES (5, 7, '1.测试123\n2.测试321\n3.测试', '日报', '1578906813', '056995f6-31f6-39d1-9015-e20e97836861', 2020, 1, 13, 2, '审核通过', '通过', NULL, NULL);

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role`  (
  `role_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '角色id',
  `role_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色名称',
  `role_remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色备注说明',
  `role_guid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `role_status` int(11) NOT NULL,
  `role_delete` int(11) NOT NULL,
  PRIMARY KEY (`role_id`, `role_guid`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES (1, '管理员1', '备注1', '0', 1, 1);
INSERT INTO `role` VALUES (2, 'asad', '', '30a04997-dcf3-b373-7b18-7314ae21ed75', 1, 2);
INSERT INTO `role` VALUES (3, '测试', '大萨达萨达', '7123ff4d-1bb3-483d-5c29-24a17abe2c7e', 1, 2);
INSERT INTO `role` VALUES (4, '测试1', '测试1', '1d1c929f-5bf6-3ccd-870b-b6dfbbc80d3d', 1, 2);
INSERT INTO `role` VALUES (5, '测试1', '测试1', '0833aac2-4a9c-e1fd-d83f-5318b01db693', 1, 2);
INSERT INTO `role` VALUES (6, '学生', '学生', 'db8107a0-ea33-e0e3-43b2-230fa536e0bb', 1, 1);
INSERT INTO `role` VALUES (7, '老师', '老师', 'f06bd389-3b98-0405-172d-8a3435f41c11', 1, 1);
INSERT INTO `role` VALUES (8, '教务处', '教务处', '78e5d218-0a0c-4c7f-6de8-6a934c8ae20a', 1, 1);
INSERT INTO `role` VALUES (9, '企业指导人', '企业指导人', '37da07e1-d538-24bd-d16e-983faa02a569', 1, 1);

-- ----------------------------
-- Table structure for role_menu
-- ----------------------------
DROP TABLE IF EXISTS `role_menu`;
CREATE TABLE `role_menu`  (
  `role_menu_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '角色菜单id',
  `menu_id` int(11) NULL DEFAULT NULL COMMENT '菜单id',
  `role_id` int(11) NULL DEFAULT NULL COMMENT '角色id',
  `role_menu_guid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`role_menu_id`, `role_menu_guid`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 276 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of role_menu
-- ----------------------------
INSERT INTO `role_menu` VALUES (269, 25, 1, 'cd5e2b79-4734-ef5c-29d4-f73368ae64f4');
INSERT INTO `role_menu` VALUES (268, 24, 1, 'd8c0146f-9b22-dd8c-7196-0bc096d8b5ba');
INSERT INTO `role_menu` VALUES (267, 28, 1, '0a544f74-9a23-6da2-4227-565fd96c4ea4');
INSERT INTO `role_menu` VALUES (266, 23, 1, '21e112ba-68ce-149e-54ac-173d8d25e1e2');
INSERT INTO `role_menu` VALUES (265, 17, 1, '2452be69-8ad4-5be6-7916-2f1a33d28781');
INSERT INTO `role_menu` VALUES (264, 27, 1, '2665199b-9019-6b67-0b2a-a1c8dbcb558f');
INSERT INTO `role_menu` VALUES (263, 10, 1, 'a1d71903-fcce-38bd-b606-52ba629822c1');
INSERT INTO `role_menu` VALUES (262, 8, 1, 'beebf1e8-5718-0813-5502-b1c0e2983cd5');
INSERT INTO `role_menu` VALUES (261, 7, 1, '86028f70-ec9a-3c39-06f7-17068a409270');
INSERT INTO `role_menu` VALUES (260, 18, 1, 'eff0fee4-4acc-8e55-6c92-54930fcafe41');
INSERT INTO `role_menu` VALUES (259, 9, 1, 'f6d14f6b-1f26-b88a-1119-50b0dca4c237');
INSERT INTO `role_menu` VALUES (258, 1, 1, '95b34704-41f9-7961-e370-02e102f8d082');
INSERT INTO `role_menu` VALUES (257, 2, 1, '59eb715f-00ea-8d5c-2df3-85585bb17884');
INSERT INTO `role_menu` VALUES (270, 26, 1, '28a41926-af89-a8a1-b7a5-5354cf6456a9');
INSERT INTO `role_menu` VALUES (271, 29, 1, '0e0c9ec3-ce99-fd5c-6862-51ae87b226a0');
INSERT INTO `role_menu` VALUES (272, 30, 1, '');
INSERT INTO `role_menu` VALUES (273, 31, 1, '');
INSERT INTO `role_menu` VALUES (274, 32, 1, '');
INSERT INTO `role_menu` VALUES (275, 33, 1, '');

-- ----------------------------
-- Table structure for sign_in
-- ----------------------------
DROP TABLE IF EXISTS `sign_in`;
CREATE TABLE `sign_in`  (
  `sign_in_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NULL DEFAULT NULL COMMENT '用户id',
  `sign_in_time` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打卡时间',
  `sign_in_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '正常|迟到',
  `sign_in_point` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '打卡点',
  `sign_in_address` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '地址',
  `sign_in_distance` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '距离',
  `sign_in_enterprise_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `sign_in_enterprise_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `sign_in_enterprise_points` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `sign_in_enterprise_radius` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `sign_in_guid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`sign_in_id`, `sign_in_guid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sign_in
-- ----------------------------
INSERT INTO `sign_in` VALUES (1, 7, '1578736820', '正常', '113.27293,22.755146', '广东省佛山市顺德区容桂街道文滘路胡锦超职业技术学校', '58', '测试企业', '123', '113.27252,22.75479', '100', '213dcd4d-f2d9-c5e6-e031-4710a845c060');
INSERT INTO `sign_in` VALUES (3, 7, '1578869844', '正常', '113.272921,22.755176', '广东省佛山市顺德区容桂街道文滘路胡锦超职业技术学校', '60', '测试企业', '123', '113.27252,22.75479', '100', 'eb54a313-fa6f-a9db-b73e-0d09f81b1e78');
INSERT INTO `sign_in` VALUES (4, 12, '1578962100', '正常', '113.272911,22.755152', '广东省佛山市顺德区容桂街道文滘路胡锦超职业技术学校', '57', '测试企业', '123', '113.27252,22.75479', '100', 'b2159675-4dbd-ad97-2f14-4542f267e8b0');
INSERT INTO `sign_in` VALUES (8, 7, '1578991330', '正常', '113.273002,22.755202', '广东省佛山市顺德区容桂街道文滘路胡锦超职业技术学校', '67', '测试企业', '123', '113.27252,22.75479', '100', '61b88a54-286c-f0cb-a0d7-bcd542f19473');
INSERT INTO `sign_in` VALUES (9, 7, '1579053942', '正常', '113.273002,22.755202', '广东省佛山市顺德区容桂街道文滘路胡锦超职业技术学校', '67', '测试企业', '123', '113.27252,22.75479', '100', 'ce599420-a6af-1614-81ad-f2b28366794a');
INSERT INTO `sign_in` VALUES (10, 11, '1579054131', '正常', '113.272896,22.755171', '广东省佛山市顺德区容桂街道文滘路胡锦超职业技术学校', '57', '测试企业', '123', '113.27252,22.75479', '100', 'b71ebd46-67f7-bfa3-75d4-50cb17d5fd40');

-- ----------------------------
-- Table structure for sms
-- ----------------------------
DROP TABLE IF EXISTS `sms`;
CREATE TABLE `sms`  (
  `sms_id` int(200) NOT NULL AUTO_INCREMENT,
  `sms_code` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `sms_phone` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `sms_guid` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `sms_time` int(200) NOT NULL,
  PRIMARY KEY (`sms_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for system
-- ----------------------------
DROP TABLE IF EXISTS `system`;
CREATE TABLE `system`  (
  `system_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '系统参数id',
  `system_day_report_status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '启用' COMMENT '日报 启用|不启用',
  `system_day_report_max` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '3' COMMENT '日报最大补写天数',
  `system_week_report_status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '启用' COMMENT '周报 启用|不启用',
  `system_week_report_max` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '7' COMMENT '周报最大补写天数',
  `system_month_report_status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '启用' COMMENT '月报 启用|不启用',
  `system_month_report_max` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '31' COMMENT '月报最大补写天数',
  `system_write_before_system` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '启用' COMMENT '是否要求学生进系统前填个人信息 (启用|不启用)',
  `system_teacher_check_update_info` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '启用' COMMENT '学生填报的实习信息或实习变更信息是否要教师审核(启用|不启用)',
  `system_write_info_after_training` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '启用' COMMENT '实习结束后是否允许录入实习相关信息(启用|不启用）',
  `system_write_enterprise_time` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '启用' COMMENT '是否提醒学生单位填报截止时间(启用|不启用)',
  `system_stu_min_age` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '启用' COMMENT '实习安排是是否检测学生的最小实习年龄(启用|不启用)',
  `system_report_before_write_enterprise_info` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '启用' COMMENT '可写实习报告前是否必须先填报实习单位相关信息(启用|不启用)',
  `system_write_summary_early_time` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '实习总结最早可写时间',
  `system_wx_stu_auto_check_in` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '启用' COMMENT '微信学生是否自动签到(启用|不启用)',
  `system_wx_sign_start_end_time` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '每天签到开始时间和结束时间',
  `system_wx_sign_warning_time` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '签到异常预警距离',
  `system_wx_teach_auto_check_in` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '启用' COMMENT '微信端教师是否自动签到(启用|不启用)',
  `system_write_info_warning` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '不启用' COMMENT '实习信息采集预警',
  `system_wirte_info_warning_time` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '实习信息采集预警间隔时间',
  `system_day_report_warning` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '不启用' COMMENT '日报预警(启用|不启用)',
  `system_day_report_warning_time` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '日报预警间隔时间',
  `system_week_report_warning` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '不启用' COMMENT '周报预警(启用|不启用)',
  `system_week_report_warning_time` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '周报预警间隔时间',
  `system_month_report_warning` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '不启用' COMMENT '月报预警(启用|不启用)',
  `system_month_report_warning_time` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '月报预警间隔时间',
  `system_evaluation_excellent_score` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '评价“优”对应分数',
  `system_evaluation_good_score` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '评价\"良“对应分数',
  `system_evaluation_middle_score` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '评价”中“对应分数',
  `system_evaluation_common_score` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '评价\"一般\"对应分数',
  `system_evaluation_poor_score` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '评价”差“对应分数',
  `system_update_time` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`system_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of system
-- ----------------------------
INSERT INTO `system` VALUES (1, '启用', '3', '启用', '7', '启用', '31', '不启用', '不启用', '不启用', '不启用', '启用', '启用', '', '启用', '08:00:00 - 08:30:00', '24', '启用', '不启用', '', '不启用', '24', '不启用', '24', '不启用', '24', '85', '75', '70', '60', '50', '1582876456');

-- ----------------------------
-- Table structure for system_set
-- ----------------------------
DROP TABLE IF EXISTS `system_set`;
CREATE TABLE `system_set`  (
  `system_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '系统设置id',
  `system_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '系统设置名称',
  `system_note` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '参数备注',
  `system_status` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '系统设置状态',
  `system_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '参数类型(1普通参数2数字参数)',
  `system_update_time` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最后更新时间(时间戳)',
  PRIMARY KEY (`system_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of system_set
-- ----------------------------
INSERT INTO `system_set` VALUES (1, '是否启用“日报”模块', '是否要求学生写日报', '启用', '1', '1581930671');
INSERT INTO `system_set` VALUES (2, '是否启用“周报”模块', '是否要求学生写周报', '不启用', '1', '1581930708');
INSERT INTO `system_set` VALUES (3, '是否启用“月报”模块', '是否要求学生写月报', '启用', '1', '1581930718');
INSERT INTO `system_set` VALUES (4, '补写日报天数', '用于设置可补写多少天前的日报。例如设置为3，则表示可以补写3天前的日报', '3', '2', '1581930729');
INSERT INTO `system_set` VALUES (5, '补写周报天数', '用于设置可补写多少天前的周报。例如设置为7，则表示可以补写7天前的周报', '7', '2', '1581930739');
INSERT INTO `system_set` VALUES (6, '补写月报天数', '用于设置可补写多少天前的月报。例如设置为31，则表示可以补写31天前的月报', '30', '2', '1581930748');

-- ----------------------------
-- Table structure for token
-- ----------------------------
DROP TABLE IF EXISTS `token`;
CREATE TABLE `token`  (
  `token_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'tokenid',
  `token_content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'token内容',
  `token_time` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '过期时间',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '用户id',
  `token_guid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`token_id`, `token_guid`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 119 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of token
-- ----------------------------
INSERT INTO `token` VALUES (111, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJ1c2VyIiwiaWF0IjoxNTgzMTM1MjU0LCJleHAiOjE1ODMxMzg4NTQsIm5iZiI6MTU4MzEzNTI1NCwic3ViIjozOTEwNCwianRpIjoiYzU2YmQ5NThlNzMzMzdmYzBhMjYzYzc4YWY5NTRjZjIifQ.Xlaso9VyHBAFKdMxUdVH6N7ofLk7riOsirykGUbV3uE', '1583138854', 1, '9494765c-2525-8ea8-0ece-572f42f22009');
INSERT INTO `token` VALUES (112, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJ1c2VyIiwiaWF0IjoxNTc3NzYwMzMzLCJleHAiOjE1Nzc3NjM5MzMsIm5iZiI6MTU3Nzc2MDMzMywic3ViIjo5MjMyMywianRpIjoiNDI0ODk1OTM2NTU4YWRmYTQ4OTdjMzliYmJkYTJkZWYifQ.lkXtor-nlaQ3CEieKbKzmWD5V1cYTY5YtCqfb53HDAg', '1577763933', 2, '21774dd3-243f-f185-5208-d416c1bdc004');
INSERT INTO `token` VALUES (113, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJ1c2VyIiwiaWF0IjoxNTc4MDk4MjYxLCJleHAiOjE1NzgxMDE4NjEsIm5iZiI6MTU3ODA5ODI2MSwic3ViIjoyODI5NiwianRpIjoiZDRjMGY4ZjVjOThmNTY5ZTZiZmEwNmUyMjdlMDQzNWQifQ.ppKutmQ-rJJ9PQJ6Nt15G26NOnPQ81kKU3N15B0cps0', '1578101861', 3, 'c53ae006-8644-c8b2-8c91-5f26baecefe4');
INSERT INTO `token` VALUES (114, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJ1c2VyIiwiaWF0IjoxNTc5MTU5MjUwLCJleHAiOjE1NzkxNjI4NTAsIm5iZiI6MTU3OTE1OTI1MCwic3ViIjo0MTMwOCwianRpIjoiNjVlMWI2ZjBkMjI5MmRlMmJlNTkwY2FmN2ZhYzcxZGMifQ.ROPhXvWHgEEy37ZdcrUG-3Udsw0ZP-TGcmYDJbz40WM', '1579162850', 15, 'fb33de2d-2932-5725-a2ec-227554ce7835');
INSERT INTO `token` VALUES (115, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJ1c2VyIiwiaWF0IjoxNTgyNTA1MTY0LCJleHAiOjE1ODI1MDg3NjQsIm5iZiI6MTU4MjUwNTE2NCwic3ViIjozMzkwOSwianRpIjoiMDI1ZTk1ZGVjMGJmMGE2Yjk0NDljZGEzZjhhNTliYjYifQ.jt2kS0Ck2KmDXCg8cTriu2rgEMtDUbmSwOWJkrbnJDQ', '1582508764', 7, '8fd2c518-3f01-8197-0d20-530f3ef1f54e');
INSERT INTO `token` VALUES (116, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJ1c2VyIiwiaWF0IjoxNTc5MjQ5MTcxLCJleHAiOjE1NzkyNTI3NzEsIm5iZiI6MTU3OTI0OTE3MSwic3ViIjoyMzMyNSwianRpIjoiY2ExYjgwZTRjMTZiMTM1Mjg2NzI3MDJlM2NhMmNkNTIifQ.Z7aDRFEzmOFFp2fntKi-JRrU_uFXcSgHrx_tKLv5438', '1579252771', 10, 'bcc7fdb8-8cc2-49cf-9d3f-6e909addf931');
INSERT INTO `token` VALUES (117, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJ1c2VyIiwiaWF0IjoxNTc5MjQ1MjE2LCJleHAiOjE1NzkyNDg4MTYsIm5iZiI6MTU3OTI0NTIxNiwic3ViIjo2OTEwMiwianRpIjoiYTA4ZTdhMWRmYjdkYzdhMzVhMGFkODllYzFjYTc3MGQifQ.nL8tjTteyAKUxZP6MgjmCG1rZek0s78I1cYXU08pVDE', '1579248816', 11, '19136be3-8dad-c153-df8a-84f1fb964efd');
INSERT INTO `token` VALUES (118, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJ1c2VyIiwiaWF0IjoxNTc5MzM4NTU2LCJleHAiOjE1NzkzNDIxNTYsIm5iZiI6MTU3OTMzODU1Niwic3ViIjoxODYyNywianRpIjoiODRkNWQxODk0MGNlOWZlOWRhOGFkNWNhZTM4YmFkMWMifQ.xWsBhTSAdzlqeWMV2Z3ojAoucCz5iXwvqpHxj3PKxik', '1579342156', 12, 'a73d6e30-9dd4-4c7e-6386-7841374415a8');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '微信小程序openid',
  `user_headimage` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '微信昵称',
  `user_password` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户密码',
  `user_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '编号',
  `user_phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户手机',
  `user_identity_card` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '身份证号码',
  `user_sex` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '启用|停用',
  `user_delete` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '启用|停用',
  `user_guid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `user_unionid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_openid_gzh` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_admin` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否超级管理员',
  PRIMARY KEY (`user_id`, `user_guid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 41 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, NULL, NULL, '432F45B44C432414D2F97DF0E5743818', NULL, '12345678910', NULL, NULL, '1', '1', '0', NULL, NULL, '2');
INSERT INTO `user` VALUES (6, '叶问', NULL, '123456', '190101', '13745673214', NULL, '男', '1', '1', '69cece66-e961-1492-b55d-21abd65d8190', NULL, NULL, '2');
INSERT INTO `user` VALUES (7, 'zhi', 'http://thirdwx.qlogo.cn/mmopen/vi_32/kQuPz4HLlnSWgo5aQ8YuaMfN8kWjBrbhVdoDgMBibNomQE0COsBL1UYKPEQgfoZZT12T5T6snogxMLCPIqvUoiaA/132', NULL, '160905', '13723568974', NULL, '男', '1', '1', 'e32388d7-8a9d-fb15-ab90-2ea603349531', 'oR2y-5qxQnhvpN29-gifCXUXeS1Q', 'oM3Ms6c46bZgNia-cxSKlCFAh8I0', '2');
INSERT INTO `user` VALUES (8, '北斗有七星', '', NULL, NULL, '12222222222', NULL, '男', '1', '1', '1b9be2c2-7596-4bc1-74bf-72bf5799d590', 'oR2y-5j-JtTTxkldftDoW1LFpGHQ', 'oM3Ms6YnuLXxa6CZ-BC4NAh9Ipqc', '2');
INSERT INTO `user` VALUES (10, 'Drav', 'http://thirdwx.qlogo.cn/mmopen/vi_32/iaJU1dZ9XeK720B4VyqQglibuHibUAcZO2XuwlKkwlrBs0fhwTEKPpDZtGJyIuA6XEcWib9CmN069M5MliawelSAs2Q/132', NULL, NULL, '13467925698', NULL, '男', '1', '1', 'adbd852b-59dc-7c3c-7ea5-73b8e8ddb158', 'oR2y-5l-EG6XJtEndqZowPSEx-Ng', 'oM3Ms6XyihM8Ef1FUqMmQRZJD89E', '2');
INSERT INTO `user` VALUES (11, 'YSproject', 'http://thirdwx.qlogo.cn/mmopen/vi_32/XiaFgEcnARPwywiaGaKZJ3lRpIFyMxKJ62yb2wAmXyke4P3xqmogSBg8oBviaEiaKrXGylMezpLEZMq1PntRHPzKfA/132', NULL, NULL, '13647985426', NULL, '男', '1', '1', '7c0d26d3-c2b8-d71f-f800-4063de9fa4c0', 'oR2y-5v8FLE1s6ZHnFjxPwVYgCQ8', 'oM3Ms6ZUjnxLmJhb-CwoPXRIxPJ4', '2');
INSERT INTO `user` VALUES (12, 'flashkk', 'http://thirdwx.qlogo.cn/mmopen/vi_32/NGMG9jcJ14ib9mqSrZhPuIqlV6Eb1KqJNm3Jl4VssURdwPedmUnyv77czib1Bbhmq7bwvicmu74T9WPcWE2Zknntw/132', NULL, NULL, '13923264548', NULL, '男', '1', '1', '5eeee72c-f27d-c192-7801-508e0144f9da', 'oR2y-5owp1RTBedNkzG_-k2NtJCE', 'oM3Ms6UwKWXogDT_LXl-O-lP_qpY', '2');
INSERT INTO `user` VALUES (13, '阿斯顿', 'http://thirdwx.qlogo.cn/mmopen/vi_32/PiajxSqBRaEL3l82LZ531IgcsnHYjfEq8lSsJRxJcRVWQVicnia2UhLY4p4jicJRYzrxY5RMiaGHS1cNFpIlwhibH5icQ/132', '', NULL, '15217657216', NULL, '男', '1', '1', '599a5bb8-d407-22dd-4350-1b2e9039ff6b', 'oR2y-5meBBe-kyJOHR0epTKoLOvM', 'oM3Ms6Q7JkQ43PZOef1IudGdKb88', '2');
INSERT INTO `user` VALUES (14, '五观端正三好青年', 'http://thirdwx.qlogo.cn/mmopen/vi_32/FBRibOc08xqOtI6hQqibqcjClPqxOdFZYzLxXSKWeic2XjFsnPicSPRopltxDRz3QMor8wAlkUWKqfLgs3bKibdvjrg/132', NULL, NULL, '15521448728', NULL, '男', '1', '1', '10838545-4e34-bb88-50b3-771828f7ad71', 'oR2y-5mSQCXpfJf4sgY0sH2M4ijk', 'oM3Ms6asp6rf_X6g6w9WeqglVsx8', '2');
INSERT INTO `user` VALUES (15, 'Brunt', 'http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eo4pLZAFKvngDBXZX9XDYT819Wr1mAS19ySyiawVS0RvQ5wXC4KaJViaSdSFffXAPCzmCShE2k9ZECQ/132', NULL, NULL, '15217657216', NULL, '男', '1', '1', 'cd9d847c-eed2-8670-8a67-99a7c76cd7e5', 'oR2y-5lfKuE05QONCnIzJ10SzQ7g', 'oM3Ms6Ut9qNqodLG-28PTzkGhCow', '2');
INSERT INTO `user` VALUES (16, '楠 AI', 'http://thirdwx.qlogo.cn/mmopen/vi_32/p1jBkPiav9jEa334NMjV5eicibt0pXRNNHLhx4JlvcX8hiaCkfeQpxVdBvcMJuhH3xj0uQRoAq4s5lA7MWQF26ibQRQ/132', NULL, NULL, '13823427278', NULL, '男', '1', '1', '5295ec8f-4b9e-5af9-fca4-52631fbe77f5', 'oR2y-5k2gW84U6vNFPQNCPb2GQ3Q', 'oM3Ms6YhG8sQKt-8XaWMmDXv5rR8', '2');
INSERT INTO `user` VALUES (17, 'abc', NULL, '123456', NULL, NULL, NULL, NULL, '', '', '0', NULL, NULL, NULL);
INSERT INTO `user` VALUES (18, '，，，', 'http://thirdwx.qlogo.cn/mmopen/vi_32/OeHgWnibBNiakh1GuoqSVCcE7EYr5HFlEM7ib0deibnyKAmicteiaibhG7rZC4MNcJ7wpA7BD98gaXiaxyic5cXiaVWh9yng/132', NULL, NULL, NULL, NULL, '男', '1', '1', '1f581fa1-a12f-c9b6-1013-0ba6ba09acc6', 'oR2y-5u0aV16VOaa3dAwPKRylRPU', 'oM3Ms6dZwuYQzZ5mUkehioiRCjRw', '2');
INSERT INTO `user` VALUES (19, 'star257248', 'http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJDLXWO7audMeQNE1lGsA62ZKKrIqcg37ZzfyISqYUTMkMnE1ZJDsVfhiafWrWOmuXAibuzzKHQ0gbA/132', NULL, NULL, '12345678910', NULL, '男', '1', '1', '81e2a4fe-79f7-8aee-422c-62e36b172f75', 'oR2y-5rmDnEYmOd37rneHId0-zrU', 'oM3Ms6eU4wvmFtSTv1nROlTcXAvg', '2');
INSERT INTO `user` VALUES (32, '叶某', NULL, NULL, '160907', '13763219980', '', '', '1', '1', '67bddb64-51e1-cd64-ff12-9ce11f10d814', '', '', '2');
INSERT INTO `user` VALUES (33, '黄某', NULL, NULL, '100001', '13763219981', '', '', '1', '1', 'a59013da-a494-dacd-809f-6be644e340c5', '', '', '2');
INSERT INTO `user` VALUES (34, '梁某', NULL, NULL, '', '13974461368', '', '', '1', '1', '3070b301-0c00-ab73-5535-5db4523dd9fe', '', '', '2');

-- ----------------------------
-- Table structure for user_organize
-- ----------------------------
DROP TABLE IF EXISTS `user_organize`;
CREATE TABLE `user_organize`  (
  `user_organize_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '用户组织架构id',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '用户id',
  `organize_id` int(11) NULL DEFAULT NULL COMMENT '组织架构id',
  `user_organize_guid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`user_organize_id`, `user_organize_guid`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 29 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_organize
-- ----------------------------
INSERT INTO `user_organize` VALUES (1, 1, 1, '3f9c856f-40f0-308d-5a00-8ba28cd0f872');
INSERT INTO `user_organize` VALUES (2, 2, 1, 'cd3a217d-6501-dfd5-b278-596b13c77bda');
INSERT INTO `user_organize` VALUES (3, 7, 2, '20dad8c8-54ef-4d9d-a6ab-e72bd57405f4');
INSERT INTO `user_organize` VALUES (4, 8, 3, '41fd3d75-0cf9-d286-737e-2e126c25ec2b');
INSERT INTO `user_organize` VALUES (5, 10, 2, '32b54605-6dcd-c91c-9604-f7ead78bd9fc');
INSERT INTO `user_organize` VALUES (6, 11, 2, 'c91e3e15-962e-9df7-0e53-2c00e7c67400');
INSERT INTO `user_organize` VALUES (7, 12, 2, '0cd76dcb-a780-272a-deda-5e33c8020862');
INSERT INTO `user_organize` VALUES (8, 14, 2, '927e1f32-c88a-db13-9937-6c7f410bfb06');
INSERT INTO `user_organize` VALUES (9, 15, 2, '39b3fb76-0b2a-e022-e4a7-1525b250fb51');
INSERT INTO `user_organize` VALUES (10, 13, 2, '5020d207-9df9-960c-a0db-1dc754a0503e');
INSERT INTO `user_organize` VALUES (11, 16, 2, 'a35ae2e6-9296-04b3-4fd2-ea5688ec64e6');
INSERT INTO `user_organize` VALUES (12, 6, 3, '65c69acf-a448-faf5-938d-8c05a2ae3a4e');
INSERT INTO `user_organize` VALUES (13, 20, 7, '6feb6013-1d66-df49-9d3d-ec6143629b39');
INSERT INTO `user_organize` VALUES (14, 21, 7, '329fa03a-e2ee-7eef-50d7-9083bd0f7950');
INSERT INTO `user_organize` VALUES (15, 23, 9, '9e15c38a-1f1d-3c8a-b39d-6c4d6fc4f5fc');
INSERT INTO `user_organize` VALUES (16, 24, 9, 'e3ff89aa-0954-2a2f-1b97-cd2b5bb95d72');
INSERT INTO `user_organize` VALUES (17, 26, 11, 'a6a548ac-98a6-02cd-bde5-eba2e2d38217');
INSERT INTO `user_organize` VALUES (18, 27, 11, '08ae031e-8a7c-5331-59ec-e74e3bc7567e');
INSERT INTO `user_organize` VALUES (19, 29, 13, 'a3e7bd60-998d-7f20-c782-80f32bd18804');
INSERT INTO `user_organize` VALUES (20, 30, 13, 'dee6d332-b5c1-c1bc-1e2a-4364e7cfa96e');
INSERT INTO `user_organize` VALUES (21, 32, 15, '3b0413c6-683c-b166-d0fa-ea241762314c');
INSERT INTO `user_organize` VALUES (22, 33, 15, '416e9ed3-5464-cfae-4a54-e005277ac7ae');
INSERT INTO `user_organize` VALUES (23, 35, 15, '5e5b14fb-057b-a724-74e7-73a6c8e29313');
INSERT INTO `user_organize` VALUES (24, 36, 15, 'c4b44eab-928f-d44d-bcb6-cb24309bc45e');
INSERT INTO `user_organize` VALUES (25, 37, 15, '7d606dd1-7021-2616-ac49-303f6ad9ca3c');
INSERT INTO `user_organize` VALUES (26, 38, 15, '618a3138-70ae-eb48-09bc-3853e75b1f33');
INSERT INTO `user_organize` VALUES (27, 39, 15, '79778447-24cf-35de-df10-323adf541ebf');
INSERT INTO `user_organize` VALUES (28, 40, 15, 'ef0bdbb2-dd9f-29d1-d9ae-042d57ac13cf');

-- ----------------------------
-- Table structure for user_role
-- ----------------------------
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role`  (
  `user_role_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '用户角色id',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '用户id',
  `role_id` int(11) NULL DEFAULT NULL COMMENT '角色id',
  `user_role_guid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`user_role_id`, `user_role_guid`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 40 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_role
-- ----------------------------
INSERT INTO `user_role` VALUES (1, 1, 1, '1');
INSERT INTO `user_role` VALUES (2, 0, 3, '90e1ef35-d6f4-bf53-ddde-2a4639ddf215');
INSERT INTO `user_role` VALUES (4, 1, 3, 'c557f45b-91eb-bbe3-b85f-9dc29e97411b');
INSERT INTO `user_role` VALUES (5, 7, 6, 'fe0f2cad-6ec7-dff4-125d-31e1e3c6f12f');
INSERT INTO `user_role` VALUES (6, 8, 6, 'a2bbf5c5-1041-150a-e6f5-8a92f4f5697f');
INSERT INTO `user_role` VALUES (7, 10, 7, '13134e50-85e1-1302-12a9-ae5626fcc100');
INSERT INTO `user_role` VALUES (16, 11, 7, 'fcaf3434-5770-4f47-89de-8abe732a24a2');
INSERT INTO `user_role` VALUES (10, 12, 6, '7f835e63-e7a0-fa40-796d-9904d3748f41');
INSERT INTO `user_role` VALUES (12, 14, 6, 'ecb65606-5f66-5dc5-b777-c593bd13c1cf');
INSERT INTO `user_role` VALUES (13, 15, 6, '7712e9c0-5df4-a8e8-a4a2-808a09724cca');
INSERT INTO `user_role` VALUES (15, 13, 6, '102e9f56-1757-c57d-25a2-38724c8ea0f1');
INSERT INTO `user_role` VALUES (17, 16, 6, '9117f2fb-2898-7b5c-c3ae-74de7a5fd3aa');
INSERT INTO `user_role` VALUES (18, 6, 6, 'c2fd3a0e-aaa4-b055-b559-d5ce068370bb');
INSERT INTO `user_role` VALUES (19, 20, 6, '8767067d-9857-eeba-1ce3-47153973a0da');
INSERT INTO `user_role` VALUES (20, 21, 7, '54ca2e9a-0c42-59c5-2262-4e56bc930952');
INSERT INTO `user_role` VALUES (21, 22, 9, 'ad068e73-8f7b-a1c7-b121-7132217ca1e8');
INSERT INTO `user_role` VALUES (22, 23, 6, '229efdeb-9e76-5a76-b917-3fecfb71e8c7');
INSERT INTO `user_role` VALUES (23, 24, 7, '7543b840-2203-447f-880a-1e67bd02ec54');
INSERT INTO `user_role` VALUES (24, 25, 9, '91b86dd8-6153-9822-ba1d-b89f4bdfabcf');
INSERT INTO `user_role` VALUES (25, 26, 6, '568dc9d9-3e4e-0751-296a-581add1ada40');
INSERT INTO `user_role` VALUES (26, 27, 7, 'b3bb3aad-a840-dfa8-f008-2a6c95b2444b');
INSERT INTO `user_role` VALUES (27, 28, 9, 'de5ecd9b-ccd7-15b0-ad6e-ec345d073f80');
INSERT INTO `user_role` VALUES (28, 29, 6, '7ea8e4d6-d775-1091-ca10-cb5c95fdded7');
INSERT INTO `user_role` VALUES (29, 30, 7, 'ebcbdc32-4843-f232-41d6-28727d54512f');
INSERT INTO `user_role` VALUES (30, 31, 9, 'd09ab7b9-802e-5b81-d514-f1ae49e04d32');
INSERT INTO `user_role` VALUES (31, 32, 6, '93141491-055c-2362-40d3-a75e99abcef3');
INSERT INTO `user_role` VALUES (32, 33, 7, '2398ec4a-3fa3-2d1b-9e82-2f560cb6cfcb');
INSERT INTO `user_role` VALUES (33, 34, 9, 'dd315372-6a30-149e-63a8-8f718f829355');
INSERT INTO `user_role` VALUES (34, 35, 6, 'da1f5c91-3138-8fd7-e62d-029e75505e9e');
INSERT INTO `user_role` VALUES (35, 36, 6, '2d3b6b9c-e76e-2ca5-1400-6fec4c4cd276');
INSERT INTO `user_role` VALUES (36, 37, 6, '9422c9cb-155e-8bf7-95ad-81c2a6d3e2ee');
INSERT INTO `user_role` VALUES (37, 38, 6, 'e0cbe62b-2bfe-6925-c98f-070ecb9ad9d4');
INSERT INTO `user_role` VALUES (38, 39, 6, '409b477c-3f2b-8476-cddc-fb3b6ccd4ce1');
INSERT INTO `user_role` VALUES (39, 40, 6, '27d894a8-b202-fbb8-395e-dc28ba16d629');

-- ----------------------------
-- Table structure for wx_token
-- ----------------------------
DROP TABLE IF EXISTS `wx_token`;
CREATE TABLE `wx_token`  (
  `wx_token_id` int(11) NOT NULL AUTO_INCREMENT,
  `wx_token_content` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `wx_token_time` int(200) NOT NULL,
  `wx_token_guid` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `wx_token_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`wx_token_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of wx_token
-- ----------------------------
INSERT INTO `wx_token` VALUES (1, '{\"access_token\":\"29_gFefoxFY9XkgomTZuTjgmpdugTn194oW05EksYYbd6Bmik4VzjzEVh4rQXgOpMa72DRk9rCv4Hak6WBZarsxS7z5U4wkzIxPJrWRBZ9FRzQ7JMFz9vU0J-4xwbObqU4jYW3V3h8brr4DKS6zBCMjAFAMNX\",\"expires_time\":157914171', 1579138072, 'cdd5e3bf-8b24-99d2-05b4-f024695513b4', 'token');
INSERT INTO `wx_token` VALUES (2, '{\"ticket\":\"O3SMpm8bG7kJnF36aXbe82we3yEfr5TeQJhnNe4yEJovdsGChMehFPC7vl3JyB5zb1WsX7EGG3RLYpMKnUuIZQ\",\"expires_time\":1579141714}', 1579138114, '93090be1-76b1-a52a-00cd-1fc0dabccd47', 'ticket');

-- ----------------------------
-- View structure for v_enterprise_position_user
-- ----------------------------
DROP VIEW IF EXISTS `v_enterprise_position_user`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_enterprise_position_user` AS select `user`.`user_id` AS `user_id`,`user`.`user_name` AS `user_name`,`user`.`user_headimage` AS `user_headimage`,`user`.`user_password` AS `user_password`,`user`.`user_number` AS `user_number`,`user`.`user_phone` AS `user_phone`,`user`.`user_identity_card` AS `user_identity_card`,`user`.`user_sex` AS `user_sex`,`user`.`user_status` AS `user_status`,`user`.`user_delete` AS `user_delete`,`user`.`user_guid` AS `user_guid`,`user`.`user_unionid` AS `user_unionid`,`user`.`user_openid_gzh` AS `user_openid_gzh`,`user`.`user_admin` AS `user_admin`,`enterprise_position_user`.`enterprise_position_user_id` AS `enterprise_position_user_id`,`enterprise_position_user`.`enterprise_position_id` AS `enterprise_position_id`,`enterprise_position_user`.`enterprise_position_user_guid` AS `enterprise_position_user_guid`,`enterprise_position`.`enterprise_position_name` AS `enterprise_position_name`,`enterprise_position`.`enterprise_position_introduction` AS `enterprise_position_introduction`,`enterprise_position`.`enterprise_position_treatment` AS `enterprise_position_treatment`,`enterprise_position`.`enterprise_position_number` AS `enterprise_position_number`,`enterprise_position`.`enterprise_position_salary_min` AS `enterprise_position_salary_min`,`enterprise_position`.`enterprise_position_salary_max` AS `enterprise_position_salary_max`,`enterprise_position`.`enterprise_position_status` AS `enterprise_position_status`,`enterprise_position`.`enterprise_position_delete` AS `enterprise_position_delete`,`enterprise_position`.`enterprise_position_guid` AS `enterprise_position_guid`,`enterprise_position`.`enterprise_id` AS `enterprise_id`,`enterprise`.`enterprise_logo` AS `enterprise_logo`,`enterprise`.`enterprise_name` AS `enterprise_name`,`enterprise`.`enterprise_address` AS `enterprise_address`,`enterprise`.`enterprise_introduction` AS `enterprise_introduction`,`enterprise`.`enterprise_nature` AS `enterprise_nature`,`enterprise`.`enterprise_phone` AS `enterprise_phone`,`enterprise`.`enterprise_user_name` AS `enterprise_user_name`,`enterprise`.`enterprise_points` AS `enterprise_points`,`enterprise`.`enterprise_radius` AS `enterprise_radius`,`enterprise`.`enterprise_status` AS `enterprise_status`,`enterprise`.`enterprise_remark` AS `enterprise_remark`,`enterprise`.`enterprise_delete` AS `enterprise_delete`,`enterprise`.`enterprise_guid` AS `enterprise_guid` from (((`user` join `enterprise_position_user` on((`enterprise_position_user`.`user_id` = `user`.`user_id`))) join `enterprise_position` on((`enterprise_position`.`enterprise_position_id` = `enterprise_position_user`.`enterprise_position_id`))) join `enterprise` on((`enterprise`.`enterprise_id` = `enterprise_position`.`enterprise_id`)));

-- ----------------------------
-- View structure for v_user_organize
-- ----------------------------
DROP VIEW IF EXISTS `v_user_organize`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_user_organize` AS select `user_organize`.`user_organize_guid` AS `user_organize_guid`,`user`.`user_id` AS `user_id`,`user`.`user_name` AS `user_name`,`user`.`user_headimage` AS `user_headimage`,`user`.`user_password` AS `user_password`,`user`.`user_number` AS `user_number`,`user`.`user_phone` AS `user_phone`,`user`.`user_identity_card` AS `user_identity_card`,`user`.`user_sex` AS `user_sex`,`user`.`user_status` AS `user_status`,`user`.`user_delete` AS `user_delete`,`user`.`user_guid` AS `user_guid`,`user`.`user_unionid` AS `user_unionid`,`user`.`user_openid_gzh` AS `user_openid_gzh`,`user`.`user_admin` AS `user_admin`,`user_organize`.`user_organize_id` AS `user_organize_id`,`organize`.`organize_id` AS `organize_id`,`organize`.`organize_parent_id` AS `organize_parent_id`,`organize`.`organize_name` AS `organize_name`,`organize`.`organize_status` AS `organize_status`,`organize`.`organize_delete` AS `organize_delete`,`organize`.`organize_guid` AS `organize_guid`,`organize`.`organize_internship_start` AS `organize_internship_start`,`organize`.`organize_internship_end` AS `organize_internship_end` from ((`user_organize` join `user` on((`user`.`user_id` = `user_organize`.`user_id`))) join `organize` on((`organize`.`organize_id` = `user_organize`.`organize_id`)));

-- ----------------------------
-- View structure for v_user_role
-- ----------------------------
DROP VIEW IF EXISTS `v_user_role`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_user_role` AS select `user_role`.`user_role_id` AS `user_role_id`,`user_role`.`user_role_guid` AS `user_role_guid`,`role`.`role_id` AS `role_id`,`role`.`role_name` AS `role_name`,`role`.`role_remark` AS `role_remark`,`role`.`role_guid` AS `role_guid`,`role`.`role_status` AS `role_status`,`role`.`role_delete` AS `role_delete`,`user`.`user_id` AS `user_id`,`user`.`user_name` AS `user_name`,`user`.`user_headimage` AS `user_headimage`,`user`.`user_password` AS `user_password`,`user`.`user_number` AS `user_number`,`user`.`user_phone` AS `user_phone`,`user`.`user_identity_card` AS `user_identity_card`,`user`.`user_sex` AS `user_sex`,`user`.`user_status` AS `user_status`,`user`.`user_delete` AS `user_delete`,`user`.`user_guid` AS `user_guid`,`user`.`user_unionid` AS `user_unionid`,`user`.`user_openid_gzh` AS `user_openid_gzh`,`user`.`user_admin` AS `user_admin` from ((`user_role` join `user` on((`user`.`user_id` = `user_role`.`user_id`))) join `role` on((`role`.`role_id` = `user_role`.`role_id`)));

SET FOREIGN_KEY_CHECKS = 1;
