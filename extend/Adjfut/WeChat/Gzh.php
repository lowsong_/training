<?php

namespace Adjfut\WeChat;

use Adjfut\WeChat\Common;
use Adjfut\WeChat\Config;
use think\Db;
use think\Request;

class Gzh extends Common
{
    protected $gzh = null;

    /*
    获取配置项
     */
    public function __construct()
    {
        $this->gzh = Config::gzh();
    }
    
    /**
     *   网页授权/获取用户基本信息
     *    @param array $config 配置项 [
     *       "scope" => "snsapi_base",   应用授权作用域，
     *       snsapi_base （不弹出授权页面，直接跳转，只能获取用户openid），
     *       snsapi_userinfo （弹出授权页面，可通过openid拿到昵称、性别、所在地。并且， 即使在未关注的情况下，只要用户授权，也能获取其信息 ）
     *       "state" => "",  重定向后会带上state参数，开发者可以填写a-zA-Z0-9的参数值，最多128字节
     *       "lang" => "zh_CN",   当scope等于snsapi_userinfo才生效
     *       返回国家地区语言版本，zh_CN 简体，zh_TW 繁体，en 英语
     *    ]
     *    @param function $callback 回调用户授权成功调用函数     ($userinfo,$state)
     *    @return null
     */
    public function authorize(array $config, $callback)
    {
        $gzh = $this->gzh;
        $save_dir = $gzh["save_dir"] . date("Y-m") . DS;
        $date = date("Y/m/d H:i:s");
        try {
            if (!array_key_exists("scope", $config)) {
                $config["scope"] = "snsapi_base";
            }
            if (!array_key_exists("state", $config)) {
                $config["state"] = "";
            }
            if (!array_key_exists("lang", $config)) {
                $config["lang"] = "zh_CN";
            }
            if (!array_key_exists("redirect_uri", $config)) {
                $request = Request::instance();
                $config["redirect_uri"] = $request->url(true);
            }            

            $get = input("get.");
            if (array_key_exists("code", $get)) {
                $get_access_token_result = $this->curl([
                    "url" => "https://api.weixin.qq.com/sns/oauth2/access_token",
                    "type" => "get",
                    "params" => [
                        "appid" => $gzh["appid"],
                        "secret" => $gzh["secret"],
                        "code" => $get["code"],
                        "grant_type" => "authorization_code",
                    ],
                ]);

                if ($get_access_token_result) {
                    $user_info = [];
                    $get_access_token_result = json_decode($get_access_token_result, true);
                    if (array_key_exists("access_token", $get_access_token_result) && array_key_exists("openid", $get_access_token_result)) {
                        if ($config["scope"] === "snsapi_userinfo") {
                            $get_userinfo_result = $this->curl([
                                "url" => "https://api.weixin.qq.com/sns/userinfo",
                                "type" => "get",
                                "params" => [
                                    "access_token" => $get_access_token_result["access_token"],
                                    "openid" => $get_access_token_result["openid"],
                                    "lang" => $config["lang"],
                                ],
                            ]);
                            if ($get_userinfo_result) {
                                $get_userinfo_result = json_decode($get_userinfo_result, true);
                                if (array_key_exists("errcode", $get_userinfo_result)) {
                                    $this->log($save_dir, date("d") . ".log", join("\r\n", [
                                        "---------------------------------------------------------------",
                                        "[ date ]  " . $date,
                                        "[ data ]  " . json_encode($get_userinfo_result),
                                        "---------------------------------------------------------------",
                                    ]));
                                } else {
                                    return $callback(array_merge($get_userinfo_result, $get_access_token_result), $config["state"]);
                                }
                            }
                        } else {
                            return $callback($get_access_token_result, $config["state"]);
                        }
                    }
                }
            } else {
                $url = "https://open.weixin.qq.com/connect/oauth2/authorize";
                $url .= "?" . http_build_query([
                    "appid" => $gzh["appid"],
                    "redirect_uri" => $config["redirect_uri"],
                    "response_type" => "code",
                    "scope" => $config["scope"],
                    "state" => $config["state"],
                ]);
                $url .= "#wechat_redirect";
                // header("location: $url");
                return redirect($url);
            }
        } catch (\Throwable $th) {
            $this->log($save_dir, date("d") . ".log", join("\r\n", [
                "---------------------------------------------------------------",
                "[ date ]  " . $date,
                "[ error ]  " . $th->getMessage(),
                "---------------------------------------------------------------",
            ]));
        }
    }

    /**
     *    微信模板消息推送
     *    微信返回errcode等于40001会更新缓存AccessToken
     *    并重新推送一次
     *    @param array $data 推送内容 [
     *         "touser" => "openid",
     *         "template_id" => "template_id",
     *         "url" => "url",
     *         "miniprogram" => [
     *              "appid" => "appid",
     *              "pagepath" => "pagepath",
     *          ],
     *         "data" => [
     *              "first" => [
     *                  "value" => "value",
     *                  "color" => "color",
     *              ],
     *              "keyword1" => [
     *                  "value" => "value",
     *                  "color" => "color",
     *              ],
     *              "keyword2" => [
     *                  "value" => "value",
     *                  "color" => "color",
     *              ],
     *              "keyword3" => [
     *                  "value" => "value",
     *                  "color" => "color",
     *              ],
     *              "remark" => [
     *                  "value" => "value",
     *                  "color" => "color",
     *              ],
     *          ],
     *    ]
     *    @return false/null
     */
    public function WxTemplatePush($data)
    {
        $access_token = $this->AccessToken();
        if ($access_token) {
            if (array_key_exists("miniprogram", $data)) {
                if (array_key_exists("appid", $data["miniprogram"])) {
                    if ($data["miniprogram"]["appid"] === "APPID") {
                        $xcx = Config::xcx();
                        $data["miniprogram"]["appid"] = $xcx["appid"];
                    }
                }
            }
            $template_send = $this->curl([
                "url" => "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=" . $access_token,
                "type" => "post",
                "params" => json_encode($data),
            ]);
            if ($template_send) {
                $template_send = json_decode($template_send, true);
                if ($template_send["errcode"] === 40001) {
                    $access_token = $this->CurlAccessToken();
                    if ($access_token) {
                        $template_send = $this->curl([
                            "url" => "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=" . $access_token,
                            "type" => "post",
                            "params" => json_encode($data),
                        ]);
                    }
                }
            }
        } else {
            return false;
        }
    }

    /**
     *   Jssdk授权
     *    @param string $url 授权url
     *    @return null
     */
    public function Jssdk(string $url = "")
    {
        $gzh = $this->gzh;
        $jsapiTicket = $this->JsApiTicket();
        $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
        if (!$url) {
            $url = "$protocol$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        }

        $timestamp = time();

        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $nonceStr = "";
        for ($i = 0; $i < 16; $i++) {
            $nonceStr .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
        }

        // 这里参数的顺序要按照 key 值 ASCII 码升序排序
        $string = "jsapi_ticket=$jsapiTicket&noncestr=$nonceStr&timestamp=$timestamp&url=$url";

        $signature = sha1($string);

        $signPackage = [
            "appId" => $gzh["appid"],
            "nonceStr" => $nonceStr,
            "timestamp" => $timestamp,
            "url" => $url,
            "signature" => $signature,
            "rawString" => $string,
        ];
        return $signPackage;
    }

    /**
     *    重新获取AccessToken并更新缓存AccessToken
     *    @return AccessToken/false
     */
    public function CurlAccessToken()
    {
        $date = date("Y-m-d H:i:s");
        $gzh = $this->gzh;
        $get_token_result = $this->curl([
            "url" => "https://api.weixin.qq.com/cgi-bin/token",
            "params" => [
                "grant_type" => "client_credential",
                "appid" => $gzh["appid"],
                "secret" => $gzh["secret"],
            ],
        ]);
        if ($get_token_result) {
            $get_token = json_decode($get_token_result, true);
            if (array_key_exists("access_token", $get_token) && array_key_exists("expires_in", $get_token)) {
                $access_token = $get_token["access_token"];
                $expires_in = $get_token["expires_in"];
                $this->CacheAccessToken(json_encode([
                    "access_token" => $access_token,
                    "expires_time" => time() + $expires_in / 2,
                ]));
                return $access_token;
            } else {
                $save_dir = $gzh["save_dir"] . date("Y-m") . DS;
                $this->log($save_dir, date("d") . ".log", join("\r\n", [
                    "---------------------------------------------------------------",
                    "[ date ]  " . $date,
                    "[ data ]  " . json_encode($get_userinfo_result),
                    "---------------------------------------------------------------",
                ]));
                return false;
            }
        }
    }

    /**
     *    重新获取JsApiTicket并更新缓存JsApiTicket
     *    @return JsApiTicket/false
     */
    public function CurlJsApiTicket()
    {
        $date = date("Y-m-d H:i:s");
        $gzh = $this->gzh;
        $get_token_result = $this->curl([
            "url" => "https://api.weixin.qq.com/cgi-bin/ticket/getticket",
            "params" => [
                "access_token" => $this->AccessToken(),
                "type" => "jsapi",
            ],
        ]);
        if ($get_token_result) {
            $get_token = json_decode($get_token_result, true);
            if (array_key_exists("ticket", $get_token) && array_key_exists("expires_in", $get_token)) {
                $ticket = $get_token["ticket"];
                $expires_in = $get_token["expires_in"];
                $this->CacheJsApiTicket(json_encode([
                    "ticket" => $ticket,
                    "expires_time" => time() + $expires_in / 2,
                ]));
                return $ticket;
            } else {
                $save_dir = $gzh["save_dir"] . date("Y-m") . DS;
                $this->log($save_dir, date("d") . ".log", join("\r\n", [
                    "---------------------------------------------------------------",
                    "[ date ]  " . $date,
                    "[ data ]  " . json_encode($get_token_result),
                    "---------------------------------------------------------------",
                ]));
                return false;
            }
        }
    }

    /**
     *    判断缓存AccessToken的时间是过期
     *    AccessToken不存在或过期将重新获取
     *    @return AccessToken
     */
    protected function AccessToken()
    {
        $read = $this->ReadAccessToken();
        if ($read) {
            $read = json_decode($read, true);
            if ($read["expires_time"] > time()) {
                return $read["access_token"];
            } else {
                return $this->CurlAccessToken();
            }
        } else {
            return $this->CurlAccessToken();
        }
    }

    /**
     *    判断缓存JsApiTicket的时间是过期
     *    JsApiTicket不存在或过期将重新获取
     *    @return JsApiTicket
     */
    protected function JsApiTicket()
    {
        $read = $this->ReadJsApiTicket();
        if ($read) {
            $read = json_decode($read, true);
            if ($read["expires_time"] > time()) {
                return $read["ticket"];
            } else {
                return $this->CurlJsApiTicket();
            }
        } else {
            return $this->CurlJsApiTicket();
        }
    }

    /**
     *    缓存AccessToken
     *    @return null
     */
    protected function CacheAccessToken(string $content)
    {
        $wx_token = Db::name("wx_token");
        $wx_token_id = $wx_token->where([
            "wx_token_type" => "token",
        ])->value("wx_token_id");
        if ($wx_token_id) {
            return $wx_token->where([
                "wx_token_id" => $wx_token_id,
            ])->update([
                "wx_token_content" => $content,
            ]);
        } else {
            return Db::name("wx_token")->insert([
                "wx_token_content" => $content,
                "wx_token_time" => time(),
                "wx_token_type" => "token",
                "wx_token_guid" => $this->guid(),
            ]);
        }
    }

    /**
     *    获取本地缓存的AccessToken
     *    @return AccessToken/false
     */
    protected function ReadAccessToken()
    {
        return Db::name("wx_token")->where([
            "wx_token_type" => "gzh",
        ])->value("wx_token_content");
    }

    /**
     *    缓存JsApiTicket
     *    @return null
     */
    protected function CacheJsApiTicket(string $content)
    {
        $wx_token = Db::name("wx_token");
        $wx_token_id = $wx_token->where([
            "wx_token_type" => "ticket",
        ])->value("wx_token_id");
        if ($wx_token_id) {
            return $wx_token->where([
                "wx_token_id" => $wx_token_id,
            ])->update([
                "wx_token_content" => $content,
            ]);
        } else {
            return Db::name("wx_token")->insert([
                "wx_token_content" => $content,
                "wx_token_time" => time(),
                "wx_token_type" => "ticket",
                "wx_token_guid" => $this->guid(),
            ]);
        }
    }

    /**
     *    获取本地缓存的JsApiTicket
     *    @return JsApiTicket/false/null
     */
    protected function ReadJsApiTicket()
    {
        return Db::name("wx_token")->where([
            "wx_token_type" => "ticket",
        ])->value("wx_token_content");
    }
}
