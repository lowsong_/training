<?php

namespace Adjfut\WeChat;

use Adjfut\WeChat\Config;

class Common
{

    /**
     *    递归创建文件夹
     *   @param string $dir 目录
     *   @param int $mode 权限
     */
    protected function mkdir($dir, $mode = 0755)
    {
        if (is_dir($dir) || @mkdir($dir, $mode)) {
            return true;
        }
        if (!$this->mkdir(dirname($dir), $mode)) {
            return false;
        }
        return @mkdir($dir, $mode);
    }

    protected function curl(array $config)
    {
        $curl_config = Config::common();
        $curl_config = $curl_config["curl"];

        $date = date("Y/m/d H:i:s");

        $type = array_key_exists("type", $config) ? $config["type"] : "post";
        $params = array_key_exists("params", $config) ? $config["params"] : [];
        $url = $config["url"];

        $curl = curl_init();
        if ($type === "post") {
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
        } else {
            $params = $params ? http_build_query($params) : "";
            $url .= $params ? "?" . $params : "";
            curl_setopt($curl, CURLOPT_URL, $url);
        }

        if (array_key_exists("proxy", $curl_config)) {
            if (
                array_key_exists("ip", $curl_config["proxy"]) &&
                array_key_exists("port", $curl_config["proxy"])
            ) {
                if ($curl_config["proxy"]["ip"] && $curl_config["proxy"]["port"]) {
                    curl_setopt($curl, CURLOPT_PROXY, $curl_config["proxy"]["ip"]);
                    curl_setopt($curl, CURLOPT_PROXYPORT, $curl_config["proxy"]["port"]);
                }
            }
        }

        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($curl);
        if (array_key_exists("log", $curl_config)) {
            if (array_key_exists("log", $curl_config["log"])) {
                if ($curl_config["log"]["log"] === true) {
                    if ($output === false) {
                        $save_dir = $curl_config["log"]["error"] . date("Y-m") . DS;

                        $this->log($save_dir, date("d") . ".log", join("\r\n", [
                            "---------------------------------------------------------------",
                            "[ date ]  " . $date,
                            "[ url ]  " . $url,
                            "[ error ]  " . curl_error($curl),
                            "---------------------------------------------------------------",
                        ]));
                    } else {
                        $save_dir = $curl_config["log"]["success"] . date("Y-m") . DS;

                        $this->log($save_dir, date("d") . ".log", join("\r\n", [
                            "---------------------------------------------------------------",
                            "[ date ]  " . $date,
                            "[ url ]  " . $url,
                            "[ data ]  " . $output,
                            "---------------------------------------------------------------",
                        ]));
                    }

                }
            }
        }
        curl_close($curl);
        return $output;
    }

    protected function log(string $dir, string $file_name, string $content, string $type = "a+")
    {
        $this->mkdir($dir);
        $file_path = $dir . $file_name;
        $file = fopen($file_path, $type);
        switch ($type) {
            case 'r':
                $content = fread($file, filesize($file_path));
                fclose($file);
                return $content;
                break;
            default:
                fwrite($file, $content);
                break;
        }
        fclose($file);
    }

    protected function guid()
    {
        $charid = strtolower(md5(uniqid(mt_rand(), true)));
        $hyphen = chr(45); // '-'
        $uuid = //chr(123)// '{'
        substr($charid, 0, 8) . $hyphen
        . substr($charid, 8, 4) . $hyphen
        . substr($charid, 12, 4) . $hyphen
        . substr($charid, 16, 4) . $hyphen
        . substr($charid, 20, 12);
        //.chr(125);// '}'
        return $uuid;
    }
}
