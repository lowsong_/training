<?php

namespace Adjfut\WeChat;

use Adjfut\WeChat\Common;
use Adjfut\WeChat\Config;

class Xcx extends Common
{
    protected $xcx = null;

    /*
    获取配置项
     */
    public function __construct()
    {
        $this->xcx = Config::xcx();
    }

    /**
     *   获取微信小程序用户openid
     *   @param string $code 微信小程序传过来的code
     */
    public function GetOpenId(string $code)
    {
        $date = date("Y-m-d H:i:s");
        $xcx = $this->xcx;
        $request = $this->curl([
            "url" => "https://api.weixin.qq.com/sns/jscode2session",
            "type" => "get",
            "params" => [
                "appid" => $xcx["appid"],
                "secret" => $xcx["secret"],
                "js_code" => $code,
                "grant_type" => "authorization_code",
            ],
        ]);
        if ($request) {
            $request = json_decode($request, true);
            if (array_key_exists('openid', $request)) {
                return $request;
            } else {
                $save_dir = $xcx["save_dir"] . date("Y-m") . DS;
                $this->log($save_dir, date("d") . ".log", join("\r\n", [
                    "---------------------------------------------------------------",
                    "[ date ]  " . $date,
                    "[ type ]  " . __FUNCTION__,
                    "[ data ]  " . json_encode($request),
                    "---------------------------------------------------------------",
                ]));
                return false;
            }
        } else {
            $save_dir = $xcx["save_dir"] . date("Y-m") . DS;
            $this->log($save_dir, date("d") . ".log", join("\r\n", [
                "---------------------------------------------------------------",
                "[ date ]  " . $date,
                "[ type ]  " . __FUNCTION__,
                "[ data ]  " . $request,
                "---------------------------------------------------------------",
            ]));
            return false;
        }
    }

    /**
     *   生成微信小程序码
     *   @param string $type 接口名称 （createQRCode/get/getUnlimited）
     *   @param array $data 请求参数
     */
    public function QrCode(string $type, array $data)
    {
        $xcx = $this->xcx;
        $url = [
            "createQRCode" => "https://api.weixin.qq.com/cgi-bin/wxaapp/createwxaqrcode",
        ];
        $request = $this->curl([
            "url" => $url[$type],
            "type" => "post",
            "paramas" => json_encode($data),
        ]);
        if ($request) {
            $request = json_decode($request, true);
            if (array_key_exists("errcode", $request) && array_key_exists("errmsg", $request)) {
                if ($request["errcode"] === 0 && $request["errmsg"] === "ok") {
                    return base64_encode($request);
                } else {
                    $save_dir = $xcx["save_dir"] . date("Y-m") . DS;
                    $this->log($save_dir, date("d") . ".log", join("\r\n", [
                        "---------------------------------------------------------------",
                        "[ date ]  " . $date,
                        "[ type ]  " . __FUNCTION__,
                        "[ data ]  " . $request,
                        "---------------------------------------------------------------",
                    ]));
                    return false;
                }
            } else {
                $save_dir = $xcx["save_dir"] . date("Y-m") . DS;
                $this->log($save_dir, date("d") . ".log", join("\r\n", [
                    "---------------------------------------------------------------",
                    "[ date ]  " . $date,
                    "[ type ]  " . __FUNCTION__,
                    "[ data ]  " . $request,
                    "---------------------------------------------------------------",
                ]));
                return false;
            }
        }
    }

    /**
     *    重新获取AccessToken并更新缓存AccessToken
     *    @return AccessToken/false
     */
    public function CurlAccessToken()
    {
        $date = date("Y-m-d H:i:s");
        $xcx = $this->xcx;
        $request = $this->curl([
            "url" => "https://api.weixin.qq.com/cgi-bin/token",
            "params" => [
                "grant_type" => "client_credential",
                "appid" => $xcx["appid"],
                "secret" => $xcx["secret"],
            ],
        ]);
        if ($request) {
            $request = json_decode($request, true);
            if (array_key_exists("access_token", $request) && array_key_exists("expires_in", $request)) {
                $access_token = $request["access_token"];
                $expires_in = $request["expires_in"];
                $this->CacheAccessToken(json_encode([
                    "access_token" => $access_token,
                    "expires_time" => time() + $expires_in / 2,
                ]));
                return $access_token;
            } else {
                $save_dir = $xcx["save_dir"] . date("Y-m") . DS;
                $this->log($save_dir, date("d") . ".log", join("\r\n", [
                    "---------------------------------------------------------------",
                    "[ date ]  " . $date,
                    "[ type ]  " . __FUNCTION__,
                    "[ data ]  " . $request,
                    "---------------------------------------------------------------",
                ]));
                return false;
            }
        }
    }

    /**
     *    缓存AccessToken
     *    @return null
     */
    protected function CacheAccessToken(string $content)
    {
        $xcx = $this->xcx;
        $this->log($xcx["save_dir"], "access_token.log", $content, "w");
    }

    /**
     *    获取本地缓存的AccessToken
     *    @return AccessToken/false
     */
    protected function ReadAccessToken()
    {
        $xcx = $this->xcx;
        if (file_exists($xcx["save_dir"] . "access_token.log")) {
            return $this->log($xcx["save_dir"], "access_token.log", "", "r");
        } else {
            return false;
        }
    }

    /**
     *    判断缓存AccessToken的时间是过期
     *    AccessToken不存在或过期将重新获取
     *    @return AccessToken
     */
    protected function AccessToken()
    {
        $read = $this->ReadAccessToken();
        if ($read) {
            $read = json_decode($read, true);
            if ($read["expires_time"] > time()) {
                return $read["access_token"];
            } else {
                return $this->CurlAccessToken();
            }
        } else {
            return $this->CurlAccessToken();
        }
    }

}
