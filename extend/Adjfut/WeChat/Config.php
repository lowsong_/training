<?php

namespace Adjfut\WeChat;

use Adjfut\WeChat\Common;

class Config extends Common
{
    static $save_dir = ROOT_PATH . "wechat" . DS;//文件保存目录
    static function gzh(){
        return [
            "appid" => "wx25b1071467594e8b",
            "secret" => "0ab6c95a8b7352a29f0783b887e7016c",
            "save_dir" => self::$save_dir . "gzh" . DS,
        ];
    }

    static function xcx(){
        return [
            "appid" => "wx2e511477b5fcc1af",
            "secret" => "66db603ce7ac6c791c9af76e3862fe5e",
            "save_dir" => self::$save_dir . "xcx" . DS,
        ];
    }
    
    static function common(){
        return [
            "curl" => [
                "log" => [
                    "log" => true,//是否开启请求记录
                    "success" => self::$save_dir."curl".DS."success".DS,
                    "error" => self::$save_dir."curl".DS."error".DS,
                ],
                
                "proxy" => [//是否代理
                    "ip" => "",
                    "port" => ""
                ]
            ],
        ];
    }
}
