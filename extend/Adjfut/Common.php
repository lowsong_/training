<?php

namespace Adjfut;

use Adjfut\Tool\Tool;
use app\adjfut\controller\Login;
use think\Config;
use think\Controller;
use think\Db;
use think\Request;

class Common extends Controller
{
    protected $login;
    protected $exclude;
    protected $UserInfo;

    public function _empty()
    {
        echo ('操作不存在');
    }

    public function _initialize()
    {
        // $login = isset($this->login) ? $this->login : true;
        // $exclude = isset($this->exclude) ? $this->exclude : [];
        $request = Request::instance();
        // $ip = $request->ip();
        // $module = $request->module();
        // $controller = $request->controller();
        // $action = $request->action();
        // if ($login === true && !in_array($action, $exclude)) {
            $token = null;
            // if ($request->session("token")) {
            //     $token = $request->session("token");
            // }
            // if ($request->cookie("token")) {
            //     $token = $request->cookie("token");
            // }
            if ($request->param("token")) {
                $token = $request->param("token");
            }
            if ($request->header('token')) {
                $token = $request->header('token');
            }
            if ($token) {
                $time = time();
                $find_token = Db::name('token')->where([
                    'token_content' => $token,
                ])->find();
                $token_time = $find_token['token_time'] + Config::get('token_exp_time');
                if ($token_time < $time) { //过期
                    Tool::die([
                        'code' => 1001,
                        'msg' => '登陆超时，请重新登陆',
                    ]);
                } else {
                    // if ($find_token['token_ip'] == $ip) {
                        $user = (new Login)->user($find_token['user_id']);
                        if ($user['code'] === 0) {
                            $find_user = $user['data'];
                            $this->UserInfo = $find_user;
                        } else {
                            Tool::die([
                                'code' => 1001,
                                'msg' => $user['msg'],
                            ]);
                        }
                    // } else {
                    //     Tool::die([
                    //         'code' => 1001,
                    //         'msg' => 'ip不匹配，请重新登陆',
                    //     ]);
                    // }
                }
            }

        // }
    }

    public function get_guid()
    {
        $charid = strtolower(md5(uniqid(mt_rand(), true)));
        $hyphen = chr(45); // '-'
        $uuid = //chr(123)// '{'
        substr($charid, 0, 8) . $hyphen
        . substr($charid, 8, 4) . $hyphen
        . substr($charid, 12, 4) . $hyphen
        . substr($charid, 16, 4) . $hyphen
        . substr($charid, 20, 12);
        //.chr(125);// '}'
        return $uuid;
    }
}
