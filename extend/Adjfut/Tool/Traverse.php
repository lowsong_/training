<?php

namespace Adjfut\Tool;

class Traverse
{
    //当前id key名称
    protected $current_key_name = "id";
    //父id key名称
    protected $parent_key_name = "pid";

    public function __construct(string $current_key_name, string $parent_key_name)
    {
        $this->current_key_name = $current_key_name;
        $this->parent_key_name = $parent_key_name;
    }

    /**
     *   获取下级
     *   @access public
     *   @param string $data 要遍历的数组
     *   @param string $id 要查找哪个id下的子集
     *   @param string $loop 是否循环遍历
     *   @return array
     */
    public function next(array $data = [], int $id = 0, bool $loop = false)
    {
        $re = [];
        foreach ($data as $data_value) {
            if ($data_value[$this->parent_key_name] == $id) {
                array_push($re, $data_value[$this->current_key_name]);
                if ($loop) {
                    $re = array_merge($re, $this->next($data, $data_value[$this->current_key_name]));
                }
            }
        }
        return array_unique($re);
    }

    /**
     *   获取上级
     *   @access public
     *   @param string $data 要遍历的数组
     *   @param string $id 要查找哪个id下的父集
     *   @param string $loop 是否循环遍历
     *   @return array
     */
    public function prev(array $data = [], int $id = 0, bool $loop = false)
    {
        $re = [];
        foreach ($data as $data_value) {
            if ($data_value[$this->current_key_name] == $id) {
                array_push($re, $data_value[$this->parent_key_name]);
                if ($loop) {
                    $re = array_merge($re, $this->prev($data, $data_value[$this->parent_key_name]));
                }
            }
        }
        return array_unique($re);
    }
}
