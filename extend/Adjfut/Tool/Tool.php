<?php

namespace Adjfut\Tool;

use think\Config;
use think\Request;

class Tool
{
    public static function die(array $data) {
        header("Content-Type: application/json; charset=utf-8");
        die(json_encode($data));
    }

    public static function upload(array $config = [])
    {
        $re = [];
        $request = Request::instance();
        $file = $request->file();
        $uploads_path = Config::get("uploads_path");
        $web_resources_uploads = Config::get("web_resources_uploads");
        foreach ($file as $file_value) {
            if ($file_value) {
                $info = $file_value->move($uploads_path);
                if ($info) {
                    $save_name = $info->getSaveName();
                    array_push($re, [
                        "success" => true,
                        "info" => [
                            "save_name" => $save_name,
                            "web_resources_uploads" => $web_resources_uploads,
                        ],
                        "msg" => "保存成功",
                    ]);
                } else {
                    array_push($re, [
                        "success" => false,
                        "msg" => $file->getError(),
                    ]);
                }
            }
        }
        return $re;
    }

    public static function Input($data, array $exclude = [])
    {
        if (is_array($data)) {
            foreach ($data as $data_key => &$data_value) {
                if (!in_array($data_key, $exclude)) {
                    $data_value = self::Input($data_value);
                }
            }
        } else {
            $data = trim($data);
            $data = stripslashes($data);
            $data = htmlspecialchars($data);
        }
        return $data;
    }

    public static function random_str($length) {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $str = '';
        for($i = 0; $i < $length; $i++){
            $str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
        }
        return $str;
    }

    public static function Judge(&$data, $field)
    {
        $re = [];
        $data_key = array_keys($data);

        $judge = [];

        foreach ($field as $field_value) {
            if (!in_array($field_value, $data_key)) {
                unset($data[$field_value]);
                array_push($judge, $field_value);
                break;
            }
        }
        if (count($judge) === 0) {
            $re["code"] = 0;
            $re["msg"] = "字段正常";
        } else {
            $re["code"] = 100;
            $re['msg'] = "缺少" . $judge[0] . "字段";
        }
        return $re;
    }

    public static function GetUrl($url)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($curl);
        curl_close($curl);
        return $output;
    }

    public static function PostUrl($url, $data)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($curl);
        curl_close($curl);
        return $output;
    }
}
