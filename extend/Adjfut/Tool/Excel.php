<?php

namespace Adjfut\Tool;

use think\Validate;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class Excel
{
    /**
     * 导出excel
     * @author lpf
     * @version 1
     * @param array $file_name 文件名称
     * @param array $data 导出excel的数据
     * @param array $file_type 文件类型默认 Xlsx
     * @return string/null
     * @example
     * try {
     *      $data = [
     *         ["名称", "密码", "编号", "手机号", "性别"],
     *         ["叶问", "123456", "190101", "13745673214", "男/女"],
     *      ];
     *      return Excel::export("用户导入模板",$data);
     *  } catch (\Throwable $th) {
     *      echo $th->getMessage();
     *  }
     */
    public static function export($file_name, $data, $callback = null, $file_type = "Xlsx")
    {
        try {
            $spreadsheet = new Spreadsheet();
            $worksheet = $spreadsheet->getActiveSheet();

            $worksheet->fromArray($data);
            if ($callback) {
                $worksheet = $callback($worksheet, $data);
            }
            $writer = IOFactory::createWriter($spreadsheet, $file_type);
            header("Content-Type: application/force-download");
            header("Content-Type: application/octet-stream");
            header("Content-Type: application/download");
            $filename = $file_name . "." . $file_type;
            header('Content-Disposition:inline;filename="' . $filename . '"');
            $writer->save('php://output');
            die;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }

    /**
     * 格式化excel数组
     * @author lpf
     * @version 3.0
     * 注意捕获异常，验证值错误会打断代码执行并抛出异常
     * 当存在 @param validate跟 @param default
     * 将优先判断 @param validate
     * 当不存在 @param validate 并且 数据值为空时才会使用 @param default
     * @param array $data excel原始数据
     * @param array $config_data字段配置
     * @return array [$error,$data]
     * @example
     * try {
     *    dump(Excel::judge_field([
     *        ["叶问", "123456", "190101", "13745673214", "男/女"],
     *    ], [
     *        //标准用法
     *        [
     *            "key" => "user_name",
     *            "validate" => [
     *                "rule" => "require",
     *                "msg" => [
     *                    "require" => "名称不能为空",
     *                ],
     *            ],
     *            "default" => "0",
     *        ],
     *        //简化调用（不验证值）
     *        "user_password",
     *        "user_number" => [
     *            "validate" => [
     *                "rule" => "require",
     *                "msg" => [
     *                    "require" => "编号不能为空",
     *                ],
     *            ],
     *        ],
     *        //简化调用（验证值）
     *        "user_phone" => [
     *            "validate" => [
     *                "rule" => "require|length:11",
     *                "msg" => [
     *                    "require" => "手机号不能为空",
     *                    "length" => "手机号长度错误",
     *                ],
     *            ],
     *        ],
     *        "user_sex" => [
     *            "validate" => [
     *                "rule" => "require|in:男,女",
     *                "msg" => [
     *                    "require" => "性别不能为空",
     *                    "in" => "性别值错误（男/女）",
     *                ],
     *            ],
     *        ],
     *    ]));
     *  } catch (\Throwable $th) {
     *      dump($th->getMessage());
     *  }
     */
    public static function judge_field(array $data, array $config_data)
    {
        $error = [];
        $validate_key = "value";
        $config = [];
        $config_data_key_list = [];
        foreach ($config_data as $config_data_key => $config_data_value) {
            array_push($config_data_key_list, "{$config_data_key}");
        }
        $index_field = "";
        foreach ($config_data as $config_data_key => &$config_data_value) {
            if (isset($config_data_value["index"])) {
                if ($config_data_value["index"] === true) {
                    $index_field = isset($config_data_value["key"]) ? $config_data_value["key"] : $config_data_key;
                }
            }
            if (is_array($config_data_value)) {
                if (isset($config_data_value["validate"])) {
                    $validate = $config_data_value["validate"];
                    $validate_rule = isset($validate["rule"]) ? $validate["rule"] : "";
                    $validate_msg = isset($validate["msg"]) ? $validate["msg"] : "";
                    $validate_rule = [
                        $validate_key => $validate_rule,
                    ];
                    if (is_array($validate_msg)) {
                        foreach ($validate_msg as $validate_msg_key => $validate_msg_value) {
                            $validate_msg["{$validate_key}.{$validate_msg_key}"] = $validate_msg_value;
                            unset($validate_msg[$validate_msg_key]);
                        }
                    }
                    $config_data_value["validate"]["rule"] = $validate_rule;
                    $config_data_value["validate"]["msg"] = $validate_msg;
                }
            }

            if (is_numeric($config_data_key)) {
                if (is_array($config_data_value)) {
                    $config[$config_data_key] = $config_data_value;
                } else {
                    $config[$config_data_key] = [
                        "key" => $config_data_value,
                    ];
                }
            } else {
                $config_data_value = array_merge($config_data_value, [
                    "key" => $config_data_key,
                ]);
                $config[array_search($config_data_key, $config_data_key_list)] = $config_data_value;
            }
        }
        foreach ($data as $data_key => &$data_value) {
            foreach ($data_value as $data_value_key => &$data_value_value) {
                if (isset($config[$data_value_key])) {
                    $field_config = $config[$data_value_key];
                    $data_value_value = trim($data_value_value);

                    if (isset($field_config["validate"])) {

                        $validate_rule = isset($field_config["validate"]["rule"]) ? $field_config["validate"]["rule"] : "";
                        $validate_msg = isset($field_config["validate"]["msg"]) ? $field_config["validate"]["msg"] : "";
                        if ($validate_rule) {
                            $validate = new Validate($validate_rule, $validate_msg);
                            $validate_check = $validate->check([
                                $validate_key => $data_value_value,
                            ]);
                            if (!$validate_check) {
                                if (empty($index_field)) {
                                    $line = $data_key + 1;
                                } else {
                                    $line = $data_value[$index_field];
                                }
                                $error_msg = "第{$line}行：" . $validate->getError() . "【{$data_value_value}】";
                                if (isset($error)) {
                                    array_push($error, $error_msg);
                                }
                                // throw new Exception("第{$line}行：" . $validate->getError());
                            }
                        }
                    } else {
                        if (isset($field_config["default"])) {
                            if (!$data_value_value) {
                                $data_value_value = $field_config["default"];
                            }
                        }
                    }
                    $data_value[$field_config["key"]] = $data_value_value;
                    unset($data_value[$data_value_key]);
                }
            }
        }
        return [$error, $data];
    }

    public static function judge_title(array $data, array $title)
    {
        $re = true;
        if (count($data[0]) === count($title)) {
            foreach ($data[0] as $data_key => $data_value) {
                if (!($data_value === $title[$data_key])) {
                    $re = false;
                    break;
                }
            }
        } else {
            $re = false;
        }
        return $re;
    }
    /*
    去除空行
     */
    public static function clear_empty_line(array $data)
    {
        $re = [];
        foreach ($data as $data_key => &$data_value) {
            $length = count($data_value);
            foreach ($data_value as &$data_value_value) {
                $data_value_value = trim($data_value_value);
                if ($data_value_value === "") {
                    $length--;
                }
            }
            if ($length === 0) {
                array_splice($data, $data_key, 1);
            }
        }
        return $data;
    }

    /*
原始版本（备份用）
lpf

public static function judge_field(array $data, array $config_data)
{
$validate_key = "value";
// $validate = new Validate([
//     "value" => []
// ]);
// if ($validate->check([
//     "value" => "asd"
// ])) {
//     dump("a");
// } else {
//     dump("b");
// }
// exit;
// dump($data);
// dump($config);
// array(1) {
//     [0] => array(5) {
//       [0] => string(6) "叶问"
//       [1] => string(6) "123456"
//       [2] => string(6) "190101"
//       [3] => string(11) "13745673214"
//       [4] => string(7) "男/女"
//     }
//   }
//   array(1) {
//     [0] => array(3) {
//       ["key"] => string(9) "user_name"
//       ["validate"] => array(1) {
//         [0] => string(7) "require"
//       }
//       ["default"] => string(1) "0"
//     }
//   }
$config = [];
$config_data_key_list = [];
foreach ($config_data as $config_data_key => $config_data_value) {
array_push($config_data_key_list, "{$config_data_key}");
}
foreach ($config_data as $config_data_key => &$config_data_value) {
if (is_array($config_data_value)) {
if (isset($config_data_value["validate"])) {
$validate = $config_data_value["validate"];
// dump($validate);
// if (isset($validate["rule"])) {
//     $config_data_value["validate"]["rule"] = [
//         "value" => $validate["rule"],
//     ];
// }
// if(isset($validate["msg"])){
//     if (is_array($validate["msg"])) {
//         foreach ($validate["msg"] as $validate_msg_key => &$validate_msg_value) {
//             $validate["msg"]["value.{$validate_msg_key}"] = $validate_msg_value;
//             unset($validate["msg"][$validate_msg_key]);
//         }
//     }
// }
// dump($config_data_value["validate"]);
$validate_rule = isset($validate["rule"]) ? $validate["rule"] : "";
$validate_msg = isset($validate["msg"]) ? $validate["msg"] : "";
// dump($validate_rule);
$validate_rule = [
$validate_key => $validate_rule,
];
if (is_array($validate_msg)) {
foreach ($validate_msg as $validate_msg_key => $validate_msg_value) {
$validate_msg["{$validate_key}.{$validate_msg_key}"] = $validate_msg_value;
unset($validate_msg[$validate_msg_key]);
}
}
$config_data_value["validate"]["rule"] = $validate_rule;
$config_data_value["validate"]["msg"] = $validate_msg;
}
// dump($config_data_value);
}

if (is_numeric($config_data_key)) {
if (is_array($config_data_value)) {
$config[$config_data_key] = $config_data_value;
} else {
// $config_data_value = [
//     "key" => $config_data_value,
// ];
$config[$config_data_key] = [
"key" => $config_data_value,
];
}
} else {
$config_data_value = array_merge($config_data_value, [
"key" => $config_data_key,
]);
$config[array_search($config_data_key, $config_data_key_list)] = $config_data_value;

// $config_data[array_search($config_data_key, $config_data_key_list)] = $config_data_value;
// unset($config_data[$config_data_key]);
}
}
// dump($config);
// exit;
foreach ($data as $data_key => &$data_value) {
foreach ($data_value as $data_value_key => &$data_value_value) {
if (isset($config[$data_value_key])) {
$field_config = $config[$data_value_key];
$data_value_value = trim($data_value_value);

if (isset($field_config["validate"])) {

$validate_rule = isset($field_config["validate"]["rule"]) ? $field_config["validate"]["rule"] : "";
$validate_msg = isset($field_config["validate"]["msg"]) ? $field_config["validate"]["msg"] : "";
if ($validate_rule) {
// dump($validate_rule);
$validate = new Validate($validate_rule, $validate_msg);
$validate_check = $validate->check([
$validate_key => $data_value_value,
]);
if (!$validate_check) {
throw new Exception($validate->getError());
}
// if (is_array($validate_msg)) {
//     foreach ($validate_msg as $validate_msg_key => $validate_msg_value) {
//         $validate_msg["value.{$validate_msg_key}"] = $validate_msg_value;
//         unset($validate_msg[$validate_msg_key]);
//     }
// }
// $validate = new Validate([
//     "value" => $validate_rule,
// ], $validate_msg);
// $validate_check = $validate->check([
//     "value" => $data_value_value,
// ]);
// if (!$validate_check) {
//     throw new Exception($validate->getError());
// }
}

} else {
if (isset($field_config["default"])) {
$data_value_value = $field_config["default"];
}
}
$data_value[$field_config["key"]] = $data_value_value;
unset($data_value[$data_value_key]);

}
}
}
return $data;
// $data = [

// ]
// $config = [
//     ""
// ];
} */
}
